# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-07
### Added
- [Module Bus](src/Modules/Bus/CHANGELOG.md)
    - Added `ServiceInterface.php` for all service classes
    - Added `ActionInterface.php` for Action classes
    - Added `TaskInterface.php` for Task classes
    - Added `KnowsUIInterface.php`
    - Added `AbstractAction.php` which all Action implementations should extend
    - Added `AbstractAction.php` which all Action implementations should extend
    - Added `ServiceDispatcher.php` which is binded to application to dispatch service classes
    - Added `CanDispatchServiceInterface.php`
    - Added `DispatchesServicesTrait.php` to implement dispatching all services via inheretence
    - Added `CanDispatchTaskInterface.php`
    - Added `DispatchesTasksTrait.php` to implement dispatching tasks via inheretence
    - Added `API`, `CLI`, `Web` and `None` UI states which implements `UIInterface.php`
- [Module Loader](src/Modules/Loader/CHANGELOG.md)
    - Laravel 5.6 support
    - Module repository that load modules from their config file from given repository
    - Artisan command `module:list`, that show module information in CLI
    - Artisan command `module:seed`, that seed every active module
    - Load modules by priority
    - Helper function `modules(string $name = null)`
- [Module Eloquent](src/Modules/Eloquent/CHANGELOG.md)
    - Abstract Eloquent model
    - Entity repository 
        - helper query builder functions
        - getters (`all`, `find`, `get`, `first`)
        - support Criterias for select queries
- [Module Doctrine](src/Modules/Doctrine/CHANGELOG.md)
    - Use Laravel Doctrine package
    - Add custom types
        - [CarbonDateTimeType](src/Modules/Doctrine/src/Types/CarbonDateTimeType.php)
        - [CarbonDateType](src/Modules/Doctrine/src/Types/CarbonDateType.php)
        - [CarbonTimeType](src/Modules/Doctrine/src/Types/CarbonTimeType.php)
        - [HashIdType](src/Modules/Doctrine/src/Types/HashIdType.php)
    - Add Entity repository 
- [Module Authentication](src/Modules/Authentication/CHANGELOG.md)
- [Module Authorization](src/Modules/Authorization/CHANGELOG.md)
- [Module User](src/Modules/User/CHANGELOG.md)
- [Module App](src/Modules/Doctrine/CHANGELOG.md)    
     - Custom HTTP Kernel
     - Custom Console Kernel
     - Custom Exception handler
         - Support multiple reporters via config
         - Add JSON Renderer for errors format in API UI
- [Module Catalog](src/Modules/Catalog/CHANGELOG.md)
- [Module Order](src/Modules/Order/CHANGELOG.md)
- Add tests for several modules
- Add Splitsh scripts for monorepo support
