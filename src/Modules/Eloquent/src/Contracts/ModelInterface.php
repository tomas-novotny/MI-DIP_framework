<?php

namespace Modules\Eloquent\Contracts;

use Modules\Eloquent\Collection;

interface ModelInterface
{
    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable();
    
    /**
     * Get the value of the model's primary key.
     *
     * @return string|int
     */
    public function getKey();
    
    /**
     * Get the primary key for the model.
     *
     * @return string
     */
    public function getKeyName();
    
    /**
     * Indicates if the model exists.
     *
     * @return bool
     */
    public function exists(): bool;
    
    /**
     * Save the model to the database.
     *
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = []);
    
    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete();
    
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param array $models
     *
     * @return \Modules\Eloquent\Collection
     */
    public function newCollection(array $models = []): Collection;
    
    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Modules\Eloquent\Builder
     */
    public function newEloquentBuilder($query);
}
