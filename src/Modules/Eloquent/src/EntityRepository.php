<?php

namespace Modules\Eloquent;

use Modules\Eloquent\Concerns\GetterTrait;
use Modules\Eloquent\Concerns\ScopesTrait;
use Modules\Eloquent\Concerns\SetupTrait;
use Modules\Eloquent\Contracts\ModelInterface;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\DeleteResourceFailedException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use RuntimeException;

class EntityRepository
{
    /**
     * Repository constructor.
     *
     * @param \Modules\Eloquent\Contracts\ModelInterface|string|null $entity
     * @param string|null                                            $connection
     * @param string|null                                            $table
     */
    public function __construct($entity = null, string $connection = null, string $table = null)
    {
        $entity and $this->setModel($entity);
        $this->setConnection($connection);
        $this->setTable($table);
    }
    
    use SetupTrait;
    
    use GetterTrait;
    
    use ScopesTrait;
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Eloquent\Contracts\ModelInterface $entity
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function persistAndFlush(ModelInterface $entity): void
    {
        try {
            $entity->save();
        }
        catch (RuntimeException $exception) {
            
            throw $entity->exists()
                ? new UpdateResourceFailedException(get_class($entity), [$entity->getKey()], $exception)
                : new CreateResourceFailedException(get_class($entity), $exception);
        }
    }
    
    /**
     * Delete model.
     *
     * @param \Modules\Eloquent\Contracts\ModelInterface $entity
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function removeAndFlush(ModelInterface $entity): void
    {
        try {
            $entity->delete();
        }
        catch (RuntimeException $exception) {
    
            throw new DeleteResourceFailedException(get_class($entity), [$entity->getKey()], $exception);
        }
    }
}
