<?php

namespace Modules\Eloquent;

use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Doctrine\Common\Collections\Expr\Expression;
use Illuminate\Database\Eloquent\Builder as LaravelEloquentBuilder;
use Modules\Support\Contracts\CriteriaInterface;

/**
 * @method offset($value)
 * @method limit($value)
 * @method toSql()
 * @method getBindings()
 * @method orderBy($column, $direction = 'asc')
 * @method whereIn($column, $values, $boolean = 'and', $not = false)
 * @method whereNotIn($column, $values, $boolean = 'and')
 */
class Builder extends LaravelEloquentBuilder
{
    /**
     * Get array of primary keys of result models from current query
     *
     * @return array
     */
    public function getPrimaryKeys()
    {
        $model = $this->getModel();
        $key   = $model->getTable().'.'.$model->getKeyName();
        
        return $this->getRaw($key)->toArray();
    }
    
    /**
     * Execute the query as a raw "select" statement (do not return as hydrated Model object).
     *
     * @param array|string $columns
     *
     * @return \Modules\Eloquent\Collection
     */
    public function getRaw($columns = ['*'])
    {
        $builder = $this->applyScopes();
        
        $result = $builder->getQuery()->get(to_array($columns));
        
        if (is_string($columns)) {
            $column = array_last(explode('.', $columns));
            
            return $result->pluck($column);
        }
        
        return $result;
    }
    
    /**
     * Apply criterias to query builder.
     *
     * @param \Modules\Support\Contracts\CriteriaInterface $criteria
     *
     * @return void
     */
    public function addCriteria(CriteriaInterface $criteria): void
    {
        if ($criteria->getWhereExpression() !== null) {
            $this->applyWhereExpression($criteria->getWhereExpression());
        }
        
        if (count($criteria->getOrderings()) > 0) {
            foreach ($criteria->getOrderings() as $sort => $order) {
                $this->orderBy($sort, $this->resolveOrder($order));
            }
        }
        
        if ($criteria->getOffset() !== null) {
            $this->offset($criteria->getOffset());
        }
        
        if ($criteria->getLimit() !== null) {
            $this->limit($criteria->getLimit());
        }
    }
    
    /**
     * Apply expression to builder.
     *
     * @param \Doctrine\Common\Collections\Expr\Expression $expression
     * @param string                                       $boolean
     *
     * @return void
     */
    private function applyWhereExpression(Expression $expression, $boolean = 'and'): void
    {
        if ($expression instanceof CompositeExpression) {
            
            $this->where(function (self $b) use ($expression) {
                foreach ($expression->getExpressionList() as $expr) {
                    $b->applyWhereExpression($expr, $this->resolveType($expression->getType()));
                }
            }, $boolean);
            
            return;
        }
        
        if ($expression instanceof Comparison) {
            
            $this->where(
                $expression->getField(),
                $expression->getOperator(),
                $expression->getValue()->getValue(),
                $boolean
            );
            
            return;
        }
    }
    
    /**
     * Resolve boolean.
     *
     * @param string $boolean
     *
     * @return string
     */
    private function resolveType(string $boolean): string
    {
        return $boolean === CompositeExpression::TYPE_OR
            ? 'or'
            : 'and';
    }
    
    /**
     * Resolve order.
     *
     * @param string $order
     *
     * @return string
     */
    private function resolveOrder(string $order): string
    {
        return $order === \Doctrine\Common\Collections\Criteria::DESC
            ? 'desc'
            : 'asc';
    }
}
