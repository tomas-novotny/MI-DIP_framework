<?php

namespace Modules\Eloquent\Concerns;

use Modules\Eloquent\Builder;
use Modules\Eloquent\Collection;
use Modules\Eloquent\Contracts\ModelInterface as Model;
use Modules\Support\Exceptions\ResourceNotFoundException;

trait GetterTrait
{
    /**
     * Get model class name.
     *
     * @return string
     */
    abstract protected function getModelClass(): string;
    
    /**
     * Repository model instance.
     *
     * @return \Modules\Eloquent\Contracts\ModelInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    abstract protected function getModel(): Model;
    
    /**
     * Get current query builder.
     *
     * @return \Modules\Eloquent\Builder
     */
    abstract protected function getBuilder(): Builder;
    
    /**
     * Clear query builder.
     *
     * @return void
     */
    abstract protected function clearBuilder(): void;
    
    /**
     * Return all model instances by query.
     *
     * @return \Modules\Eloquent\Collection
     */
    public function all(): Collection
    {
        $models = $this->cloneBuilder()->get();
        
        $this->clearBuilder();
        
        return $this->getModel()->newCollection($models->all());
    }
    
    /**
     * Find model instance by query with given primary key.
     *
     * @param int|string $key
     *
     * @return \Modules\Eloquent\Contracts\ModelInterface|null
     */
    public function find($key): ?Model
    {
        if ($key === null) {
            return null;
        }
        
        $model = $this->cloneBuilder()->find($key);
        
        $this->clearBuilder();
        
        return $model;
    }
    
    /**
     * Return first model instance by query.
     *
     * @return \Modules\Eloquent\Contracts\ModelInterface|null
     */
    public function first(): ?Model
    {
        $model = $this->cloneBuilder()->first();
        
        $this->clearBuilder();
        
        return $model;
    }
    
    /**
     * Find model instance by query with given primary key.
     *
     * @param int|string|null $key
     *
     * @return \Modules\Eloquent\Contracts\ModelInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function get($key = null): Model
    {
        if ($key === null) {
            $model = $this->first();
        }
        else {
            $model = $this->find($key);
        }
        
        if ($model === null) {
            throw new ResourceNotFoundException($this->getModelClass(), [$key]);
        }
        
        return $model;
    }
    
    /**
     * Get Eloquent query builder.
     *
     * @return \Modules\Eloquent\Builder
     */
    private function cloneBuilder(): Builder
    {
        // clone builder instance
        $builder = clone $this->getBuilder();
        
        // return query builder
        return $builder;
    }
    
    
    /**
     * Get the SQL representation of the builder current query.
     *
     * @return string
     */
    public function toSql(): string
    {
        // get a copy of builder with final query
        $builder = $this->cloneBuilder();
        
        // get SQL query string and bindings
        $sql      = $builder->toSql();
        $binfings = $builder->getBindings();
        
        // create final query with bindings
        foreach ($binfings as $binding) {
            // get bindig value
            $value = is_numeric($binding) ? $binding : "'".$binding."'";
            $value = str_replace('\\', '\\\\\\', $value);
            // input ot sql query
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }
        
        // return finalize sql query with bindings
        return $sql;
    }
}
