<?php

namespace Modules\Eloquent\Concerns;

use Modules\Eloquent\Builder;
use Modules\Eloquent\Contracts\ModelInterface;

trait ScopesTrait
{
    /**
     * Get current query builder.
     *
     * @return \Modules\Eloquent\Builder
     */
    abstract protected function getBuilder(): Builder;
    
    /**
     * Set query builder.
     *
     * @param \Modules\Eloquent\Builder|null $builder
     *
     * @return $this
     */
    abstract protected function setBuilder(Builder $builder = null): void;
    
    /**
     * Repository model instance.
     *
     * @return \Modules\Eloquent\Contracts\ModelInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    abstract protected function getModel(): ModelInterface;
    
    /**
     * Apply criteria to builder.
     *
     * @param array $criterias
     *
     * @return void
     */
    public function applyCriterias(array $criterias): void
    {
        $builder = $this->getBuilder();
        
        foreach ($criterias as $criteria) {
            $builder->addCriteria($criteria);
        }
        
        $this->setBuilder($builder);
    }
    
    /**
     * Limit query.
     *
     * @param int $limit
     *
     * @return void
     */
    public function limit(int $limit): void
    {
        $builder = $this->getBuilder()->limit($limit);
        
        $this->setBuilder($builder);
    }
    
    /**
     * Offset query.
     *
     * @param int $offset
     *
     * @return void
     */
    public function offset(int $offset): void
    {
        $builder = $this->getBuilder()->offset($offset);
        
        $this->setBuilder($builder);
    }
    
    /**
     * Set the limit and offset for a given page.
     *
     * @param int $page
     * @param int $perPage
     *
     * @return void
     */
    public function forPage(int $page, int $perPage = 24): void
    {
        $this->offset(($page - 1) * $perPage);
        $this->limit($perPage);
    }
    
    /**
     * Where nested query.
     *
     * @param callable $callbale
     * @param string   $boolean
     *
     * @return void
     */
    public function whereNested(callable $callbale, string $boolean = 'and'): void
    {
        $builder = $this->getBuilder()->where($callbale, null, null, $boolean);
        
        $this->setBuilder($builder);
    }
    
    /**
     * Where query.
     *
     * @param string           $key
     * @param string|int|float $value
     * @param string           $operator
     * @param string           $boolean
     *
     * @return void
     */
    public function where(string $key, $value, string $boolean = 'and', string $operator = '='): void
    {
        $builder = $this->getBuilder()->where($key, $operator, $value, $boolean);
        
        $this->setBuilder($builder);
    }
    
    /**
     * Where not query.
     *
     * @param string           $key
     * @param int|float|string $value
     * @param string           $boolean
     * @param bool             $strict
     *
     * @return void
     */
    public function whereNot(string $key, $value, string $boolean = 'and', bool $strict = false): void
    {
        $this->where($key, $value, $boolean, $strict ? '!==' : '!=');
    }
    
    /**
     * Where in query.
     *
     * @param string $column
     * @param array  $values
     * @param string $boolean
     *
     * @return void
     */
    public function whereIn(string $column, array $values, string $boolean = 'and'): void
    {
        $count = count($values);
        
        if ($count === 0) {
            return;
        }
        
        if ($count === 1) {
            $this->where($column, $values[0], $boolean);
            
            return;
        }
        
        $builder = $this->getBuilder()->whereIn($column, $values, $boolean);
        
        $this->setBuilder($builder);
    }
    
    /**
     * Where not in query.
     *
     * @param string $column
     * @param array  $values
     * @param string $boolean
     *
     * @return void
     */
    public function whereNotIn(string $column, array $values, string $boolean = 'and'): void
    {
        $count = count($values);
        
        if ($count === 0) {
            return;
        }
        
        if ($count === 1) {
            $this->whereNot($column, $values[0], $boolean);
            
            return;
        }
        
        $builder = $this->getBuilder()->whereNotIn($column, $values, $boolean);
        
        $this->setBuilder($builder);
    }
    
    /**
     * Include item by given primary key.
     *
     * @param int|string $id
     * @param string     $boolean
     * @param bool       $strict
     *
     * @return void
     */
    public function is($id, string $boolean = 'and', bool $strict = false): void
    {
        $this->where($this->getModel()->getKeyName(), $id, $boolean, $strict ? '===' : '=');
    }
    
    /**
     * Exclude item by given primary key.
     *
     * @param int|string $id
     *
     * @return void
     */
    public function not($id): void
    {
        $this->whereNot($this->getModel()->getKeyName(), $id);
    }
    
    /**
     * Include items by given array of primary keys.
     *
     * @param array $items
     *
     * @return void
     */
    public function in(array $items): void
    {
        $count = count($items);
        
        if ($count === 0) {
            return;
        }
        
        if ($count === 1) {
            $this->is($items[0]);
            
            return;
        }
        
        $this->whereIn($this->getModel()->getKeyName(), $items);
    }
    
    /**
     * Exclude items by given array of primary keys.
     *
     * @param array $items
     *
     * @return void
     */
    public function notIn(array $items): void
    {
        $count = count($items);
        
        if ($count === 0) {
            return;
        }
        
        if ($count === 1) {
            $this->not($items[0]);
            
            return;
        }
        
        $this->whereNotIn($this->getModel()->getKeyName(), $items);
    }
}