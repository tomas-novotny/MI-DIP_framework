<?php

namespace Modules\Eloquent\Concerns;

use Modules\Eloquent\Builder;
use Modules\Eloquent\Contracts\ModelInterface as Model;
use Modules\Support\Exceptions\ResourceNotFoundException;

trait SetupTrait
{
    /**
     * Builder with current eloquent query
     *
     * @var \Modules\Eloquent\Builder
     */
    private $builder;
    
    /**
     * Repository model instance
     *
     * @var \Modules\Eloquent\Contracts\ModelInterface
     */
    private $model;
    
    /**
     * Connection name
     *
     * @var string|null
     */
    private $connection;
    
    /**
     * Table name
     *
     * @var string|null
     */
    private $table;
    
    /**
     * Get model class name.
     *
     * @return string
     */
    protected function getModelClass(): string
    {
        return get_class($this->getModel());
    }
    
    /**
     * Repository model instance.
     *
     * @return \Modules\Eloquent\Contracts\ModelInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    protected function getModel(): Model
    {
        if ($this->hasModel() === false) {
            throw new ResourceNotFoundException('');
        }
        
        return $this->model;
    }
    
    /**
     * Set new model to repository to work with.
     *
     * @param \Modules\Eloquent\Contracts\ModelInterface|string $model
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    protected function setModel($model): void
    {
        // try to create model by its class name
        if (is_string($model)) {
            $model = new $model();
        }
        
        // test if it is proper model class
        if ($this->hasModel($model) === false) {
            throw new ResourceNotFoundException(get_class($model));
        }
        
        // set model
        $this->model = $model;
        
        // reset builder
        $this->clearBuilder();
    }
    
    /**
     * Determinate if repository has assigned model.
     *
     * @param \Modules\Eloquent\Contracts\ModelInterface|null $model
     *
     * @return bool
     */
    private function hasModel(Model $model = null): bool
    {
        $model = $model ?? $this->model;
        
        return $model instanceof Model;
    }
    
    /**
     * Clear query builder.
     *
     * @return void
     */
    protected function clearBuilder(): void
    {
        $this->setBuilder(null);
    }
    
    /**
     * Set query builder.
     *
     * @param \Modules\Eloquent\Builder|null $builder
     *
     * @return $this
     */
    protected function setBuilder(Builder $builder = null): void
    {
        $this->builder = $builder;
    }
    
    /**
     * Get current query builder.
     *
     * @return \Modules\Eloquent\Builder
     */
    protected function getBuilder(): Builder
    {
        if ($this->builder === null) {
            // get new model eloquent builder
            $this->builder = $this->newBuilder();
        }
        
        return $this->builder;
    }
    
    /**
     * Get new eloquent query builder.
     *
     * @param \Modules\Eloquent\Contracts\ModelInterface|null $model
     * @param string|null                                     $connection
     * @param string|null                                     $table
     *
     * @return \Modules\Eloquent\Builder
     */
    private function newBuilder(Model $model = null, string $connection = null, string $table = null): Builder
    {
        $model      = $model ?: $this->getModel();
        $connection = $connection ?: $this->getConnection();
        $table      = $table ?: $this->getTable();
        
        // copy a new model instance
        $model = $model->newInstance();
        
        // set custom connection
        if ($connection !== null) {
            $model->setConnection($connection);
        }
        
        // set custom table
        if ($table !== null) {
            $model->setTable($table);
        }
        
        // get new query builder
        return $model->newQuery();
    }
    
    /**
     * Get connection
     *
     * @return string|null
     */
    protected function getConnection(): ?string
    {
        return $this->connection ?: $this->getModel()->getConnectionName();
    }
    
    /**
     * Set connection
     *
     * @param string|null $connection
     *
     * @return void
     */
    protected function setConnection($connection = null): void
    {
        // if it is new connection -> clear builder
        if ($this->connection !== $connection) {
            $this->clearBuilder();
        };
        
        // set connection
        $this->connection = $connection;
    }
    
    /**
     * Get table
     *
     * @return string
     */
    protected function getTable(): string
    {
        return $this->table ?: $this->getModel()->getTable();
    }
    
    /**
     * Set table
     *
     * @param string|null $table
     *
     * @return $this
     */
    protected function setTable($table = null): void
    {
        // if it is new table -> clear builder
        if ($this->table !== $table) {
            $this->clearBuilder();
        };
        
        $this->table = $table;
    }
}
