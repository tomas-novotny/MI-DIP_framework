<?php

namespace Modules\Eloquent\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response;

class NotEloquentModelException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Given modelNot eloquentModelException.';
    
    /**
     * NotEloquentModelException constructor.
     *
     * @param string $class
     */
    public function __construct(string $class)
    {
        parent::__construct('Given model ['.$class.'] is not Eloquent model');
    }
}
