<?php

namespace Modules\Eloquent\Parents;

use Illuminate\Database\Eloquent\Model as LaravelEloquentModel;
use Modules\Eloquent\Builder;
use Modules\Eloquent\Collection;
use Modules\Eloquent\Contracts\ModelInterface;

abstract class AbstractModel extends LaravelEloquentModel implements ModelInterface
{
    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 24;
    
    /**
     * Indicates if the model exists.
     *
     * @return bool
     */
    public function exists(): bool
    {
        return $this->exists;
    }
    
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param array $models
     *
     * @return \Modules\Eloquent\Collection
     */
    public function newCollection(array $models = []): Collection
    {
        return new Collection($models);
    }
    
    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Modules\Eloquent\Builder
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }
    
    /**
     * Unset the specific relationship in the model.
     *
     * @param string $relation
     *
     * @return $this
     */
    protected function unsetRelation($relation)
    {
        unset($this->relations[$relation]);
        
        return $this;
    }
}
