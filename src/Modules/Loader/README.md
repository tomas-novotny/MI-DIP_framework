# Modules autoloader

[![Software License][ico-license]](LICENSE.md)

This module is part of example [modular project][link-framework]. 
This module handles modules autoloading via registering ServiceProvider for each module in scanned directories. 

## Installation

Add `novott20/modules-loader` to your `composer.json`:
```bash
$ composer require novott20/modules-loader
```

Register our Service Provider in `providers` array in `config/app.php` before your `AppServiceProvider::class`:
```php
'providers' => [
    // ...
    Modules\Loader\Providers\ServiceProvider::class,
    // ...
],
```

## Usage
Configurate in which directory should repository scan for modules. 
Publish config and changed source directory:
```bash
$ php artisan vendor:publish --tag="config"
```

Edit config file in `config/modules.php`, se module path, or use `.env` variable `MODULES_PATH`.
```php
'paths' => [
    'modules' => env('MODULES_PATH', base_path('vendor/modules')),
],
```
Support multiple directories separeted by comma.
```ini
MODULES_PATH=/path/to/modules,/path/to/another/modules/
```

You can specify which modules NOT to autoload (module names separeted by comma)
```ini
MODULES_BLACKLIST=app,support
```

Repository will scan in given directories for modules with config file named `module.json`. 
This is example file with minimum variables to load module.
```json
{
  "name": "App",
  "namespace": "Modules\\App\\",
  "priority": 0,
  "autoload": true
}
```

+ Modules will be loaded in order (ascending) given by `priority` parameter. 
+ Only modules with `autoload: true` will be loaded.
+ Module names (`name`) should be unique. Only one module per name will be loaded.
+ Namespace parameter (`namespace`) helps with autoloading seeders and 

## Contributing

Please see [CONTRIBUTING][link-contributing] and [CODE_OF_CONDUCT][link-code-of-conduct] for details.

## Security

If you discover any security related issues, please email novott20@fit.cvut.cz instead of using the issue tracker.

## Credits

- [Tomáš Novotný][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-blue.svg

[link-framework]: https://github.com/novott20/MI-DIP_framework
[link-author]: https://github.com/novott20
[link-contributors]: ../../../../../contributors
[link-contributing]: ../../../CONTRIBUTING.md
[link-code-of-conduct]: ../../../CODE_OF_CONDUCT.md