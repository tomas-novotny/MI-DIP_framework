# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2018-05-07
### Added
- Laravel 5.6 support
- Module repository that load modules from their config file from given repository
- Artisan command `module:list`, that show module information in CLI
- Artisan command `module:seed`, that seed every active module
- Load modules by priority
- Helper function `modules(string $name = null)`