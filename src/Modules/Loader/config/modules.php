<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Modules path
    |--------------------------------------------------------------------------
    |
    | This path used for save the generated module. This path also will added
    | automatically to list of scanned folders.
    |
    */
    
    'paths' => [
        'modules' => env('MODULES_PATH', base_path('vendor/novott20/framework/src/Modules')),
    ],
    
    'mappings' => [
        'source'       => 'src/',
        'config'       => 'config/',
        'migrations'   => 'src/Database/Migrations/',
        'translations' => 'resources/lang/',
        'entities'     => 'src/Model/Entities/',
        'provider'     => 'src/Providers/ServiceProvider.php',
    ],
    
    'blacklist' => env('MODULES_BLACKLIST', ''),
    
    /*
    |--------------------------------------------------------------------------
    | Caching
    |--------------------------------------------------------------------------
    |
    | Here is the config for setting up caching feature.
    | "enabled"   use memcache for modules config file
    | "key"       cache key
    | "lifetime"  module-cache lifetime in minutes
    |
    */
    
    'cache' => [
        'enabled'  => env('CACHE_MODULES_ENABLED', false),
        'key'      => env('CACHE_MODULES_KEY', 'modules'),
        'lifetime' => env('CACHE_MODULES_LIFETIME', 1440),
    ],

];
