<?php

namespace Modules\Loader\Loaders;

use Modules\Loader\Contracts\ModuleInterface;

/**
 * TODO: remove
 *
 * @property \Illuminate\Contracts\Foundation\Application $app
 * @method void mergeConfigFrom(string $path, string $key)
 * @method void publishes(array $paths, string $group = null)
 */
trait ConfigsLoaderTrait
{
    /**
     * Load config from Module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function loadConfigsFromModule(ModuleInterface $module): void
    {
        $path = $module->getPath();
        
        $this->loadConfigs($path);
    }
    
    /**
     * Register all config.
     *
     * @param string $module_path
     *
     * @return void
     */
    protected function loadConfigs(string $module_path): void
    {
        // get all config files from folder
        $directory = $module_path.config('modules.mappings.config');
        
        if (is_dir($directory) === false) {
            return;
        }
        
        $files = $this->app['files']->files(realpath($directory));
        
        // register by their filename
        /* @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($files as $file) {
            $this->loadConfig($file->getPathname());
        }
    }
    
    /**
     * Register config.
     *
     * @param string $file_path
     *
     * @return void
     */
    protected function loadConfig(string $file_path): void
    {
        $name = basename($file_path, '.php');
        
        $this->mergeConfigFrom($file_path, $name);
        $this->publishes([$file_path => config_path($name).'.php'], 'config');
    }
}
