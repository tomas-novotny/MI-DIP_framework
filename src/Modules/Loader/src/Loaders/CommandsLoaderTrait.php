<?php

namespace Modules\Loader\Loaders;

use Modules\Loader\Contracts\ModuleInterface;

/**
 * TODO: remove
 *
 * @property \Illuminate\Contracts\Foundation\Application $app
 * @method void commands(array $commands)
 */
trait CommandsLoaderTrait
{
    /**
     * Load command from Module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function loadCommandsFromModule(ModuleInterface $module): void
    {
        $path      = $module->getPath();
        $namespace = $module->getNamespace();
        
        $this->loadCommands($path, $namespace);
    }
    
    /**
     * Register all command.
     *
     * @codeCoverageIgnore
     *
     * @param string $modulenamespace
     * @param string $modulePath
     *
     * @return void
     */
    protected function loadCommands(string $modulenamespace, string $modulePath): void
    {
        if ($this->app->runningInConsole() === false) {
            return;
        }
        
        // get all command files from folder
        $directory = $modulePath.'src/UI/CLI/Commands/';
        $namespace = $modulenamespace.'UI\\CLI\\Commands\\';
        
        if (is_dir($directory) === false) {
            return;
        }
        
        $files = $this->app['files']->files(realpath($directory));
        
        // register by their filename
        /* @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($files as $file) {
            $name = $file->getBasename('.php');
            
            if (class_exists($namespace.$name)) {
                $this->commands([$namespace.$name]);
            }
        }
    }
}
