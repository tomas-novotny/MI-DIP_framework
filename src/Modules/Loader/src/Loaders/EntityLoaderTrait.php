<?php

namespace Modules\Loader\Loaders;

use Illuminate\Config\Repository;
use Modules\Loader\Contracts\ModuleInterface;

/**
 * TODO: remove
 *
 * @property \Illuminate\Contracts\Foundation\Application $app
 */
trait EntityLoaderTrait
{
    /**
     * Load entities from Module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function loadEntitiesFromModule(ModuleInterface $module): void
    {
        $path = $module->getPath();
        
        $this->addEntityToDoctrineConfig($path);
    }
    
    /**
     * Register all entities.
     *
     * @param string $moduleNamespace
     *
     * @return void
     */
    protected function addEntityToDoctrineConfig(string $moduleNamespace): void
    {
        if ($this->app->runningInConsole() === false) {
            return;
        }
        
        $configKey    = 'doctrine.managers.default.paths';
        $entitiesPath = realpath($moduleNamespace.config('modules.mappings.entities', 'src/Model/Entities/'));
        
        if ($entitiesPath) {
            $this->app[Repository::class]->push($configKey, $entitiesPath);
        }
    }
}
