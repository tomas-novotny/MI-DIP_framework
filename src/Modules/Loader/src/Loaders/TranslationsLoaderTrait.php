<?php

namespace Modules\Loader\Loaders;

use Modules\Loader\Contracts\ModuleInterface;

/**
 * TODO: remove
 * @method void loadTranslationsFrom(string $path, string $namespace)
 * @method void publishes(array $paths, string $group = null)
 */
trait TranslationsLoaderTrait
{
    /**
     * Load translations for module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    public function loadTranslationsFromModule(ModuleInterface $module): void
    {
        $name = $module->getName();
        $path = $module->getPath();
        
        $this->loadTranslations($name, $path);
    }
    
    /**
     * Load translations.
     *
     * @param string $moduleName
     * @param string $modulePath
     *
     * @return void
     */
    protected function loadTranslations(string $moduleName, string $modulePath): void
    {
        $directory = $modulePath.config('modules.mappings.translations', 'resources/lang/');
        
        $langPath = base_path('resources/lang/modules/'.$moduleName);
        
        $this->publishes([$directory => $langPath], 'translations');
        
        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $moduleName);
        }
        else {
            $this->loadTranslationsFrom($directory, $moduleName);
        }
    }
}
