<?php

namespace Modules\Loader\Loaders;

use Modules\Loader\Contracts\ModuleInterface;

/**
 * TODO: remove
 *
 * @property \Illuminate\Contracts\Foundation\Application $app
 * @method void loadMigrationsFrom(string $paths)
 */
trait MigrationsLoaderTrait
{
    /**
     * Load migrations from Module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function loadMigrationsFromModule(ModuleInterface $module): void
    {
        $path = $module->getPath();
        
        $this->loadMigrations($path);
    }
    
    /**
     * Register all migrations.
     *
     * @param string $modulePath
     *
     * @return void
     */
    protected function loadMigrations(string $modulePath): void
    {
        if ($this->app->runningInConsole() === false) {
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }
        
        // get all config files from folder
        $directory = $modulePath.config('modules.mappings.migrations', 'src/Database/Migrations/');
        
        if (is_dir($directory) === false) {
            return;
        }
        
        $this->loadMigrationsFrom($directory);
    }
}
