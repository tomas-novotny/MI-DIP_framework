<?php

namespace Modules\Loader\Loaders;

use Modules\Loader\Contracts\ModuleInterface;

/**
 * TODO: remove
 *
 * @property \Illuminate\Contracts\Foundation\Application $app
 */
trait ProvidersLoaderTrait
{
    /**
     * Load main service provider from Module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function loadProviderFromModule(ModuleInterface $module): void
    {
        $namespace = $module->getNamespace();
        
        $this->loadProvider($namespace);
    }
    
    /**
     * Register main service provider.
     *
     * @param string $namespace
     *
     * @return void
     */
    protected function loadProvider(string $namespace): void
    {
        $provider = $namespace.$this->resolveProviderClass();
        
        if (class_exists($provider) === false) {
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }
        
        $this->app->register($provider);
    }
    
    /**
     * Resolve provider class from path.
     *
     * @return string
     */
    protected function resolveProviderClass(): string
    {
        $path = config('modules.mappings.provider', 'src/Providers/ServiceProvider.php');
        
        $namespace = str_replace(config('modules.mappings.source', 'src/'), '', $path);
        $namespace = str_replace('.php', '', $namespace);
        $namespace = str_replace('/', '\\', $namespace);
        
        return $namespace;
    }
}
