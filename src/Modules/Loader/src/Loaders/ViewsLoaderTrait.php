<?php

namespace Modules\Loader\Loaders;

use Illuminate\Filesystem\Filesystem;
use Modules\Loader\Contracts\ModuleInterface;

/**
 * TODO: remove
 * @method void publishes(array $paths, string $group = null)
 * @method void loadViewsFrom($path, string $namespace)
 */
trait ViewsLoaderTrait
{
    /**
     * Load view from Module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function loadViewsFromModule(ModuleInterface $module): void
    {
        $name = $module->getName();
        $path = $module->getPath();
        
        $this->loadViews($name, $path);
    }
    
    /**
     * Register all view.
     *
     * @param string $moduleName
     * @param string $modulePath
     *
     * @return void
     */
    protected function loadViews(string $moduleName, string $modulePath): void
    {
        // get all view files from folder
        $directory = $modulePath.'src/UI/';
        
        // no UI views in module
        if (is_dir($directory) === false) {
            return;
        }
        
        $directories = (new Filesystem())->directories(realpath($directory));
        
        foreach ($directories as $directory) {
            $this->loadViewsFromUI($moduleName, basename($directory), $directory);
        }
    }
    
    /**
     * Register all view.
     *
     * @param string $moduleName
     * @param string $UI
     * @param string $UIPath
     *
     * @return void
     */
    protected function loadViewsFromUI(string $moduleName, string $UI, string $UIPath): void
    {
        // get all view files from folder
        $directory = $UIPath.'/views/';
        
        if (is_dir($directory) === false) {
            return;
        }
        
        $this->publishes([$directory => $this->getPublicViewPath($moduleName, $UI)], 'views');
        
        $namespace = $moduleName.':'.strtolower($UI);
        
        // merge from published views
        $this->loadViewsFrom(array_merge(array_map(function ($path) use ($moduleName, $UI) {
            return $path.'/modules/'.$moduleName.'/'.strtolower($UI);
        }, config('view.paths')), [$directory]), $namespace);
        
        // load original views from modules
        $this->loadViewsFrom($directory, $namespace.':module');
    }
    
    /**
     * Get path to module published view.
     *
     * @param string      $module_name
     * @param string|null $UI
     *
     * @return string
     */
    private function getPublicViewPath(string $module_name, string $UI = null): string
    {
        return base_path('resources/views/modules/'.$module_name.($UI ? '/'.strtolower($UI) : ''));
    }
}
