<?php

namespace Modules\Loader\Loaders;

use Modules\Loader\Contracts\ModuleInterface;

trait AutoLoaderTrait
{
    use ConfigsLoaderTrait;
    use TranslationsLoaderTrait;
    use MigrationsLoaderTrait;
    use ViewsLoaderTrait;
    use RoutesLoaderTrait;
    use CommandsLoaderTrait;
    use EntityLoaderTrait;
    
    /**
     *  Autoload all parts from given module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    public function autoloadModule(ModuleInterface $module): void
    {
        $this->loadTranslationsFromModule($module);
        $this->loadMigrationsFromModule($module);
        $this->loadViewsFromModule($module);
        $this->loadRoutesFromModule($module);
        $this->loadCommandsFromModule($module);
        $this->loadEntitiesFromModule($module);
    }
}
