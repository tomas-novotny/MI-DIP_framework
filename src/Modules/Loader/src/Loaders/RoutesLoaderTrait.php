<?php

namespace Modules\Loader\Loaders;

use Illuminate\Support\Facades\Route;
use Modules\Loader\Contracts\ModuleInterface;

trait RoutesLoaderTrait
{
    /**
     * Load routes from Module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function loadRoutesFromModule(ModuleInterface $module): void
    {
        $path = $module->getPath();
        
        $this->loadWebRoutes($path);
        $this->loadApiRoutes($path);
        
        //        if ($this->shouldRegisterAdmin()) {
        //            $this->loadAdminRoutes($path);
        //        }
    }
    
    /**
     * Register routes for the application.
     *
     * @param string $modulePath
     *
     * @return void
     */
    private function loadWebRoutes(string $modulePath): void
    {
        $file = $modulePath.'src/UI/Web/routes.php';
        
        if (is_file($file) === false) {
            return;
        }
        
        Route::group([], $file);
    }
    
    /**
     * Register API routes for the application.
     *
     * @param string $modulePath
     *
     * @return void
     */
    private function loadApiRoutes(string $modulePath): void
    {
        $file = $modulePath.'src/UI/API/routes.php';
        
        if (is_file($file) === false) {
            return;
        }
        
        Route::group([], $file);
    }
}
