<?php

namespace Modules\Loader\Providers;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Modules\Loader\Contracts\ModuleRepositoryInterface;
use Modules\Loader\Loaders\AutoLoaderTrait;
use Modules\Loader\Loaders\ConfigsLoaderTrait;
use Modules\Loader\Loaders\ProvidersLoaderTrait;
use Modules\Loader\Repositories\ModuleRepository as ConcreteModuleRepository;
use Modules\Loader\UI\CLI\Commands\ListCommand;
use Modules\Loader\UI\CLI\Commands\SeedCommand;

class ServiceProvider extends LaravelServiceProvider
{
    use ConfigsLoaderTrait;
    use ProvidersLoaderTrait;
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        $this->loadModuleConfig();
        $this->registerModules();
        
        $this->commands([
            ListCommand::class,
            SeedCommand::class,
        ]);
    }
    
    /**
     * Register the view environment.
     *
     * @return void
     */
    private function registerModules(): void
    {
        $this->app->singleton(ModuleRepositoryInterface::class, function ($app) {
            $paths     = explode(',', $app['config']->get('modules.paths.modules'));
            $blacklist = explode(',', $app['config']->get('modules.blacklist'));
            
            return new ConcreteModuleRepository($paths, $blacklist);
        });
    }
    
    /**
     * Load config file.
     *
     * @return void
     */
    private function loadModuleConfig(): void
    {
        $configFile = __DIR__.'/../../config/modules.php';
        
        $this->loadConfig($configFile);
    }
    
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->autoloadModules();
    }
    
    /**
     * Autoload all parts from modules.
     *
     * @return void
     *
     * @throws \Modules\Loader\Exceptions\ModuleNotFoundException
     */
    public function autoloadModules(): void
    {
        foreach ($this->app[ModuleRepositoryInterface::class]->active() as $module) {
            $this->loadProviderFromModule($module);
        }
    }
}
