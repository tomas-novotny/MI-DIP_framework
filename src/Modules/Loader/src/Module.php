<?php

namespace Modules\Loader;

use InvalidArgumentException;
use Modules\Loader\Contracts\ModuleInterface;

class Module implements ModuleInterface
{
    /**
     * The module name.
     *
     * @var string
     */
    protected $name;
    
    /**
     * The module path.
     *
     * @var string
     */
    protected $path;
    
    /**
     * Module priority
     *
     * @var int
     */
    protected $priority;
    
    /**
     * Modules namespace
     *
     * @var string
     */
    protected $namespace;
    
    /**
     * Determinate if module is active
     *
     * @var bool
     */
    protected $autoload;
    
    /**
     * The constructor.
     *
     * @param string $name
     * @param string $path
     * @param string $namespace
     * @param int    $priority
     * @param bool   $autoload
     */
    public function __construct(string $name, string $path, string $namespace, int $priority, bool $autoload)
    {
        $this->name      = $name;
        $this->namespace = $namespace;
        $this->path      = $path;
        $this->priority  = $priority;
        $this->autoload  = $autoload;
    }
    
    /**
     * Create module from config.
     *
     * @param string $path
     *
     * @return \Modules\Loader\Contracts\ModuleInterface
     */
    public static function createFromConfig(string $path): ModuleInterface
    {
        $config = static::parseConfig($path);
        
        $name      = $config['name'];
        $path      = realpath(dirname($path)).'/';
        $namespace = $config['namespace'] ?? '//';
        $priority  = (int) ($config['priority'] ?? 0);
        $active    = (bool) ($config['autoload'] ?? false);
        
        return new static($name, $path, $namespace, $priority, $active);
    }
    
    /**
     * Parse config json file into assoc array.
     *
     * @param string $filename
     *
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    protected static function parseConfig(string $filename): array
    {
        $config = json_decode(file_get_contents($filename), true);
        
        if (isset($config['name']) === false) {
            throw new InvalidArgumentException('Module config does not contains "name" in "'.$filename.'".');
        }
        
        return $config;
    }
    
    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string
    {
        return strtolower($this->name);
    }
    
    /**
     * Get priority.
     *
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
    
    /**
     * Get path.
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
    
    /**
     * Get used namespace.
     *
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }
    
    /**
     * Module is active
     *
     * @return bool
     */
    public function isAutoload(): bool
    {
        return $this->autoload;
    }
    
    /**
     * Make module active.
     *
     * @return void
     */
    public function activate(): void
    {
        $this->autoload = true;
    }
    
    /**
     * Make module disabled.
     *
     * @return void
     */
    public function disable(): void
    {
        $this->autoload = false;
    }
}
