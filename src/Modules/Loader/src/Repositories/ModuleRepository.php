<?php

namespace Modules\Loader\Repositories;

use InvalidArgumentException;
use Modules\Loader\Contracts\ModuleInterface;
use Modules\Loader\Contracts\ModuleRepositoryInterface;
use Modules\Loader\Events\ModuleLoaded;
use Modules\Loader\Exceptions\AbstractException as ModuleException;
use Modules\Loader\Exceptions\ModuleDisabledException;
use Modules\Loader\Exceptions\ModuleNotFoundException;
use Modules\Loader\Module;

class ModuleRepository implements ModuleRepositoryInterface
{
    /**
     * Directories that contains modules.
     *
     * @var array
     */
    private $paths;
    
    /**
     * Blacklist modules.
     *
     * @var array
     */
    private $blacklist;
    
    /**
     * ModuleRepository constructor.
     *
     * @param array $paths
     * @param array $blacklist
     */
    public function __construct(array $paths = [], array $blacklist = [])
    {
        $this->paths     = $paths;
        $this->blacklist = $blacklist;
    }
    
    /**
     * Get all modules ordered by priority.
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    public function ordered(): array
    {
        $modules = $this->all();
        
        $this->sort($modules);
        
        return $modules;
    }
    
    /**
     * Get all modules.
     * TODO: add caching
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    public function all(): array
    {
        // scan modules in filesystem
        $modules = array_values($this->scanPaths());
        
        // return array with modules
        return $modules;
    }
    
    /**
     * Get all modules.
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    protected function scanPaths(): array
    {
        $modules = [];
        
        foreach ($this->paths as $path) {
            $modules = array_merge($modules, $this->scanPath($path));
        }
        
        return $modules;
    }
    
    /**
     * Scan path for modules directories.
     *
     * @param string $path
     *
     * @return array
     */
    protected function scanPath(string $path): array
    {
        $modules = [];
        
        $manifests = glob($path.'/*/'.ModuleInterface::CONFIG_FILE);
        foreach ($manifests as $manifest) {
            $module = Module::createFromConfig($manifest);
            
            if ($this->isBlacklisted($module->getName())) {
                $module->disable();
            }
            
            event(new ModuleLoaded($module));
            
            $modules[strtolower($module->getName())] = $module;
        }
        
        return $modules;
    }
    
    /**
     * Module is not blacklisted.
     *
     * @param string $name
     *
     * @return bool
     */
    private function isBlacklisted(string $name): bool
    {
        return array_search(strtolower($name), array_map('strtolower', $this->blacklist)) !== false;
    }
    
    /**
     * Sort given modules array by priority.
     *
     * @param array $modules
     *
     * @return void
     */
    protected function sort(array &$modules): void
    {
        usort($modules, function (ModuleInterface $a, ModuleInterface $b) {
            return $a->getPriority() <=> $b->getPriority();
        });
    }
    
    /**
     * Get all modules ordered by priority.
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    public function active(): array
    {
        $modules = $this->all();
        
        $modules = array_filter($modules, function (ModuleInterface $module) {
            return $module->isAutoload();
        });
        
        $this->sort($modules);
        
        return $modules;
    }
    
    /**
     * Determine whether the given module exist.
     *
     * @param string $name
     *
     * @return bool
     */
    public function has(string $name): bool
    {
        try {
            $this->get($name);
            
            return true;
        }
        catch (ModuleException $e) {
            return false;
        }
    }
    
    /**
     * Determine whether the given module exist and it is active.
     *
     * @param string $name
     *
     * @return \Modules\Loader\Contracts\ModuleInterface
     *
     * @throws \Modules\Loader\Exceptions\ModuleNotFoundException
     * @throws \Modules\Loader\Exceptions\ModuleDisabledException
     */
    public function get(string $name): ModuleInterface
    {
        foreach ($this->all() as $module) {
            if ($module->getName() == strtolower($name)) {
                
                if ($module->isAutoload() === false) {
                    throw new ModuleDisabledException($name);
                }
                
                return $module;
            }
        }
        
        throw new ModuleNotFoundException($name);
    }
}
