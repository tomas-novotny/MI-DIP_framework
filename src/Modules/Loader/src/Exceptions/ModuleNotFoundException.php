<?php

namespace Modules\Loader\Exceptions;

use Throwable;

class ModuleNotFoundException extends AbstractException
{
    /**
     * Exception constructor.
     *
     * @param string          $module
     * @param \Throwable|null $previous
     */
    public function __construct(string $module, Throwable $previous = null)
    {
        parent::__construct("Module [$module] not found.", 0, $previous);
    }
}
