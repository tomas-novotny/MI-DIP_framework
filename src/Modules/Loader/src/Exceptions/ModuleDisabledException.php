<?php

namespace Modules\Loader\Exceptions;

use Throwable;

class ModuleDisabledException extends AbstractException
{
    /**
     * Exception constructor.
     *
     * @param string          $module
     * @param \Throwable|null $previous
     */
    public function __construct(string $module, Throwable $previous = null)
    {
        parent::__construct("Module [$module] is disabled.", 0, $previous);
    }
}
