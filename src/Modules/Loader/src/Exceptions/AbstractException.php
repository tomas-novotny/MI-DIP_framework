<?php

namespace Modules\Loader\Exceptions;

use RuntimeException as BaseRuntimeException;

abstract class AbstractException extends BaseRuntimeException
{
    //
}
