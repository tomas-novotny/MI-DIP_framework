<?php

namespace Modules\Loader\UI\CLI\Commands;

use Modules\Loader\Contracts\ModuleRepositoryInterface;
use Modules\Support\Parents\AbstractCommand;

class ListCommand extends AbstractCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'module:list';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show list of all modules.';
    
    /**
     * Module repository.
     *
     * @var \Modules\Loader\Contracts\ModuleRepositoryInterface
     */
    private $repository;
    
    /**
     * Module list command constructor.
     *
     * @param \Modules\Loader\Contracts\ModuleRepositoryInterface $repository
     */
    public function __construct(ModuleRepositoryInterface $repository)
    {
        parent::__construct();
        
        $this->repository = $repository;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        // Show modules list as table
        $this->table(['Active', 'Name', 'Priority', 'Path'], $this->getRows());
    }
    
    /**
     * Get table rows.
     *
     * @return array
     */
    private function getRows(): array
    {
        $rows = [];
        
        foreach ($this->repository->ordered() as $module) {
            $rows[] = [
                (int) $module->isAutoload(),
                $module->getName(),
                $module->getPriority(),
                str_replace(base_path(), '', $module->getPath()),
            ];
        }
        
        return $rows;
    }
}
