<?php

namespace Modules\Loader\UI\CLI\Commands;

use Modules\Loader\Contracts\ModuleInterface;
use Modules\Loader\Contracts\ModuleRepositoryInterface;
use Modules\Loader\Exceptions\ModuleDisabledException;
use Modules\Loader\Exceptions\ModuleNotFoundException;
use Modules\Support\Parents\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SeedCommand extends AbstractCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'module:seed';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run database seeder from the specified module or from all modules.';
    
    /**
     * Module repository.
     *
     * @var \Modules\Loader\Contracts\ModuleRepositoryInterface
     */
    private $repository;
    
    /**
     * Module list command constructor.
     *
     * @param \Modules\Loader\Contracts\ModuleRepositoryInterface $repository
     */
    public function __construct(ModuleRepositoryInterface $repository)
    {
        parent::__construct();
        
        $this->repository = $repository;
    }
    
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $modules = $this->resolveModules();
        
        if (count($modules) === 0) {
            return;
        }
        
        foreach ($modules as $module) {
            $this->seedModule($module);
        }
        
        $this->info('All modules seeded.');
    }
    
    /**
     * Get modules to migrate.
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    private function resolveModules(): array
    {
        $name = ucfirst((string) $this->argument('module'));
        
        // no module specified, get all active modules
        if (empty($name)) {
            $modules = $this->repository->active();
            
            if (count($modules) === 0) {
                $this->error('No modules found.');
            }
            
            return $modules;
        }
        
        // get module by given name
        try {
            $module = $this->repository->get($name);
        }
        catch (ModuleNotFoundException $e) {
            $this->error("Module [$name] does not exists.");
            
            return [];
        }
        catch (ModuleDisabledException $e) {
            $this->error("Module [$name] is not active.");
            
            return [];
        }
        
        return [$module];
    }
    
    /**
     * Seed module.
     *
     * @codeCoverageIgnore
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @throws \Exception
     *
     * @return void
     */
    protected function seedModule(ModuleInterface $module): void
    {
        $class = $this->getSeederName($module);
        
        if (class_exists($class)) {
            $this->dbseed($module);
            
            $name = $module->getName();
            $this->info("Module [$name] seeded.");
        }
    }
    
    /**
     * Get master database seeder name for the specified module.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return string
     */
    protected function getSeederName(ModuleInterface $module): string
    {
        // TODO: use config - parse from path
        return $module->getNamespace().'Database\\Seeders\\DatabaseSeeder';
    }
    
    /**
     * Seed the specified module.
     *
     * @codeCoverageIgnore
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     *
     * @return void
     */
    protected function dbseed(ModuleInterface $module): void
    {
        $params = [
            '--class' => $this->option('class') ?: $this->getSeederName($module),
        ];
        
        if ($option = $this->option('database')) {
            $params['--database'] = $option;
        }
        
        $this->call('db:seed', $params);
    }
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments(): array
    {
        return [
            ['module', InputArgument::OPTIONAL, 'The name of module will be used.'],
        ];
    }
    
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions(): array
    {
        return [
            ['class', null, InputOption::VALUE_OPTIONAL, 'The class name of the root seeder', null],
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to seed.'],
        ];
    }
}
