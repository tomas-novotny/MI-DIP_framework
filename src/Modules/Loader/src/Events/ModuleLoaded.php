<?php

namespace Modules\Loader\Events;

use Modules\Loader\Contracts\ModuleInterface;
use Modules\Support\Parents\AbstractEvent;

class ModuleLoaded extends AbstractEvent
{
    /**
     * Loaded module.
     *
     * @var \Modules\Loader\Contracts\ModuleInterface
     */
    public $module;
    
    /**
     * ModulesLoaded constructor.
     *
     * @param \Modules\Loader\Contracts\ModuleInterface $module
     */
    public function __construct(ModuleInterface $module)
    {
        $this->module = $module;
    }
}
