<?php

use Modules\Loader\Contracts\ModuleRepositoryInterface;

if (function_exists('modules') === false) {
    
    /**
     * Modules
     *
     * @param string|null $key
     *
     * @return \Modules\Loader\Contracts\ModuleRepositoryInterface|bool
     */
    function modules(string $key = null)
    {
        /* @var \Modules\Loader\Contracts\ModuleRepositoryInterface $modules */
        $modules = app(ModuleRepositoryInterface::class);
        
        if ($key === null) {
            return $modules;
        }
        
        return $modules->has($key);
    }
}
