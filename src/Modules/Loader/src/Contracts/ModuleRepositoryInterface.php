<?php

namespace Modules\Loader\Contracts;

interface ModuleRepositoryInterface
{
    /**
     * Get all modules.
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    public function all(): array;
    
    /**
     * Get all modules ordered by priority.
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    public function ordered(): array;
    
    /**
     * Get only active modules ordered by priority.
     *
     * @return \Modules\Loader\Contracts\ModuleInterface[]
     */
    public function active(): array;
    
    /**
     * Determine whether the given module exist and it is active.
     *
     * @param string $name
     *
     * @return bool
     */
    public function has(string $name): bool;
    
    /**
     * Determine whether the given module exist and it is active.
     *
     * @param string $name
     *
     * @return \Modules\Loader\Contracts\ModuleInterface
     *
     * @throws \Modules\Loader\Exceptions\ModuleNotFoundException
     * @throws \Modules\Loader\Exceptions\ModuleDisabledException
     */
    public function get(string $name): ModuleInterface;
}
