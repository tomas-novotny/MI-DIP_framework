<?php

namespace Modules\Loader\Contracts;

interface ModuleInterface
{
    /**
     * Config file name.
     *
     * @var string
     */
    public const CONFIG_FILE = 'module.json';
    
    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string;
    
    /**
     * Get priority.
     *
     * @return int
     */
    public function getPriority(): int;
    
    /**
     * Get path.
     *
     * @return string
     */
    public function getPath(): string;
    
    /**
     * Get used namespace.
     *
     * @return string
     */
    public function getNamespace(): string;
    
    /**
     * Make module disabled.
     *
     * @return void
     */
    public function disable(): void;
    
    /**
     * Make module active.
     *
     * @return void
     */
    public function activate(): void;
    
    /**
     * Module is active
     *
     * @return bool
     */
    public function isAutoload(): bool;
}
