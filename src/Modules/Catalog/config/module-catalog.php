<?php

use Modules\Catalog\Model\Entities\Concretes\Doctrine\Product as DoctrineProduct;
use Modules\Catalog\Model\Entities\Concretes\Eloquent\Product as EloquentProduct;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Model\Factories\Concretes\DoctrineProductFactory;
use Modules\Catalog\Model\Factories\Concretes\EloquentProductFactory;
use Modules\Catalog\Model\Factories\Contracts\ProductFactoryInterface;
use Modules\Catalog\Model\Repositories\Concretes\DoctrineProductRepository;
use Modules\Catalog\Model\Repositories\Concretes\EloquentProductRepository;
use Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface;

return [
    'bindings' => [
        'eloquent' => [
            ProductInterface::class           => EloquentProduct::class,
            ProductFactoryInterface::class    => EloquentProductFactory::class,
            ProductRepositoryInterface::class => EloquentProductRepository::class,
        ],
        'doctrine' => [
            ProductInterface::class           => DoctrineProduct::class,
            ProductFactoryInterface::class    => DoctrineProductFactory::class,
            ProductRepositoryInterface::class => DoctrineProductRepository::class,
        ],
    ],
    
    'views' => [
        'product-catalog' => 'catalog:web::product-catalog',
        'product-detail'  => 'catalog:web::product-detail',
    ],
];