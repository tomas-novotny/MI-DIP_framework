# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-07
### Added
- Add [`GetProductBySlugAction.php`](src/Services/Actions/GetProductBySlugAction.php)
- Add [`GetSaleableProductsAction.php`](src/Services/Actions/GetSaleableProductsAction.php)
