<?php

namespace Modules\Catalog\Database\Seeders;

use Modules\Catalog\Model\Factories\Contracts\ProductFactoryInterface;
use Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface;
use Modules\Support\Parents\AbstractSeeder;

class ProductSeeder extends AbstractSeeder
{
    /**
     * Product factory.
     *
     * @var \Modules\Catalog\Model\Factories\Contracts\ProductFactoryInterface
     */
    private $factory;
    
    /**
     * Product repository.
     *
     * @var \Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface
     */
    private $repository;
    
    /**
     * ProductSeeder constructor.
     *
     * @param \Modules\Catalog\Model\Factories\Contracts\ProductFactoryInterface       $factory
     * @param \Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface $repository
     */
    public function __construct(ProductFactoryInterface $factory, ProductRepositoryInterface $repository)
    {
        $this->factory    = $factory;
        $this->repository = $repository;
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->flushTable('products');
        
        for ($i = 0; $i < 10; $i++) {
            $product = $this->factory->newProduct();
            $product->setDisplayName(ucfirst($this->faker()->words(3, true)));
            $product->setSlug(str_slug($product->getDisplayName()));
            $product->setDescription($this->faker()->sentences(2, true));
            $product->setPrice($this->faker()->randomFloat(2, 100, 10000));
            $product->setStock($this->faker()->numberBetween(1, 100));
            
            $this->repository->persist($product);
        }
    }
}
