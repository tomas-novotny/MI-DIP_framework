<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateProductsTable extends AbstractMigration
{
    /**
     * Run the migration up.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('products', function (Blueprint $table) {
            // primary key
            $table->increments('id')->unsigned();
            // attributes
            $table->string('slug')->unique();
            $table->string('display_name');
            $table->text('description');
            $table->decimal('price', 8, 2)->default(0);
            $table->decimal('tax', 3, 2)->default(0);
            $table->integer('stock')->default(0);
            // basic dates
            $table->timestamps();
        });
    }
    
    /**
     * Run the migration down.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->dropIfExists('products');
    }
}
