<?php

namespace Modules\Catalog\UI\Web\Controllers;

use Modules\Catalog\Services\Actions\GetProductBySlugAction;
use Modules\Catalog\Services\Actions\GetSaleableProductDetailsAction;
use Modules\Support\Parents\Controllers\AbstractWebController;

class CatalogController extends AbstractWebController
{
    /**
     * Show catalog page.
     *
     * @return \Illuminate\View\View
     */
    public function showCatalog()
    {
        $productDetails = $this->dispatchService(new GetSaleableProductDetailsAction());
        
        return $this->view(config('module-catalog.views.product-catalog'), compact('productDetails'));
    }
    
    /**
     * Show product page.
     *
     * @param string $slug
     *
     * @return \Illuminate\View\View
     */
    public function showProduct(string $slug)
    {
        $product = $this->dispatchService(new GetProductBySlugAction($slug));
        
        return $this->view(config('module-catalog.views.product-detail'), compact('product'));
    }
}
