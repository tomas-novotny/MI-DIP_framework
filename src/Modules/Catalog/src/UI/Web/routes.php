<?php

use Illuminate\Routing\Router;
use Modules\Catalog\UI\Web\Controllers\CatalogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

web_router(function (Router $router) {
    $router->group(['middleware' => ['stateless']], function (Router $router) {
        
        $router->get(trans('catalog::routes.catalog'), [
            'as'   => 'catalog',
            'uses' => CatalogController::class.'@showCatalog',
        ]);
    
        $router->get(trans('catalog::routes.product'), [
            'as'   => 'catalog.product',
            'uses' => CatalogController::class.'@showProduct',
        ]);
    });
});
