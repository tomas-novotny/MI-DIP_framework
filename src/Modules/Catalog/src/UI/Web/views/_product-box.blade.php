<div class="product-box" data-test-type="product-box" data-test-value="{{ $product->getKey() }}">
    <h2 data-test-type="product-title">{{ $product->getDisplayName() }}</h2>

    <p data-test-type="product-desc">{{ $product->getDescription() }}</p>

    <p data-test-type="product-price-with-vat">{{ currency_format($price->getPriceWithVat(), 2, ',', ' ') }} Kč</p>
    <p data-test-type="product-stock">{{ $product->getStock() }} ks</p>

    @if($product->inStock() > 0)
        <form action="{{ route('cart.add') }}" method="post" data-test-type="add-to-cart-form">
            @csrf
            <input type="hidden" name="product_id" value="{{ $product->getKey() }}"/>
            <div class="row">
                <div class="col-8">
                    <input type="number" name="amount" min="1" max="{{ $product->getStock() }}" value="1" step="1"
                           class="form-control"/>
                </div>
                <div class="col-4">

                    <button type="submit" name="add-to-cart" class="btn btn-success">
                        {{ trans('catalog::templates/common.add_to_cart') }}
                    </button>
                </div>
            </div>
        </form>
    @else
        <div>
            <button type="submit" class="btn btn-danger disabled">
                {{ trans('catalog::templates/common.sold_out') }}
            </button>
        </div>
    @endif
</div>
