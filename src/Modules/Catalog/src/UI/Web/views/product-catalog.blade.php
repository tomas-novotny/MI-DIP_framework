@extends('app:web::_layouts.master')

@section('title'){{ trans('catalog::templates/common.products') }} | @parent @endsection

@section('content')

    <h1>
        {{ trans('catalog::templates/common.products') }}
    </h1>
    <div data-test-type="products-box">
        @foreach($productDetails->chunk(3) as $chunk)
            <div class="row">
                @foreach($chunk as $productDetail)
                    <div class="col-sm-4 mb-4">
                        @include('catalog:web::_product-box', [
                            'product' => $productDetail->getProduct(),
                            'price' => $productDetail->getSellingPrice(),
                            'url' => $productDetail->getProductUrl(),
                        ])
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>

@endsection
