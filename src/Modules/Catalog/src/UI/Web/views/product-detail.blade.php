@extends('app:web::_layouts.master')

@section('title'){{ trans('catalog::templates/common.products') }} | @parent @endsection

@section('content')

    @include('catalog:web::_product-box')

@endsection
