<?php

namespace Modules\Catalog\Providers;

use Modules\Support\Parents\Providers\AbstractAutoloadServiceProvider;

class ServiceProvider extends AbstractAutoloadServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        BindingServiceProvider::class,
    ];
}
