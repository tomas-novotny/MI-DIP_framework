<?php

namespace Modules\Catalog\Services\Tasks;

use Modules\Bus\Parents\AbstractTask;
use Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface;

/**
 * Class GetSaleableProductByIdTask
 *
 * @package Modules\Catalog
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class GetSaleableProductByIdTask extends AbstractTask
{
    /**
     * Product repository.
     *
     * @var \Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface
     */
    private $productRepository;
    
    /**
     * Product primary key.
     *
     * @var string
     */
    private $key;
    
    /**
     * GetSaleableProductByIdTask constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->productRepository = $this->resolve(ProductRepositoryInterface::class);
        
        $this->key = $key;
    }
    
    /**
     * Get product.
     *
     * @return \Modules\Support\Collection
     */
    public function handle()
    {
        $product = $this->productRepository->getSableableById($this->key);
        
        return $product;
    }
}
