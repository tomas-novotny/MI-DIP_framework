<?php

namespace Modules\Catalog\Services\Tasks;

use Modules\Bus\Parents\AbstractTask;
use Modules\Catalog\Model\Aggregates\ProductDetail;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Services\ProductPriceCalculation;

/**
 * Class GetProductDetailTask
 *
 * @package Modules\Catalog
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class GetProductDetailTask extends AbstractTask
{
    /**
     * Product
     *
     * @var \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    private $product;
    
    /**
     * Price calculation
     *
     * @var \Modules\Catalog\Services\ProductPriceCalculation
     */
    private $priceCalculation;
    
    /**
     * GetProductDetailTask constructor.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->priceCalculation = $this->resolve(ProductPriceCalculation::class);
        
        $this->product = $product;
    }
    
    /**
     * @return \Modules\Catalog\Model\Aggregates\ProductDetail
     */
    public function handle()
    {
        $productPrice = $this->priceCalculation->calculatePrice($this->product);
        
        $productDetail = new ProductDetail($this->product, $productPrice);
        
        return $productDetail;
    }
}
