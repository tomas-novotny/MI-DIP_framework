<?php

namespace Modules\Catalog\Services\Actions;

use Modules\Bus\Parents\AbstractAction;
use Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface;
use Modules\Catalog\Services\Tasks\GetProductDetailTask;
use Modules\Support\Collection;

/**
 * Class GetSaleableProductDetailsAction
 *
 * @package Modules\Catalog
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class GetSaleableProductDetailsAction extends AbstractAction
{
    /**
     * Product repository.
     *
     * @var \Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface
     */
    private $productRepository;
    
    /**
     * GetSaleableProductsAction constructor.
     */
    public function __construct()
    {
        $this->productRepository = $this->resolve(ProductRepositoryInterface::class);
    }
    
    /**
     * Get saleable products.
     *
     * @return \Modules\Support\Collection
     */
    public function handle()
    {
        $products       = $this->productRepository->getSaleableProducts();
        $productDetails = new Collection();
        
        foreach ($products as $product) {
            $productDetail = $this->dispatchTask(new GetProductDetailTask($product));
            
            $productDetails->push($productDetail);
        }
        
        return $productDetails;
    }
}
