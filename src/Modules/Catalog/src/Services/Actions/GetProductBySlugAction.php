<?php

namespace Modules\Catalog\Services\Actions;

use Modules\Bus\Parents\AbstractAction;
use Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class GetProductBySlugAction
 *
 * @package Modules\Catalog
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class GetProductBySlugAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * Slug.
     *
     * @var string
     */
    private $slug;
    
    /**
     * Cart repository.
     *
     * @var \Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface
     */
    private $productRepository;
    
    /**
     * GetProductBySlugAction constructor.
     *
     * @param string $slug
     */
    public function __construct(string $slug)
    {
        $this->productRepository = $this->resolve(ProductRepositoryInterface::class);
        
        $this->slug              = $slug;
    }
    
    /**
     * Get saleable products.
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        $product = $this->productRepository->getBySlug($this->slug);
        
        return $product;
    }
}
