<?php

namespace Modules\Catalog\Services;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Model\Values\ProductPrice;
use Modules\Pricing\Model\Values\Price;
use Modules\Pricing\Model\Values\Vat;
use Modules\Pricing\Services\PriceCalculation;

class ProductPriceCalculation extends Price
{
    /**
     * @var \Modules\Pricing\Services\PriceCalculation
     */
    private $priceCalculation;
    
    /**
     * ProductPriceCalculation constructor.
     *
     * @param \Modules\Pricing\Services\PriceCalculation $priceCalculation
     */
    public function __construct(PriceCalculation $priceCalculation)
    {
        $this->priceCalculation = $priceCalculation;
    }
    
    /**
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return \Modules\Catalog\Model\Values\ProductPrice
     */
    public function calculatePrice(ProductInterface $product)
    {
        $price = $this->priceCalculation->calculatePrice($product->getPrice(), true, new Vat($product->getTax()));
        
        $productPrice = new ProductPrice($price, false);
        
        return $productPrice;
    }
}