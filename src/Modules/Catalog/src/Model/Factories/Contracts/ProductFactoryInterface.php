<?php

namespace Modules\Catalog\Model\Factories\Contracts;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;

interface ProductFactoryInterface
{
    /**
     * Create new product instance.
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    public function newProduct(): ProductInterface;
}
