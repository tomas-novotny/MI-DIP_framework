<?php

namespace Modules\Catalog\Model\Factories\Concretes;

use Modules\Catalog\Model\Entities\Concretes\Doctrine\Product;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Model\Factories\Contracts\ProductFactoryInterface;

class DoctrineProductFactory implements ProductFactoryInterface
{
    /**
     * Create new product instance.
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    public function newProduct(): ProductInterface
    {
        $product = new Product();
        
        $product->setDescription('');
        $product->setPrice(0.0);
        $product->setStock(0);
        $product->setTax(0.21);
        
        return $product;
    }
}
