<?php

namespace Modules\Catalog\Model\Values;

use Modules\Pricing\Model\Values\Price;

class ProductPrice extends Price
{
    /**
     * Indicate if its minimum variant price.
     *
     * @var bool
     */
    private $minimum;
    
    /**
     * ProductPrice constructor.
     *
     * @param \Modules\Pricing\Model\Values\Price $price
     * @param bool                                $minimum
     */
    public function __construct(Price $price, bool $minimum = false)
    {
        $this->minimum = $minimum;
        
        parent::__construct($price->getPriceWithoutVat(), $price->getPriceWithVat());
    }
    
    /**
     * Indicate if its minimum variant price.
     *
     * @return bool
     */
    public function isMinimumPrice(): bool
    {
        return $this->minimum;
    }
}