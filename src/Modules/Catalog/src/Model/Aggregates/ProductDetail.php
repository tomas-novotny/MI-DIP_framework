<?php

namespace Modules\Catalog\Model\Aggregates;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Model\Values\ProductPrice;

class ProductDetail
{
    /**
     * @var \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    private $product;
    
    /**
     * @var \Modules\Catalog\Model\Values\ProductPrice
     */
    private $sellingPrice;
    
    /**
     * ProductDetail constructor.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param \Modules\Catalog\Model\Values\ProductPrice|null            $sellingPrice
     */
    public function __construct(ProductInterface $product, ProductPrice $sellingPrice)
    {
        $this->product      = $product;
        $this->sellingPrice = $sellingPrice;
    }
    
    /**
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->product;
    }
    
    /**
     * @return \Modules\Catalog\Model\Values\ProductPrice
     */
    public function getSellingPrice(): ProductPrice
    {
        return $this->sellingPrice;
    }
    
    /**
     * @return string
     */
    public function getProductUrl(): string
    {
        return route('catalog', [$this->product->getSlug()]);
    }
}