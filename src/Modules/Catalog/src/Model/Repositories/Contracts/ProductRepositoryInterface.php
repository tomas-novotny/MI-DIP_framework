<?php

namespace Modules\Catalog\Model\Repositories\Contracts;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Support\Contracts\CollectionInterface;

interface ProductRepositoryInterface
{
    /**
     * Persist model to database.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     */
    public function persist(ProductInterface $product): void;
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\PermissionInterface $product
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(ProductInterface $product): void;
    
    /**
     * Get all user products.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function getSaleableProducts(): CollectionInterface;
    
    /**
     * Get product by slug.
     *
     * @param string $slug
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getBySlug(string $slug): ProductInterface;
    
    /**
     * Get product by id.
     *
     * @param string $id
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getById(string $id): ProductInterface;
    
    /**
     * Get product by id.
     *
     * @param string $id
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getSableableById(string $id): ProductInterface;
}
