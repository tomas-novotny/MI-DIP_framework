<?php

namespace Modules\Catalog\Model\Repositories\Criterias;

use Doctrine\Common\Collections\Criteria;
use Modules\Support\Parents\AbstractCriteria;

class HasSlugCriteria extends AbstractCriteria
{
    /**
     * HasSlugCriteria constructor.
     *
     * @param string $slug
     */
    public function __construct(string $slug)
    {
        $expr = $this->expr();
        
        $this->andWhere($expr->eq('slug', $slug));
    }
}
