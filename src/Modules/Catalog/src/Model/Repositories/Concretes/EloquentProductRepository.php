<?php

namespace Modules\Catalog\Model\Repositories\Concretes;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface;
use Modules\Catalog\Model\Repositories\Criterias\HasSlugCriteria;
use Modules\Catalog\Model\Repositories\Criterias\SaleableCriteria;
use Modules\Doctrine\Criterias\HasIdCriteria;
use Modules\Eloquent\EntityRepository;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;

class EloquentProductRepository implements ProductRepositoryInterface
{
    /**
     * Entity repository.
     *
     * @var \Modules\Eloquent\EntityRepository
     */
    private $entityRepository;
    
    /**
     * EloquentProductRepository constructor.
     */
    public function __construct()
    {
        $this->entityRepository = new EntityRepository(app()->getAlias(ProductInterface::class));
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     */
    public function persist(ProductInterface $product): void
    {
        $this->entityRepository->persistAndFlush($product);
    }
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(ProductInterface $product): void
    {
        $this->entityRepository->removeAndFlush($product);
    }
    
    /**
     * Get all user products.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function getSaleableProducts(): CollectionInterface
    {
        $criterias = [
            new SaleableCriteria(),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return new Collection($this->entityRepository->all());
    }
    
    /**
     * Get product by slug.
     *
     * @param string $slug
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getBySlug(string $slug): ProductInterface
    {
        $criterias = [
            new HasSlugCriteria($slug),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
    
    /**
     * Get product by code.
     *
     * @param string $id
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getById(string $id): ProductInterface
    {
        $criterias = [
            new HasIdCriteria($id),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
    
    /**
     * Get product by code.
     *
     * @param string $id
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getSableableById(string $id): ProductInterface
    {
        $criterias = [
            new SaleableCriteria(),
            new HasIdCriteria($id),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
}
