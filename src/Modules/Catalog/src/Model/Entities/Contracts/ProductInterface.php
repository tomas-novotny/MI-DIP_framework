<?php

namespace Modules\Catalog\Model\Entities\Contracts;

use Modules\Order\Model\Entities\Contracts\ShoppableInterface;

interface ProductInterface
{
    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */
    
    public const        TABLE             = 'products';
    
    public const        PRIMARY_KEY       = 'id';
    
    public const        FOREIGN_KEY       = 'product_id';
    
    public const        ATTR_DISPLAY_NAME = 'display_name';
    
    public const        ATTR_SLUG         = 'slug';
    
    public const        ATTR_DESCRIPTION  = 'description';
    
    public const        ATTR_PRICE        = 'price';
    
    public const        ATTR_TAX          = 'tax';
    
    public const        ATTR_STOCK        = 'stock';
    
    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get hashed ID.
     *
     * @return string
     */
    public function getKey();
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setDisplayName(string $name): void;
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string;
    
    /**
     * Set product slug.
     *
     * @param string $slug
     *
     * @return void
     */
    public function setSlug(string $slug): void;
    
    /**
     * Get product slug.
     *
     * @return string
     */
    public function getSlug(): string;
    
    /**
     * Set description.
     *
     * @param string $description
     *
     * @return void
     */
    public function setDescription(string $description): void;
    
    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): string;
    
    /**
     * Get price with tax.
     *
     * @param float $price
     *
     * @return void
     */
    public function setPrice(float $price): void;
    
    /**
     * Get price with tax.
     *
     * @return float
     */
    public function getPrice(): float;
    
    /**
     * Get price with tax.
     *
     * @param int $amount
     *
     * @return void
     */
    public function setStock(int $amount): void;
    
    /**
     * Get stock.
     *
     * @return int
     */
    public function getStock(): int;
    
    /**
     * Determinates if product variant is in stock.
     *
     * @return bool
     */
    public function inStock(): bool;
    
    /**
     * Set tax value.
     *
     * @param  float $tax
     *
     * @return void
     */
    public function setTax(float $tax): void;
    
    /**
     * Get tax value.
     *
     * @return float
     */
    public function getTax(): float;
}
