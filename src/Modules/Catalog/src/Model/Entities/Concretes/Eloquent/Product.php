<?php

namespace Modules\Catalog\Model\Entities\Concretes\Eloquent;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Eloquent\Parents\AbstractModel;

class Product extends AbstractModel implements ProductInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = ProductInterface::TABLE;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->getAttribute(ProductInterface::ATTR_DISPLAY_NAME);
    }
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setDisplayName(string $name): void
    {
        $this->setAttribute(ProductInterface::ATTR_DISPLAY_NAME, $name);
    }
    
    /**
     * Set product slug.
     *
     * @param string $slug
     *
     * @return void
     */
    public function setSlug(string $slug): void
    {
        $this->setAttribute(ProductInterface::ATTR_SLUG, $slug);
    }
    
    /**
     * Get product slug.
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->getAttribute(ProductInterface::ATTR_SLUG);
    }
    
    /**
     * Set description.
     *
     * @param string $description
     *
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->setAttribute(ProductInterface::ATTR_DESCRIPTION, $description);
    }
    
    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getAttribute(ProductInterface::ATTR_DESCRIPTION);
    }
    
    /**
     * Get price with tax.
     *
     * @param float $price
     *
     * @return void
     */
    public function setPrice(float $price): void
    {
        $this->setAttribute(ProductInterface::ATTR_PRICE, $price);
    }
    
    /**
     * Get price with tax.
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->getAttribute(ProductInterface::ATTR_PRICE);
    }
    
    /**
     * Set tax value.
     *
     * @param float $tax
     *
     * @return void
     */
    public function setTax(float $tax): void
    {
        $this->setAttribute(ProductInterface::ATTR_TAX, $tax);
    }
    
    /**
     * Get tax value.
     *
     * @return float
     */
    public function getTax(): float
    {
        return $this->getAttribute(ProductInterface::ATTR_TAX);
    }
    
    /**
     * Get price with tax.
     *
     * @param int $amount
     *
     * @return void
     */
    public function setStock(int $amount): void
    {
        $this->stock = $amount;
    }
    
    /**
     * Determinates if product variant is in stock.
     *
     * @return bool
     */
    public function inStock(): bool
    {
        return $this->getStock() > 0;
    }
    
    /**
     * Get stock.
     *
     * @return int
     */
    public function getStock(): int
    {
        return $this->getAttribute(ProductInterface::ATTR_STOCK);
    }
}
