<?php

namespace Modules\Catalog\Model\Entities\Concretes\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Pricing\Model\Values\Vat;

/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Product implements ProductInterface
{
    use Timestamps;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * ID.
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var string
     */
    private $id;
    
    /**
     * Display name.
     *
     * @ORM\Column(name="display_name", type="string")
     *
     * @var string
     */
    protected $displayName;
    
    /**
     * Slug.
     *
     * @ORM\Column(name="slug", type="string", unique=true)
     *
     * @var string
     */
    protected $slug;
    
    /**
     * Description.
     *
     * @ORM\Column(name="description", type="text")
     *
     * @var string
     */
    protected $description;
    
    /**
     * Price with tax
     *
     * @ORM\Column(name="price", type="float")
     *
     * @var float
     */
    protected $price;
    
    /**
     * Stock.
     *
     * @ORM\Column(name="stock", type="integer")
     *
     * @var int
     */
    protected $stock;
    
    /**
     * Stock.
     *
     * @ORM\Column(name="tax", type="float")
     *
     * @var float
     */
    protected $tax;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get hashed ID.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->id;
    }
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setDisplayName(string $name): void
    {
        $this->displayName = $name;
    }
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }
    
    /**
     * Set product slug.
     *
     * @param string $slug
     *
     * @return void
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }
    
    /**
     * Get product slug.
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }
    
    /**
     * Set description.
     *
     * @param string $description
     *
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
    
    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
    
    /**
     * Get price with tax.
     *
     * @param float $price
     *
     * @return void
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }
    
    /**
     * Get price with tax.
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
    
    /**
     * Set tax value.
     *
     * @param  float $tax
     *
     * @return void
     */
    public function setTax(float $tax): void
    {
        $this->tax = $tax;
    }
    
    /**
     * Get tax value.
     *
     * @return float
     */
    public function getTax(): float
    {
        return new Vat($this->tax);
    }
    
    /**
     * Get price with tax.
     *
     * @param int $amount
     *
     * @return void
     */
    public function setStock(int $amount): void
    {
        $this->stock = $amount;
    }
    
    /**
     * Get stock.
     *
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }
    
    /**
     * Determinates if product variant is in stock.
     *
     * @return bool
     */
    public function inStock(): bool
    {
        return $this->stock > 0;
    }
}
