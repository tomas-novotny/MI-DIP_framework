<?php

return [
    'catalog' => 'products',
    'product' => 'product/{slug}',
];
