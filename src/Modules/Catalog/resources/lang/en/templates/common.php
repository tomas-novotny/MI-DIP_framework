<?php

return [
    'products'    => 'Products',
    'add_to_cart' => 'Add to cart',
    'sold_out'    => 'Sold out',
];
