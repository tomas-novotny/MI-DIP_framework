<?php

return [
    'products'    => 'Produkty',
    'add_to_cart' => 'Do košíku',
    'sold_out'    => 'Vyprodáno',
];
