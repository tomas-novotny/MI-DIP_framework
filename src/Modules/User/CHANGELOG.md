# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-07
### Added
- Add [`GetUsersAction`](src/Services/Actions/GetUsersAction.php)
- Add [`UpdateUserAction`](src/Services/Actions/UpdateUserAction.php)
- Add [`AddRolesToUserAction`](src/Services/Actions/AddRolesToUserAction.php)
- Add [`RemoveRolesFromUserAction`](src/Services/Actions/RemoveRolesFromUserAction.php)
