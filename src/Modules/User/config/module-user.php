<?php

use Modules\Authentication\Model\Factories\Contracts\AuthenticationFactoryInterface;
use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\User\Model\Entities\Concretes\Doctrine\User as DoctrineUser;
use Modules\User\Model\Entities\Concretes\Eloquent\User as EloquentUser;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Model\Factories\Concretes\DoctrineUserFactory;
use Modules\User\Model\Factories\Concretes\EloquentUserFactory;
use Modules\User\Model\Factories\Contracts\UserFactoryInterface;
use Modules\User\Model\Repositories\Concretes\DoctrineUserRepository;
use Modules\User\Model\Repositories\Concretes\EloquentUserRepository;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;

return [
    'bindings' => [
        'eloquent' => [
            UserRepositoryInterface::class           => EloquentUserRepository::class,
            AuthenticationRepositoryInterface::class => EloquentUserRepository::class,
            UserFactoryInterface::class              => EloquentUserFactory::class,
            AuthenticationFactoryInterface::class    => EloquentUserFactory::class,
            UserInterface::class                     => EloquentUser::class,
        ],
        'doctrine' => [
            UserRepositoryInterface::class           => DoctrineUserRepository::class,
            AuthenticationRepositoryInterface::class => DoctrineUserRepository::class,
            UserFactoryInterface::class              => DoctrineUserFactory::class,
            AuthenticationFactoryInterface::class    => DoctrineUserFactory::class,
            UserInterface::class                     => DoctrineUser::class,
        ],
    ],
    
    'views' => [
    
    ],
];