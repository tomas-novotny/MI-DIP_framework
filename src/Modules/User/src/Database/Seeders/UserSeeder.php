<?php

namespace Modules\User\Database\Seeders;

use Carbon\Carbon;
use Modules\Support\Parents\AbstractSeeder;
use Modules\User\Model\Factories\Contracts\UserFactoryInterface;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;

class UserSeeder extends AbstractSeeder
{
    /**
     * @var \Modules\User\Model\Factories\Contracts\UserFactoryInterface
     */
    protected $factory;
    
    /**
     * User repository.
     *
     * @var \Modules\User\Model\Repositories\Contracts\UserRepositoryInterface
     */
    protected $repository;
    
    /**
     * UserSeeder constructor.
     *
     * @param \Modules\User\Model\Factories\Contracts\UserFactoryInterface       $factory
     * @param \Modules\User\Model\Repositories\Contracts\UserRepositoryInterface $repository
     */
    public function __construct(UserFactoryInterface $factory, UserRepositoryInterface $repository)
    {
        $this->factory    = $factory;
        $this->repository = $repository;
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->flushTable('users');
        $this->flushTable('user_roles');
        $this->flushTable('user_has_roles');
        
        $user = $this->factory->newUser('novott20@fit.cvut.cz');
        $user->setPassword('hesloheslo');
        $user->setProvider('web', null);
        $user->setRegisteredAt(Carbon::now());
        $user->setConfirmedAt(Carbon::now());
        
        $this->repository->persist($user);
    }
}
