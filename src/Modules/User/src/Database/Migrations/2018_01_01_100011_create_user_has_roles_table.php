<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface as Role;
use Modules\Support\Parents\AbstractMigration;
use Modules\User\Model\Entities\Contracts\UserInterface as User;

class CreateUserHasRolesTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('user_has_roles', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->string('role_code', Role::PRIMARY_KEY_SIZE);
            
            $table->foreign('user_id')
                  ->references('id')
                  ->on(User::TABLE)
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            
            $table->foreign('role_code')
                  ->references(Role::PRIMARY_KEY)
                  ->on(Role::TABLE)
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
    
            $table->primary(['user_id', 'role_code']);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->table('user_has_roles', function (Blueprint $table) {
            $table->dropForeign('user_has_roles_user_id_foreign');
            $table->dropForeign('user_has_roles_role_code_foreign');
        });
        
        $this->builder()->dropIfExists('user_has_roles');
    }
}
