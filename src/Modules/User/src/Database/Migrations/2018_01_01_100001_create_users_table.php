<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;
use Modules\User\Model\Entities\Contracts\UserInterface as User;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Run the migration up.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create(User::TABLE, function (Blueprint $table) {
            // primary key
            $table->increments('id')->unsigned();
            // basic information
            $table->string('email')->nullable();
            $table->string('password', 60)->nullable();
            // provider (login via FB, twitter, etc.)
            $table->string('provider')->default('web');
            $table->string('provider_id')->nullable();
            // auth info
            $table->unique(['provider', 'provider_id'], 'provider_unique');
            // basic dates
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamp('registered_at')->nullable();
            $table->timestamps();
            $table->rememberToken();
            
            $table->unique('email');
        });
    }
    
    /**
     * Run the migration down.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->table(User::TABLE, function (Blueprint $table) {
            $table->dropUnique('provider_unique');
        });
        
        $this->builder()->dropIfExists(User::TABLE);
    }
}
