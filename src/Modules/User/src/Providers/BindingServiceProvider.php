<?php

namespace Modules\User\Providers;

use Modules\Support\Parents\Providers\AbstractBindingServiceProvider;
use Modules\User\Http\Controllers\API\Transformers\Concretes\UserTransformer;
use Modules\User\UI\API\Transformers\Contracts\UserTransformerInterface;

class BindingServiceProvider extends AbstractBindingServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $contracts = [
        UserTransformerInterface::class => UserTransformer::class,
    ];
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadBindingsFromConfig('module-user.bindings');
        
        parent::register();
    }
}
