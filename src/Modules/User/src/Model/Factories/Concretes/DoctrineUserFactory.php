<?php

namespace Modules\User\Model\Factories\Concretes;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\User\Model\Entities\Concretes\Doctrine\User;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Model\Factories\Contracts\UserFactoryInterface;

class DoctrineUserFactory implements UserFactoryInterface
{
    /**
     * Create new user instance.
     *
     * @param string $email
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    public function newUser(string $email): UserInterface
    {
        $user = new User();
        
        $user->setEmail($email);
        
        return $user;
    }
    
    /**
     * Create new user instance.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    public function newAuthenticatableUser(string $email): AuthenticatableInterface
    {
        return $this->newUser($email);
    }
}
