<?php

namespace Modules\User\Model\Factories\Contracts;

use Modules\Authentication\Model\Factories\Contracts\AuthenticationFactoryInterface;
use Modules\User\Model\Entities\Contracts\UserInterface;

interface UserFactoryInterface extends AuthenticationFactoryInterface
{
    /**
     * Create new user instance.
     *
     * @param string $email
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    public function newUser(string $email): UserInterface;
}
