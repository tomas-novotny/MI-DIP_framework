<?php

namespace Modules\User\Model\Repositories\Criterias\User;

use Modules\Support\Parents\AbstractCriteria;

class HasEmailCriteria extends AbstractCriteria
{
    /**
     * HasEmailCriteria constructor.
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $expr = $this->expr();
        
        $this->andWhere($expr->eq('email', $email));
    }
}
