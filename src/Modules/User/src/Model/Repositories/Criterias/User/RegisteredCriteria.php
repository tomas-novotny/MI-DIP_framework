<?php

namespace Modules\User\Model\Repositories\Criterias\User;

use Modules\Support\Parents\AbstractCriteria;

class RegisteredCriteria extends AbstractCriteria
{
    /**
     * RegisteredCriteria constructor.
     */
    public function __construct()
    {
        $expr = $this->expr();
        
        $this->andWhere($expr->neq('email', null));
        
        $this->andWhere($expr->neq('registeredAt', null));
    }
}
