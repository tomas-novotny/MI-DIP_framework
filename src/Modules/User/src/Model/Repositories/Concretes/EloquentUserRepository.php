<?php

namespace Modules\User\Model\Repositories\Concretes;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Doctrine\Criterias\HasIdCriteria;
use Modules\Eloquent\EntityRepository;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;
use Modules\User\Model\Entities\Concretes\Eloquent\User;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;
use Modules\User\Model\Repositories\Criterias\User\HasEmailCriteria;
use Modules\User\Model\Repositories\Criterias\User\RegisteredCriteria;

class EloquentUserRepository implements UserRepositoryInterface
{
    /**
     * @var \Modules\Eloquent\EntityRepository
     */
    private $entityRepository;
    
    /**
     * EloquentUserRepository constructor.
     */
    public function __construct()
    {
        $this->entityRepository = new EntityRepository(User::class);
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return void
     */
    public function persist(UserInterface $user): void
    {
        $this->entityRepository->persistAndFlush($user);
    }
    
    /**
     * Get all users.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface
    {
        return new Collection($this->entityRepository->all());
    }
    
    /**
     * Get user by ID.
     *
     * @param string $id
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getById(string $id): UserInterface
    {
        $criterias = [
            new HasIdCriteria($id),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
    
    /**
     * Get registered user by email.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getRegisteredByEmail(string $email): AuthenticatableInterface
    {
        $criterias = [
            new RegisteredCriteria(),
            new HasEmailCriteria($email),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
    
    /**
     * Get user by email.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByEmail(string $email): AuthenticatableInterface
    {
        $criterias = [
            new HasEmailCriteria($email),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
    
    /**
     * Persis authenticatable user to database.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     */
    public function persistAuthenticatableUser(AuthenticatableInterface $user): void
    {
        $this->entityRepository->persistAndFlush($user);
    }
}
