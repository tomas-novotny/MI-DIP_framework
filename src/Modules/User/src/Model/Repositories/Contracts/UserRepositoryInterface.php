<?php

namespace Modules\User\Model\Repositories\Contracts;

use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Support\Contracts\CollectionInterface;
use Modules\User\Model\Entities\Contracts\UserInterface;

interface UserRepositoryInterface extends AuthenticationRepositoryInterface
{
    /**
     * Persist model to database.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return void
     */
    public function persist(UserInterface $user): void;
    
    /**
     * Get all users.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface;
    
    /**
     * Get user by ID.
     *
     * @param string $id
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getById(string $id): UserInterface;
}
