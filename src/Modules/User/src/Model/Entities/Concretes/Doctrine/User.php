<?php

namespace Modules\User\Model\Entities\Concretes\Doctrine;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Laravel\Passport\HasApiTokens;
use LaravelDoctrine\ORM\Auth\Authenticatable;
use LaravelDoctrine\ORM\Notifications\Notifiable;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Tasks\User\GetUserByEmailTask;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Doctrine\Concerns\HasTimestampsTrait;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\User\Model\Entities\Contracts\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    use Authenticatable;
    use HasTimestampsTrait;
    use Notifiable;
    use HasApiTokens;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * ID.
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var string
     */
    protected $id;
    
    /**
     * Many User have Many Roles.
     *
     * @ORM\ManyToMany(targetEntity="\Modules\Authorization\Model\Entities\Concretes\Doctrine\Role", cascade={"persist"})
     * @ORM\JoinTable(name="user_has_roles",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_code", referencedColumnName="code", unique=true)}
     * )
     */
    protected $roles;
    
    /**
     * Email
     *
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $email;
    
    /**
     * Password.
     *
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $password;
    
    /**
     * Registered datetime.
     *
     * @ORM\Column(name="registered_at", type="datetime", nullable=true)
     *
     * @var \Carbon\Carbon
     */
    protected $registeredAt;
    
    /**
     * Confirmed datetime.
     *
     * @ORM\Column(name="confirmed_at", type="datetime", nullable=true)
     *
     * @var \Carbon\Carbon
     */
    protected $confirmedAt;
    
    /**
     * Last login datetime.
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     *
     * @var \Carbon\Carbon
     */
    protected $lastLogin;
    
    /**
     * Provider.
     *
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $provider;
    
    /**
     * Provider id.
     *
     * @ORM\Column(name="provider_id", type="string")
     *
     * @var string
     */
    protected $providerId;
    
    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }
    
    /**
     * Get ID
     *
     * @return string
     */
    public function getKey()
    {
        return $this->id;
    }
    
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    
    /**
     * Set email.
     *
     * @param string $email
     *
     * @return void
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    
    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->email;
    }
    
    /**
     * Get datetime of confirmation.
     *
     * @return \Carbon\Carbon
     */
    public function getConfirmedAt(): ?Carbon
    {
        return $this->confirmedAt;
    }
    
    /**
     * User is activated.
     *
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->confirmedAt !== null;
    }
    
    /**
     * @return \Carbon\Carbon
     */
    public function getRegisteredAt(): Carbon
    {
        return $this->registeredAt;
    }
    
    /**
     * Set registered at datetime.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setRegisteredAt(Carbon $date): void
    {
        $this->registeredAt = $date;
    }
    
    /**
     * Get last login date.
     *
     * @return \Carbon\Carbon $date
     */
    public function getLastLogin(): ?Carbon
    {
        return $this->lastLogin;
    }
    
    /**
     * Set registered at datetime.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setLastLogin(Carbon $date): void
    {
        $this->lastLogin = $date;
    }
    
    /**
     * @return bool
     */
    public function isRegistered(): bool
    {
        return $this->registeredAt !== null;
    }
    
    /**
     * Set email.
     *
     * @param string $password
     *
     * @return void
     */
    public function setPassword(string $password): void
    {
        $this->password = bcrypt($password);
    }
    
    /**
     * Set confirmed at datetime.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setConfirmedAt(?Carbon $date): void
    {
        $this->confirmedAt = $date;
    }
    
    /**
     * Set register provider.
     *
     * @param string      $type
     * @param string|null $id
     *
     * @return void
     */
    public function setProvider(string $type, ?string $id): void
    {
        $this->provider   = $type;
        $this->providerId = $id;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Password reset
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->getEmail();
    }
    
    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        return null;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Authorization
    |--------------------------------------------------------------------------
    */
    
    /**
     * Add role to user.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     */
    public function addRole(RoleInterface $role): void
    {
        $this->roles->add($role);
    }
    
    /**
     * Remove role.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return void
     */
    public function removeRole(RoleInterface $role): void
    {
        $this->roles->removeElement($role);
    }
    
    /**
     * Get user roles.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function getRoles(): CollectionInterface
    {
        return new Collection($this->roles->toArray());
    }
    
    /**
     * Determine if the user has the given role.
     *
     * @param string $code
     *
     * @return bool
     */
    public function hasRole(string $code): bool
    {
        return $this->getRoles()->contains('code', $code);
    }
    
    /**
     * Determine if the user has the given roles.
     *
     * @param array $roles
     * @param bool  $requireAll
     *
     * @return bool
     */
    public function hasRoles(array $roles, bool $requireAll = false): bool
    {
        foreach ($roles as $role) {
            $hasRole = $this->hasRole($role);
            
            if ($requireAll and $hasRole === false) {
                return false;
            }
            
            if ($requireAll === false and $hasRole) {
                return true;
            }
        }
        
        return $requireAll;
    }
    
    /**
     * Determine if the user may perform the given permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        foreach ($this->roles as $role) {
            if ($role->hasPermission($permission)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * @param string $ability
     * @param array  $arguments
     *
     * @return bool|void
     */
    public function can($ability, $arguments = [])
    {
        return $this->hasPermission($ability);
    }
    
    /**
     * Find user.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface|null
     */
    public function findForPassport(string $email): ?AuthenticatableInterface
    {
        $action = new GetUserByEmailTask($email);
        
        try {
            return $action->handle();
        }
        catch (ResourceNotFoundException $exception) {
            return null;
        }
    }
}
