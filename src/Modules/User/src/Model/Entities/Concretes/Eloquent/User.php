<?php

namespace Modules\User\Model\Entities\Concretes\Eloquent;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Tasks\User\GetUserByEmailTask;
use Modules\Authorization\Model\Entities\Concretes\Eloquent\Role;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Eloquent\Contracts\ModelInterface;
use Modules\Eloquent\Exceptions\NotEloquentModelException;
use Modules\Eloquent\Parents\AbstractModel;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\User\Model\Entities\Contracts\UserInterface;

class User extends AbstractModel implements UserInterface
{
    use Authenticatable;
    use Notifiable;
    use HasApiTokens;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = UserInterface::TABLE;
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'password',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        UserInterface::ATTR_REGISTERED_AT,
        UserInterface::ATTR_CONFIMERD_AT,
        UserInterface::ATTR_LAST_LOGIN,
    ];
    
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->getAttribute(UserInterface::ATTR_EMAIL);
    }
    
    /**
     * Set email.
     *
     * @param string $email
     *
     * @return void
     */
    public function setEmail(string $email): void
    {
        $this->setAttribute(UserInterface::ATTR_EMAIL, $email);
    }
    
    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(UserInterface::ATTR_EMAIL);
    }
    
    /**
     * Get datetime of confirmation.
     *
     * @return \Carbon\Carbon
     */
    public function getConfirmedAt(): ?Carbon
    {
        return $this->getAttribute(UserInterface::ATTR_CONFIMERD_AT);
    }
    
    /**
     * User is activated.
     *
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->getAttribute(UserInterface::ATTR_CONFIMERD_AT) !== null;
    }
    
    /**
     * @return \Carbon\Carbon
     */
    public function getRegisteredAt(): Carbon
    {
        return $this->getAttribute(UserInterface::ATTR_REGISTERED_AT);
    }
    
    /**
     * Set registered at datetime.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setRegisteredAt(Carbon $date): void
    {
        $this->setAttribute(UserInterface::ATTR_REGISTERED_AT, $date);
    }
    
    /**
     * Get last login date.
     *
     * @return \Carbon\Carbon $date
     */
    public function getLastLogin(): ?Carbon
    {
        return $this->getAttribute(UserInterface::ATTR_LAST_LOGIN);
    }
    
    /**
     * Set registered at datetime.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setLastLogin(Carbon $date): void
    {
        $this->setAttribute(UserInterface::ATTR_LAST_LOGIN, $date);
    }
    
    /**
     * @return bool
     */
    public function isRegistered(): bool
    {
        return $this->getAttribute(UserInterface::ATTR_REGISTERED_AT) !== null;
    }
    
    /**
     * Set email.
     *
     * @param string $password
     *
     * @return void
     */
    public function setPassword(string $password): void
    {
        $this->setAttribute(UserInterface::ATTR_PASSWORD, bcrypt($password));
    }
    
    /**
     * Set confirmed at datetime.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setConfirmedAt(?Carbon $date): void
    {
        $this->setAttribute(UserInterface::ATTR_CONFIMERD_AT, $date);
    }
    
    /**
     * Set register provider.
     *
     * @param string      $type
     * @param string|null $id
     *
     * @return void
     */
    public function setProvider(string $type, ?string $id): void
    {
        $this->setAttribute(UserInterface::ATTR_PROVIDER, $type);
        $this->setAttribute(UserInterface::ATTR_PROVIDER_ID, $id);
    }
    
    /*
    |--------------------------------------------------------------------------
    | Password reset
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->getEmail();
    }
    
    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        return null;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Authorization
    |--------------------------------------------------------------------------
    */
    
    /**
     * Roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected function roles(): BelongsToMany
    {
        return $this->belongsToMany(
            Role::class,
            UserInterface::HAS_ROLES_TABLE,
            UserInterface::FOREGIN_KEY,
            RoleInterface::FOREIGN_KEY
        );
    }
    
    /**
     * Get user roles.
     *
     * @return \Modules\Support\Contracts\CollectionInterface|\Modules\Authorization\Model\Entities\Contracts\RoleInterface[]
     */
    public function getRoles(): CollectionInterface
    {
        return new Collection($this->getRelationValue('roles'));
    }
    
    /**
     * Add role to user.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     */
    public function addRole(RoleInterface $role): void
    {
        if ($role instanceof ModelInterface) {
            // TODO: fix
            if ($this->exists() === false) {
                $this->save();
            }
            $this->roles()->attach($role->getCode());
            
            return;
        }
        
        throw new NotEloquentModelException(get_class($role));
    }
    
    /**
     * Remove role.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return void
     */
    public function removeRole(RoleInterface $role): void
    {
        if ($role instanceof ModelInterface) {
            $this->roles()->detach($role->getCode());
        
            return;
        }
    
        throw new NotEloquentModelException(get_class($role));
    }
    
    /**
     * Determine if the user has the given role.
     *
     * @param string $code
     *
     * @return bool
     */
    public function hasRole(string $code): bool
    {
        foreach ($this->getRoles() as $roleEntity) {
            if ($roleEntity->getCode() === $code) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Determine if the user has the given roles.
     *
     * @param array $roles
     * @param bool  $requireAll
     *
     * @return bool
     */
    public function hasRoles(array $roles, bool $requireAll = false): bool
    {
        foreach ($roles as $role) {
            $hasRole = $this->hasRole($role);
            
            if ($requireAll and $hasRole === false) {
                return false;
            }
            
            if ($requireAll === false and $hasRole) {
                return true;
            }
        }
        
        return $requireAll;
    }
    
    /**
     * Determine if the user may perform the given permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        foreach ($this->getRoles() as $role) {
            if ($role->hasPermission($permission)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Determine if the entity has a given ability.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     * @return bool
     */
    public function can($ability, $arguments = [])
    {
        return $this->hasPermission($ability);
    }
    
    /**
     * Find user.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface|null
     */
    public function findForPassport(string $email): ?AuthenticatableInterface
    {
        $action = new GetUserByEmailTask($email);
        
        try {
            return $action->handle();
        }
        catch (ResourceNotFoundException $exception) {
            return null;
        }
    }
}
