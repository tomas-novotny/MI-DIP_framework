<?php

namespace Modules\User\Model\Entities\Contracts;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Entities\Contracts\OAuthInterface;
use Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Support\Contracts\CollectionInterface;

interface UserInterface extends AuthenticatableInterface, AuthorizableInterface, OAuthInterface
{
    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */
    
    public const TABLE              = 'users';
    
    public const PRIMARY_KEY        = 'id';
    
    public const FOREGIN_KEY        = 'user_id';
    
    public const HAS_ROLES_TABLE    = 'user_has_roles';
    
    public const ATTR_EMAIL         = 'email';
    
    public const ATTR_PASSWORD      = 'password';
    
    public const ATTR_REGISTERED_AT = 'registered_at';
    
    public const ATTR_CONFIMERD_AT  = 'confirmed_at';
    
    public const ATTR_LAST_LOGIN    = 'last_login';
    
    public const ATTR_PROVIDER      = 'provider';
    
    public const ATTR_PROVIDER_ID   = 'provider_id';
    
    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get Hashed ID
     *
     * @return string
     */
    public function getKey();
    
    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail(): string;
    
    /**
     * Set email.
     *
     * @param string $email
     *
     * @return void
     */
    public function setEmail(string $email): void;
    
    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string;
    
    /**
     * Add role to user.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     */
    public function addRole(RoleInterface $role): void;
    
    /**
     * Remove role from user.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     */
    public function removeRole(RoleInterface $role): void;
    
    /**
     * Get user roles.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function getRoles(): CollectionInterface;
}
