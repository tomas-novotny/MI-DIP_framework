<?php

namespace Modules\User\Events;

use Modules\Support\Contracts\CollectionInterface;
use Modules\Support\Parents\AbstractEvent;
use Modules\User\Model\Entities\Contracts\UserInterface;

class UserDataEditing extends AbstractEvent
{
    /**
     * User.
     *
     * @var \Modules\User\Model\Entities\Contracts\UserInterface
     */
    public $user;
    
    /**
     * Registration request data.
     *
     * @var \Modules\Support\Contracts\CollectionInterface
     */
    public $data;
    
    /**
     * UserDataEditing constructor.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     * @param \Modules\Support\Contracts\CollectionInterface       $data
     */
    public function __construct(UserInterface $user, CollectionInterface $data)
    {
        $this->user = $user;
        $this->data = $data;
    }
}
