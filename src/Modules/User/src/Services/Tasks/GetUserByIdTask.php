<?php

namespace Modules\User\Services\Tasks;

use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;

/**
 * Class GetUserByIdTask
 *
 * @package Modules\User
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class GetUserByIdTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * Hashed primary key.
     *
     * @var string
     */
    private $key;
    
    /**
     * User repository.
     *
     * @var \Modules\User\Model\Repositories\Contracts\UserRepositoryInterface
     */
    private $userRepository;
    
    /**
     * GetUserByIdTask constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->userRepository = $this->resolve(UserRepositoryInterface::class);
        
        $this->key = $key;
    }
    
    /**
     * Get user by ID.
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        $user = $this->userRepository->getById($this->key);
        
        return $user;
    }
}
