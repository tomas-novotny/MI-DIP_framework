<?php

namespace Modules\User\Services\Tasks;

use Carbon\Carbon;
use InvalidArgumentException;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Collection;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\User\Events\UserDataEditing;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;

/**
 * Class EditUserTask
 *
 * @package Modules\User
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\CreateResourceFailedException
 *
 * @evet    \Modules\User\Events\UserDataEditing
 */
class EditUserTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        CreateResourceFailedException::class,
        UpdateResourceFailedException::class,
    ];
    
    /**
     * User instance.
     *
     * @var \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private $user;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * User repository.
     *
     * @var \Modules\User\Model\Repositories\Contracts\UserRepositoryInterface
     */
    private $userRepository;
    
    /**
     * EditUserTask constructor.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     * @param \Modules\Support\Collection                          $data
     */
    public function __construct(UserInterface $user, Collection $data)
    {
        $this->userRepository = $this->resolve(UserRepositoryInterface::class);
        
        $this->user = $user;
        $this->data = $data;
    }
    
    /**
     * Edit user.
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function handle()
    {
        $this->fillData();
        
        $this->persist();
        
        return $this->user;
    }
    
    /**
     * Fill data
     *
     * @return void
     */
    private function fillData(): void
    {
        $confirmedAt = $this->getDateFromData('confirmedAt');
        if ($confirmedAt) {
            $this->user->setConfirmedAt($confirmedAt);
        }
        
        $password = $this->data->get('password');
        if (empty($password) === false) {
            $this->user->setPassword($password);
        }
        
        event(new UserDataEditing($this->user, $this->data));
    }
    
    /**
     * Get date parsed from data.
     *
     * @param string $key
     *
     * @return \Carbon\Carbon|null
     */
    private function getDateFromData(string $key): ?Carbon
    {
        try {
            return Carbon::createFromFormat(Carbon::ATOM, $this->data->get($key, ''));
        }
        catch (InvalidArgumentException $exception) {
            return null;
        }
    }
    
    /**
     * Persist user to database.
     *
     * @return void
     */
    private function persist(): void
    {
        $this->userRepository->persist($this->user);
    }
}
