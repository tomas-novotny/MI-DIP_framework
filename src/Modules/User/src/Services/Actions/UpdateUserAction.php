<?php

namespace Modules\User\Services\Actions;

use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\User\Services\Tasks\EditUserTask;
use Modules\User\Services\Tasks\GetUserByIdTask;

/**
 * Class UpdateUserAction
 *
 * @package Modules\User
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\User\Services\Tasks\EditUserTask
 * @uses    \Modules\User\Services\Tasks\GetUserByIdTask
 *
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 */
class UpdateUserAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        UpdateResourceFailedException::class,
        InvalidArgumentException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $key;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * UpdateUserAction constructor.
     *
     * @param string                      $id
     * @param \Modules\Support\Collection $data
     */
    public function __construct(string $id, Collection $data)
    {
        $this->key  = $id;
        $this->data = $data;
    }
    
    /**
     * Update user.
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        $user = $this->dispatchTask(new GetUserByIdTask($this->key));
        
        $this->validateData();
        
        $this->dispatchTask(new EditUserTask($user, $this->data));
        
        return $user;
    }
    
    /**
     * Validate input data.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    private function validateData(): void
    {
        // TODO: add validation
    }
}
