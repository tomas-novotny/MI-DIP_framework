<?php

namespace Modules\User\Services\Actions;

use Modules\Bus\Parents\AbstractAction;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;

/**
 * Class GetUsersAction
 *
 * @package Modules\User
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class GetUsersAction extends AbstractAction
{
    /**
     * User repository.
     *
     * @var \Modules\User\Model\Repositories\Contracts\UserRepositoryInterface
     */
    private $userRepository;
    
    /**
     * GetUsersAction constructor.
     */
    public function __construct()
    {
        $this->userRepository = $this->resolve(UserRepositoryInterface::class);
    }
    
    /**
     * Get all user collection.
     *
     * @return \Modules\Support\Collection
     */
    public function handle()
    {
        $users = $this->userRepository->all();
        
        return $users;
    }
}
