<?php

namespace Modules\User\Services\Actions;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;
use Modules\User\Services\Tasks\GetUserByIdTask;

/**
 * Class AddRolesToUserAction
 *
 * @package Modules\User
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask
 * @uses    \Modules\User\Services\Tasks\GetUserByIdTask
 *
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class AddRolesToUserAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        UpdateResourceFailedException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $userCode;
    
    /**
     * Role codes.
     *
     * @var array
     */
    private $rolesCodes;
    
    /**
     * User repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\UserRepositoryInterface
     */
    private $userRepository;
    
    /**
     * AddRolesToUserAction constructor.
     *
     * @param string $userCode
     * @param array  $rolesCodes
     */
    public function __construct(string $userCode, array $rolesCodes)
    {
        $this->userRepository = $this->resolve(UserRepositoryInterface::class);
        
        $this->userCode   = $userCode;
        $this->rolesCodes = $rolesCodes;
    }
    
    /**
     * Add roles to user
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function handle()
    {
        $user = $this->getUser();
        
        $this->addRoles($user);
        
        $this->userRepository->persist($user);
        
        return $user;
    }
    
    /**
     * Add roles to user.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    private function addRoles(UserInterface $user): void
    {
        try {
            foreach ($this->rolesCodes as $code) {
                $user->addRole($this->getRole($code));
            }
        }
        catch (ResourceNotFoundException $exception) {
            throw new UpdateResourceFailedException(get_class($user), [$user->getKey()], $exception);
        }
    }
    
    /**
     * Get role.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private function getRole(string $code): RoleInterface
    {
        return $this->dispatchTask(new GetRoleByCodeTask($code));
    }
    
    /**
     * Get user.
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getUser(): UserInterface
    {
        return $this->dispatchTask(new GetUserByIdTask($this->userCode));
    }
}
