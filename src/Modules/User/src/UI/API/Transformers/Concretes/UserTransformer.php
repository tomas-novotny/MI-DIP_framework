<?php

namespace Modules\User\Http\Controllers\API\Transformers\Concretes;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\RoleTransformer;
use Modules\Support\Parents\AbstractTransformer;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\UI\API\Transformers\Contracts\UserTransformerInterface;

class UserTransformer extends AbstractTransformer implements UserTransformerInterface
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    protected $availableIncludes = [
        'roles',
    ];
    
    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     *  Turn model object into a generic array.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return array
     */
    public function transform($user)
    {
        $data = [
            'id'           => $user->getKey(),
            'email'        => $user->getEmail(),
            'registeredAt' => $user->isRegistered() ? $user->getRegisteredAt()->toIso8601String() : null,
            'confirmedAt'  => $user->isConfirmed() ? $user->getConfirmedAt()->toIso8601String() : null,
        ];
        
        return $data;
    }
    
    /**
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRoles(UserInterface $user)
    {
        return $this->collection($user->getRoles(), new RoleTransformer());
    }
}
