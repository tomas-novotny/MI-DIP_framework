<?php

namespace Modules\User\UI\API\Controllers\User;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\RoleTransformer;
use Modules\Support\Parents\Controllers\AbstractApiController;
use Modules\Support\Request;
use Modules\User\Services\Actions\AddRolesToUserAction;
use Modules\User\Services\Actions\RemoveRolesFromUserAction;
use Modules\User\Services\Tasks\GetUserByIdTask;

class RoleController extends AbstractApiController
{
    /**
     * @param string $userCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(string $userCode)
    {
        $user = $this->dispatchService(new GetUserByIdTask($userCode));
        
        return $this->transformRole($user->getRoles());
    }
    
    /**
     * TODO: use request validation
     *
     * @param string                   $userCode
     * @param \Modules\Support\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(string $userCode, Request $request)
    {
        $roleCodes = explode(',', $request->get('codes'));
        
        $this->dispatchService(new AddRolesToUserAction($userCode, $roleCodes));
        
        return $this->accepted();
    }
    
    /**
     * @param string $userCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $userCode, string $roleCode)
    {
        $this->dispatchService(new RemoveRolesFromUserAction($userCode, [$roleCode]));
        
        return $this->deleted();
    }
    
    /**
     * Transform role.
     *
     * @param mixed $role
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function transformRole($role)
    {
        return $this->transform($role, RoleTransformer::class);
    }
}
