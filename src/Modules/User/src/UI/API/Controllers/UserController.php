<?php

namespace Modules\User\UI\API\Controllers;

use Modules\Support\Parents\Controllers\AbstractApiController;
use Modules\Support\Request;
use Modules\User\Services\Actions\GetUsersAction;
use Modules\User\Services\Actions\UpdateUserAction;
use Modules\User\Services\Tasks\GetUserByIdTask;
use Modules\User\UI\API\Transformers\Contracts\UserTransformerInterface;

class UserController extends AbstractApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = $this->dispatchService(new GetUsersAction());
        
        return $this->transformUser($users);
    }
    
    /**
     * @param string $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $id)
    {
        $user = $this->dispatchService(new GetUserByIdTask($id));
        
        return $this->transformUser($user);
    }
    
    /**
     * TODO: use request validation
     *
     * @param string                   $id
     * @param \Modules\Support\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(string $id, Request $request)
    {
        $permission = $this->dispatchService(new UpdateUserAction($id, $request->data()));
        
        return $this->transformUser($permission);
    }
    
    /**
     * Transform user.
     *
     * @param mixed $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function transformUser($user)
    {
        return $this->transform($user, UserTransformerInterface::class);
    }
}
