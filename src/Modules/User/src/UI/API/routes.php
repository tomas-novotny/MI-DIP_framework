<?php

use Illuminate\Routing\Router;
use Modules\User\UI\API\Controllers\User\RoleController;
use Modules\User\UI\API\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

api_router('v1', function (Router $router) {
    $router->group(
        ['middleware' => [
//             'auth:api',
//             'permission:access.users',
        ],
        ], function (Router $router) {
        
        $router->apiResource('users', UserController::class);
        $router->apiResource('users/{user}/roles', RoleController::class);
    });
});
