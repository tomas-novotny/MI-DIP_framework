<?php

use Modules\Authorization\Model\Entities\Concretes\Doctrine\Permission as DoctrinePermission;
use Modules\Authorization\Model\Entities\Concretes\Doctrine\Role as DoctrineRole;
use Modules\Authorization\Model\Entities\Concretes\Eloquent\Permission as EloquentPermission;
use Modules\Authorization\Model\Entities\Concretes\Eloquent\Role as EloquentRole;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Factories\Concretes\DoctrinePermissionFactory;
use Modules\Authorization\Model\Factories\Concretes\DoctrineRoleFactory;
use Modules\Authorization\Model\Factories\Concretes\EloquentPermissionFactory;
use Modules\Authorization\Model\Factories\Concretes\EloquentRoleFactory;
use Modules\Authorization\Model\Factories\Contracts\PermissionFactoryInterface;
use Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface;
use Modules\Authorization\Model\Repositories\Concretes\DoctrinePermissionRepository;
use Modules\Authorization\Model\Repositories\Concretes\DoctrineRoleRepository;
use Modules\Authorization\Model\Repositories\Concretes\EloquentPermissionRepository;
use Modules\Authorization\Model\Repositories\Concretes\EloquentRoleRepository;
use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;

return [
    'bindings' => [
        'eloquent' => [
            RoleRepositoryInterface::class       => EloquentRoleRepository::class,
            PermissionRepositoryInterface::class => EloquentPermissionRepository::class,
            RoleFactoryInterface::class          => EloquentRoleFactory::class,
            PermissionFactoryInterface::class    => EloquentPermissionFactory::class,
            RoleInterface::class                 => EloquentRole::class,
            PermissionInterface::class           => EloquentPermission::class,
        ],
        'doctrine' => [
            RoleRepositoryInterface::class       => DoctrineRoleRepository::class,
            PermissionRepositoryInterface::class => DoctrinePermissionRepository::class,
            RoleFactoryInterface::class          => DoctrineRoleFactory::class,
            PermissionFactoryInterface::class    => DoctrinePermissionFactory::class,
            RoleInterface::class                 => DoctrineRole::class,
            PermissionInterface::class           => DoctrinePermission::class,
        ],
    ],
    
    'views' => [
    
    ],
];