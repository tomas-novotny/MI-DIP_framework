# Authorization module

[![Software License][ico-license]](LICENSE.md)

This module is part of example [modular project][link-framework]. 

## Actions

### Permission
+ [`CreatePermissionAction`](src/Services/Actions/Permission/CreatePermissionAction.php)
+ [`GetPermissionsAction`](src/Services/Actions/Permission/GetPermissionsAction.php)
+ [`UpdatePermissionAction`](src/Services/Actions/Permission/UpdatePermissionAction.php)
+ [`DeletePermissionAction`](src/Services/Actions/Permission/DeletePermissionAction.php)

### Role
+ [`CreateRoleAction`](src/Services/Actions/Role/CreateRoleAction.php)
+ [`GetRolesAction`](src/Services/Actions/Role/GetRolesAction.php)
+ [`UpdateRoleAction`](src/Services/Actions/Role/UpdateRoleAction.php)
+ [`DeleteRoleAction`](src/Services/Actions/Role/DeleteRoleAction.php)
+ [`AddPermissionsToRoleAction`](src/Services/Actions/Role/AddPermissionsToRoleAction.php)
+ [`RemovePermissionsFromRoleAction`](src/Services/Actions/Role/RemovePermissionsFromRoleAction.php)

## Contributing

Please see [CONTRIBUTING][link-contributing] and [CODE_OF_CONDUCT][link-code-of-conduct] for details.

## Security

If you discover any security related issues, please email novott20@fit.cvut.cz instead of using the issue tracker.

## Credits

- [Tomáš Novotný][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-blue.svg

[link-framework]: https://github.com/novott20/MI-DIP_framework
[link-author]: https://github.com/novott20
[link-contributors]: ../../../../../contributors
[link-contributing]: ../../../CONTRIBUTING.md
[link-code-of-conduct]: ../../../CODE_OF_CONDUCT.md