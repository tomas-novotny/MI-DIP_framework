# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-07
### Added
- Add [`CreatePermissionAction`](src/Services/Actions/Permission/CreatePermissionAction.php)
- Add [`GetPermissionsAction`](src/Services/Actions/Permission/GetPermissionsAction.php)
- Add [`UpdatePermissionAction`](src/Services/Actions/Permission/UpdatePermissionAction.php)
- Add [`DeletePermissionAction`](src/Services/Actions/Permission/DeletePermissionAction.php)
- Add [`CreateRoleAction`](src/Services/Actions/Role/CreateRoleAction.php)
- Add [`GetRolesAction`](src/Services/Actions/Role/GetRolesAction.php)
- Add [`UpdateRoleAction`](src/Services/Actions/Role/UpdateRoleAction.php)
- Add [`DeleteRoleAction`](src/Services/Actions/Role/DeleteRoleAction.php)
- Add [`AddPermissionsToRoleAction`](src/Services/Actions/Role/AddPermissionsToRoleAction.php)
- Add [`RemovePermissionsFromRoleAction`](src/Services/Actions/Role/RemovePermissionsFromRoleAction.php)
