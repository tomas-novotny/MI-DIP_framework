<?php

namespace Modules\Authorization\Providers;

use Illuminate\Auth\Middleware\Authorize;
use Modules\Authorization\Middlewares\HasPermission;
use Modules\Authorization\Middlewares\HasRole;
use Modules\Support\Parents\Providers\AbstractMiddlewareServiceProvider;

class MiddlewareServiceProvider extends AbstractMiddlewareServiceProvider
{
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        //
    ];
    
    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'role'       => HasRole::class,
        'permission' => HasPermission::class,
        'can'        => Authorize::class,
    ];
}
