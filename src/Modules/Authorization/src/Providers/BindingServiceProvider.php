<?php

namespace Modules\Authorization\Providers;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\PermissionTransformer;
use Modules\Authorization\UI\API\Transformers\Contracts\PermissionTransformerInterface;
use Modules\Authorization\UI\API\Transformers\Contracts\RoleTransformerInterface;
use Modules\Support\Parents\Providers\AbstractBindingServiceProvider;

class BindingServiceProvider extends AbstractBindingServiceProvider
{
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $contracts = [
        PermissionTransformerInterface::class => PermissionTransformer::class,
        RoleTransformerInterface::class       => RoleTransformerInterface::class,
    ];
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadBindingsFromConfig('module-authorization.bindings');
        
        parent::register();
    }
}
