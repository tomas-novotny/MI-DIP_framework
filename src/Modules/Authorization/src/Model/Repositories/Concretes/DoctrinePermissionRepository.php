<?php

namespace Modules\Authorization\Model\Repositories\Concretes;

use Doctrine\ORM\EntityManagerInterface;
use Modules\Authorization\Model\Entities\Concretes\Doctrine\Permission;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Doctrine\Criterias\HasCodeCriteria;
use Modules\Doctrine\EntityRepository;
use Modules\Support\Contracts\CollectionInterface;

class DoctrinePermissionRepository implements PermissionRepositoryInterface
{
    /**
     * Entity repository.
     *
     * @var \Modules\Doctrine\EntityRepository
     */
    private $entityRepository;
    
    /**
     * DoctrineProductRepository constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityRepository = new EntityRepository($entityManager, Permission::class);
    }
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(PermissionInterface $permission): void
    {
        $this->entityRepository->removeAndFlush($permission);
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function persist(PermissionInterface $permission): void
    {
        $this->entityRepository->persistAndFlush($permission);
    }
    
    /**
     * Get all permissions.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface
    {
        return $this->entityRepository->all();
    }
    
    /**
     * Get permission by code.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByCode(string $code): PermissionInterface
    {
        $criterias = [
            new HasCodeCriteria($code),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
}
