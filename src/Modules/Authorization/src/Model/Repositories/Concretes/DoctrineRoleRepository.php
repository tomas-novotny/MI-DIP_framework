<?php

namespace Modules\Authorization\Model\Repositories\Concretes;

use Doctrine\ORM\EntityManagerInterface;
use Modules\Authorization\Model\Entities\Concretes\Doctrine\Role;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Doctrine\Criterias\HasCodeCriteria;
use Modules\Doctrine\EntityRepository;
use Modules\Support\Contracts\CollectionInterface;

class DoctrineRoleRepository implements RoleRepositoryInterface
{
    /**
     * Entity repository.
     *
     * @var \Modules\Doctrine\EntityRepository
     */
    private $entityRepository;
    
    /**
     * DoctrineProductRepository constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityRepository = new EntityRepository($entityManager, Role::class);
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return void
     */
    public function persist(RoleInterface $role): void
    {
        $this->entityRepository->persistAndFlush($role);
    }
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $role
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(RoleInterface $role): void
    {
        $this->entityRepository->removeAndFlush($role);
    }
    
    /**
     * Get all user roles.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface
    {
        return $this->entityRepository->all();
    }
    
    /**
     * Get role by code.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByCode(string $code): RoleInterface
    {
        $criterias = [
            new HasCodeCriteria($code),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
}
