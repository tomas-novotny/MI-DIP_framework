<?php

namespace Modules\Authorization\Model\Repositories\Concretes;

use Modules\Authorization\Model\Entities\Concretes\Eloquent\Role;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Doctrine\Criterias\HasCodeCriteria;
use Modules\Eloquent\EntityRepository;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;

class EloquentRoleRepository implements RoleRepositoryInterface
{
    /**
     * @var \Modules\Eloquent\EntityRepository
     */
    private $entityRepository;
    
    /**
     * EloquentRoleRepository constructor.
     */
    public function __construct()
    {
        $this->entityRepository = new EntityRepository(Role::class);
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return void
     */
    public function persist(RoleInterface $role): void
    {
        $this->entityRepository->persistAndFlush($role);
    }
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(RoleInterface $role): void
    {
        $this->entityRepository->removeAndFlush($role);
    }
    
    /**
     * Get all user roles.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface
    {
        return new Collection($this->entityRepository->all());
    }
    
    /**
     * Get role by code.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByCode(string $code): RoleInterface
    {
        $criterias = [
            new HasCodeCriteria($code),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
}
