<?php

namespace Modules\Authorization\Model\Repositories\Concretes;

use Modules\Authorization\Model\Entities\Concretes\Eloquent\Permission;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Doctrine\Criterias\HasCodeCriteria;
use Modules\Eloquent\EntityRepository;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;

class EloquentPermissionRepository implements PermissionRepositoryInterface
{
    /**
     * @var \Modules\Eloquent\EntityRepository
     */
    private $entityRepository;
    
    /**
     * EloquentPermissionRepository constructor.
     */
    public function __construct()
    {
        $this->entityRepository = new EntityRepository(Permission::class);
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function persist(PermissionInterface $permission): void
    {
        $this->entityRepository->persistAndFlush($permission);
    }
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(PermissionInterface $permission): void
    {
        $this->entityRepository->removeAndFlush($permission);
    }
    
    /**
     * Get all user Permissions.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface
    {
        return new Collection($this->entityRepository->all());
    }
    
    /**
     * Get Permission by code.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByCode(string $code): PermissionInterface
    {
        $criterias = [
            new HasCodeCriteria($code),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
}
