<?php

namespace Modules\Authorization\Model\Repositories\Contracts;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Support\Contracts\CollectionInterface;

interface PermissionRepositoryInterface
{
    /**
     * Persist model to database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function persist(PermissionInterface $permission): void;
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(PermissionInterface $permission): void;
    
    /**
     * Get all permissions.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface;
    
    /**
     * Get permission by code.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByCode(string $code): PermissionInterface;
}
