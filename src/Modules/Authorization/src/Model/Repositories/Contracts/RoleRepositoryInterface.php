<?php

namespace Modules\Authorization\Model\Repositories\Contracts;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Support\Contracts\CollectionInterface;

interface RoleRepositoryInterface
{
    /**
     * Persist model to database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return void
     */
    public function persist(RoleInterface $role): void;
    
    /**
     * Delete model from database.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $role
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function remove(RoleInterface $role): void;
    
    /**
     * Get all user roles.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface;
    
    /**
     * Get role by code.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByCode(string $code): RoleInterface;
}
