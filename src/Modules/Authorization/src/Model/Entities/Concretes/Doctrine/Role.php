<?php

namespace Modules\Authorization\Model\Entities\Concretes\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Doctrine\Concerns\HasTimestampsTrait;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_roles")
 */
class Role implements RoleInterface
{
    use HasTimestampsTrait;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $code;
    
    /**
     * @ORM\Column(name="display_name", type="string")
     */
    private $displayName;
    
    /**
     * Many User have Many Roles.
     *
     * @ORM\ManyToMany(targetEntity="\Modules\Authorization\Model\Entities\Concretes\Doctrine\Permission", cascade={"persist"})
     * @ORM\JoinTable(name="user_role_has_permissions",
     *     joinColumns={@ORM\JoinColumn(name="role_code", referencedColumnName="code")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="permission_code", referencedColumnName="code", unique=true)}
     * )
     */
    private $permissions;
    
    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }
    
    /**
     * Set code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }
    
    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    
    public function setDisplayName(string $name): void
    {
        $this->displayName = $name;
    }
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }
    
    /**
     * Add permission.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function addPermission(PermissionInterface $permission): void
    {
        if ($this->permissions->contains($permission) === false) {
            $this->permissions->add($permission);
        }
    }
    
    /**
     * Remove permission.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function removePermission(PermissionInterface $permission): void
    {
        $this->permissions->removeElement($permission);
    }
    
    /**
     * Determine if the role may perform the given permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        foreach ($this->permissions as $permissionEntity) {
            if ($permissionEntity->getCode() === $permission) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Get roles permissions.
     *
     * @return \Modules\Support\Contracts\CollectionInterface|\Modules\Authorization\Model\Entities\Contracts\PermissionInterface[]
     */
    public function getPermissions(): CollectionInterface
    {
        return new Collection($this->permissions->toArray());
    }
}
