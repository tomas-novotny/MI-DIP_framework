<?php

namespace Modules\Authorization\Model\Entities\Concretes\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Doctrine\Concerns\HasTimestampsTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_role_permissions")
 */
class Permission implements PermissionInterface
{
    use HasTimestampsTrait;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $code;
    
    /**
     * @ORM\Column(name="display_name", type="string")
     */
    private $displayName;
    
    /**
     * Set code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }
    
    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setDisplayName(string $name): void
    {
        $this->displayName = $name;
    }
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }
}
