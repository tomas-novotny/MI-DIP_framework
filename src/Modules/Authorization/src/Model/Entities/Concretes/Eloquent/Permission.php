<?php

namespace Modules\Authorization\Model\Entities\Concretes\Eloquent;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Eloquent\Parents\AbstractModel;

class Permission extends AbstractModel implements PermissionInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = PermissionInterface::TABLE;
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = PermissionInterface::PRIMARY_KEY;
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * Set code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void
    {
        $this->setAttribute(PermissionInterface::ATTR_CODE, $code);
    }
    
    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->getKey();
    }
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setDisplayName(string $name): void
    {
        $this->setAttribute(PermissionInterface::ATTR_DISPLAY_NAME, $name);
    }
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->getAttribute(PermissionInterface::ATTR_DISPLAY_NAME);
    }
}
