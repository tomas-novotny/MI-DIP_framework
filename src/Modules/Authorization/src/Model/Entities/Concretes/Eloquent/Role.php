<?php

namespace Modules\Authorization\Model\Entities\Concretes\Eloquent;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Eloquent\Parents\AbstractModel;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;

class Role extends AbstractModel implements RoleInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = RoleInterface::TABLE;
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = RoleInterface::PRIMARY_KEY;
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * Set code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void
    {
        $this->setAttribute(RoleInterface::ATTR_CODE, $code);
    }
    
    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->getAttribute(RoleInterface::ATTR_CODE);
    }
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    
    public function setDisplayName(string $name): void
    {
        $this->setAttribute(RoleInterface::ATTR_DISPLAY_NAME, $name);
    }
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->getAttribute(RoleInterface::ATTR_DISPLAY_NAME);
    }
    
    /**
     * Permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected function permissions(): BelongsToMany
    {
        return $this->belongsToMany(
            Permission::class,
            RoleInterface::HAS_PERMISSIONS_TABLE,
            Role::FOREIGN_KEY,
            PermissionInterface::FOREIGN_KEY
        );
    }
    
    /**
     * Get roles permissions.
     *
     * @return \Modules\Support\Contracts\CollectionInterface|\Modules\Authorization\Model\Entities\Contracts\PermissionInterface[]
     */
    public function getPermissions(): CollectionInterface
    {
        return new Collection($this->getRelationValue('permissions')->toArray());
    }
    
    /**
     * Add permission.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function addPermission(PermissionInterface $permission): void
    {
        // TODO: fix
        if ($this->exists() === false) {
            $this->save();
        }
        
        $this->permissions()->attach($permission->getCode());
    }
    
    /**
     * Remove permission.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function removePermission(PermissionInterface $permission): void
    {
        $this->permissions()->detach($permission->getCode());
    }
    
    /**
     * Determine if the role may perfRoleInterface the given permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        foreach ($this->getPermissions() as $permissionEntity) {
            if ($permissionEntity->getCode() === $permission) {
                return true;
            }
        }
        
        return false;
    }
}
