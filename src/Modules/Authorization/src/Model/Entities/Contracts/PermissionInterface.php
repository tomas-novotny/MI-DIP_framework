<?php

namespace Modules\Authorization\Model\Entities\Contracts;

interface PermissionInterface
{
    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */
    
    public const TABLE             = 'user_role_permissions';
    
    public const PRIMARY_KEY       = 'code';
    
    public const FOREIGN_KEY       = 'permission_code';
    
    public const PRIMARY_KEY_SIZE  = 32;
    
    public const ATTR_CODE         = self::PRIMARY_KEY;
    
    public const ATTR_DISPLAY_NAME = 'display_name';
    
    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */
    
    /**
     * Set code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void;
    
    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): string;
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setDisplayName(string $name): void;
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string;
}
