<?php

namespace Modules\Authorization\Model\Entities\Contracts;

use Modules\Support\Contracts\CollectionInterface;

interface RoleInterface extends HasPermissionInterface
{
    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */
    
    public const    TABLE                 = 'user_roles';
    
    public const    HAS_PERMISSIONS_TABLE = 'user_role_has_permissions';
    
    public const    PRIMARY_KEY           = 'code';
    
    public const    FOREIGN_KEY           = 'role_code';
    
    public const    PRIMARY_KEY_SIZE      = 32;
    
    public const    ATTR_CODE             = self::PRIMARY_KEY;
    
    public const    ATTR_DISPLAY_NAME     = 'display_name';
    
    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */
    
    /**
     * Set code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void;
    
    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): string;
    
    /**
     * Set display name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setDisplayName(string $name): void;
    
    /**
     * Get display name.
     *
     * @return string
     */
    public function getDisplayName(): string;
    
    /**
     * Add permission.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function addPermission(PermissionInterface $permission): void;
    
    /**
     * Remove permission.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return void
     */
    public function removePermission(PermissionInterface $permission): void;
    
    /**
     * Get role permissions.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function getPermissions(): CollectionInterface;
}
