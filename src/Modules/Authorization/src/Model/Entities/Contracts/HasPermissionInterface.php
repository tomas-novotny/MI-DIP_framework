<?php

namespace Modules\Authorization\Model\Entities\Contracts;

interface HasPermissionInterface
{
    /**
     * Determine if the user may perform the given permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function hasPermission(string $permission): bool;
}
