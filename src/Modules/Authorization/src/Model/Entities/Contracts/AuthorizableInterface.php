<?php

namespace Modules\Authorization\Model\Entities\Contracts;

use Illuminate\Contracts\Auth\Access\Authorizable as LaravelAuthorizable;

interface AuthorizableInterface extends HasRoleInterface, HasPermissionInterface, LaravelAuthorizable
{
    //
}
