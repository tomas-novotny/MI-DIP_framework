<?php

namespace Modules\Authorization\Model\Entities\Contracts;

interface HasRoleInterface
{
    /**
     * Determine if the user has the given role.
     *
     * @param string $code
     *
     * @return bool
     */
    public function hasRole(string $code): bool;

    /**
     * Determine if the user has the given roles.
     *
     * @param array $roles
     * @param bool  $requireAll
     *
     * @return bool
     */
    public function hasRoles(array $roles, bool $requireAll = false): bool;
}
