<?php

namespace Modules\Authorization\Model\Factories\Concretes;

use Modules\Authorization\Model\Entities\Concretes\Doctrine\Permission;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Factories\Contracts\PermissionFactoryInterface;
use Modules\Doctrine\EntityFactory;

class DoctrinePermissionFactory implements PermissionFactoryInterface
{
    /**
     * Create new permission instance.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    public function newPermission(string $code): PermissionInterface
    {
        $pemission = new Permission();
        
        $pemission->setCode($code);
        $pemission->setDisplayName(ucfirst($code));
        
        return $pemission;
    }
}
