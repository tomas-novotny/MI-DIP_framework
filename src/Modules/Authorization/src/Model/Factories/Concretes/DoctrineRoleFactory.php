<?php

namespace Modules\Authorization\Model\Factories\Concretes;

use Modules\Authorization\Model\Entities\Concretes\Doctrine\Role;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface;
use Modules\Doctrine\EntityFactory;

class DoctrineRoleFactory implements RoleFactoryInterface
{
    /**
     * Create new role instance.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    public function newRole(string $code): RoleInterface
    {
        $role = new Role();
        
        $role->setCode($code);
        $role->setDisplayName(ucfirst($code));
        
        return $role;
    }
}
