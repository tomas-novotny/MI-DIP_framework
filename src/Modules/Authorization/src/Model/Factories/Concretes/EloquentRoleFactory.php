<?php

namespace Modules\Authorization\Model\Factories\Concretes;

use Modules\Authorization\Model\Entities\Concretes\Eloquent\Role;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface;
use Modules\Eloquent\EntityFactory;

class EloquentRoleFactory implements RoleFactoryInterface
{
    /**
     * Create new role instance.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    public function newRole(string $code): RoleInterface
    {
        $role = new Role();
        
        $role->setCode($code);
        $role->setDisplayName(ucfirst($code));
        
        return $role;
    }
}
