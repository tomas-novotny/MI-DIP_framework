<?php

namespace Modules\Authorization\Model\Factories\Contracts;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;

interface RoleFactoryInterface
{
    /**
     * Create new role instance.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    public function newRole(string $code): RoleInterface;
}
