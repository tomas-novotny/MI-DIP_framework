<?php

namespace Modules\Authorization\Model\Factories\Contracts;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Support\Contracts\CollectionInterface;

interface PermissionFactoryInterface
{
    /**
     * Create new permission instance.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    public function newPermission(string $code): PermissionInterface;
}
