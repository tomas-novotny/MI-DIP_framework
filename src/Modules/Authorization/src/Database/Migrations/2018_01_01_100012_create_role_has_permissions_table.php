<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface as Permission;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface as Role;
use Modules\Support\Parents\AbstractMigration;

class CreateRoleHasPermissionsTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('user_role_has_permissions', function (Blueprint $table) {
            $table->string('role_code', Role::PRIMARY_KEY_SIZE);
            $table->string('permission_code', Permission::PRIMARY_KEY_SIZE);
            
            $table->foreign('role_code')
                  ->references(Role::PRIMARY_KEY)
                  ->on(Role::TABLE)
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
    
            $table->foreign('permission_code')
                  ->references(Permission::PRIMARY_KEY)
                  ->on(Permission::TABLE)
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
    
            $table->primary(['role_code', 'permission_code']);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->table('user_role_has_permissions', function (Blueprint $table) {
            $table->dropForeign('user_role_has_permissions_role_code_foreign');
            $table->dropForeign('user_role_has_permissions_permission_code_foreign');
        });
        
        $this->builder()->dropIfExists('user_role_has_permissions');
    }
}
