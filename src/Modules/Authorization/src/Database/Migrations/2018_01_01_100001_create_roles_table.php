<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface as Role;
use Modules\Support\Parents\AbstractMigration;

class CreateRolesTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create(Role::TABLE, function (Blueprint $table) {
            // primary key
            $table->string(Role::PRIMARY_KEY, Role::PRIMARY_KEY_SIZE)->primary();
            // attributes
            $table->string('display_name');
            // basic dates
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->dropIfExists(Role::TABLE);
    }
}
