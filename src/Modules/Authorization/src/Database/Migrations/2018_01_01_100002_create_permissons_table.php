<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface as Permission;
use Modules\Support\Parents\AbstractMigration;

class CreatePermissonsTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up():void
    {
        $this->builder()->create(Permission::TABLE, function (Blueprint $table) {
            // primary key
            $table->string(Permission::PRIMARY_KEY, Permission::PRIMARY_KEY_SIZE)->primary();
            // attributes
            $table->string('display_name');
            // basic times
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down():void
    {
        $this->builder()->dropIfExists(Permission::TABLE);
    }
}
