<?php

namespace Modules\Authorization\Services\Tasks\Role;

use Modules\Authorization\Events\RoleDataEditing;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Collection;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\UpdateResourceFailedException;

/**
 * Class EditRoleTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\CreateResourceFailedException
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 *
 * @event   \Modules\Authorization\Events\RoleDataEditing
 */
class EditRoleTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        CreateResourceFailedException::class,
        UpdateResourceFailedException::class,
    ];
    
    /**
     * Role instance.
     *
     * @var \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private $role;
    
    /**
     * Display name.
     *
     * @var string
     */
    private $displayName;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private $roleRepository;
    
    /**
     * EditRoleTask constructor.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     * @param string                                                        $displayName
     * @param \Modules\Support\Collection                                   $data
     */
    public function __construct(RoleInterface $role, string $displayName, Collection $data)
    {
        $this->roleRepository = $this->resolve(RoleRepositoryInterface::class);
        
        $this->role        = $role;
        $this->displayName = $displayName;
        $this->data        = $data;
    }
    
    /**
     * Edit role.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function handle()
    {
        $this->fillData();
        
        $this->persist();
        
        return $this->role;
    }
    
    /**
     * Fill data
     *
     * @return void
     */
    private function fillData(): void
    {
        $this->role->setDisplayName($this->displayName);
        
        event(new RoleDataEditing($this->role, $this->data));
    }
    
    /**
     * Persist role to database.
     *
     * @return void
     */
    private function persist(): void
    {
        $this->roleRepository->persist($this->role);
    }
}
