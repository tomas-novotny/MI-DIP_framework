<?php

namespace Modules\Authorization\Services\Tasks\Role;

use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class GetRoleByCodeTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 *
 * @event   \Modules\Authorization\Events\RoleDataEditing
 */
class GetRoleByCodeTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $key;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private $roleRepository;
    
    /**
     * GetRoleByCodeAction constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->roleRepository = $this->resolve(RoleRepositoryInterface::class);
        
        $this->key = $key;
    }
    
    /**
     * Get role by code.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        $role = $this->roleRepository->getByCode($this->key);
        
        return $role;
    }
}
