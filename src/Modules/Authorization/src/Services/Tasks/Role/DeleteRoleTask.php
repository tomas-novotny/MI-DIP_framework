<?php

namespace Modules\Authorization\Services\Tasks\Role;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\DeleteResourceFailedException;

/**
 * Class DeleteRoleTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\DeleteResourceFailedException
 */
class DeleteRoleTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        DeleteResourceFailedException::class,
    ];
    
    /**
     * Role instance.
     *
     * @var \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private $role;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private $roleRepository;
    
    /**
     * DeleteRoleTask constructor.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     */
    public function __construct(RoleInterface $role)
    {
        $this->roleRepository = $this->resolve(RoleRepositoryInterface::class);
        
        $this->role = $role;
    }
    
    /**
     * Remove role.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function handle()
    {
        $this->roleRepository->remove($this->role);
    }
}
