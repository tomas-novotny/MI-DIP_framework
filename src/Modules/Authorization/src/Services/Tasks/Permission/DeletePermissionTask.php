<?php

namespace Modules\Authorization\Services\Tasks\Permission;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\DeleteResourceFailedException;

/**
 * Class DeletePermissionTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\DeleteResourceFailedException
 */
class DeletePermissionTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        DeleteResourceFailedException::class,
    ];
    
    /**
     * Permission instance.
     *
     * @var \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    private $permission;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface
     */
    private $permissionRepository;
    
    /**
     * CreatePermissionTask constructor.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     */
    public function __construct(PermissionInterface $permission)
    {
        $this->permissionRepository = $this->resolve(PermissionRepositoryInterface::class);
        
        $this->permission = $permission;
    }
    
    /**
     * Remove permission.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function handle()
    {
        $this->permissionRepository->remove($this->permission);
    }
}
