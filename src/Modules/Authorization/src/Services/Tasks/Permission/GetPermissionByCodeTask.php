<?php

namespace Modules\Authorization\Services\Tasks\Permission;

use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class GetPermissionByCodeTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class GetPermissionByCodeTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $key;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface
     */
    private $permissionRepository;
    
    /**
     * GetRoleByCodeAction constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->permissionRepository = $this->resolve(PermissionRepositoryInterface::class);
        
        $this->key = $key;
    }
    
    /**
     * Get permission by code.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        $permission = $this->permissionRepository->getByCode($this->key);
        
        return $permission;
    }
}
