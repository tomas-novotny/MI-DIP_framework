<?php

namespace Modules\Authorization\Services\Tasks\Permission;

use Modules\Authorization\Events\PermissionDataEditing;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Collection;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\UpdateResourceFailedException;

/**
 * Class EditPermissionTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\CreateResourceFailedException
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 *
 * @event   \Modules\Authorization\Events\PermissionDataEditing
 */
class EditPermissionTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        CreateResourceFailedException::class,
        UpdateResourceFailedException::class,
    ];
    
    /**
     * Permission instance.
     *
     * @var \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    private $permission;
    
    /**
     * Display name.
     *
     * @var string
     */
    private $displayName;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface
     */
    private $permissionRepository;
    
    /**
     * EditPermissionTask constructor.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     * @param string                                                              $displayName
     * @param \Modules\Support\Collection                                         $data
     */
    public function __construct(PermissionInterface $permission, string $displayName, Collection $data)
    {
        $this->permissionRepository = $this->resolve(PermissionRepositoryInterface::class);
        
        $this->permission  = $permission;
        $this->displayName = $displayName;
        $this->data        = $data;
    }
    
    /**
     * Edit permission task.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function handle()
    {
        $this->fillData();
        
        $this->persist();
        
        return $this->permission;
    }
    
    /**
     * Fill data
     *
     * @return void
     */
    private function fillData(): void
    {
        $this->permission->setDisplayName($this->displayName);
        
        event(new PermissionDataEditing($this->permission, $this->data));
    }
    
    /**
     * Persist permission to database.
     *
     * @return void
     */
    private function persist(): void
    {
        $this->permissionRepository->persist($this->permission);
    }
}
