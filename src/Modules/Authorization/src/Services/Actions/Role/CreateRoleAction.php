<?php

namespace Modules\Authorization\Services\Actions\Role;

use Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Authorization\Services\Tasks\Role\EditRoleTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\InvalidArgumentException;

/**
 * Class CreateRoleAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Role\EditRoleTask
 *
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 * @throws  \Modules\Support\Exceptions\CreateResourceFailedException
 */
class CreateRoleAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        InvalidArgumentException::class,
        CreateResourceFailedException::class,
    ];
    
    /**
     * Role factory.
     *
     * @var \Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface
     */
    private $roleFactory;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private $roleRepository;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * CreateRoleAction constructor.
     *
     * @param \Modules\Support\Collection $data
     */
    public function __construct(Collection $data)
    {
        $this->roleFactory    = $this->resolve(RoleFactoryInterface::class);
        $this->roleRepository = $this->resolve(RoleRepositoryInterface::class);
        
        $this->data = $data;
    }
    
    /**
     * Create role.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     */
    public function handle()
    {
        $this->validateData();
        
        $role = $this->roleFactory->newRole($this->data->get('code'));
        
        $this->dispatchTask(new EditRoleTask($role, $this->data->get('displayName', ''), $this->data));
        
        return $role;
    }
    
    /**
     * Validate input data.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    private function validateData(): void
    {
        if (empty($this->data->get('code'))) {
            throw new InvalidArgumentException('Must have "code" attribute');
        }
    }
}
