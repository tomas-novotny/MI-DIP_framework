<?php

namespace Modules\Authorization\Services\Actions\Role;

use Modules\Authorization\Services\Tasks\Role\DeleteRoleTask;
use Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Exceptions\DeleteResourceFailedException;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class DeleteRoleAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Role\DeleteRoleTask
 * @uses    \Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\DeleteResourceFailedException
 */
class DeleteRoleAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        DeleteResourceFailedException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $key;
    
    /**
     * UpdateRoleAction constructor.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->key = $code;
    }
    
    /**
     * Delete role.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function handle()
    {
        $role = $this->dispatchTask(new GetRoleByCodeTask($this->key));
        
        $this->dispatchTask(new DeleteRoleTask($role));
    }
}
