<?php

namespace Modules\Authorization\Services\Actions\Role;

use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Bus\Parents\AbstractAction;

/**
 * Class GetRolesAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class GetRolesAction extends AbstractAction
{
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private $roleRepository;
    
    /**
     * GetRolesAction constructor.
     */
    public function __construct()
    {
        $this->roleRepository = $this->resolve(RoleRepositoryInterface::class);
    }
    
    /**
     * Get all roles as collection.
     *
     * @return \Modules\Support\Collection
     */
    public function handle()
    {
        $users = $this->roleRepository->all();
        
        return $users;
    }
}
