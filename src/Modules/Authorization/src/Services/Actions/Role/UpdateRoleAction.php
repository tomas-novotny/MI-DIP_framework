<?php

namespace Modules\Authorization\Services\Actions\Role;

use Modules\Authorization\Services\Tasks\Role\EditRoleTask;
use Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;

/**
 * Class UpdateRoleAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask
 * @uses    \Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 */
class UpdateRoleAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        UpdateResourceFailedException::class,
        InvalidArgumentException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $key;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * UpdateRoleAction constructor.
     *
     * @param string                      $code
     * @param \Modules\Support\Collection $data
     */
    public function __construct(string $code, Collection $data)
    {
        $this->key  = $code;
        $this->data = $data;
    }
    
    /**
     * Update role.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        $role = $this->dispatchTask(new GetRoleByCodeTask($this->key));
        
        $this->validateData();
        
        $this->dispatchTask(new EditRoleTask($role, $this->data->get('displayName', ''), $this->data));
        
        return $role;
    }
    
    /**
     * Validate input data.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    private function validateData(): void
    {
        // TODO: add validation
    }
}
