<?php

namespace Modules\Authorization\Services\Actions\Role;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;
use Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask;
use Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;

/**
 * Class RemovePermissionsFromRoleAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask
 * @uses    \Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 */
class RemovePermissionsFromRoleAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        UpdateResourceFailedException::class,
        InvalidArgumentException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $roleCode;
    
    /**
     * Permission codes.
     *
     * @var array
     */
    private $permissionsCodes;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private $roleRepository;
    
    /**
     * RemovePermissionsFromRoleAction constructor.
     *
     * @param string $roleCode
     * @param array  $permissionsCodes
     */
    public function __construct(string $roleCode, array $permissionsCodes)
    {
        $this->roleRepository = $this->resolve(RoleRepositoryInterface::class);
        
        $this->roleCode         = $roleCode;
        $this->permissionsCodes = $permissionsCodes;
    }
    
    /**
     * Add permissions to role
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function handle()
    {
        $role = $this->getRole();
        
        $this->removePermissions($role);
        
        $this->roleRepository->persist($role);
        
        return $role;
    }
    
    /**
     * Add permissions to role.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    private function removePermissions(RoleInterface $role): void
    {
        try {
            foreach ($this->permissionsCodes as $code) {
                $role->removePermission($this->getPermission($code));
            }
        }
        catch (ResourceNotFoundException $exception) {
            throw new UpdateResourceFailedException(get_class($role), [$role->getCode()], $exception);
        }
    }
    
    /**
     * Get permission.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    private function getPermission(string $code): PermissionInterface
    {
        return $this->dispatchTask(new GetPermissionByCodeTask($code));
    }
    
    /**
     * Get role.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getRole(): RoleInterface
    {
        return $this->dispatchTask(new GetRoleByCodeTask($this->roleCode));
    }
}
