<?php

namespace Modules\Authorization\Services\Actions\Permission;

use Modules\Authorization\Model\Factories\Contracts\PermissionFactoryInterface;
use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Authorization\Services\Tasks\Permission\EditPermissionTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\InvalidArgumentException;

/**
 * Class CreatePermissionAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Permission\EditPermissionTask
 *
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 * @throws  \Modules\Support\Exceptions\CreateResourceFailedException
 */
class CreatePermissionAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        InvalidArgumentException::class,
        CreateResourceFailedException::class,
    ];
    
    /**
     * Permission factory.
     *
     * @var \Modules\Authorization\Model\Factories\Contracts\PermissionFactoryInterface
     */
    private $permissionFactory;
    
    /**
     * Permission repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface
     */
    private $permissionRepository;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * CreatePermissionAction constructor.
     *
     * @param \Modules\Support\Collection $data
     */
    public function __construct(Collection $data)
    {
        $this->permissionRepository = $this->resolve(PermissionRepositoryInterface::class);
        $this->permissionFactory    = $this->resolve(PermissionFactoryInterface::class);
        
        $this->data = $data;
    }
    
    /**
     * Create permission.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     */
    public function handle()
    {
        $this->validateData();
        
        $permission = $this->permissionFactory->newPermission($this->data->get('code'));
        
        $this->dispatchTask(new EditPermissionTask($permission, $this->data->get('displayName', ''), $this->data));
        
        return $permission;
    }
    
    /**
     * Validate input data.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    private function validateData(): void
    {
        if (empty($this->data->get('code'))) {
            throw new InvalidArgumentException('Must have "code" attribute');
        }
    }
}
