<?php

namespace Modules\Authorization\Services\Actions\Permission;

use Modules\Authorization\Services\Tasks\Permission\DeletePermissionTask;
use Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Exceptions\DeleteResourceFailedException;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class DeletePermissionAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask
 * @uses    \Modules\Authorization\Services\Tasks\Permission\DeletePermissionTask
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\DeleteResourceFailedException
 */
class DeletePermissionAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        DeleteResourceFailedException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $key;
    
    /**
     * UpdatePermissionAction constructor.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->key = $code;
    }
    
    /**
     * Delete permission.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function handle()
    {
        $permission = $this->dispatchTask(new GetPermissionByCodeTask($this->key));
        
        $this->dispatchTask(new DeletePermissionTask($permission));
    }
}
