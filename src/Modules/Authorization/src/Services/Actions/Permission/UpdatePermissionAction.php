<?php

namespace Modules\Authorization\Services\Actions\Permission;

use Modules\Authorization\Services\Tasks\Permission\EditPermissionTask;
use Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;

/**
 * Class UpdatePermissionAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authorization\Services\Tasks\Permission\EditPermissionTask
 * @uses    \Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 */
class UpdatePermissionAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        UpdateResourceFailedException::class,
        InvalidArgumentException::class,
    ];
    
    /**
     * Primary code.
     *
     * @var string
     */
    private $key;
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * UpdatePermissionAction constructor.
     *
     * @param string                      $code
     * @param \Modules\Support\Collection $data
     */
    public function __construct(string $code, Collection $data)
    {
        $this->key  = $code;
        $this->data = $data;
    }
    
    /**
     * Update permission.
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        $permission = $this->dispatchTask(new GetPermissionByCodeTask($this->key));
        
        $this->validateData();
        
        $this->dispatchTask(new EditPermissionTask($permission, $this->data->get('displayName', ''), $this->data));
        
        return $permission;
    }
    
    /**
     * Validate input data.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    private function validateData(): void
    {
        // TODO: add validation
    }
}
