<?php

namespace Modules\Authorization\Services\Actions\Permission;

use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;
use Modules\Bus\Parents\AbstractAction;

/**
 * Class GetPermissionsAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class GetPermissionsAction extends AbstractAction
{
    /**
     * Role permission repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface
     */
    private $permissionRepository;
    
    /**
     * GetPermissionsAction constructor.
     */
    public function __construct()
    {
        $this->permissionRepository = $this->resolve(PermissionRepositoryInterface::class);
    }
    
    /**
     * Get all permissions as collection.
     *
     * @return \Modules\Support\Collection
     */
    public function handle()
    {
        $permissions = $this->permissionRepository->all();
        
        return $permissions;
    }
}
