<?php

namespace Modules\Authorization\Events;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Support\Contracts\CollectionInterface;
use Modules\Support\Parents\AbstractEvent;

class PermissionDataEditing extends AbstractEvent
{
    /**
     * Permission.
     *
     * @var \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    public $permission;
    
    /**
     * Registration request data.
     *
     * @var \Modules\Support\Contracts\CollectionInterface
     */
    public $data;
    
    /**
     * PermissionDataEditing constructor.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     * @param \Modules\Support\Contracts\CollectionInterface                      $data
     */
    public function __construct(PermissionInterface $permission, CollectionInterface $data)
    {
        $this->permission = $permission;
        $this->data       = $data;
    }
}
