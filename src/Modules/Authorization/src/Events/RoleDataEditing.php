<?php

namespace Modules\Authorization\Events;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Support\Contracts\CollectionInterface;
use Modules\Support\Parents\AbstractEvent;

class RoleDataEditing extends AbstractEvent
{
    /**
     * Role.
     *
     * @var \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    public $role;
    
    /**
     * Registration request data.
     *
     * @var \Modules\Support\Contracts\CollectionInterface
     */
    public $data;
    
    /**
     * RoleDataEditing constructor.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     * @param \Modules\Support\Contracts\CollectionInterface                $data
     */
    public function __construct(RoleInterface $role, CollectionInterface $data)
    {
        $this->role = $role;
        $this->data = $data;
    }
}
