<?php

namespace Modules\Authorization\Middlewares;

use Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface;

class HasRole extends Authorize
{
    /**
     * Validate if user has given permissions or roles
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface $user
     * @param array                                                                 $roles
     *
     * @return bool
     */
    protected function validate(AuthorizableInterface $user, array $roles)
    {
        return $user->hasRoles($roles, false);
    }
}