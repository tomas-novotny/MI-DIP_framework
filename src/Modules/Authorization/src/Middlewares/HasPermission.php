<?php

namespace Modules\Authorization\Middlewares;

use Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface;

class HasPermission extends Authorize
{
    /**
     * Validate if user has given permissions or roles
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface $user
     * @param array                                                                 $permissions
     *
     * @return bool
     */
    protected function validate(AuthorizableInterface $user, array $permissions)
    {
        //        // iterate through permissions
        //        foreach ($permissions as $permission) {
        //            // redirect back if user does not have given permission
        //            if ($user->hasPermission($permission) === false) {
        //
        //                return false;
        //            }
        //        }
        
        return false;
    }
}