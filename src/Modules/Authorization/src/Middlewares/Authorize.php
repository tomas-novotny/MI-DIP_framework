<?php

namespace Modules\Authorization\Middlewares;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface;

abstract class Authorize
{
    /**
     * Validate if user has given permissions or roles
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface $user
     * @param array                                                  $data
     *
     * @return bool
     */
    abstract protected function validate(AuthorizableInterface $user, array $data);
    
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param array                    $data
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ...$data)
    {
        // only auth users
        if (auth()->guest()) {
            return redirect()->guest(route('auth.login'));
        }
        
        /* @var \Modules\Authorization\Model\Entities\Contracts\AuthorizableInterface $user */
        $user = auth()->user();
        
//        // super admin can everything
//        if ($user->hasRole('super.admin')) {
//            return $next($request);
//        }
        
        if ($this->validate($user, $data) === false) {
            
            throw new AuthorizationException('User not authorized for ['.implode(',', $data).'].');
        }
        
        return $next($request);
    }
}