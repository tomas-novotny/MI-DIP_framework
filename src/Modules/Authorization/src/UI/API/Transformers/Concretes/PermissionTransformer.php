<?php

namespace Modules\Authorization\Http\Controllers\API\Transformers\Concretes;

use Modules\Authorization\UI\API\Transformers\Contracts\PermissionTransformerInterface;
use Modules\Support\Parents\AbstractTransformer;

class PermissionTransformer extends AbstractTransformer implements PermissionTransformerInterface
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     *  Turn model object into a generic array.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\PermissionInterface $permission
     *
     * @return array
     */
    public function transform($permission)
    {
        $data = [
            'code'        => $permission->getCode(),
            'displayName' => $permission->getDisplayName(),
        ];
        
        return $data;
    }
}
