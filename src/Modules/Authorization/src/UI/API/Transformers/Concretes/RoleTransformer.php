<?php

namespace Modules\Authorization\Http\Controllers\API\Transformers\Concretes;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\UI\API\Transformers\Contracts\RoleTransformerInterface;
use Modules\Support\Parents\AbstractTransformer;

class RoleTransformer extends AbstractTransformer implements RoleTransformerInterface
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    protected $availableIncludes = [
        'permissions',
    ];
    
    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     *  Turn model object into a generic array.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return array
     */
    public function transform($role)
    {
        $data = [
            'code'        => $role->getCode(),
            'displayName' => $role->getDisplayName(),
        ];
        
        return $data;
    }
    
    /**
     * Include permissions.
     *
     * @param \Modules\Authorization\Model\Entities\Contracts\RoleInterface $role
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePermissions(RoleInterface $role)
    {
        return $this->collection($role->getPermissions(), new PermissionTransformer());
    }
}
