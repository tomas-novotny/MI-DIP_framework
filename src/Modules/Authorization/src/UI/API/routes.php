<?php

use Illuminate\Routing\Router;
use Modules\Authorization\UI\API\Controllers\PermissionController;
use Modules\Authorization\UI\API\Controllers\Role\PermissionController as RolePermissionController;
use Modules\Authorization\UI\API\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

api_router('v1', function (Router $router) {
    $router->group(
        ['middleware' => [
            //             'auth:api',
            //             'permission:access.users',
        ],
        ], function (Router $router) {
        
        $router->apiResource('roles', RoleController::class);
        $router->apiResource('permissions', PermissionController::class);
        $router->apiResource('roles/{role}/permissions', RolePermissionController::class);
    });
});
