<?php

namespace Modules\Authorization\UI\API\Controllers;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\RoleTransformer;
use Modules\Authorization\Services\Actions\Role\CreateRoleAction;
use Modules\Authorization\Services\Actions\Role\DeleteRoleAction;
use Modules\Authorization\Services\Actions\Role\GetRoleByCodeAction;
use Modules\Authorization\Services\Actions\Role\GetRolesAction;
use Modules\Authorization\Services\Actions\Role\UpdateRoleAction;
use Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask;
use Modules\Support\Parents\Controllers\AbstractApiController;
use Modules\Support\Request;

class RoleController extends AbstractApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $roles = $this->dispatchService(new GetRolesAction());
        
        return $this->transformRole($roles);
    }
    
    /**
     * @param string $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $code)
    {
        $role = $this->dispatchService(new GetRoleByCodeTask($code));
        
        return $this->transformRole($role);
    }
    
    /**
     * TODO: use request validation
     *
     * @param \Modules\Support\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $role = $this->dispatchService(new CreateRoleAction($request->data()));
        
        return $this->transformRole($role);
    }
    
    /**
     * TODO: use request validation
     *
     * @param string                   $code
     * @param \Modules\Support\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(string $code, Request $request)
    {
        $role = $this->dispatchService(new UpdateRoleAction($code, $request->data()));
        
        return $this->transformRole($role);
    }
    
    /**
     * @param string $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $code)
    {
        $this->dispatchService(new DeleteRoleAction($code));
        
        return $this->deleted();
    }
    
    /**
     * Transform role..
     *
     * @param mixed $role
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function transformRole($role)
    {
        return $this->transform($role, RoleTransformer::class);
    }
}
