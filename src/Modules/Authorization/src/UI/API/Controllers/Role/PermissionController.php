<?php

namespace Modules\Authorization\UI\API\Controllers\Role;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\PermissionTransformer;
use Modules\Authorization\Services\Actions\Role\AddPermissionsToRoleAction;
use Modules\Authorization\Services\Actions\Role\RemovePermissionsFromRoleAction;
use Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask;
use Modules\Support\Parents\Controllers\AbstractApiController;
use Modules\Support\Request;

class PermissionController extends AbstractApiController
{
    /**
     * @param string $roleCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(string $roleCode)
    {
        $role = $this->dispatchService(new GetRoleByCodeTask($roleCode));
        
        return $this->transformPermission($role->getPermissions());
    }
    
    /**
     * TODO: use request validation
     *
     * @param string                   $roleCode
     * @param \Modules\Support\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(string $roleCode, Request $request)
    {
        $permissionCodes = explode(',', $request->get('codes'));
        
        $this->dispatchService(new AddPermissionsToRoleAction($roleCode, $permissionCodes));
        
        return $this->accepted();
    }
    
    /**
     * @param string $roleCode
     * @param string $permissionCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $roleCode, string $permissionCode)
    {
        $this->dispatchService(new RemovePermissionsFromRoleAction($roleCode, [$permissionCode]));
        
        return $this->deleted();
    }
    
    /**
     * Transform permission.
     *
     * @param mixed $permission
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function transformPermission($permission)
    {
        return $this->transform($permission, PermissionTransformer::class);
    }
}
