<?php

namespace Modules\Authorization\UI\API\Controllers;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\PermissionTransformer;
use Modules\Authorization\Services\Actions\Permission\CreatePermissionAction;
use Modules\Authorization\Services\Actions\Permission\DeletePermissionAction;
use Modules\Authorization\Services\Actions\Permission\GetPermissionsAction;
use Modules\Authorization\Services\Actions\Permission\UpdatePermissionAction;
use Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask;
use Modules\Support\Parents\Controllers\AbstractApiController;
use Modules\Support\Request;

class PermissionController extends AbstractApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $permissions = $this->dispatchService(new GetPermissionsAction());
        
        return $this->transformPermission($permissions);
    }
    
    /**
     * @param string $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $code)
    {
        $permission = $this->dispatchService(new GetPermissionByCodeTask($code));
        
        return $this->transformPermission($permission);
    }
    
    /**
     * TODO: use request validation
     *
     * @param \Modules\Support\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $permission = $this->dispatchService(new CreatePermissionAction($request->data()));
        
        return $this->transformPermission($permission);
    }
    
    /**
     * TODO: use request validation
     *
     * @param string                   $code
     * @param \Modules\Support\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(string $code, Request $request)
    {
        $permission = $this->dispatchService(new UpdatePermissionAction($code, $request->data()));
        
        return $this->transformPermission($permission);
    }
    
    /**
     * @param string $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $code)
    {
        $this->dispatchService(new DeletePermissionAction($code));
        
        return $this->deleted();
    }
    
    /**
     * Transform permission.
     *
     * @param mixed $permission
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function transformPermission($permission)
    {
        return $this->transform($permission, PermissionTransformer::class);
    }
}
