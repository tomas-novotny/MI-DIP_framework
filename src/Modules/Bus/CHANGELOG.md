# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-04-30
### Added
- Added `ServiceInterface.php` for all service classes
- Added `ActionInterface.php` for Action classes
- Added `TaskInterface.php` for Task classes
- Added `KnowsUIInterface.php`
- Added `AbstractAction.php` which all Action implementations should extend
- Added `AbstractAction.php` which all Action implementations should extend
- Added `ServiceDispatcher.php` which is binded to application to dispatch service classes
- Added `CanDispatchServiceInterface.php`
- Added `DispatchesServicesTrait.php` to implement dispatching all services via inheretence
- Added `CanDispatchTaskInterface.php`
- Added `DispatchesTasksTrait.php` to implement dispatching tasks via inheretence
- Added `API`, `CLI`, `Web` and `None` UI states which implements `UIInterface.php`
