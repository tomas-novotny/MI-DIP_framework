# Bus module

[![Software License][ico-license]](LICENSE.md)

This module is part of example [modular project][link-framework]. 

This module implements Service layer from [Porto SAP][link-porto]. 
Provider interfaces for service classes like `Action` and `Task` and `ServiceDispatcher` that handle service class dispatching.

## Installation

Add `novott20/modules-bus` to your `composer.json`:
```bash
$ composer require novott20/modules-bus
```

Register our service provider in providers array in `config/app.php`:
```php
'providers' => [
    // ...
    Modules\Bus\Providers\ServiceProvider::class,
    // ...
],
```

## Service Dispatcher

Containes `ServiceDispatcher` which is decorator of Laravel Job Dispatcher (`Illuminate\Bus\Dispatcher`) which is bind to `Illuminate\Contracts\Bus\Dispatcher` interface.

![](docs/dispatcher.png)

### Action

Actions represent the Use Cases of the Application (the actions that can be taken by a User or a Software in the Application).

Actions CAN hold business logic or/and they orchestrate the Tasks to perform the business logic.

![](docs/action_new.png)

### Task

The Tasks are the classes that hold the shared business logic between multiple Actions accross different Containers.

Every Task is responsible for a small part of the logic.

![](docs/task_new.png)

## Usage

Create Action class via extending class to `Modules\Bus\Parents\AbstractAction`

```php
use Modules\Bus\Parents\AbstractAction;

class AddPositiveNumbersAction extends AbstractAction
{
    protected $expectedExceptions = [
        \InvalidArgumentException::class,
    ];
    
    private $a;
    
    private $b;

    public function __construct(int $a, int $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
    
    public function handle()
    {
        if($this->a < 0 or $this->b < 0) {
            throw new \InvalidArgumentException('Can add only positive numbers');
        }
        
        return $this->a + $this->b; 
    }
}
```

Action can dispatch Tasks with accesible `dispatchTask()` method.

```php
use Modules\Bus\Parents\AbstractAction;
use Modules\Bus\Parents\AbstractTask;

class DivideNumbersTask extends AbstractTask
{
    protected $expectedExceptions = [
        \InvalidArgumentException::class,
    ];
    
    private $a;
    
    private $b;

    public function __construct(float $a, float $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
    
    public function handle()
    {
        if($this->b == 0) {
            throw new \InvalidArgumentException('Can add only positive numbers');
        }
        
        return $this->a / $this->b; 
    }
}

class MakeAdvanceCalculationAction extends AbstractAction
{   
    private $a;
    
    private $b;

    public function __construct(float $a, float $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
    
    public function handle()
    {
        try {
            $result = $this->dispatchTask(new DivideNumbersTask(3.14, $this->b));
        } catch(\InvalidArgumentException $exception) {
            $result = 2;
        }
        
        return $this->a * $result;
    }
}
```

You can dispatch services from Controllers or other classes with adding `Modules\Bus\Concerns\DispatchesServicesTrait`. 
It will add `dispatchService()` and also require `getUI()` method which should return current UI.
```php
use Modules\Bus\Concerns\DispatchesServicesTrait;
use Modules\Bus\States\Contracts\UIInterface;
use Modules\Bus\States\UI\Web;

class Controller
{
    use DispatchesServicesTrait;
    
    public function getUI(): UIInterface
    {
        return new Web();
    }
    
    public function getAction()
    {
        $data = $this->dispatchService(new MakeAdvanceCalculationAction(3.8, 10));
        
        return response()->json($data);
    }
```
## Contributing

Please see [CONTRIBUTING][link-contributing] and [CODE_OF_CONDUCT][link-code-of-conduct] for details.

## Security

If you discover any security related issues, please email novott20@fit.cvut.cz instead of using the issue tracker.

## Credits

- [Tomáš Novotný][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-blue.svg

[link-framework]: https://github.com/novott20/MI-DIP_framework
[link-author]: https://github.com/novott20
[link-contributors]: ../../../../../contributors
[link-contributing]: ../../../CONTRIBUTING.md
[link-code-of-conduct]: ../../../CODE_OF_CONDUCT.md
[link-porto]: https://github.com/Mahmoudz/Porto