<?php

namespace Modules\Bus;

use Illuminate\Bus\Dispatcher;
use Illuminate\Contracts\Bus\Dispatcher as DispatcherInterface;
use Illuminate\Contracts\Container\Container;
use Modules\Bus\Contracts\CanDispatchServiceInterface;
use Modules\Bus\Contracts\ServiceInterface;
use Modules\Bus\Exceptions\ServiceFailedException;
use Throwable;

class ServiceDispatcher implements DispatcherInterface, CanDispatchServiceInterface
{
    /**
     * The container implementation.
     *
     * @var \Illuminate\Contracts\Container\Container
     */
    private $container;
    
    /**
     * Bus dispatcher.
     *
     * @var \Illuminate\Contracts\Bus\Dispatcher
     */
    private $dispatcher;
    
    /**
     * ServiceDispatcher constructor.
     *
     * @param \Illuminate\Contracts\Container\Container $container
     * @param \Illuminate\Contracts\Bus\Dispatcher|null $dispatcher
     */
    public function __construct(Container $container, DispatcherInterface $dispatcher = null)
    {
        $this->container  = $container;
        $this->dispatcher = $dispatcher ?: new Dispatcher($container);
    }
    
    /**
     * Dispatch a command to its appropriate handler.
     *
     * @param mixed $command
     *
     * @return mixed
     */
    public function dispatch($command)
    {
        if ($command instanceof ServiceInterface) {
            return $this->dispatchService($command);
        }
        
        return $this->dispatcher->dispatch($command);
    }
    
    /**
     * Dispatch a command to its appropriate handler in the current process.
     *
     * @param mixed $command
     * @param mixed $handler
     *
     * @return mixed
     */
    public function dispatchNow($command, $handler = null)
    {
        if ($command instanceof ServiceInterface) {
            return $this->dispatchService($command);
        }
        
        return $this->dispatcher->dispatchNow($command);
    }
    
    /**
     * Call given callable object (actions, tasks, etc.)
     *
     * @param \Modules\Bus\Contracts\ServiceInterface $service
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface
     */
    public function dispatchService(ServiceInterface $service)
    {
        try {
            return $this->dispatcher->dispatch($service);
        }
        catch (Throwable $exception) {
            if ($service->isUnexpectedException($exception)) {
                $service->failed($exception);
                
                throw $this->resolveException($service, $exception, get_class($service));
            }
            
            // pass expected exception
            throw $exception;
        }
    }
    
    /**
     * Resolve exception.
     *
     * @param \Modules\Bus\Contracts\ServiceInterface $service
     * @param \Throwable                              $exception
     * @param string                                  $task
     *
     * @return \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface
     */
    private function resolveException(ServiceInterface $service, Throwable $exception, string $task)
    {
        $message = 'Service ['.$task.'] failed.';
        
        if (config('app.debug')) {
            $message .= ' ['.get_class($exception).'] '.$exception->getMessage();
        }
        
        return new ServiceFailedException($service, $message, $exception);
    }
    
    /**
     * Determine if the given command has a handler.
     *
     * @param mixed $command
     *
     * @return bool
     */
    public function hasCommandHandler($command)
    {
        return $this->dispatcher->hasCommandHandler($command);
    }
    
    /**
     * Retrieve the handler for a command.
     *
     * @param mixed $command
     *
     * @return bool|mixed
     */
    public function getCommandHandler($command)
    {
        return $this->dispatcher->getCommandHandler($command);
    }
    
    /**
     * Set the pipes commands should be piped through before dispatching.
     *
     * @param array $pipes
     *
     * @return $this
     */
    public function pipeThrough(array $pipes)
    {
        return $this->dispatcher->pipeThrough($pipes);
    }
    
    /**
     * Map a command to a handler.
     *
     * @param array $map
     *
     * @return $this
     */
    public function map(array $map)
    {
        return $this->dispatcher->map($map);
    }
}
