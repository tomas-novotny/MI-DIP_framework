<?php

namespace Modules\Bus\Parents;

use Modules\Bus\Contracts\KnowsUIInterface;
use Modules\Bus\Contracts\ServiceInterface;
use Modules\Bus\States\Contracts\UIInterface;
use Modules\Bus\States\UI\None;
use Throwable;

abstract class AbstractService extends AbstractJob implements ServiceInterface, KnowsUIInterface
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [];
    
    /**
     * User interface state.
     *
     * @var \Modules\Bus\States\Contracts\UIInterface
     */
    private $interface;
    
    /**
     * Handle action or task.
     *
     * @return mixed
     */
    abstract public function handle();
    
    /**
     * Rollback action or task.
     *
     * @param \Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        // usually we dont want to rollback anything
    }
    
    /**
     *  Determinate if given exception is not expected.
     *
     * @param \Throwable $exception
     *
     * @return bool
     */
    public function isUnexpectedException(Throwable $exception): bool
    {
        foreach ($this->expectedExceptions as $expectedException) {
            if ($exception instanceof $expectedException) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Set current UI
     *
     * @param \Modules\Bus\States\Contracts\UIInterface $interface
     *
     * @return void
     */
    public function setUI(UIInterface $interface): void
    {
        $this->interface = $interface;
    }
    
    /**
     * Get current UI
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    public function getUI(): UIInterface
    {
        return $this->interface ?: new None();
    }
    
    /**
     * Resolve class by DI container.
     *
     * @param string $abstract
     *
     * @return mixed
     */
    protected function resolve(string $abstract)
    {
        return app()->make($abstract);
    }
}
