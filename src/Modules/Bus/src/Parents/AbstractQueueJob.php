<?php

namespace Modules\Bus\Parents;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

abstract class AbstractQueueJob extends AbstractJob implements ShouldQueue
{
    use Queueable;
    
    /**
     * Default priority queue name.
     *
     * @var string
     */
    const NORMAL = 'default';
    
    /**
     * High priority queue name.
     *
     * @var string
     */
    const HIGH = 'high';
    
    /**
     * Low priority queue name.
     *
     * @var string
     */
    const LOW = 'low';
    
    /**
     * Emails queue name.
     *
     * @var string
     */
    const EMAILS = 'emails';
    
    /**
     * Export queue name.
     *
     * @var string
     */
    const EXPORT = self::HIGH;
    
    /**
     * Import queue name.
     *
     * @var string
     */
    const IMPORT = self::LOW;
}
