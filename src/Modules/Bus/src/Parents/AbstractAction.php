<?php

namespace Modules\Bus\Parents;

use Modules\Bus\Concerns\DispatchesTasksTrait;
use Modules\Bus\Contracts\ActionInterface;

abstract class AbstractAction extends AbstractService implements ActionInterface
{
    use DispatchesTasksTrait;
}
