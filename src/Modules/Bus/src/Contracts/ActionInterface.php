<?php

namespace Modules\Bus\Contracts;

interface ActionInterface extends ServiceInterface, CanDispatchTaskInterface
{
    //
}
