<?php

namespace Modules\Bus\Contracts;

interface CanDispatchTaskInterface
{
    /**
     * Call given callable object (actions, tasks, etc.)
     *
     * @param \Modules\Bus\Contracts\TaskInterface $task
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface
     */
    public function dispatchTask(TaskInterface $task);
}
