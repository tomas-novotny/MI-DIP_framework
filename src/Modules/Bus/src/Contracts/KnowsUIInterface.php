<?php

namespace Modules\Bus\Contracts;

use Modules\Bus\States\Contracts\UIInterface;

interface KnowsUIInterface
{
    /**
     * Set current UI.
     *
     * @param \Modules\Bus\States\Contracts\UIInterface $interface
     *
     * @return void
     */
    public function setUI(UIInterface $interface): void;
    
    /**
     * Get current UI.
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    public function getUI(): UIInterface;
}
