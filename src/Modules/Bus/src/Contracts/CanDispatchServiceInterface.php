<?php

namespace Modules\Bus\Contracts;

interface CanDispatchServiceInterface
{
    /**
     * Call given callable object (actions, tasks, etc.)
     *
     * @param \Modules\Bus\Contracts\ServiceInterface $service
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface
     */
    public function dispatchService(ServiceInterface $service);
}
