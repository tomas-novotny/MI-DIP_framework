<?php

namespace Modules\Bus\Contracts;

use Throwable;

interface ServiceInterface
{
    /**
     * Handle action or task.
     *
     * @return void
     */
    public function handle();
    
    /**
     * Rollback action or task.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    function failed(Throwable $exception): void;
    
    /**
     * Determinate if given exception is not expected.
     *
     * @param \Exception $exception
     *
     * @return bool
     */
    function isUnexpectedException(Throwable $exception): bool;
}
