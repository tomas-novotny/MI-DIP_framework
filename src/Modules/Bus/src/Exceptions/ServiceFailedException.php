<?php

namespace Modules\Bus\Exceptions;

use Error;
use Exception;
use Modules\Bus\Contracts\ServiceInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException as SymfonyHttpException;
use Throwable;

class ServiceFailedException extends SymfonyHttpException
{
    /**
     * Failed service.
     *
     * @var \Modules\Bus\Contracts\ServiceInterface
     */
    private $service;
    
    /**
     * ServiceFailedException constructor.
     *
     * @param \Modules\Bus\Contracts\ServiceInterface $service
     * @param string|null                             $message
     * @param \Throwable|null                         $previous
     */
    public function __construct(ServiceInterface $service, string $message = null, Throwable $previous = null)
    {
        $this->service = $service;
        
        if ($previous instanceof Error) {
            $previous = new Exception($previous->getMessage(), $previous->getCode(), $previous);
        }
        
        parent::__construct(Response::HTTP_INTERNAL_SERVER_ERROR, $message, $previous);
    }
    
    /**
     * Failed service.
     *
     * @return \Modules\Bus\Contracts\ServiceInterface
     */
    public function getService(): ServiceInterface
    {
        return $this->service;
    }
}
