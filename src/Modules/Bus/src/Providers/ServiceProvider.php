<?php

namespace Modules\Bus\Providers;

use Illuminate\Bus\Dispatcher;
use Illuminate\Contracts\Bus\Dispatcher as DispatcherInterface;
use Illuminate\Support\AggregateServiceProvider;
use Modules\Bus\ServiceDispatcher;

class ServiceProvider extends AggregateServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServiceDispatcher();
    }
    
    /**
     * Register Service Dispatcher to application container.
     *
     * @return void
     */
    private function registerServiceDispatcher(): void
    {
        $this->app->singleton(ServiceDispatcher::class, function ($app) {
            return new ServiceDispatcher($app, $app[Dispatcher::class]);
        });
        
        $this->app->alias(ServiceDispatcher::class, DispatcherInterface::class);
    }
}
