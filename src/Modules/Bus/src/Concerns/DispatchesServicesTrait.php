<?php

namespace Modules\Bus\Concerns;

use Illuminate\Contracts\Bus\Dispatcher;
use Modules\Bus\Contracts\KnowsUIInterface;
use Modules\Bus\Contracts\ServiceInterface;
use Modules\Bus\States\Contracts\UIInterface;

trait DispatchesServicesTrait
{
    /**
     * Get current UI.
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    abstract public function getUI(): UIInterface;
    
    /**
     * Call given callable object (actions, tasks, etc.)
     *
     * @param \Modules\Bus\Contracts\ServiceInterface $service
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface
     */
    public function dispatchService(ServiceInterface $service)
    {
        if ($service instanceof KnowsUIInterface) {
            $service->setUI($this->getUI());
        }
        
        return app()->make(Dispatcher::class)->dispatch($service, 'handle');
    }
}
