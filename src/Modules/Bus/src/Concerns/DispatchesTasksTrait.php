<?php

namespace Modules\Bus\Concerns;

use Illuminate\Contracts\Bus\Dispatcher;
use Modules\Bus\Contracts\KnowsUIInterface;
use Modules\Bus\Contracts\TaskInterface;
use Modules\Bus\States\Contracts\UIInterface;

trait DispatchesTasksTrait
{
    /**
     * Get current UI.
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    abstract public function getUI(): UIInterface;
    
    /**
     * Call given callable object (actions, tasks, etc.)
     *
     * @param \Modules\Bus\Contracts\TaskInterface $task
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface
     */
    public function dispatchTask(TaskInterface $task)
    {
        if ($task instanceof KnowsUIInterface) {
            $task->setUI($this->getUI());
        }
        
        return app()->make(Dispatcher::class)->dispatch($task, 'handle');
    }
}
