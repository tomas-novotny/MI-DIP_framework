<?php

use Modules\App\Engine\Exceptions\Reporters\BugsnagReporter;

return [
    
    'reporters' => [
        BugsnagReporter::class,
    ]
];
