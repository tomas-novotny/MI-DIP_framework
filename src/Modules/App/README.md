# App module

[![Software License][ico-license]](LICENSE.md)

This module is part of example [modular project][link-framework]. 

## Installation

Add `novott20/modules-app` to your `composer.json`:
```bash
$ composer require novott20/modules-app
```

Register main bootstrap classes into application. Extends existing classes with theirs equivalent in `app` module. 

Extends HTTP Kernel (`app\Http\Kernel.php`):
```php
namespace App\Http;

use Modules\App\Engine\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    //
}
```

Extends Console Kernel (`app\Console\Kernel.php`):
```php
namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Modules\App\Engine\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
  protected $commands = [
    // commands provided by your application
  ];

  protected function schedule(Schedule $schedule)
  {
      parent::schedule($schedule);
      // $schedule->command('inspire')->hourly();
  }
}
```

Extends Exception Handler (`app\Exceptions\Handler.php`):
```php
namespace App\Exceptions;

use Modules\App\Engine\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
  //
}
```

## Contributing

Please see [CONTRIBUTING][link-contributing] and [CODE_OF_CONDUCT][link-code-of-conduct] for details.

## Security

If you discover any security related issues, please email novott20@fit.cvut.cz instead of using the issue tracker.

## Credits

- [Tomáš Novotný][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-blue.svg

[link-framework]: https://github.com/novott20/MI-DIP_framework
[link-author]: https://github.com/novott20
[link-contributors]: ../../../../../contributors
[link-contributing]: ../../../CONTRIBUTING.md
[link-code-of-conduct]: ../../../CODE_OF_CONDUCT.md