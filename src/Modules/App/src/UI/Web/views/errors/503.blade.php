@extends('app:web::_layouts.master')

@section('title'){{ $code }} |@parent @endsection

@section('content')
    <div class="text-center">
        <h1>{{ $code }}</h1>
    </div>
@endsection
