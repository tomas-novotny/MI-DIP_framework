@section('meta')
    <meta charset='utf-8'/>
    <title>@section('title'){{ config('app.name') }}@show</title>
    <meta name='description' content='@section('description')@show'/>
    <meta name='keywords' content='@section('keywords')@show'/>
    <meta name='robots' content='@section('robots')index, follow@show'/>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'/>
@show

@section('styles')
    <link rel='stylesheet' href='{{ mix('css/app.css') }}'/>
@show
