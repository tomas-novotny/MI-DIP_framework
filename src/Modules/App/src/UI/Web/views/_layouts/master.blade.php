<!DOCTYPE html>
<html lang="{{ $__locale }}">
<head>
    @include('app:web::_layouts._head')
</head>
<body>
<div id="wrapper">
    <header id="header">
        @include('app:web::_layouts._header')
    </header>

    <div id="content">
        @include('app:web::_templates._messages')

        <div class="container">
            @yield('content')
        </div>
    </div>

    <footer id="footer">
        @include('app:web::_layouts._footer')
    </footer>
</div>

@include('app:web::_layouts._scripts')
@include('app:web::_layouts._no-scripts')

</body>
</html>