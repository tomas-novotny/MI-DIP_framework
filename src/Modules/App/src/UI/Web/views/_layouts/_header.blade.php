<div class="header-top-wrapper fs-small">
    <div class="container">
        <div class="header-top grid--middle mt-3">
            <div class="links-info">
                @if(modules('authentication'))
                    @if($__user ?? false)
                        {{ $__user->getName() }}
                        <span class="text-muted font-weight-light">/</span>
                        <a href="{{ route('auth.logout') }}">Logout</a>
                    @else
                        <a href="{{ route('auth.login') }}">Login</a>
                    @endif
                @endif
                <span class="mx-2 text-muted font-weight-light">|</span>
                <a href="{{ route('catalog') }}">Catalog</a>
                <span class="mx-2 text-muted font-weight-light">|</span>
                <a href="{{ route('cart.items') }}">Cart</a>
            </div>
        </div>
        <hr/>
    </div>
</div>