<ul class="nav nav-pills language-bar-chooser">
    @foreach($_languages as $language)
        <li class="nav-item @if($language->getKey() == $__locale) active @endif">
            <a rel="alternate" hreflang="{{ $language->getKey() }}" href="{{ $language->url() }}">
                <span class="flag-icon flag-icon-{{ $language->flag }}"></span>
            </a>
        </li>
    @endforeach
</ul>