<div id="flash-messages">
    @if(session()->has('flash_notification'))
        @foreach(session()->get('flash_notification') as $message)
            <div class="alert alert-{{ array_get($message, 'level', 'info') }}">
                @if(array_get($message, 'important', false) === false)
                    <button type="button" class="close p-l-1" data-dismiss="alert" aria-hidden="true">&times;</button>
                @endif

                @if(array_has($message, 'title'))
                    <strong>{{ array_get($message, 'title') }}</strong>
                @endif

                {!! array_get($message, 'message') !!}
            </div>
        @endforeach
    @endif
</div>