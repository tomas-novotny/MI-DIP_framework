<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateSequencesTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('sequences', function (Blueprint $table) {
            $table->increments('id')->setUnsigned(true);
            $table->string('sequenceable_id');
            $table->string('sequenceable_type');
            $table->string('batch');
            $table->string('key');
            $table->integer('value')->setUnsigned(true)->setNotnull(false);
            $table->timestamps();
            
            $table->unique(['sequenceable_type', 'sequenceable_id', 'key', 'value'], 'sequences_unique');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->dropIfExists('sequences');
    }
}
