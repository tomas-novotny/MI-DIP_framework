<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateJobsTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('queue');
            $table->text('payload');
            $table->boolean('attempts')->setUnsigned(true);
            $table->integer('reserved_at')->setUnsigned(true)->setNotnull(false);
            $table->integer('available_at')->setUnsigned(true);
            $table->integer('created_at')->setUnsigned(true);
            
            $table->index(['queue', 'reserved_at']);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->dropIfExists('jobs');
    }
}
