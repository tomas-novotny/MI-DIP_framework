<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateSessionsTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('sessions', function (Blueprint $table) {
            $table->string('id');
            $table->integer('user_id')->setNotnull(false);
            $table->string('ip_address', 45)->setNotnull(false);
            $table->text('user_agent')->setNotnull(false);
            $table->text('payload');
            $table->integer('last_activity');
            
            $table->primary('id');
            $table->unique('id');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->dropIfExists('sessions');
    }
}
