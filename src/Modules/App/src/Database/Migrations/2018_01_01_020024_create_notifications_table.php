<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateNotificationsTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('notifications', function (Blueprint $table) {
            $table->string('id', 36);
            $table->string('type');
            $table->string('notifiable_id');
            $table->string('notifiable_type');
            $table->text('data');
            $table->timestamp('read_at')->setNotnull(false);
            $table->timestamps();
            
            $table->primary('id');
            $table->index(['notifiable_id', 'notifiable_type']);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->dropIfExists('notifications');
    }
}
