<?php

namespace Modules\App\Providers;

use Barryvdh\Cors\ServiceProvider as CorsServiceProvider;
use Bugsnag\BugsnagLaravel\BugsnagServiceProvider;
use Modules\Support\Parents\Providers\AbstractAutoloadServiceProvider;

class ServiceProvider extends AbstractAutoloadServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        EventServiceProvider::class,
        BugsnagServiceProvider::class,
        CorsServiceProvider::class,
    ];
}
