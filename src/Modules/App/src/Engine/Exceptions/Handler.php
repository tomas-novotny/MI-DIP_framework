<?php

namespace Modules\App\Engine\Exceptions;

use Exception as BaseException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as LaravelExceptionHandler;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response as LaravelResponse;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Modules\Support\Collection;
use Modules\Support\Exceptions\MissingJSONHeaderException;
use Modules\Support\Exceptions\ValidationFailedException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

class Handler extends LaravelExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];
    
    /**
     * A list of the internal exception types that should not be reported.
     *
     * @var array
     */
    protected $internalDontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
        ValidationFailedException::class,
    ];
    
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function report(BaseException $exception)
    {
        if ($this->shouldntReport($exception)) {
            return;
        }
        
        $this->reportToServices($exception);
        
        parent::report($exception);
    }
    
    /**
     *
     * Report or log an exception in reporters.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    private function reportToServices(BaseException $exception): void
    {
        // TODO: use tagged instances istead of config
        foreach (config('exceptions.reporters', []) as $reporterClass) {
            try {
                $reporter = $this->container->make($reporterClass);
                $reporter->report($exception);
            }
            catch (Throwable $reporterException) {
                // this cannot (shouldn't) fail, skip reporting to service
            }
        }
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, BaseException $exception)
    {
        // return back JSON format exception if request expect JSON
        if ($request->expectsJson() or $exception instanceof MissingJSONHeaderException) {
            $renderer = new JsonRenderer();
            
            return $renderer->renderJson($exception);
        }
        
        // return back token with token mismatch exception
        if ($exception instanceof TokenMismatchException) {
            return $this->tokenMismatchResponse();
        }
        
        // return back unauthorized response
        if ($exception instanceof AuthorizationException) {
            return $this->unauthorized($exception);
        }
        
        return parent::render($request, $exception);
    }
    
    /**
     * Create a Token Mismatch response.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function tokenMismatchResponse(): RedirectResponse
    {
        flash()->error(trans('app::messages.expired_session'));
        
        return redirect()->back()->withInput()->with('token', csrf_token());
    }
    
    /**
     * Convert an authorization exception into an unauthorized response.
     *
     * @param \Illuminate\Auth\Access\AuthorizationException $exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function unauthorized(AuthorizationException $exception): RedirectResponse
    {
        flash()->error(trans('app::authorization.messages.unauthorized'));
        
        if (config('app.debug')) {
            flash()->error($exception->getMessage());
        }
        
        $back = redirect()->back();
        
        // prevent infinite redirects
        if ($this->isSameUrlPath($back->getTargetUrl())) {
            return redirect()->route('home');
        }
        
        // redirect back
        return $back;
    }
    
    /**
     * Determinate if given url is the same as current
     *
     * @param string $url
     *
     * @return bool
     */
    private function isSameUrlPath(string $url): bool
    {
        try {
            $url = new Collection(parse_url($url));
            $url = $url->get('scheme').'://'.$url->get('host').':'.$url->get('port', 80).$url->get('path', '/');
        }
        catch (BaseException $e) {
            return false;
        }
        
        return $url === url('/').request()->getPathInfo();
    }
    
    /**
     * Render the given Http exception.
     *
     * @param \Symfony\Component\HttpKernel\Exception\HttpException $exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpException $exception)
    {
        $status = $exception->getStatusCode();
        
        if ($custom = $this->customErrorPage($status, $exception)) {
            return $custom;
        }
        
        return parent::renderHttpException($exception);
    }
    
    /**
     * Get custom error page
     *
     * @param int        $status
     * @param \Exception $exception
     *
     * @return \Illuminate\Http\Response|null
     */
    private function customErrorPage(int $status, BaseException $exception): ?LaravelResponse
    {
        $view = 'app:web::errors.'.(string) $status;
        
        if (view()->exists($view)) {
            return response()->view(
                $view,
                ['exception' => $exception->getMessage(), 'code' => $status, 'header' => 'errors'],
                $status,
                $exception instanceof HttpExceptionInterface ? $exception->getHeaders() : []
            );
        }
        
        return null;
    }
    
    /**
     * Create a Symfony response for the given exception.
     *
     * @param \Exception $exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertExceptionToResponse(BaseException $exception): SymfonyResponse
    {
        $flatten = FlattenException::create($exception);
        
        if (config('app.debug') === false and $custom = $this->customErrorPage($flatten->getStatusCode(), $exception)) {
            return $custom;
        }
        
        return parent::convertExceptionToResponse($exception);
    }
    
    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param \Illuminate\Http\Request                 $request
     * @param \Illuminate\Auth\AuthenticationException $exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function unauthenticated($request, AuthenticationException $exception): RedirectResponse
    {
        if (config('app.debug')) {
            flash()->error($exception->getMessage());
        }
        
        return redirect()->guest(route('auth.login'));
    }
}
