<?php

namespace Modules\App\Engine\Exceptions\Reporters;

use Exception;

interface Reporter
{
    /**
     * Report exception to service.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function report(Exception $exception): void;
}
