<?php

namespace Modules\App\Engine\Exceptions\Reporters;

use Bugsnag\Report;
use Exception;

class BugsnagReporter implements Reporter
{
    /**
     * Report exception to Bugsnag.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function report(Exception $exception): void
    {
        // send exception to bugsnag
        if (config('bugsnag.api_key')) {
            $bugsnag = $this->getClient();
            // set url
            $bugsnag->setHostname(config('app.url'));
            
            //            TODO: resolve version
            //            $version = Application::VERSION;
            //            if ($build = env('BUILD_ID')) {
            //                $version .= ':'.$build;
            //            }
            $version = 'v0.0.1';
            $bugsnag->setAppVersion($version);
            
            $bugsnag->registerCallback(function (Report $report) {
                try {
                    if (auth()->check()) {
                        $user = auth()->user();
                        
                        $report->setUser([
                            'id'   => $user->getAuthIdentifier(),
                            'name' => $user->getAuthIdentifierName(),
                        ]);
                    }
                }
                catch (Exception $_exception) {
                    //
                }
            });
            
            $bugsnag->notifyException($exception);
        }
    }
    
    /**
     * Get Bugsnag client.
     *
     * @return \Bugsnag\Client|\Bugsnag\Configuration
     */
    private function getClient()
    {
        return app('bugsnag');
    }
}
