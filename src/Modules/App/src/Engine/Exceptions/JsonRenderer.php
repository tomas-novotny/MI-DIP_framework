<?php

namespace Modules\App\Engine\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class JsonRenderer
{
    /**
     * Render JSON response
     *
     * @param \Throwable $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderJson(Throwable $exception): JsonResponse
    {
        if ($exception instanceof TokenMismatchException) {
            return $this->renderTokenMismatchException($exception);
        }
        
        if ($exception instanceof ValidationException) {
            return $this->renderValidationException($exception);
        }
        
        if ($exception instanceof AuthenticationException) {
            return $this->renderAuthenticationException($exception);
        }
        
        if ($exception instanceof AuthorizationException) {
            return $this->renderAuthorizationException($exception);
        }
        
        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->renderMethodNotAllowedException($exception);
        }
        
        if ($exception instanceof HttpExceptionInterface) {
            return $this->renderHttpException($exception);
        }
        
        return $this->renderException($exception);
    }
    
    /**
     * Render AuthenticationException.
     *
     * @param \Illuminate\Auth\AuthenticationException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderAuthenticationException(AuthenticationException $exception): JsonResponse
    {
        $status_code = Response::HTTP_UNAUTHORIZED;
        $errors      = [];
        
        $errors[] = [
            'status' => $status_code,
            'code'   => $exception->getCode(),
            'title'  => 'Missing or invalid Access Token.',
            'detail' => $exception->getMessage(),
        ];
        
        return $this->response($exception, $status_code, $this->prepareResponseData($errors));
    }
    
    /**
     * Render AuthorizationException.
     *
     * @param \Illuminate\Auth\Access\AuthorizationException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderAuthorizationException(AuthorizationException $exception): JsonResponse
    {
        $status_code = Response::HTTP_FORBIDDEN;
        $errors      = [];
        
        $errors[] = [
            'status' => $status_code,
            'code'   => $exception->getCode(),
            'title'  => 'You have no access to this resource.',
            'detail' => $exception->getMessage(),
        ];
        
        return $this->response($exception, $status_code, $this->prepareResponseData($errors));
    }
    
    /**
     * Render MethodNotAllowedHttpException.
     *
     * @param \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderMethodNotAllowedException(MethodNotAllowedHttpException $exception): JsonResponse
    {
        $status_code = Response::HTTP_METHOD_NOT_ALLOWED;
        $errors      = [];
        
        $errors[] = [
            'status' => $status_code,
            'code'   => $exception->getCode(),
            'title'  => '405 Method Not Allowed.',
            'detail' => $exception->getMessage(),
        ];
        
        return $this->response($exception, $status_code, $this->prepareResponseData($errors));
    }
    
    /**
     * Render ValidationException.
     *
     * @param \Illuminate\Validation\ValidationException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderValidationException(ValidationException $exception): JsonResponse
    {
        $status_code = Response::HTTP_UNPROCESSABLE_ENTITY;
        
        $errors = $exception->validator->errors()->getMessages();
        
        return $this->response($exception, $status_code, $this->prepareResponseData($errors));
    }
    
    /**
     * Render TokenMismatchException.
     *
     * @param \Illuminate\Session\TokenMismatchException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderTokenMismatchException(TokenMismatchException $exception): JsonResponse
    {
        $status_code = 419;
        $errors      = [];
        
        $errors[] = [
            'status' => $status_code,
            'code'   => $exception->getCode(),
            'title'  => $exception->getMessage() ?: 'Expired session.',
        ];
        
        return $this->response($exception, $status_code, $this->prepareResponseData($errors));
    }
    
    /**
     * Render HttpException.
     *
     * @param \Symfony\Component\HttpKernel\Exception\HttpException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderHttpException(HttpException $exception): JsonResponse
    {
        // if this exception is an instance of HttpException get the HTTP status else use the default
        $status_code = $exception->getStatusCode();
        $errors      = [];
        
        $errors[] = [
            'status' => $status_code,
            'code'   => $exception->getCode(),
            'title'  => $exception->getMessage(),
        ];
        
        return $this->response($exception, $status_code, $this->prepareResponseData($errors));
    }
    
    /**
     * Render Exception.
     *
     * @param \Throwable $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderException(Throwable $exception): JsonResponse
    {
        $status_code = Response::HTTP_INTERNAL_SERVER_ERROR;
        $errors      = [];
        
        $errors[] = [
            'status' => $status_code,
            'code'   => $exception->getCode(),
            'title'  => $exception->getMessage(),
        ];
        
        return $this->response($exception, $status_code, $this->prepareResponseData($errors));
    }
    
    /**
     * Prepare response data.
     *
     * @param array $errors
     * @param array $data
     *
     * @return array
     */
    private function prepareResponseData(array $errors, array $data = []): array
    {
        // add errors to response
        if (empty($errors) === false) {
            $data['errors'] = $errors;
        }
        
        return $data;
    }
    
    /**
     * JSON response.
     *
     * @param \Throwable $exception
     * @param int        $status_code
     * @param array      $response
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function response(Throwable $exception, int $status_code, array $response): JsonResponse
    {
        // add debug
        if (config('app.debug')) {
            $this->addDebugData($response, $exception);
        }
        
        // return a JSON response with the response array and status code
        return response()->json($response, $status_code);
    }
    
    /**
     * Add debug data.
     *
     * @param array      $response
     * @param \Throwable $exception
     * @param int        $maxTraceDepth
     * @param int        $maxPreviousDepth
     *
     * @return void
     */
    private function addDebugData(
        array & $response,
        Throwable $exception,
        $maxTraceDepth = 20,
        $maxPreviousDepth = 5
    ): void {
        $response['debug'] = [
            'exception' => get_class($exception),
            'message'   => $exception->getMessage(),
            'previous'  => [],
            'trace'     => array_slice(explode("\n", $exception->getTraceAsString()), 0, $maxTraceDepth),
        ];
        
        $depth = 0;
        while ($exception = $exception->getPrevious() and $depth++ < $maxPreviousDepth) {
            $response['debug']['previous'][] = get_class($exception).': '.$exception->getMessage();
        }
    }
}
