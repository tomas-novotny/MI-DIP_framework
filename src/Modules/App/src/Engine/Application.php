<?php

namespace Modules\App\Engine;

class Application
{
    /**
     * Inspishop version
     *
     * @var string
     */
    const VERSION = '2018.1';
}
