<?php

namespace Modules\App\Engine\Http;

use Barryvdh\Cors\HandleCors;
use Illuminate\Foundation\Http\Kernel as LaravelHttpKernel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Http\Middleware\SetCacheHeaders;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Session\Middleware\StartSession;
use Modules\App\Engine\Http\Middleware\AcceptContentType;
use Modules\App\Engine\Http\Middleware\CacheHeaders;
use Modules\App\Engine\Http\Middleware\ShareLocalization;
use Modules\App\Http\Middleware\TrimStrings;
use Modules\App\Http\Middleware\TrustProxies;

class Kernel extends LaravelHttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        ShareLocalization::class,
        CacheHeaders::class,
        SetCacheHeaders::class,
        CheckForMaintenanceMode::class,
        ValidatePostSize::class,
        TrimStrings::class,
        ConvertEmptyStringsToNull::class,
        TrustProxies::class,
    ];
    
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web'       => [
            StartSession::class,
        ],
        'stateless' => [
            //
        ],
        'stateful'  => [
            //
        ],
        'api'       => [
            HandleCors::class,
            AcceptContentType::class,
            'throttle:60,1',
        ],
    ];
    
    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'bindings' => SubstituteBindings::class,
        'throttle' => ThrottleRequests::class,
    ];
}
