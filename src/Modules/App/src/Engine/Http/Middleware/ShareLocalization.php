<?php

namespace Modules\App\Engine\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Modules\Support\Parents\AbstractMiddleware;
use Symfony\Component\HttpFoundation\Response;

class ShareLocalization extends AbstractMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $locale = config('app.locale');
        
        $this->shareLocale($request, $locale);
        
        return $this->response($request, $next);
    }
    
    /**
     * Share locale to all possible configurations.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $locale
     *
     * @return void
     */
    private function shareLocale(Request $request, string $locale): void
    {
        // set locale to request
        $request->setLocale($locale);
        
        // set locale to translator
        trans()->setLocale($locale);
        
        // share locale to views as global variable
        view()->share('__locale', $locale);
        
        // set locale to Carbon date class
        Carbon::setLocale($locale);
    }
}
