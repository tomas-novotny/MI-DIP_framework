<?php

namespace Modules\App\Engine\Http\Middleware;

use App;
use Closure;
use Config;
use Illuminate\Http\Request;
use Modules\Support\Exceptions\MissingJSONHeaderException;
use Modules\Support\Parents\AbstractMiddleware;
use Symfony\Component\HttpFoundation\Response;

class AcceptContentType extends AbstractMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $contentType  = 'application/json';
        $acceptHeader = $request->header('accept');
        
        if (strpos($acceptHeader, $contentType) === false) {
            if (config('app.env') == 'local') {
                $request->headers->set('accept', $contentType);
            }
            else {
                throw new MissingJSONHeaderException;
            }
        }
        
        // get the response after the request is done
        $response = $this->response($request, $next);
        
        // set Content Languages header in the response
        $response->headers->set('Content-Type', $contentType);
        
        // return the response
        return $response;
    }
}
