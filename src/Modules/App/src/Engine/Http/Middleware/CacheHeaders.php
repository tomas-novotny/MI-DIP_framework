<?php

namespace Modules\App\Engine\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Support\Parents\AbstractMiddleware;
use Session;
use Symfony\Component\HttpFoundation\Response;

class CacheHeaders extends AbstractMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        // if routes without specific (stateless/stateful) middleware should use http cache
        $default = config('cache.http.default');
        
        view()->share('__stateless', $default);
        
        $response = $this->response($request, $next);
        
        $stateless = $response->headers->get('X-No-Session', (int) $default);
        
        if ($stateless) {
            $this->setCacheControlHeaders($request, $response);
            $this->removeCookies($response);
        };
        
        $response->headers->remove('X-No-Session');
        
        return $response;
    }
    
    /**
     * Set cache headers
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\Response $response
     *
     * @return \Illuminate\Http\Response
     */
    private function setCacheControlHeaders(Request $request, Response $response): Response
    {
        if ($request->isMethodCacheable() and $response->getContent() !== "") {
            return $response;
        }
        
        $options = [
            'public' => true,
            'max_age' => config('cache.http.lifetime') * 60,
            'etag'    => md5($response->getContent()),
        ];
        
        $response->setCache($options);
        $response->isNotModified($request);
        
        return $response;
    }
    
    /**
     * Remove all cookies from response
     *
     * @param  \Illuminate\Http\Response $response
     *
     * @return  \Illuminate\Http\Response
     */
    private function removeCookies(Response $response): Response
    {
        $cookies = $response->headers->getCookies();
        
        foreach ($cookies as $cookie) {
            $response->headers->removeCookie($cookie->getName(), $cookie->getPath(), $cookie->getDomain());
        }
        
        return $response;
    }
}
