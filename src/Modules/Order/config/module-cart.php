<?php

use Modules\Order\Model\Entities\Concretes\Doctrine\Cart as DoctrineCart;
use Modules\Order\Model\Entities\Concretes\Eloquent\Cart as EloquentCart;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Factories\Contracts\CartFactoryInterface;
use Modules\Order\Model\Factories\Contracts\CartItemFactoryInterface;
use Modules\Order\Model\Factories\Contracts\DoctrineCartFactory;
use Modules\Order\Model\Factories\Contracts\DoctrineCartItemFactory;
use Modules\Order\Model\Factories\Contracts\EloquentCartFactory;
use Modules\Order\Model\Factories\Contracts\EloquentCartItemFactory;
use Modules\Order\Model\Repositories\Concretes\DoctrineCartRepository;
use Modules\Order\Model\Repositories\Concretes\EloquentCartRepository;
use Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface;

return [
    'bindings' => [
        'eloquent' => [
            CartRepositoryInterface::class  => EloquentCartRepository::class,
            CartFactoryInterface::class     => EloquentCartFactory::class,
            CartItemFactoryInterface::class => EloquentCartItemFactory::class,
            CartInterface::class            => EloquentCart::class,
            ItemInterface::class            => EloquentCart\Item::class,
        ],
        'doctrine' => [
            CartRepositoryInterface::class  => DoctrineCartRepository::class,
            CartFactoryInterface::class     => DoctrineCartFactory::class,
            CartItemFactoryInterface::class => DoctrineCartItemFactory::class,
            CartInterface::class            => DoctrineCart::class,
            ItemInterface::class            => DoctrineCart\Item::class,
        ],
    ],
    'views'    => [
        'cart-items' => 'order:web::cart.items',
    ],
];