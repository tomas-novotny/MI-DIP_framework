<?php

use Illuminate\Routing\Router;
use Modules\Order\UI\Web\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

web_router(function (Router $router) {
    $router->group(['middleware' => ['stateful']], function (Router $router) {
        
        $router->post(trans('order::routes.cart.add'), [
            'as'   => 'cart.add',
            'uses' => CartController::class.'@addItem',
        ]);
    
        $router->patch(trans('order::routes.cart.update'), [
            'as'   => 'cart.update',
            'uses' => CartController::class.'@updateItem',
        ]);
    
        $router->delete(trans('order::routes.cart.delete'), [
            'as'   => 'cart.delete',
            'uses' => CartController::class.'@deleteItem',
        ]);
    
        $router->get(trans('order::routes.cart.items'), [
            'as'   => 'cart.items',
            'uses' => CartController::class.'@showItems',
        ]);
    });
});
