<?php

namespace Modules\Order\UI\Web\Requests;

use Modules\Support\Request;

class AddItemToCartRequest extends Request
{
    public function rules(): array
    {
        return [
            'product_id' => ['required'],
            'amount'     => ['required', 'integer'],
        ];
    }
}
