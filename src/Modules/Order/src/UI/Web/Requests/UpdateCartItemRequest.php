<?php

namespace Modules\Order\UI\Web\Requests;

use Modules\Support\Request;

class UpdateCartItemRequest extends Request
{
    public function rules(): array
    {
        return [
            'amount' => [
                'required',
                'integer',
                'min:1',
            ],
        ];
    }
}
