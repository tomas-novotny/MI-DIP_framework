<?php

namespace Modules\Order\UI\Web\Controllers;

use Exception;
use Modules\Order\Services\Actions\Cart\AddItemToCartAction;
use Modules\Order\Services\Actions\Cart\DeleteItemFromCartAction;
use Modules\Order\Services\Actions\Cart\GetOrderPreviewAction;
use Modules\Order\Services\Actions\Cart\UpdateCartItemAction;
use Modules\Order\UI\Web\Requests\AddItemToCartRequest;
use Modules\Order\UI\Web\Requests\UpdateCartItemRequest;
use Modules\Pricing\Model\Values\Price;
use Modules\Support\Collection;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Parents\Controllers\AbstractWebController;

class CartController extends AbstractWebController
{
    /**
     * Add shoppable item to cart.
     *
     * @param \Modules\Order\UI\Web\Requests\AddItemToCartRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addItem(AddItemToCartRequest $request)
    {
        try {
            $this->dispatchService(
                new AddItemToCartAction($request->get('product_id'), $request->get('amount'))
            );
            
            $this->addSuccessMessage('order::messages.cart.added');
        }
        catch (Exception $exception) {
            $this->addErrorMessage($exception->getMessage());
            
            return $this->redirect()->back();
        }
        
        return $this->redirect()->route('cart.items');
    }
    
    /**
     * Update cart item.
     *
     * @param string                                               $id
     * @param \Modules\Order\UI\Web\Requests\UpdateCartItemRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateItem(string $id, UpdateCartItemRequest $request)
    {
        try {
            $this->dispatchService(new UpdateCartItemAction($id, (int) $request->get('amount')));
            
            $this->addSuccessMessage('order::messages.cart.updated');
        }
        catch (Exception $exception) {
            $this->addErrorMessage($exception->getMessage());
        }
        
        return $this->redirect()->route('cart.items');
    }
    
    /**
     * Remove item from cart.
     *
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteItem(string $id)
    {
        try {
            $this->dispatchService(new DeleteItemFromCartAction($id));
            
            $this->addSuccessMessage('order::messages.cart.deleted');
        }
        catch (Exception $exception) {
            $this->addErrorMessage($exception->getMessage());
        }
        
        return $this->redirect()->route('cart.items');
    }
    
    /**
     * Show shopping cart content.
     *
     * @return \Illuminate\View\View
     */
    public function showItems()
    {
        try {
            /* @var \Modules\Order\Model\Aggregates\OrderPreview $cartDetail */
            $cartDetail = $this->dispatchService(new GetOrderPreviewAction());
            
            $cart       = $cartDetail->getCart();
            $items      = $cartDetail->getItems();
            $itemsPrice = $cartDetail->getPrice();
        }
        catch (ResourceNotFoundException $exception) {
            $cart       = null;
            $items      = new Collection();
            $itemsPrice = new Price(0.0, 0.0);
        }
        
        return $this->view(config('module-cart.views.cart-items'), compact('cart', 'items', 'itemsPrice'));
    }
}
