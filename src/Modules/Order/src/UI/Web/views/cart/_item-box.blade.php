<div data-test-type="cart-item-box" data-test-value="{{ $item->getKey() }}"
     data-test-attr-product="{{ $product->getKey() }}">
    <div class="row">
        <div class="col-sm-4">
            <h5 class="m-0">
                #{{ $position }}
                <a data-test-type="shoppable-link" href="{{ route('catalog') }}">
                    <span data-test-type="item-name">{{ $product->getDisplayName() }}</span>
                </a>
            </h5>
        </div>
        <div class="col-sm-2">
            <div class="text-right">
                <p class="m-0" data-test-type="item-unit-price-with-vat">
                    {{ currency_format($price->getUnitPrice()->getPriceWithVat()) }}
                </p>
                {{--<small class="font-weight-light text-muted" data-test-type="item-unit-price-without-vat">--}}
                    {{--{{ currency_format($price->getUnitPrice()->getPriceWithoutVat()) }}--}}
                {{--</small>--}}
            </div>
        </div>
        <div class="col-sm-3">
            <form action="{{ route('cart.update', [$item->getKey()]) }}" method="post" data-test-type="update-item-form">
                @csrf
                @method('patch')

                <div class="row">
                    <div class="col-8">
                        <input type="number" name="amount" data-test-type="item-amount" value="{{ $item->getAmount() }}" class="form-control form-control-sm"/>
                    </div>
                    <div class="col-4 pl-0">
                        <button type="submit" name="update" class="btn btn-outline-primary btn-sm btn-block">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-2">
            <div class="text-right">
                <p class="m-0" data-test-type="item-total-price-with-vat">
                    {{ currency_format($price->getTotalPrice()->getPriceWithVat()) }}
                </p>
                {{--<small class="font-weight-light text-muted" data-test-type="item-total-price-without-vat">--}}
                    {{--{{ currency_format($price->getTotalPrice()->getPriceWithoutVat()) }}--}}
                {{--</small>--}}
            </div>
        </div>
        <div class="col-sm-1">
            <form action="{{ route('cart.delete', [$item->getKey()]) }}" method="post" class="form-inline"
                  data-test-type="delete-item-form">
                @csrf
                @method('delete')

                <button type="submit" name="delete" class="btn btn-outline-danger btn-sm">
                    Delete
                </button>
            </form>
        </div>
    </div>
</div>
