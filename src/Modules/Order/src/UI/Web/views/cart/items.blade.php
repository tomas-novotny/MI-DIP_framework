@extends('app:web::_layouts.master')

@section('title'){{ trans('order::templates/cart.shopping_cart') }} | @parent @endsection

@section('content')

    <h1>
        {{ trans('order::templates/cart.shopping_cart') }}
    </h1>

    @if($items->count() > 0)
        <div data-test-type="cart-items-box">
            @foreach($items as $i => $itemDetail)
                <hr/>
                @include('order:web::cart._item-box', [
                   'item' => $itemDetail->getItem(),
                   'product' => $itemDetail->getItem()->getProduct(),
                   'price' => $itemDetail->getPrice(),
                   'position' => $i + 1
               ])
            @endforeach
        </div>

        <hr/>

        <div class="text-right">
            <p class="h3">
                {{ currency_format($itemsPrice->getPriceWithVat()) }}
            </p>
            <p class="text-muted">
                {{ currency_format($itemsPrice->getPriceWithoutVat()) }}
            </p>
        </div>
    @else
        <div class="h3 text-muted font-weight-light text-center m-5">No items</div>

        <hr/>
        <p class="text-center">
            <a href="{{ route('catalog') }}" data-test-type="products-link">Add more items</a>
        </p>
    @endif

@endsection
