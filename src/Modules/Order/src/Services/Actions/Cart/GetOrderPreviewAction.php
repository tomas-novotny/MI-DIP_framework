<?php

namespace Modules\Order\Services\Actions\Cart;

use Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Order\Model\Aggregates\CartItemDetail;
use Modules\Order\Model\Aggregates\OrderPreview;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Services\Tasks\Cart\CalculateQuantifiedProductPriceTask;
use Modules\Order\Services\Tasks\Cart\GetCartTask;
use Modules\Pricing\Model\Aggregates\QuantifiedPrice;
use Modules\Pricing\Model\Values\Price;
use Modules\Support\Collection;

/**
 * Class GetOrderPreviewAction
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask
 * @uses    \Modules\Order\Services\Tasks\Cart\CalculateQuantifiedProductPriceTask
 * @uses    \Modules\Order\Services\Tasks\Cart\GetCartTask
 */
class GetOrderPreviewAction extends AbstractAction
{
    /**
     * GetOrderPreviewAction
     *
     * @return \Modules\Order\Model\Aggregates\OrderPreview
     */
    public function handle()
    {
        $cart = $this->getCart();
        
        $items       = $cart->getItems();
        $itemDetails = new Collection();
        $itemsPrice  = new Price(0.0, 0.0);
        
        foreach ($items as $item) {
            
            // calculate item quantified price
            $itemQuantifiedPrice = $this->calculateItemQuantifiedPrice($item);
            // add to items price
            $itemsPrice = $itemsPrice->add($itemQuantifiedPrice->getTotalPrice());
            
            $itemDetails->push(new CartItemDetail($item, $itemQuantifiedPrice));
        }
        
        $orderPreview = new OrderPreview($cart, $itemDetails, $itemsPrice);
        
        return $orderPreview;
    }
    
    /**
     * @param \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface $item
     *
     * @return \Modules\Pricing\Model\Aggregates\QuantifiedPrice
     */
    private function calculateItemQuantifiedPrice(ItemInterface $item): QuantifiedPrice
    {
        return $this->dispatchTask(new CalculateQuantifiedProductPriceTask($item->getProduct(), $item->getAmount()));
    }
    
    /**
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    private function getCart(): CartInterface
    {
        $user = $this->dispatchTask(new FindAuthenticatedUserTask());
        
        $cart = $this->dispatchTask(new GetCartTask($user));
        
        return $cart;
    }
}

