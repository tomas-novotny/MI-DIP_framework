<?php

namespace Modules\Order\Services\Actions\Cart;

use Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Entities\Contracts\ShoppableInterface;
use Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface;
use Modules\Order\Services\Tasks\Cart\GetCartTask;
use Modules\Order\Services\Tasks\IncreaseReservedStockTask;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;

/**
 * Class DeleteItemFromCartAction
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask
 * @uses    \Modules\Order\Services\Tasks\Cart\GetCartTask
 * @uses    \Modules\Order\Services\Tasks\IncreaseReservedStockTask
 *
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 */
class DeleteItemFromCartAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        UpdateResourceFailedException::class,
        ResourceNotFoundException::class,
        InvalidArgumentException::class,
    ];
    
    /**
     * Item key.
     *
     * @var string
     */
    private $key;
    
    /**
     * Shopping cart repository.
     *
     * @var \Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface
     */
    private $cartRepository;
    
    /**
     * DeleteItemFromCartAction constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->cartRepository = $this->resolve(CartRepositoryInterface::class);
        
        $this->key = $key;
    }
    
    /**
     * Add item to cart.
     *
     * @return void
     *
     * @throws \Modules\Order\Exceptions\NotEnoughStockException
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        // get shopping cart
        $cart = $this->getCart();
        
        // get item
        $item = $cart->getItemByKey($this->key);
        
        // increase stock
        $this->increaseStock($item->getProduct(), $item->getAmount());
        
        // remove item from cart
        $cart->removeItem($item);
        $this->cartRepository->persist($cart);
    }
    
    /**
     * Increase stock.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    private function increaseStock(ProductInterface $product, int $amount): void
    {
        $this->dispatchTask(new IncreaseReservedStockTask($product, $amount));
    }
    
    /**
     * Get cart instance for authenticated user.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getCart(): CartInterface
    {
        $user = $this->dispatchTask(new FindAuthenticatedUserTask());
        
        return $this->dispatchTask(new GetCartTask($user));
    }
}
