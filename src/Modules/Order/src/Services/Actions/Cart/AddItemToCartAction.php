<?php

namespace Modules\Order\Services\Actions\Cart;

use Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Services\Tasks\GetSaleableProductByIdTask;
use Modules\Order\Exceptions\NotEnoughStockException;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Entities\Contracts\ShoppableInterface;
use Modules\Order\Services\Tasks\Cart\AddItemToCartTask;
use Modules\Order\Services\Tasks\Cart\CreateCartTask;
use Modules\Order\Services\Tasks\Cart\GetCartTask;
use Modules\Order\Services\Tasks\ReduceReservedStockTask;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class AddItemToCartAction
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask
 * @uses    \Modules\Catalog\Services\Tasks\GetSaleableProductByIdTask
 * @uses    \Modules\Order\Services\Tasks\Cart\AddItemToCartTask
 * @uses    \Modules\Order\Services\Tasks\Cart\CreateCartTask
 * @uses    \Modules\Order\Services\Tasks\Cart\GetCartTask
 * @uses    \Modules\Order\Services\Tasks\ReduceReservedStockTask
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 * @throws  \Modules\Order\Exceptions\NotEnoughStockException
 */
class AddItemToCartAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
        InvalidArgumentException::class,
        NotEnoughStockException::class,
    ];
    
    /**
     * Shoppable model class name.
     *
     * @var string
     */
    private $productId;
    
    /**
     * Item amount.
     *
     * @var int
     */
    private $amount;
    
    /**
     * AddItemToCartAction constructor.
     *
     * @param string $productId
     * @param int    $amount
     */
    public function __construct(string $productId, int $amount)
    {
        $this->productId = $productId;
        $this->amount    = $amount;
    }
    
    /**
     * Add item to cart.
     *
     * @return void
     *
     * @throws \Modules\Order\Exceptions\NotEnoughStockException
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        // get product
        $product = $this->getProduct();
        
        // get shopping cart
        $cart = $this->getShoppingCart();
        
        // try to reduce stock
        $this->reserveStockInProduct($product);
        
        // add item to cart
        $this->addItemToCart($cart, $product);
    }
    
    /**
     * Reduce stock.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     *
     * @throws \Modules\Order\Exceptions\NotEnoughStockException
     */
    private function reserveStockInProduct(ProductInterface $product): void
    {
        $this->dispatchTask(new ReduceReservedStockTask($product, $this->amount));
    }
    
    /**
     * Add item to cart.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface      $cart
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     */
    private function addItemToCart(CartInterface $cart, ProductInterface $product): void
    {
        // if item should be always as a new line
        $newItem = (bool) config('cart.new-item');
        
        // create item
        $this->dispatchTask(new AddItemToCartTask($cart, $product, $this->amount, $newItem));
    }
    
    /**
     * Get product instance.
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    private function getProduct(): ProductInterface
    {
        $product = $this->dispatchTask(new GetSaleableProductByIdTask($this->productId));
        
        return $product;
    }
    
    /**
     * Get cart instance for authenticated user.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    private function getShoppingCart(): CartInterface
    {
        $user = $this->dispatchTask(new FindAuthenticatedUserTask());
        
        try {
            return $this->dispatchTask(new GetCartTask());
        }
        catch (ResourceNotFoundException $exception) {
            return $this->dispatchTask(new CreateCartTask($user));
        }
    }
}
