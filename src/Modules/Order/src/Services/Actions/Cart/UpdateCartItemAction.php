<?php

namespace Modules\Order\Services\Actions\Cart;

use Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Exceptions\NotEnoughStockException;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Entities\Contracts\ShoppableInterface;
use Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface;
use Modules\Order\Services\Tasks\Cart\GetCartTask;
use Modules\Order\Services\Tasks\IncreaseReservedStockTask;
use Modules\Order\Services\Tasks\ReduceReservedStockTask;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;

/**
 * Class UpdateCartItemAction
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\User\FindAuthenticatedUserTask
 * @uses    \Modules\Order\Services\Tasks\Cart\GetCartTask
 * @uses    \Modules\Order\Services\Tasks\IncreaseReservedStockTask
 * @uses    \Modules\Order\Services\Tasks\ReduceReservedStockTask
 *
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 * @throws  \Modules\Order\Exceptions\NotEnoughStockException
 */
class UpdateCartItemAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        UpdateResourceFailedException::class,
        ResourceNotFoundException::class,
        InvalidArgumentException::class,
        NotEnoughStockException::class,
    ];
    
    /**
     * Item key.
     *
     * @var string
     */
    private $key;
    
    /**
     * New amount.
     *
     * @var int
     */
    private $amount;
    
    /**
     * Shopping cart repository.
     *
     * @var \Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface
     */
    private $cartRepository;
    
    /**
     * UpdateCartItemAction constructor.
     *
     * @param string $key
     * @param int    $amount
     */
    public function __construct(string $key, int $amount)
    {
        $this->cartRepository = $this->resolve(CartRepositoryInterface::class);
        
        $this->key    = $key;
        $this->amount = $amount;
    }
    
    /**
     * Update cart item.
     *
     * @return void
     *
     * @throws \Modules\Order\Exceptions\NotEnoughStockException
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        if ($this->amount <= 0) {
            throw new InvalidArgumentException('Reduced amount should be more than zero.');
        }
        
        // get cart
        $cart = $this->getCart();
        
        // get item
        $item = $cart->getItemByKey($this->key);
        
        // increase stock
        if ($this->amount > $item->getAmount()) {
            $this->reduceStock($item->getProduct(), $this->amount - $item->getAmount());
        }
        // reduce stock
        elseif ($this->amount < $item->getAmount()) {
            $this->increaseStock($item->getProduct(), $item->getAmount() - $this->amount);
        }
        // else no change
        
        // set amount
        $item->setAmount($this->amount);
        
        // persist change
        $this->cartRepository->persist($cart);
    }
    
    /**
     * Reduce stock.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     *
     * @return void
     *
     * @throws \Modules\Order\Exceptions\NotEnoughStockException
     */
    private function reduceStock(ProductInterface $product, int $amount): void
    {
        $this->dispatchTask(new ReduceReservedStockTask($product, $amount));
    }
    
    /**
     * Increase stock.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    private function increaseStock(ProductInterface $product, int $amount): void
    {
        $this->dispatchTask(new IncreaseReservedStockTask($product, $amount));
    }
    
    /**
     * Get cart instance for authenticated user.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getCart(): CartInterface
    {
        $user = $this->dispatchTask(new FindAuthenticatedUserTask());
        
        return $this->dispatchTask(new GetCartTask($user));
    }
}
