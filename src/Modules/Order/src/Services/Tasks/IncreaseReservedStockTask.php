<?php

namespace Modules\Order\Services\Tasks;

use Modules\Bus\Parents\AbstractTask;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface;
use Modules\Support\Exceptions\InvalidArgumentException;

/**
 * Class IncreaseReservedStockTask
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 */
class IncreaseReservedStockTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        InvalidArgumentException::class,
    ];
    
    /**
     * Product repository.
     *
     * @var \Modules\Catalog\Model\Repositories\Contracts\ProductRepositoryInterface
     */
    private $productRepository;
    
    /**
     * Product instance.
     *
     * @var \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    private $product;
    
    /**
     * Amount
     *
     * @var int
     */
    private $amount;
    
    /**
     * IncreaseReservedStockTask constructor.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     */
    public function __construct(ProductInterface $product, int $amount)
    {
        $this->productRepository = $this->resolve(ProductRepositoryInterface::class);
        
        $this->product = $product;
        $this->amount  = $amount;
    }
    
    /**
     * Reduce stock.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        if ($this->amount <= 0) {
            throw new InvalidArgumentException('Increased amount should be more than zero.');
        }
        
        $current = $this->product->getStock();
        
        $this->product->setStock($current + $this->amount);
        
        $this->productRepository->persist($this->product);
    }
}
