<?php

namespace Modules\Order\Services\Tasks\Cart;

use Modules\Bus\Parents\AbstractTask;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Factories\Contracts\CartFactoryInterface;
use Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface;
use Modules\User\Model\Entities\Contracts\UserInterface;

/**
 * Class CreateCartTask
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class CreateCartTask extends AbstractTask
{
    /**
     * Shopping cart factory.
     *
     * @var \Modules\Order\Model\Factories\Contracts\CartFactoryInterface
     */
    private $cartFactory;
    
    /**
     * Shopping cart repository.
     *
     * @var \Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface
     */
    private $cartRepository;
    
    /**
     * Customer.
     *
     * @var \Modules\User\Model\Entities\Contracts\UserInterface|null
     */
    private $user;
    
    /**
     * CreateCartTask constructor.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface|null $user
     */
    public function __construct(UserInterface $user = null)
    {
        $this->cartFactory    = $this->resolve(CartFactoryInterface::class);
        $this->cartRepository = $this->resolve(CartRepositoryInterface::class);
        
        $this->user = $user;
    }
    
    /**
     * Get cart instance.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function handle()
    {
        $cart = $this->user !== null
            ? $this->cartFactory->newCartForUser($this->user)
            : $this->cartFactory->newCartForGuest();
        
        $this->cartRepository->persist($cart);
        
        $this->setGuestCartCookie($cart);
        
        return $cart;
    }
    
    /**
     * Set guest shopping cart cookie.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface $cart
     *
     * @return void
     */
    private function setGuestCartCookie(CartInterface $cart): void
    {
        set_cookie(CartInterface::GUEST_CART_COOKIE, $cart->getKey(), 43200, false, true);
    }
}
