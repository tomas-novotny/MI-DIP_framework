<?php

namespace Modules\Order\Services\Tasks\Cart;

use Modules\Bus\Parents\AbstractTask;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Catalog\Services\ProductPriceCalculation;
use Modules\Pricing\Model\Aggregates\QuantifiedPrice;
use Modules\Pricing\Model\Values\Price;
use Modules\Pricing\Model\Values\Vat;
use Modules\Pricing\Services\PriceCalculation;

/**
 * Class CalculateQuantifiedProductPriceTask
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class CalculateQuantifiedProductPriceTask extends AbstractTask
{
    /**
     * Shopping cart repository.
     *
     * @var \Modules\Catalog\Services\ProductPriceCalculation
     */
    private $productPriceCalculation;
    
    /**
     * @var \Modules\Pricing\Services\PriceCalculation
     */
    private $priceCalculation;
    
    /**
     * @var \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    private $product;
    
    /**
     * @var int
     */
    private $amount;
    
    /**
     * CalculateQuantifiedProductPriceTask constructor.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     */
    public function __construct(ProductInterface $product, int $amount)
    {
        $this->productPriceCalculation = $this->resolve(ProductPriceCalculation::class);
        $this->priceCalculation        = $this->resolve(PriceCalculation::class);
        
        $this->product = $product;
        $this->amount  = $amount;
    }
    
    /**
     * @return mixed|\Modules\Pricing\Model\Aggregates\QuantifiedPrice
     */
    public function handle()
    {
        $productPrice = $this->calculateProductPrice();
        $productVat   = $this->getProductVat();
        
        $quantifiedPrice = new QuantifiedPrice(
            $productPrice,
            $this->calculateTotalPrice($productPrice, $this->amount, $productVat),
            $productVat
        );
        
        return $quantifiedPrice;
    }
    
    /**
     * @return \Modules\Pricing\Model\Values\Vat
     */
    private function getProductVat(): Vat
    {
        return new Vat($this->product->getTax());
    }
    
    /**
     * @return \Modules\Pricing\Model\Values\Price
     */
    private function calculateProductPrice(): Price
    {
        $productPrice = $this->productPriceCalculation->calculatePrice($this->product);
        
        return $productPrice;
    }
    
    /**
     * @param \Modules\Pricing\Model\Values\Price $price
     * @param int                                 $amount
     * @param \Modules\Pricing\Model\Values\Vat   $vat
     *
     * @return \Modules\Pricing\Model\Values\Price
     */
    private function calculateTotalPrice(Price $price, int $amount, Vat $vat): Price
    {
        $totalPriceWithVat = $price->getPriceWithVat() * $amount;
        $price             = $this->priceCalculation->calculatePrice($totalPriceWithVat, true, $vat);
        
        return $price;
    }
}
