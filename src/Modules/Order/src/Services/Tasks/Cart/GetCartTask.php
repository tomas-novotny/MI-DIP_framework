<?php

namespace Modules\Order\Services\Tasks\Cart;

use Modules\Bus\Parents\AbstractTask;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\User\Model\Entities\Contracts\UserInterface;

/**
 * Class GetCartTask
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class GetCartTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * Shopping cart repository.
     *
     * @var \Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface
     */
    private $cartRepository;
    
    /**
     * Customer.
     *
     * @var \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private $user;
    
    /**
     * GetCartTask constructor.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface|null $user
     */
    public function __construct(UserInterface $user = null)
    {
        $this->cartRepository = $this->resolve(CartRepositoryInterface::class);
        
        $this->user = $user;
    }
    
    /**
     * Get cart instance.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        if ($this->user !== null) {
            return $this->getCartForUser();
        }
        
        return $this->getGuestCart();
    }
    
    /**
     * Get cart for user.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getCartForUser()
    {
        return $this->cartRepository->getByUser($this->user);
    }
    
    /**
     * Get cart by cookie.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getGuestCart()
    {
        $cookie = get_cookie(CartInterface::GUEST_CART_COOKIE);
        
        if ($cookie !== null) {
            return $this->cartRepository->getById($cookie);
        }
        
        throw new ResourceNotFoundException(CartInterface::class);
    }
}
