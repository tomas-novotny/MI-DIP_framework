<?php

namespace Modules\Order\Services\Tasks\Cart;

use Modules\Bus\Parents\AbstractTask;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Factories\Contracts\CartItemFactoryInterface;
use Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class AddItemToCartTask
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class AddItemToCartTask extends AbstractTask
{
    /**
     * Shopping cart repository.
     *
     * @var \Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface
     */
    private $cartRepository;
    
    /**
     * Shopping cart item repository.
     *
     * @var \Modules\Order\Model\Factories\Contracts\CartItemFactoryInterface
     */
    private $cartItemFactory;
    
    /**
     * Cart instance.
     *
     * @var \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    private $cart;
    
    /**
     * Shoppable instance.
     *
     * @var \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    private $product;
    
    /**
     * Amount.
     *
     * @var int
     */
    private $amount;
    
    /**
     * As new item.
     *
     * @var bool
     */
    private $newItem;
    
    /**
     * AddItemToCartTask constructor.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface      $cart
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     * @param bool                                                       $newItem
     */
    public function __construct(
        CartInterface $cart,
        ProductInterface $product,
        int $amount,
        bool $newItem = false
    ) {
        $this->cartRepository  = $this->resolve(CartRepositoryInterface::class);
        $this->cartItemFactory = $this->resolve(CartItemFactoryInterface::class);
        
        $this->cart    = $cart;
        $this->product = $product;
        $this->amount  = $amount;
        $this->newItem = $newItem;
    }
    
    /**
     * Get cart instance.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $item = $this->getItem();
            $item->setAmount($item->getAmount() + $this->amount);
        }
        catch (ResourceNotFoundException $exception) {
            $item = $this->cartItemFactory->newItem($this->cart, $this->product, $this->amount);
            $this->cart->addItem($item);
        }
        
        $this->cartRepository->persist($this->cart);
    }
    
    /**
     * Get item.
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getItem(): ItemInterface
    {
        if ($this->newItem) {
            throw new ResourceNotFoundException(ItemInterface::class);
        }
        
        $item = $this->cart->getItemByProduct($this->product);
        
        return $item;
    }
}
