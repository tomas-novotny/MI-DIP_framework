<?php

namespace Modules\Order\Model\Factories\Contracts;

use Modules\Doctrine\Criterias\HasIdCriteria;
use Modules\Eloquent\EntityRepository;
use Modules\Order\Model\Entities\Concretes\Eloquent\Cart;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Repositories\Criterias\ShoppingCart\HasUserCriteria;
use Modules\User\Model\Entities\Contracts\UserInterface;

class EloquentCartFactory implements CartFactoryInterface
{
    /**
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCartForUser(UserInterface $user): CartInterface
    {
        $cart = new Cart();
    
        if ($user) {
            $cart->setCustomer($user);
        }
    
        return $cart;
    }
    
    /**
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCartForGuest(): CartInterface
    {
        $cart = new Cart();
    
        return $cart;
    }
}
