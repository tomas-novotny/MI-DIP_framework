<?php

namespace Modules\Order\Model\Factories\Contracts;

use Modules\Order\Model\Entities\Concretes\Doctrine\Cart;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\User\Model\Entities\Contracts\UserInterface;

class DoctrineCartFactory implements CartFactoryInterface
{
    /**
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCartForUser(UserInterface $user): CartInterface
    {
        $cart = new Cart();
        
        if ($user) {
            $cart->setCustomer($user);
        }
        
        return $cart;
    }
    
    /**
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCartForGuest(): CartInterface
    {
        $cart = new Cart();
        
        return $cart;
    }
}
