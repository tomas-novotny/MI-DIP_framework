<?php

namespace Modules\Order\Model\Factories\Contracts;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Model\Entities\Concretes\Doctrine\Cart\Item;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;

class DoctrineCartItemFactory implements CartItemFactoryInterface
{
    /**
     * Create new item instance.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface      $cart
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     */
    public function newItem(CartInterface $cart, ProductInterface $product, int $amount): ItemInterface
    {
        $item = new Item();
        
        $item->setCart($cart);
        $item->setProduct($product);
        $item->setAmount($amount);
        
        return $item;
    }
}
