<?php

namespace Modules\Order\Model\Factories\Contracts;

use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\User\Model\Entities\Contracts\UserInterface;

interface CartFactoryInterface
{
    /**
     * New shopping cart for user.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCartForUser(UserInterface $user): CartInterface;
    
    /**
     * New shopping cart.
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCartForGuest(): CartInterface;
}
