<?php

namespace Modules\Order\Model\Factories\Contracts;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;

interface CartItemFactoryInterface
{
    /**
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface      $cart
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     * @param int                                                        $amount
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     */
    public function newItem(CartInterface $cart, ProductInterface $product, int $amount): ItemInterface;
}
