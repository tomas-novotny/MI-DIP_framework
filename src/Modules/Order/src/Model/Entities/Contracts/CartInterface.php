<?php

namespace Modules\Order\Model\Entities\Contracts;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Support\Collection;
use Modules\User\Model\Entities\Contracts\UserInterface;

interface CartInterface
{
    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */
    const TABLE       = 'shopping_carts';
    
    const PRIMARY_KEY = 'id';
    
    const FOREIGN_KEY = 'cart_id';
    
    /**
     * Guest cart cookie name.
     *
     * @var string
     */
    const GUEST_CART_COOKIE = 'guest_cookie';
    
    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get key.
     *
     * @return string
     */
    public function getKey();
    
    /**
     * Set customer.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     */
    public function setCustomer(UserInterface $user): void;
    
    /**
     * Get customer.
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    public function getCustomer(): UserInterface;
    
    /**
     * Get items.
     *
     * @return \Modules\Support\Collection|\Modules\Order\Model\Entities\Contracts\Cart\ItemInterface[]
     */
    public function getItems(): Collection;
    
    /**
     * Add item.
     *
     * @param \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface $item
     *
     * @return void
     */
    public function addItem(ItemInterface $item): void;
    
    /**
     * Remove item.
     *
     * @param \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface $item
     *
     * @return void
     */
    public function removeItem(ItemInterface $item): void;
    
    /**
     * Get item.
     *
     * @param strin $key
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getItemByKey(string $key): ItemInterface;
    
    /**
     * Get item.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getItemByProduct(ProductInterface $product): ItemInterface;
}
