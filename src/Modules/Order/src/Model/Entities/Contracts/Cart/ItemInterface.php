<?php

namespace Modules\Order\Model\Entities\Contracts\Cart;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;

interface ItemInterface
{
    /*
    |--------------------------------------------------------------------------
    | Constants
    |--------------------------------------------------------------------------
    */
    
    /**
     *
     */
    const TABLE       = 'shopping_cart_items';
    
    const ATTR_AMOUNT = 'amount';
    
    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get key.
     *
     * @return string
     */
    public function getKey();
    
    /**
     * Set shopping cart.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface $cart
     *
     * @return void
     */
    public function setCart(CartInterface $cart):void;
    
    /**
     * Set product.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     */
    public function setProduct(ProductInterface $product):void;
    
    /**
     * Get product.
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    public function getProduct(): ProductInterface;
   
    /**
     * Set amount.
     *
     * @return int
     */
    public function setAmount(int $amount): void;
    
  
    public function getAmount(): int;
}
