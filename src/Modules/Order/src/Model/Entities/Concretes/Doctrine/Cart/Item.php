<?php

namespace Modules\Order\Model\Entities\Concretes\Doctrine\Cart;

use Doctrine\ORM\Mapping as ORM;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Doctrine\Concerns\HasTimestampsTrait;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="shopping_cart_items")
 */
class Item implements ItemInterface
{
    use HasTimestampsTrait;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * ID.
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var string
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Cart")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
     *
     * @var \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    private $cart;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Modules\Catalog\Model\Entities\Concretes\Doctrine\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *
     * @var \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    private $product;
    
    /**
     * Amount.
     *
     * @ORM\Column(name="amount", type="integer")
     *
     * @var int
     */
    private $amount;
    
    /**
     * Get key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->id;
    }
    
    /**
     * Set shopping cart.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface $cart
     *
     * @return void
     */
    public function setCart(CartInterface $cart): void
    {
        $this->cart = $cart;
    }
    
    /**
     * Set product.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     */
    public function setProduct(ProductInterface $product): void
    {
        $this->product = $product;
    }
    
    /**
     * Get product.
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->product;
    }
    
    /**
     * Set amount.
     *
     * @return int
     */
    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }
    
    /**
     * Get amount.
     *
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}
