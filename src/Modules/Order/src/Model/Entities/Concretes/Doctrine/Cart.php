<?php

namespace Modules\Order\Model\Entities\Concretes\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Doctrine\Concerns\HasTimestampsTrait;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Entities\Contracts\ShoppableInterface;
use Modules\Support\Collection;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\User\Model\Entities\Contracts\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="shopping_carts")
 */
class Cart implements CartInterface
{
    use HasTimestampsTrait;
    
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * ID.
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var string
     */
    private $id;
    
    /**
     * One Cart has One Customer.
     *
     * @ORM\OneToOne(targetEntity="\Modules\User\Model\Entities\Concretes\Doctrine\User", mappedBy="shoppingCart")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;
    
    /**
     *
     * @ORM\OneToMany(
     *     targetEntity="\Modules\Order\Model\Entities\Concretes\Doctrine\ShoppingCart\Item",
     *     mappedBy="cart",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     *
     * @var \Doctrine\Common\Collections\ArrayCollection|\Modules\Order\Model\Entities\Contracts\Cart\ItemInterface[]
     */
    protected $items;
    
    /**
     * ShoppingCart constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    
    /**
     * Get key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->id;
    }
    
    /**
     * Set customer.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     */
    public function setCustomer(UserInterface $user): void
    {
        $this->customer = $user;
    }
    
    /**
     * Get customer.
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    public function getCustomer(): UserInterface
    {
        return $this->customer;
    }
    
    /**
     * Get items.
     *
     * @return \Modules\Support\Collection
     */
    public function getItems(): Collection
    {
        return new Collection($this->items->toArray());
    }
    
    /**
     * Add item.
     *
     * @param \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface $item
     *
     * @return void
     */
    public function addItem(ItemInterface $item): void
    {
        $this->items->add($item);
    }
    
    /**
     * Remove item.
     *
     * @param \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface $item
     *
     * @return void
     */
    public function removeItem(ItemInterface $item): void
    {
        $this->items->removeElement($item);
    }
    
    /**
     * Get item.
     *
     * @param strin $key
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getItemByKey(string $key): ItemInterface
    {
        foreach ($this->items as $item) {
            if ($item->getKey() === $key) {
                return $item;
            }
        }
        
        throw new ResourceNotFoundException(ItemInterface::class);
    }
    
    /**
     * Get item.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getItemByProduct(ProductInterface $product): ItemInterface
    {
        foreach ($this->items as $item) {
            if ($item->getProduct() === $product) {
                return $item;
            }
        }
        
        throw new ResourceNotFoundException(ItemInterface::class);
    }
    
    /**
     * Get items price.
     *
     * @return float
     */
    public function getItemsPrice(): float
    {
        return $this->getItemsPriceWithTax();
    }
    
    /**
     * Get items price with tax.
     *
     * @return float
     */
    public function getItemsPriceWithTax(): float
    {
        $total = 0.0;
        
        foreach ($this->items as $item) {
            $total += $item->getTotalPriceWithTax();
        }
        
        return round($total, 2);
    }
    
    /**
     * Get items price without tax.
     *
     * @return float
     */
    public function getItemsPriceWithoutTax(): float
    {
        $total = 0.0;
        
        foreach ($this->items as $item) {
            $total += $item->getTotalPriceWithoutTax();
        }
        
        return round($total, 2);
    }
}
