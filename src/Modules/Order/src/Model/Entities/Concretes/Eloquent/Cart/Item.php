<?php

namespace Modules\Order\Model\Entities\Concretes\Eloquent\Cart;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Eloquent\Contracts\ModelInterface;
use Modules\Eloquent\Exceptions\NotEloquentModelException;
use Modules\Eloquent\Parents\AbstractModel;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Entities\Contracts\ShoppableInterface;
use Modules\Order\Model\Entities\Contracts\strin;

class Item extends AbstractModel implements ItemInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = ItemInterface::TABLE;
    
    /**
     * Relationship with product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function product()
    {
        return $this->belongsTo(
            app()->getAlias(ProductInterface::class),
            ProductInterface::FOREIGN_KEY,
            ProductInterface::PRIMARY_KEY
        );
    }
    
    /**
     * Set shopping cart.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface $cart
     *
     * @return void
     */
    public function setCart(CartInterface $cart): void
    {
        if ($cart instanceof ModelInterface) {
            $this->setAttribute(CartInterface::FOREIGN_KEY, $cart->getKey());
            $this->setRelation('cart', $cart);
            
            return;
        }
        
        throw new NotEloquentModelException(get_class($cart));
    }
    
    /**
     * Set product.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return void
     */
    public function setProduct(ProductInterface $product): void
    {
        if ($product instanceof ModelInterface) {
            $this->setAttribute(ProductInterface::FOREIGN_KEY, $product->getKey());
            $this->setRelation('product', $product);
            
            return;
        }
        
        throw new NotEloquentModelException(get_class($product));
    }
    
    /**
     * Get product.
     *
     * @return \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->getRelationValue('product');
    }
    
    /**
     * Set amount.
     *
     * @return int
     */
    public function setAmount(int $amount): void
    {
        $this->setAttribute(ItemInterface::ATTR_AMOUNT, $amount);
    }
    
    /**
     * Get amount.
     *
     * @return int
     */
    public function getAmount(): int
    {
        return $this->getAttribute(ItemInterface::ATTR_AMOUNT);
    }
}
