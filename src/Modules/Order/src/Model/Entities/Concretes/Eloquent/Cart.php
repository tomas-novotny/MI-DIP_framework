<?php

namespace Modules\Order\Model\Entities\Concretes\Eloquent;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Eloquent\Contracts\ModelInterface;
use Modules\Eloquent\Exceptions\NotEloquentModelException;
use Modules\Eloquent\Parents\AbstractModel;
use Modules\Order\Model\Entities\Concretes\Eloquent\Cart\Item;
use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Entities\Contracts\ShoppableInterface;
use Modules\Order\Model\Entities\Contracts\strin;
use Modules\Support\Collection;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\User\Model\Entities\Concretes\Eloquent\User;
use Modules\User\Model\Entities\Contracts\UserInterface;

class Cart extends AbstractModel implements CartInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = CartInterface::TABLE;
    
    /**
     * Relationship with items.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    protected function items()
    {
        return $this->hasMany(Item::class, CartInterface::FOREIGN_KEY, CartInterface::PRIMARY_KEY);
    }
    
    /**
     * Relationship with customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    protected function customer()
    {
        return $this->hasOne(User::class, UserInterface::FOREIGN_KEY, UserInterface::PRIMARY_KEY);
    }
    
    /**
     * Set customer.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     */
    public function setCustomer(UserInterface $user): void
    {
        if ($user instanceof ModelInterface) {
            $this->setAttribute(UserInterface::FOREIGN_KEY, $user->getKey());
            $this->setRelation('customer', $user);
            
            return;
        }
        
        throw new NotEloquentModelException(get_class($user));
    }
    
    /**
     * Get customer.
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    public function getCustomer(): UserInterface
    {
        return $this->getRelationValue('customer');
    }
    
    /**
     * Get items.
     *
     * @return \Modules\Support\Collection|\Modules\Order\Model\Entities\Contracts\Cart\ItemInterface[]
     */
    public function getItems(): Collection
    {
        return new Collection($this->getRelationValue('items'));
    }
    
    /**
     * Add item.
     *
     * @param \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface $item
     *
     * @return void
     */
    public function addItem(ItemInterface $item): void
    {
        if ($item instanceof ModelInterface) {
            // TODO: fix
            if ($this->exists() === false) {
                $this->save();
            }
            
            $this->items()->save($item);
            
            return;
        }
        
        throw new NotEloquentModelException(get_class($item));
    }
    
    /**
     * Remove item.
     *
     * @param \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface $item
     *
     * @return void
     */
    public function removeItem(ItemInterface $item): void
    {
        if ($item instanceof ModelInterface) {
            $item->delete();
            $this->unsetRelation('items');
            
            return;
        }
        
        throw new NotEloquentModelException(get_class($item));
    }
    
    /**
     * Get item.
     *
     * @param strin $key
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getItemByKey(string $key): ItemInterface
    {
        foreach ($this->getItems() as $item) {
            if ($item->getKey() == $key) {
                return $item;
            }
        }
        
        throw new ResourceNotFoundException(ItemInterface::class);
    }
    
    /**
     * Get item.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     *
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getItemByProduct(ProductInterface $product): ItemInterface
    {
        foreach ($this->getItems() as $item) {
            if ($item->getProduct()->getKey() === $product->getKey()) {
                return $item;
            }
        }
        
        throw new ResourceNotFoundException(ItemInterface::class);
    }
}
