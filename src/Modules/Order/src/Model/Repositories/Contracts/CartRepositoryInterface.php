<?php

namespace Modules\Order\Model\Repositories\Contracts;

use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\User\Model\Entities\Contracts\UserInterface;

interface CartRepositoryInterface
{
    /**
     * New shopping cart.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface|null $user
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCart(UserInterface $user = null): CartInterface;
    
    /**
     * Get cart for user.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByUser(UserInterface $user): CartInterface;
    
    /**
     * Get cart by key.
     *
     * @param string $id
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getById(string $id): CartInterface;
    
    /**
     * Persist shopping cart.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface $cart
     *
     * @return void
     */
    public function persist(CartInterface $cart): void;
}
