<?php

namespace Modules\Order\Model\Repositories\Concretes;

use Modules\Doctrine\Criterias\HasIdCriteria;
use Modules\Eloquent\EntityRepository;
use Modules\Order\Model\Entities\Concretes\Eloquent\Cart;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Order\Model\Repositories\Contracts\CartRepositoryInterface;
use Modules\Order\Model\Repositories\Criterias\ShoppingCart\HasUserCriteria;
use Modules\User\Model\Entities\Contracts\UserInterface;

class EloquentCartRepository implements CartRepositoryInterface
{
    /**
     * Entity repository.
     *
     * @var \Modules\Eloquent\EntityRepository
     */
    private $entityRepository;
    
    /**
     * EloquentShoppingCartRepository constructor.
     */
    public function __construct()
    {
        $this->entityRepository = new EntityRepository(Cart::class);
    }
    
    /**
     * New shopping cart.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface|null $user
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function newCart(UserInterface $user = null): CartInterface
    {
        $cart = new Cart();
        
        if ($user) {
            $cart->setCustomer($user);
        }
        
        return $cart;
    }
    
    /**
     * Persist shopping cart.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface $cart
     *
     * @return void
     */
    public function persist(CartInterface $cart): void
    {
        $this->entityRepository->persistAndFlush($cart);
        
        foreach ($cart->getItems() as $item) {
            $this->entityRepository->persistAndFlush($item);
        }
    }
    
    /**
     * Get cart for user.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByUser(UserInterface $user): CartInterface
    {
        $criterias = [
            new HasUserCriteria($user),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
    
    /**
     * Get cart by key.
     *
     * @param string $id
     *
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getById(string $id): CartInterface
    {
        $criterias = [
            new HasIdCriteria($id),
        ];
        
        $this->entityRepository->applyCriterias($criterias);
        
        return $this->entityRepository->get();
    }
}
