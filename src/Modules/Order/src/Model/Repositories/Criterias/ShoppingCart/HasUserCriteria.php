<?php

namespace Modules\Order\Model\Repositories\Criterias\ShoppingCart;

use Modules\Support\Parents\AbstractCriteria;
use Modules\User\Model\Entities\Contracts\UserInterface;

class HasUserCriteria extends AbstractCriteria
{
    /**
     * HasUserCriteria constructor.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $expr = $this->expr();
        
        $this->andWhere($expr->eq('customer', $user->getKey()));
    }
}
