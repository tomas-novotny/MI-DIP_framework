<?php

namespace Modules\Order\Model\Aggregates;

use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Pricing\Model\Aggregates\QuantifiedPrice;

class CartItemDetail
{
    /**
     * @var \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     */
    private $item;
    
    /**
     * @var \Modules\Pricing\Model\Aggregates\QuantifiedPrice
     */
    private $quantifiedPrice;
    
    /**
     * ProductDetail constructor.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $item
     * @param \Modules\Catalog\Model\Values\ProductPrice|null            $price
     */
    public function __construct(ItemInterface $item, QuantifiedPrice $price)
    {
        $this->item            = $item;
        $this->quantifiedPrice = $price;
    }
    
    /**
     * @return \Modules\Order\Model\Entities\Contracts\Cart\ItemInterface
     */
    public function getItem(): ItemInterface
    {
        return $this->item;
    }
    
    /**
     * @return \Modules\Pricing\Model\Aggregates\QuantifiedPrice
     */
    public function getPrice(): QuantifiedPrice
    {
        return $this->quantifiedPrice;
    }
}