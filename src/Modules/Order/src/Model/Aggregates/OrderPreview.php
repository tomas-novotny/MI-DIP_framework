<?php

namespace Modules\Order\Model\Aggregates;

use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Pricing\Model\Values\Price;
use Modules\Support\Collection;

class OrderPreview
{
    /**
     * @var \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    private $cart;
    
    /**
     * @var \Modules\Support\Collection|\Modules\Order\Model\Aggregates\CartItemDetail[]
     */
    private $itemDetails;
    
    /**
     * @var \Modules\Pricing\Model\Values\Price
     */
    private $price;
    
    /**
     * CartDetail constructor.
     *
     * @param \Modules\Order\Model\Entities\Contracts\CartInterface $cart
     * @param \Modules\Support\Collection                           $itemDetails
     * @param \Modules\Pricing\Model\Values\Price                   $price
     */
    public function __construct(CartInterface $cart, Collection $itemDetails, Price $price)
    {
        $this->cart        = $cart;
        $this->itemDetails = $itemDetails;
        $this->price       = $price;
    }
    
    /**
     * @return \Modules\Order\Model\Entities\Contracts\CartInterface
     */
    public function getCart(): CartInterface
    {
        return $this->cart;
    }
    
    /**
     * @return \Modules\Support\Collection
     */
    public function getItems(): Collection
    {
        return $this->itemDetails;
    }
    
    /**
     * @return \Modules\Pricing\Model\Values\Price
     */
    public function getPrice(): Price
    {
        return $this->price;
    }
}