<?php

namespace Modules\Order\Exceptions;

use Modules\Catalog\Model\Entities\Contracts\ProductInterface;
use Modules\Order\Model\Entities\Contracts\ShoppableInterface;
use Modules\Support\Exceptions\ValidationFailedException;

class NotEnoughStockException extends ValidationFailedException
{
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Not enough items in stock';
    
    /**
     * Product instance.
     *
     * @var \Modules\Catalog\Model\Entities\Contracts\ProductInterface
     */
    protected $product;
    
    /**
     * NotEnoughStockException constructor.
     *
     * @param \Modules\Catalog\Model\Entities\Contracts\ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
        
        parent::__construct();
    }
}
