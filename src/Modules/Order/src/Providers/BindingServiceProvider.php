<?php

namespace Modules\Order\Providers;

use Modules\Support\Parents\Providers\AbstractBindingServiceProvider;

class BindingServiceProvider extends AbstractBindingServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $contracts = [
        //
    ];
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadBindingsFromConfig('module-cart.bindings');
        
        parent::register();
    }
}
