<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateShoppingCartsTable extends AbstractMigration
{
    /**
     * Run the migration up.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('shopping_carts', function (Blueprint $table) {
            // primary key
            $table->increments('id')->unsigned();
            
            $table->integer('customer_id')->unsigned()->nullable();
            
            $table->timestamps();
            
            $table->foreign('customer_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate(AbstractMigration::CASCADE)
                  ->onDelete(AbstractMigration::SET_NULL);
        });
    }
    
    /**
     * Run the migration down.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->table('shopping_carts', function (Blueprint $table) {
            $table->dropForeign('shopping_carts_customer_id_foreign');
        });
        
        $this->builder()->dropIfExists('shopping_carts');
    }
}
