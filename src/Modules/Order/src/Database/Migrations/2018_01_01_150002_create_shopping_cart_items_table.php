<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateShoppingCartItemsTable extends AbstractMigration
{
    /**
     * Run the migration up.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('shopping_cart_items', function (Blueprint $table) {
            // primary key
            $table->increments('id');
            
            $table->unsignedInteger('cart_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('amount')->default(1);
    
            $table->timestamps();
            
            $table->foreign('cart_id')
                  ->references('id')
                  ->on('shopping_carts')
                  ->onUpdate(AbstractMigration::CASCADE)
                  ->onDelete(AbstractMigration::CASCADE);
            
            $table->foreign('product_id')
                  ->references('id')
                  ->on('products')
                  ->onUpdate(AbstractMigration::CASCADE)
                  ->onDelete(AbstractMigration::CASCADE);
        });
    }
    
    /**
     * Run the migration down.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->table('shopping_cart_items', function (Blueprint $table) {
            $table->dropForeign('shopping_cart_items_shopping_cart_id_foreign');
        });
        
        $this->builder()->dropIfExists('shopping_cart_items');
    }
}
