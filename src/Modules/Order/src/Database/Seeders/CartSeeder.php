<?php

namespace Modules\Order\Database\Seeders;

use Modules\Order\Model\Entities\Contracts\Cart\ItemInterface;
use Modules\Order\Model\Entities\Contracts\CartInterface;
use Modules\Support\Parents\AbstractSeeder;

class CartSeeder extends AbstractSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->flushTable(CartInterface::TABLE);
        $this->flushTable(ItemInterface::TABLE);
    }
}
