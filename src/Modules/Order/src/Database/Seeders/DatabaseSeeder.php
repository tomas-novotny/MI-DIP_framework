<?php

namespace Modules\Order\Database\Seeders;

use Modules\Support\Parents\AbstractSeeder;

class DatabaseSeeder extends AbstractSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CartSeeder::class);
    }
}
