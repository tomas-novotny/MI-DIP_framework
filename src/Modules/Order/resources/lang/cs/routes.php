<?php

return [
    'cart' => [
        'add'    => 'kosik/pridat',
        'update' => 'kosik/upravit/{id}',
        'delete' => 'kosik/smazat/{id}',
        'items'  => 'kosik',
    ],
];
