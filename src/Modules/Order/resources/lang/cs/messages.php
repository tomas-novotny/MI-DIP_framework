<?php

return [
    'cart' => [
        'added'   => 'Položka přidána..',
        'updated' => 'Položka upravena.',
        'deleted' => 'Položka smazána.',
    ],
];
