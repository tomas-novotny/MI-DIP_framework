<?php

return [
    'cart' => [
        'add'    => 'cart/add',
        'update' => 'cart/update/{id}',
        'delete' => 'cart/delete/{id}',
        'items'  => 'cart',
    ],
];
