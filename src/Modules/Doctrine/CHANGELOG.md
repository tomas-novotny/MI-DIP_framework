# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-07
### Added
- Use Laravel Doctrine package
- Add custom types
    - [CarbonDateTimeType](src/Types/CarbonDateTimeType.php)
    - [CarbonDateType](src/Types/CarbonDateType.php)
    - [CarbonTimeType](src/Types/CarbonTimeType.php)
    - [HashIdType](src/Types/HashIdType.php)
- Add Entity repository 