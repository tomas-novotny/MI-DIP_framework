<?php

namespace Modules\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Modules\Doctrine\Concerns\GetterTrait;
use Modules\Doctrine\Concerns\ScopesTrait;
use Modules\Doctrine\Concerns\SetupTrait;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\DeleteResourceFailedException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Throwable;

class EntityRepository
{
    /**
     * EntityRepository constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     * @param string                               $className
     */
    public function __construct(EntityManagerInterface $entityManager, string $className)
    {
        $this->entityManager = $entityManager;
        $this->entity        = $className;
    }
    
    use SetupTrait;
    
    use ScopesTrait;
    
    use GetterTrait;
    
    /**
     * Persist model to database.
     *
     * @param mixed $entity
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\CreateResourceFailedException
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    public function persistAndFlush($entity): void
    {
        $state = $this->entityManager->getUnitOfWork()->getEntityState($entity);
        
        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
        catch (Throwable $exception) {
            if ($state == UnitOfWork::STATE_NEW) {
                throw new CreateResourceFailedException(get_class($entity), $exception);
            }
            
            throw new UpdateResourceFailedException(get_class($entity), [], $exception);
        }
    }
    
    /**
     * Delete model.
     *
     * @param mixed $entity
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\DeleteResourceFailedException
     */
    public function removeAndFlush($entity): void
    {
        try {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }
        catch (Throwable $exception) {
            
            throw new DeleteResourceFailedException(get_class($entity), [], $exception);
        }
    }
}