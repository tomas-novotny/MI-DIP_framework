<?php

namespace Modules\Doctrine\Criterias;

use Modules\Support\Parents\AbstractCriteria;

class HasIdCriteria extends AbstractCriteria
{
    /**
     * HasCodeCriteria constructor.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $expr = $this->expr();
        
        $this->andWhere($expr->eq('id', $code));
    }
}
