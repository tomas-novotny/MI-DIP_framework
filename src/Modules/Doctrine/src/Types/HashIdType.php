<?php

namespace Modules\Doctrine\Types;

use Doctrine\DBAL\Exception\InvalidArgumentException as DbalInvalidArgumentException;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use InvalidArgumentException;
use Vinkla\Hashids\Facades\Hashids;

class HashidType extends IntegerType
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'hashid';
    }
    
    /**
     * ConvertToPHPValue
     *
     * @param int              $value
     * @param AbstractPlatform $platform
     *
     * @return null|string
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            return $this->getHashidsConnection()->encode($value);
        }
        
        return $value;
    }
    
    /**
     * @return \Hashids\Hashids An instance of Hashids
     */
    protected function getHashidsConnection()
    {
        try {
            $connection = Hashids::connection('doctrine');
        }
        catch (InvalidArgumentException $e) {
            $connection = Hashids::connection();
        }
        
        return $connection;
    }
    
    /**
     * ConvertToDatabaseValue
     *
     * @param string           $value
     * @param AbstractPlatform $platform
     *
     * @return null|string
     *
     * @throws DbalInvalidArgumentException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            $value = $this->getHashidsConnection()->decode($value);
            
            $value = array_shift($value);
        }
        
        return $value;
    }
}
