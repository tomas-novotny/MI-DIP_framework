<?php

namespace Modules\Doctrine\Concerns;

use Doctrine\ORM\QueryBuilder;

trait ScopesTrait
{
    /**
     * Get current query builder.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    abstract protected function getBuilder(): QueryBuilder;
    
    /**
     * Set query builder.
     *
     * @param \Doctrine\ORM\QueryBuilder|null $builder
     *
     * @return $this
     */
    abstract protected function setBuilder(QueryBuilder $builder = null): void;
    
    /**
     * Apply criteria to builder.
     *
     * @param array $criterias
     *
     * @return void
     */
    public function applyCriterias(array $criterias): void
    {
        $builder = $this->getBuilder();
        
        foreach ($criterias as $criteria) {
            $builder->addCriteria($criteria);
        }
        
        $this->setBuilder($builder);
    }
}