<?php

namespace Modules\Doctrine\Concerns;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait HasTimestampsTrait
{
    /**
     * Created at datetime.
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     *
     * @var \Carbon\Carbon
     */
    protected $createdAt;
    
    /**
     * Updated at datetime.
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     *
     * @var \Carbon\Carbon
     */
    protected $updatedAt;
    
    /**
     * Get created datetime.
     *
     * @var \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return Carbon::instance($this->createdAt);
    }
    
    /**
     * Set created datetime.
     *
     * @param \Carbon\Carbon $createdAt
     *
     * @return void
     */
    public function setCreatedAt(Carbon $createdAt)
    {
        $this->createdAt = $createdAt;
    }
    
    /**
     * Get updated datetime.
     *
     * @var \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return Carbon::instance($this->updatedAt);
    }
    
    /**
     * Set $updated datetime.
     *
     * @param \Carbon\Carbon $updatedAt
     *
     * @return void
     */
    public function setUpdatedAt(Carbon $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
