<?php

namespace Modules\Doctrine\Concerns;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Modules\Support\Collection;
use Modules\Support\Exceptions\ResourceNotFoundException;

trait GetterTrait
{
    /**
     * Clear query builder.
     *
     * @return void
     */
    abstract protected function clearBuilder(): void;
    
    /**
     * Repository model.
     *
     * @return string
     */
    abstract protected function getEntity(): string;
    
    /**
     * Get current query builder.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    abstract protected function getBuilder(): QueryBuilder;
    
    /**
     * Return all model instances by query.
     *
     * @return \Modules\Support\Collection
     */
    public function all(): Collection
    {
        $models = $this->cloneBuilder()->getQuery()->execute();
        
        $this->clearBuilder();
        
        return new Collection($models);
    }
    
    /**
     * Return first model instance by query.
     *
     * @return mixed|null
     */
    public function first()
    {
        try {
            $model = $this->cloneBuilder()->getQuery()->getOneOrNullResult();
        }
        catch (NonUniqueResultException $exception) {
            $model = null;
        }
        
        $this->clearBuilder();
        
        return $model;
    }
    
    /**
     * @return mixed
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function get()
    {
        try {
            $model = $this->cloneBuilder()->getQuery()->getSingleResult();
        }
        catch (NoResultException | NonUniqueResultException $exception) {
            throw new ResourceNotFoundException($this->getEntity(), [], $exception);
        }
        
        $this->clearBuilder();
        
        return $model;
    }
    
    /**
     * Get query builder.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function cloneBuilder(): QueryBuilder
    {
        // clone builder instance
        $builder = clone $this->getBuilder();
        
        // return query builder
        return $builder;
    }
}
