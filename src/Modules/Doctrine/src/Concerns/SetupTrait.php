<?php

namespace Modules\Doctrine\Concerns;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

trait SetupTrait
{
    /**
     * Entity manager.
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;
    
    /**
     * Query builder.
     *
     * @var \Doctrine\ORM\QueryBuilder
     */
    private $builder;
    
    /**
     * Entity.
     *
     * @var string
     */
    private $entity;
    
    /**
     * Entity manager
     *
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entity;
    }
    
    /**
     * Set new model to repository to work with.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     *
     * @return void
     */
    protected function setEntityManager(EntityManagerInterface $entityManager): void
    {
        // set model
        $this->entityManager = $entityManager;
        
        // reset builder
        $this->clearBuilder();
    }
    
    /**
     * Repository model.
     *
     * @return string
     */
    protected function getEntity(): string
    {
        return $this->entity;
    }
    
    /**
     * Set new model to repository to work with.
     *
     * @param string $entity
     *
     * @return void
     */
    protected function setEntity($entity): void
    {
        // set model
        $this->entity = $entity;
        
        // reset builder
        $this->clearBuilder();
    }
    
    /**
     * Clear query builder.
     *
     * @return void
     */
    protected function clearBuilder(): void
    {
        $this->setBuilder(null);
    }
    
    /**
     * Set query builder.
     *
     * @param \Doctrine\ORM\QueryBuilder|null $builder
     *
     * @return $this
     */
    protected function setBuilder(QueryBuilder $builder = null): void
    {
        $this->builder = $builder;
    }
    
    /**
     * Get current query builder.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getBuilder(): QueryBuilder
    {
        if ($this->builder === null) {
            // get new model eloquent builder
            $this->builder = $this->newBuilder();
        }
        
        return $this->builder;
    }
    
    /**
     * Creates a new QueryBuilder instance that is prepopulated for this entity name.
     *
     * @param string $alias
     * @param string $indexBy
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function newBuilder(string $alias = 'm', string $indexBy = null): QueryBuilder
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select($alias)
            ->from($this->entity, $alias, $indexBy);
    }
}
