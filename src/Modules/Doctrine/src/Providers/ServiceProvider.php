<?php

namespace Modules\Doctrine\Providers;

use Doctrine\DBAL\Types\Type;
use LaravelDoctrine\Extensions\GedmoExtensionsServiceProvider as DoctrineExtensionsServiceProvider;
use LaravelDoctrine\Migrations\MigrationsServiceProvider as DoctrineMigrationsServiceProvider;
use LaravelDoctrine\ORM\DoctrineServiceProvider;
use Modules\Doctrine\Types\CarbonDateTimeType;
use Modules\Doctrine\Types\CarbonDateType;
use Modules\Doctrine\Types\CarbonTimeType;
use Modules\Support\Parents\Providers\AbstractAutoloadServiceProvider;

class ServiceProvider extends AbstractAutoloadServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        DoctrineServiceProvider::class,
        DoctrineMigrationsServiceProvider::class,
        DoctrineExtensionsServiceProvider::class,
    ];
    
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->overrideDoctrineDateTypes();
    }
    
    /**
     * Overide Laravel Doctrine mapping to use Carbon instead DateTime.
     *
     * @return void
     */
    private function overrideDoctrineDateTypes()
    {
        Type::overrideType('datetime', CarbonDateTimeType::class);
        Type::overrideType('date', CarbonDateType::class);
        Type::overrideType('time', CarbonTimeType::class);
    }
}
