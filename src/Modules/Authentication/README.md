# Authentication module

[![Software License][ico-license]](LICENSE.md)

This module is part of example [modular project][link-framework]. 

## Actions

### Registration
+ [`RegisterAction`](src/Services/Actions/Registration/RegisterAction.php)

### Confirmation
+ [`SendConfirmationLink`](src/Services/Actions/Confirmation/SendConfirmationLinkAction.php)
+ [`ConfirmUser`](src/Services/Actions/Confirmation/ConfirmUserAction.php)

### Login
+ [`WebLoginAction`](src/Services/Actions/Login/WebLoginAction.php)
+ [`WebLogoutAction`](src/Services/Actions/Login/WebLogoutAction.php)
+ [`ApiProxyLoginAction`](src/Services/Actions/Login/ApiProxyLoginAction.php)
+ [`ApiProxyRefreshAction`](src/Services/Actions/Login/ApiProxyRefreshAction.php)
+ [`ApiLogoutAction`](src/Services/Actions/Login/ApiLogoutAction.php)
+ [`MarkLoginTimeAction`](src/Services/Actions/Login/MarkLoginTimeAction.php)

### Password reset
+ [`ResetPasswordAction`](src/Services/Actions/Password/ResetPasswordAction.php)
+ [`SendResetPasswordLinkAction`](src/Services/Actions/Password/SendResetPasswordLinkAction.php)


## Contributing

Please see [CONTRIBUTING][link-contributing] and [CODE_OF_CONDUCT][link-code-of-conduct] for details.

## Security

If you discover any security related issues, please email novott20@fit.cvut.cz instead of using the issue tracker.

## Credits

- [Tomáš Novotný][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-blue.svg

[link-framework]: https://github.com/novott20/MI-DIP_framework
[link-author]: https://github.com/novott20
[link-contributors]: ../../../../../contributors
[link-contributing]: ../../../CONTRIBUTING.md
[link-code-of-conduct]: ../../../CODE_OF_CONDUCT.md