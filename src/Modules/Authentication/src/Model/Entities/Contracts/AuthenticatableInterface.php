<?php

namespace Modules\Authentication\Model\Entities\Contracts;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable as LaravelAuthenticatable;
use Illuminate\Contracts\Auth\CanResetPassword as LaravelCanResetPassword;
use Modules\Support\Contracts\CanBeNotifyInterface;

interface AuthenticatableInterface extends LaravelAuthenticatable, LaravelCanResetPassword, CanBeNotifyInterface
{
    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmail();
    
    /**
     * Set register provider.
     *
     * @param string $type
     * @param string $id
     *
     * @return void
     */
    public function setProvider(string $type, string $id): void;
    
    /**
     * Set email.
     *
     * @param string $password
     *
     * @return void
     */
    public function setPassword(string $password): void;
    
    /**
     * Set last login date.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setLastLogin(Carbon $date): void;
    
    /**
     * Get last login date.
     *
     * @return \Carbon\Carbon|null $date
     */
    public function getLastLogin(): ?Carbon;
    
    /**
     * Set registered at datetime.
     *
     * @param \Carbon\Carbon $date
     *
     * @return void
     */
    public function setRegisteredAt(Carbon $date): void;
    
    /**
     * Get datetime of registration.
     *
     * @return \Carbon\Carbon
     */
    public function getRegisteredAt(): Carbon;
    
    /**
     * If user is registered.
     *
     * @return bool
     */
    public function isRegistered(): bool;
    
    /**
     * Set confirmed at datetime.
     *
     * @param \Carbon\Carbon|null $date
     *
     * @return void
     */
    public function setConfirmedAt(?Carbon $date): void;
    
    /**
     * Get datetime of confirmation.
     *
     * @return \Carbon\Carbon|null
     */
    public function getConfirmedAt(): ?Carbon;
    
    /**
     * User is activated
     *
     * @return bool
     */
    public function isConfirmed(): bool;
}
