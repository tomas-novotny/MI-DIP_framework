<?php

namespace Modules\Authentication\Model\Entities\Contracts;

interface OAuthInterface
{
    /**
     * Find user.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface|null
     */
    public function findForPassport(string $email): ?AuthenticatableInterface;
    
    /**
     * Get key.
     *
     * @return string
     */
    public function getKey();
}
