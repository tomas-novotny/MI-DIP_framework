<?php

namespace Modules\Authentication\Model\Factories\Contracts;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;

interface AuthenticationFactoryInterface
{
    /**
     * Create new user instance.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    public function newAuthenticatableUser(string $email): AuthenticatableInterface;
}
