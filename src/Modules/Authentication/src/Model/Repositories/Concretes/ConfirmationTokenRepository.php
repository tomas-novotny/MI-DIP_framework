<?php

namespace Modules\Authentication\Model\Repositories\Concretes;

use Carbon\Carbon;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\QueryException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface;
use Modules\Support\Exceptions\ResourceNotFoundException;

class ConfirmationTokenRepository implements ConfirmationTokenRepositoryInterface
{
    /**
     * The database connection instance.
     *
     * @var \Illuminate\Database\ConnectionInterface
     */
    private $connection;
    
    /**
     * The token database table.
     *
     * @var string
     */
    private $table;
    
    /**
     * The number of minutes a token should last.
     *
     * @var int
     */
    private $expires;
    
    /**
     * Create a new token repository instance.
     *
     * @param \Illuminate\Database\ConnectionInterface $connection
     * @param string                                   $table
     * @param int                                      $expires
     */
    public function __construct(
        ConnectionInterface $connection,
        string $table = 'user_confirmations',
        int $expires = 60
    ) {
        $this->connection = $connection;
        $this->table      = $table;
        $this->expires    = $expires;
    }
    
    /**
     * Set expires in minutes.
     *
     * @param int $expires
     *
     * @return void
     */
    public function setExpiresTime(int $expires): void
    {
        $this->expires = $expires;
    }
    
    /**
     * Create a new token record.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return string
     */
    public function createToken(AuthenticatableInterface $user): string
    {
        // delete all tokens for this user
        $this->deleteTokensByEmail($user->getEmailForPasswordReset());
        
        // create new one
        $token = $this->createNewToken();
        
        // insert record to table
        $this->getTable()->insert($this->getPayload($user, $token));
        
        // return new token
        return $token;
    }
    
    /**
     * Delete existing for given user
     *
     * @param string $email
     *
     * @return void
     */
    private function deleteTokensByEmail(string $email): void
    {
        $this->getTable()->where('email', '=', $email)->delete();
    }
    
    /**
     * Create a new token for the user.
     *
     * @return string
     */
    private function createNewToken(): string
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }
    
    /**
     * Build the record payload for the table.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param string                                                                    $token
     *
     * @return array
     */
    private function getPayload(AuthenticatableInterface $user, string $token): array
    {
        return [
            'email'      => $user->getEmailForPasswordReset(),
            'token'      => $token,
            'created_at' => Carbon::now()->toDateTimeString(),
        ];
    }
    
    /**
     * Determine if a token record exists
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return string
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getTokenByUser(AuthenticatableInterface $user): string
    {
        $tokenData = (array) $this->getTable()->where('email', $user->getEmailForPasswordReset())->latest()->first();
        
        $this->validateTokenData($tokenData);
        
        return $tokenData['token'];
    }
    
    /**
     * Get users email address by given token.
     *
     * @param string $token
     *
     * @return string
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getEmailByToken(string $token): string
    {
        try {
            $tokenData = (array) $this->getTable()->where('token', $token)->first();
        }
        catch (QueryException $exception) {
            throw new ResourceNotFoundException('token', [], $exception, $exception->getMessage());
        }
        
        $this->validateTokenData($tokenData);
        
        return $tokenData['email'];
    }
    
    /**
     * @param array $tokenData
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function validateTokenData(array $tokenData): void
    {
        if (isset($tokenData['token']) and $this->tokenExpired($tokenData)) {
            $this->deleteToken($tokenData['token']);
            
            throw new ResourceNotFoundException('token', [], null, 'Expired token');
        }
        
        if (empty($tokenData)) {
            throw new ResourceNotFoundException('token', [], null, 'No token found');
        }
    }
    
    /**
     * Determine if the token has expired.
     *
     * @param array    $token
     * @param int|null $minutes
     *
     * @return bool
     */
    private function tokenExpired(array $token, $minutes = null): bool
    {
        $minutes = $minutes ?: $this->expires;
        
        $expiresAt = Carbon::parse($token['created_at'])->addMinute($minutes);
        
        return $expiresAt->isPast();
    }
    
    /**
     * Delete existing
     *
     * @param string $token
     *
     * @return void
     */
    public function deleteToken(string $token): void
    {
        $this->getTable()->where('token', $token)->delete();
    }
    
    /**
     * Begin a new database query against the table.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    private function getTable(): Builder
    {
        return $this->connection->table($this->table);
    }
}
