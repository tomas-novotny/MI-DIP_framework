<?php

namespace Modules\Authentication\Model\Repositories\Contracts;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;

interface AuthenticationRepositoryInterface
{
    /**
     * Persist authenticatable user to database.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     */
    public function persistAuthenticatableUser(AuthenticatableInterface $user): void;
    
    /**
     * Get registered user by email.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getRegisteredByEmail(string $email): AuthenticatableInterface;
    
    /**
     * Get user by email.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByEmail(string $email): AuthenticatableInterface;
}
