<?php

namespace Modules\Authentication\Model\Repositories\Contracts;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;

interface ConfirmationTokenRepositoryInterface
{
    /**
     * Set expires in minutes.
     *
     * @param int $expires
     *
     * @return void
     */
    public function setExpiresTime(int $expires): void;
    
    /**
     * Create a new token record.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return string
     */
    public function createToken(AuthenticatableInterface $user): string;
    
    /**
     * Delete existing
     *
     * @param string $token
     *
     * @return void
     */
    public function deleteToken(string $token): void;
    
    /**
     * Determine if a token record exists and is valid.
     *
     * @param string $token
     *
     * @return string
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getEmailByToken(string $token): string;
    
    /**
     * Determine if a token record exists
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return string
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getTokenByUser(AuthenticatableInterface $user): string;
}
