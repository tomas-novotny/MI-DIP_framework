<?php

namespace Modules\Authentication\Providers;

use Modules\Authentication\Model\Repositories\Concretes\ConfirmationTokenRepository;
use Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface;
use Modules\Support\Parents\Providers\AbstractBindingServiceProvider;

class BindingServiceProvider extends AbstractBindingServiceProvider
{
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $contracts = [
        ConfirmationTokenRepositoryInterface::class => ConfirmationTokenRepository::class,
    ];
}
