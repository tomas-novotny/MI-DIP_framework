<?php

namespace Modules\Authentication\Providers;

use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Laravel\Passport\Http\Middleware\CreateFreshApiToken;
use Modules\Authentication\Middlewares\EncryptCookies;
use Modules\Authentication\Middlewares\RedirectIfAuthenticated;
use Modules\Authentication\Middlewares\ResolveAuthCookie;
use Modules\Authentication\Middlewares\SetSession;
use Modules\Authentication\Middlewares\SetStickyCookie;
use Modules\Authentication\Middlewares\ShareAuthenticated;
use Modules\Authentication\Middlewares\UnsetSession;
use Modules\Authentication\Middlewares\VerifyCsrfToken;
use Modules\Support\Parents\Providers\AbstractMiddlewareServiceProvider;

class MiddlewareServiceProvider extends AbstractMiddlewareServiceProvider
{
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web'       => [
            EncryptCookies::class,
            AddQueuedCookiesToResponse::class,
            ShareErrorsFromSession::class,
        ],
        'stateless' => [
            UnsetSession::class,
        ],
        'stateful'  => [
            SetSession::class,
            VerifyCsrfToken::class,
            ShareAuthenticated::class,
        ],
        'api'       => [
//            CreateFreshApiToken::class,
        ],
    ];
    
    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'       => Authenticate::class,
        'guest'      => RedirectIfAuthenticated::class,
        'auth.basic' => AuthenticateWithBasicAuth::class,
    ];
}
