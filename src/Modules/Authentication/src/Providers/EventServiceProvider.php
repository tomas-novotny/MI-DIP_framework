<?php

namespace Modules\Authentication\Providers;

use Illuminate\Auth\Events\Login;
use Modules\Authentication\Events\Listeners\MarkLoginTime;
use Modules\Support\Parents\Providers\AbstractEventServiceProvider;

class EventServiceProvider extends AbstractEventServiceProvider
{
    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Login::class => [
            MarkLoginTime::class,
        ],
    ];
}
