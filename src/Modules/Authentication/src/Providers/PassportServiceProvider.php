<?php

namespace Modules\Authentication\Providers;

use Carbon\Carbon;
use Laravel\Passport\Console\ClientCommand;
use Laravel\Passport\Console\InstallCommand;
use Laravel\Passport\Console\KeysCommand;
use Laravel\Passport\Passport;
use Laravel\Passport\PassportServiceProvider as LaravelPassportServiceProvider;

class PassportServiceProvider extends LaravelPassportServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        // TODO: load views
        //        $this->loadViews();
        $this->loadRoutes();
        $this->customizeSettings();
        
        $this->deleteCookieOnLogout();
        
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                ClientCommand::class,
                KeysCommand::class,
            ]);
        }
    }
    
    /**
     * Register passport package views.
     *
     * @return void
     */
    private function loadViews()
    {
        //        $path = $this->getModuleRoot().'resources/views/vendor/passport';
        //
        //        $this->loadViewsFrom($path, 'passport');
    }
    
    /**
     * Customize passport package settings.
     *
     * @return void
     */
    private function customizeSettings()
    {
        $path = 'keys/oauth';
        
        Passport::ignoreMigrations();
        
        // TODO: use custom
        //        Passport::loadKeysFrom($path);
        
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
    }
    
    /**
     * Register passport package routes.
     *
     * TODO: make own router with proper middleware
     *
     * @return void
     */
    private function loadRoutes()
    {
        Passport::routes(function ($router) {
            $router->forAuthorization();
        });
        
        Passport::routes(function ($router) {
            $router->forClients();
            $router->forAccessTokens();
            $router->forPersonalAccessTokens();
            $router->forTransientTokens();
        }, ['middleware' => ['web', 'api']]);
    }
}
