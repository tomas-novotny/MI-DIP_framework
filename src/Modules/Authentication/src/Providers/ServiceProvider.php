<?php

namespace Modules\Authentication\Providers;

use LaravelDoctrine\ORM\Auth\Passwords\PasswordResetServiceProvider as DoctrinePasswordResetServiceProvider;
use Modules\Support\Parents\Providers\AbstractAutoloadServiceProvider;

class ServiceProvider extends AbstractAutoloadServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        MiddlewareServiceProvider::class,
        BindingServiceProvider::class,
        EventServiceProvider::class,
        DoctrinePasswordResetServiceProvider::class,
//        PassportServiceProvider::class,
    ];
}
