<?php

namespace Modules\Authentication\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Support\Contracts\CanBeNotifyInterface;
use Modules\Support\Parents\AbstractNotification;

class RegisteredNotification extends AbstractNotification
{
    /**
     * The confirmation token.
     *
     * @var string|null
     */
    private $token;
    
    /**
     * Create a notification instance.
     *
     * @param string|null $token
     */
    public function __construct(string $token = null)
    {
        $this->token = $token;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     *
     * @return array
     */
    public function via(CanBeNotifyInterface $user): array
    {
        return [
            'mail',
        ];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(AuthenticatableInterface $user)
    {
        // TODO: use mailable class instead
        $message = new MailMessage;
        
        $message->line('Thank for your registration '.$user->getEmailForPasswordReset().'.');
        
        if ($user->isConfirmed() === false and $this->token !== null) {
            $message->action('Please activate your account', route('auth.confirm', [$this->token]));
        }
        
        return $message;
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return array
     */
    public function toArray(AuthenticatableInterface $user): array
    {
        return [
            'message'       => 'User was registered',
            'confirmed'     => $user->isConfirmed(),
            'registered_at' => $user->getRegisteredAt()->toDateTimeString(),
        ];
    }
}
