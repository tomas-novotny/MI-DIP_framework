<?php

namespace Modules\Authentication\Notifications;

use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Support\Contracts\CanBeNotifyInterface;
use Modules\Support\Parents\AbstractNotification;

class ResetPasswordNotification extends AbstractNotification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    private $token;
    
    /**
     * Create a notification instance.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param \Modules\Support\Contracts\CanBeNotifyInterface $user
     *
     * @return array
     */
    public function via(CanBeNotifyInterface $user): array
    {
        return [
            'mail',
        ];
    }
    
    /**
     * Build the mail representation of the notification.
     *
     * @param \Illuminate\Contracts\Auth\CanResetPassword $user
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(CanResetPassword $user)
    {
        // TDOO: use mailable objects
        $message = new MailMessage;
        
        // TODO: format better message
        $message->line('You are receiving this email because we received a password reset request for your account.');
        $message->action(
            'Reset Password',
            route('password.reset.token', [$this->token, 'email' => $user->getEmailForPasswordReset()])
        );
        $message->line('If you did not request a password reset, no further action is required.');
        
        return $message;
    }
}
