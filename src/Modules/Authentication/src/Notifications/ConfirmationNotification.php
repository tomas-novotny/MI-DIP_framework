<?php

namespace Modules\Authentication\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Support\Contracts\CanBeNotifyInterface;
use Modules\Support\Parents\AbstractNotification;

class ConfirmationNotification extends AbstractNotification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    private $token;
    
    /**
     * Create a notification instance.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param \Modules\Support\Contracts\CanBeNotifyInterface $user
     *
     * @return array
     */
    public function via(CanBeNotifyInterface $user): array
    {
        return [
            'mail',
        ];
    }
    
    /**
     * Build the mail representation of the notification.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(AuthenticatableInterface $user)
    {
        // TODO: use mailable objects
        $message = new MailMessage;
        
        $message->line('You are receiving this email to confirm your account '.$user->getAuthIdentifierName().'.');
        
        $message->action('Confirm your account', route('auth.confirm', [$this->token]));
        
        return $message;
    }
}
