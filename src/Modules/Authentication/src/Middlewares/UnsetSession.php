<?php

namespace Modules\Authentication\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Modules\Support\Parents\AbstractMiddleware;
use Symfony\Component\HttpFoundation\Response;

class UnsetSession extends AbstractMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $stateless = $this->responseIsStateless();
        
        view()->share('__stateless', $stateless);
        
        $response = $this->response($request, $next);
        
        $response->headers->set('X-No-Session', (int) $stateless);
        
        return $response;
    }
    
    /**
     * If request is stateless (can use http cache like varnish)
     *
     * @return bool
     */
    private function responseIsStateless(): bool
    {
        // HTTP cache not enabled
        if (config('cache.http.enabled') === false) {
            return false;
        }
        
        // response has flash data (intpus values from forms, etc.)
        if ($this->sessionHas('_flash.old')) {
            return false;
        }
        
        // response has new flash data (validation errors, etc.)
        if ($this->sessionHas('_flash.new')) {
            return false;
        }
        
        // response has flash notification
        if ($this->sessionHas('flash_notification')) {
            return false;
        }
        
        // possibly add new conditions...
        
        return true;
    }
    
    /**
     * Determinate if session has value by given key
     *
     * @param string $key
     *
     * @return bool
     */
    private function sessionHas(string $key): bool
    {
        return count((array) session($key, [])) > 0;
    }
}
