<?php

namespace Modules\Authentication\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Modules\Support\Parents\AbstractMiddleware;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated extends AbstractMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->check()) {
            return redirect()->route('home');
        }
        
        return $this->response($request, $next);
    }
}
