<?php

namespace Modules\Authentication\Middlewares;

use Closure;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\Request;
use Modules\Support\Parents\AbstractMiddleware;
use Symfony\Component\HttpFoundation\Response;

class ShareAuthenticated extends AbstractMiddleware
{
    /**
     * The view factory implementation.
     *
     * @var \Illuminate\Contracts\View\Factory
     */
    protected $view;
    
    /**
     * Create a new error binder instance.
     *
     * @param \Illuminate\Contracts\View\Factory $view
     */
    public function __construct(ViewFactory $view)
    {
        $this->view = $view;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->check()) {
            $this->view->share('__user', auth()->user());
        }
        
        return $this->response($request, $next);
    }
}
