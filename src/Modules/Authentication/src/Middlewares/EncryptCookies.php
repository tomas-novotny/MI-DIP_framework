<?php

namespace Modules\Authentication\Middlewares;

use Illuminate\Cookie\Middleware\EncryptCookies as LaravelEncrypter;

class EncryptCookies extends LaravelEncrypter
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        // TODO: auth variant/skip cache oms model
    ];
}
