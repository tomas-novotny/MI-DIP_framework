<?php

namespace Modules\Authentication\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Modules\Support\Parents\AbstractMiddleware;
use Symfony\Component\HttpFoundation\Response;

class SetSession extends AbstractMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        view()->share('__stateless', false);
        
        $response = $this->response($request, $next);
        
        $response->headers->set('X-No-Session', 0);
        
        return $response;
    }
}
