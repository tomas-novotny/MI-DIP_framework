<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreatePasswordRemindersTable extends AbstractMigration
{
    /**
     * Run the migration up.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('user_password_resets', function (Blueprint $table) {
            // primary key
            $table->increments('id')->unsigned();
            // data
            $table->string('email');
            $table->string('token');
            $table->timestamp('created_at');
            
            $table->index('email');
            $table->index('token');
        });
    }
    
    /**
     * Run the migration down.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->dropIfExists('user_password_resets');
    }
}
