<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateOauthPersonalAccessClientsTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('oauth_personal_access_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('client_id')
                  ->references('id')
                  ->on('oauth_clients')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->table('oauth_personal_access_clients', function (Blueprint $table) {
            $table->dropForeign('oauth_personal_access_clients_client_id_foreign');
        });
        
        $this->builder()->dropIfExists('oauth_personal_access_clients');
    }
}
