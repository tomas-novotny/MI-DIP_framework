<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateOauthAccessTokensTable extends AbstractMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('oauth_access_tokens', function (Blueprint $table) {
            $table->string('id', 100)->primary();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('client_id')->unsigned();
            $table->string('name')->nullable();
            $table->text('scopes')->nullable();
            $table->boolean('revoked');
            $table->timestamps();
            $table->dateTime('expires_at')->nullable();
            
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_id')
                  ->references('id')
                  ->on('oauth_clients')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $this->builder()->table('oauth_access_tokens', function (Blueprint $table) {
            $table->dropForeign('oauth_access_tokens_user_id_foreign');
            $table->dropForeign('oauth_access_tokens_client_id_foreign');
        });
        
        $this->builder()->dropIfExists('oauth_access_tokens');
    }
}
