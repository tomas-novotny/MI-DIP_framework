<?php

namespace Modules\Authentication\Events;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Support\Contracts\CollectionInterface;
use Modules\Support\Parents\AbstractEvent;

class UserRegistration extends AbstractEvent
{
    /**
     * User.
     *
     * @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    public $user;
    
    /**
     * Registration request data.
     *
     * @var \Modules\Support\Contracts\CollectionInterface
     */
    public $data;
    
    /**
     * UserRegistration constructor.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param \Modules\Support\Contracts\CollectionInterface                            $data
     */
    public function __construct(AuthenticatableInterface $user, CollectionInterface $data)
    {
        $this->user = $user;
        $this->data = $data;
    }
}
