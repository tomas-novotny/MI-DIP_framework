<?php

namespace Modules\Authentication\Events\Listeners;

use Illuminate\Auth\Events\Login;
use Modules\Authentication\Services\Actions\Login\MarkLoginTimeAction;
use Modules\Support\Parents\AbstractListener;

class MarkLoginTime extends AbstractListener
{
    /**
     * Handle the event.
     *
     * @param \Illuminate\Auth\Events\Login $login
     *
     * @return void
     */
    public function handle(Login $login): void
    {
        $this->dispatchService(new MarkLoginTimeAction($login->user));
    }
}
