<?php

namespace Modules\Authentication\Services\Actions\Confirmation;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Notifications\ConfirmationNotification;
use Modules\Authentication\Services\Tasks\Confirmation\CreateConfirmationTokenTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Exceptions\InternalErrorException;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ValidationFailedException;

/**
 * Class SendConfirmationLinkAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Confirmation\CreateConfirmationTokenTask
 *
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 * @throws  \Modules\Support\Exceptions\InternalErrorException
 */
class SendConfirmationLinkAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        InvalidArgumentException::class,
        InternalErrorException::class,
    ];
    
    /**
     * Auth user.
     *
     * @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private $user;
    
    /**
     * SendConfirmationLinkAction constructor.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     */
    public function __construct(AuthenticatableInterface $user)
    {
        $this->user = $user;
    }
    
    /**
     * Send notification with confirmation link.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     * @throws \Modules\Support\Exceptions\InternalErrorException
     */
    public function handle()
    {
        // user is already confirmed
        if ($this->user->isConfirmed()) {
            throw new InvalidArgumentException('User already confirmed');
        }
        
        try {
            // create new confirmation token
            $token = $this->dispatchTask(new CreateConfirmationTokenTask($this->user));
    
            // sent email notification
            $this->user->notify(new ConfirmationNotification($token));
        }
        catch (ValidationFailedException $e) {
            
            throw new InternalErrorException($e->getMessage(), $e);
        }
    }
}
