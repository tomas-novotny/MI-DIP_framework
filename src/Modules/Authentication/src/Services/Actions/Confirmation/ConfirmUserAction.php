<?php

namespace Modules\Authentication\Services\Actions\Confirmation;

use Carbon\Carbon;
use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Authentication\Services\Tasks\Confirmation\DeleteConfirmationTokenTask;
use Modules\Authentication\Services\Tasks\Login\WebLoginTask;
use Modules\Authentication\Services\Tasks\User\GetUserByEmailTask;
use Modules\Authentication\Services\Tasks\User\GetUserEmailByConfirmationTokenTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Throwable;

/**
 * Class ConfirmUserAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Confirmation\DeleteConfirmationTokenTask
 * @uses    \Modules\Authentication\Services\Tasks\Login\WebLoginTask
 * @uses    \Modules\Authentication\Services\Tasks\User\GetUserByEmailTask
 * @uses    \Modules\Authentication\Services\Tasks\User\GetUserEmailByConfirmationTokenTask
 *
 * @throws  \Modules\Authentication\Exceptions\UserNotConfirmedException
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class ConfirmUserAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        UserNotConfirmedException::class,
        LoginFailedException::class,
    ];
    
    /**
     * User rrepository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface
     */
    private $authenticationRepository;
    
    /**
     * Confirmation token.
     *
     * @var string
     */
    private $token;
    
    /**
     * ConfirmUserAction constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->authenticationRepository = $this->resolve(AuthenticationRepositoryInterface::class);
        
        $this->token = $token;
    }
    
    /**
     * Confirm user account by token.
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\UserNotConfirmedException
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    public function handle()
    {
        // confirm user
        $user = $this->confirmUser($this->token);
        
        // delete token
        $this->deleteToken($this->token);
        
        // login user into application
        $user = $this->login($user);
        
        // get activated user
        return $user;
    }
    
    /**
     * Confirm user.
     *
     * @param string $token
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\UserNotConfirmedException
     */
    private function confirmUser(string $token): AuthenticatableInterface
    {
        // get user from token
        $user = $this->getUser($token);
        
        // user is already confirmed
        if ($user->isConfirmed()) {
            throw new InvalidArgumentException('User already confirmed');
        }
        
        // confirm user
        try {
            $user->setConfirmedAt(Carbon::now());
            $this->authenticationRepository->persistAuthenticatableUser($user);
        }
        catch (Throwable $exception) {
            throw new UserNotConfirmedException('Confirmation failed.', $exception, $user);
        }
        
        return $user;
    }
    
    /**
     * Get user.
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\UserNotConfirmedException
     */
    private function getUser(string $token): AuthenticatableInterface
    {
        try {
            // get user email by given token
            $email = $this->dispatchTask(new GetUserEmailByConfirmationTokenTask($token));
            
            // get user
            $user = $this->dispatchTask(new GetUserByEmailTask($email));
        }
        catch (ResourceNotFoundException $exception) {
            throw new UserNotConfirmedException('User with given confirmation token does not exist.', $exception);
        }
        
        return $user;
    }
    
    /**
     * Delete used token.
     *
     * @param string $token
     *
     * @return void
     */
    private function deleteToken(string $token): void
    {
        $this->dispatchTask(new DeleteConfirmationTokenTask($token));
    }
    
    /**
     * Login user.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    private function login(AuthenticatableInterface $user): AuthenticatableInterface
    {
        return $this->dispatchTask(new WebLoginTask($user));
    }
}
