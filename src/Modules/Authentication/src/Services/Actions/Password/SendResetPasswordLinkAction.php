<?php

namespace Modules\Authentication\Services\Actions\Password;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Notifications\ResetPasswordNotification;
use Modules\Authentication\Services\Concerns\PasswordBrokerTrait;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class SendResetPasswordLinkAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class SendResetPasswordLinkAction extends AbstractAction
{
    use PasswordBrokerTrait;
    
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * User email.
     *
     * @var string
     */
    private $email;
    
    /**
     * SendResetPasswordLinkAction constructor.
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }
    
    /**
     * Send notification with confirmation link.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        // get user
        $user = $this->getUserByEmail($this->email);
        
        // generate new token
        $token = $this->getToken($user);
        
        // send reset password with reset link
        $user->notify(new ResetPasswordNotification($token));
    }
    
    /**
     * Get user by email
     *
     * @param string $email
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    private function getUserByEmail(string $email)
    {
        // get user from credentials
        $user = $this->broker()->getUser(compact('email'));
        
        // not user was found
        if ($user === null) {
            throw new ResourceNotFoundException(trans('authentication::messages.invalid_user_email'));
        }
        
        return $user;
    }
    
    /**
     * Get token
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return string
     */
    private function getToken(AuthenticatableInterface $user)
    {
        return $this->broker()->createToken($user);
    }
}
