<?php

namespace Modules\Authentication\Services\Actions\Password;

use Carbon\Carbon;
use Illuminate\Support\Facades\Password;
use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Authentication\Services\Concerns\PasswordBrokerTrait;
use Modules\Authentication\Services\Tasks\Login\WebLoginTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Throwable;

/**
 * Class ResetPasswordAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Login\WebLoginTask
 *
 * @throws  \Modules\Support\Exceptions\UpdateResourceFailedException
 * @throws  \Modules\Support\Exceptions\InvalidArgumentException
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class ResetPasswordAction extends AbstractAction
{
    use PasswordBrokerTrait;
    
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        UpdateResourceFailedException::class,
        InvalidArgumentException::class,
        LoginFailedException::class,
    ];
    
    /**
     * User repository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface
     */
    private $authenticationRepository;
    
    /**
     * Form data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * ResetPasswordAction constructor.
     *
     * @param \Modules\Support\Collection $data
     */
    public function __construct(Collection $data)
    {
        $this->authenticationRepository = $this->resolve(AuthenticationRepositoryInterface::class);
        
        $this->data = $data;
    }
    
    /**
     * Send notification with confirmation link.
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     * @throws \Modules\Support\Exceptions\InvalidArgumentException
     */
    public function handle()
    {
        try {
            $response = $this->broker()->reset(
                $this->getCredentials($this->data),
                function (AuthenticatableInterface $user, string $password) {
                    $this->resetPassword($user, $password);
                    
                    $this->login($user);
                }
            );
        }
        catch (Throwable $exception) {
            throw new InvalidArgumentException($exception->getMessage(), $exception);
        }
        
        if ($response !== Password::PASSWORD_RESET) {
            
            throw new UpdateResourceFailedException(
                UserInterface::class, [], null, is_string($response) ? $response : null
            );
        }
    }
    
    /**
     * Get user payload data
     *
     * @param \Modules\Support\Collection $data
     *
     * @return array
     */
    private function getCredentials(Collection $data)
    {
        $data = $data->only('email', 'password', 'password_confirmation', 'token');
        
        if ($data->count() !== 4) {
            throw new InvalidArgumentException('Missing all credentials');
        }
        
        return $data->all();
    }
    
    /**
     * Reset the given user's password.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param string                                                                    $password
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\UpdateResourceFailedException
     */
    private function resetPassword(AuthenticatableInterface $user, string $password): void
    {
        $user->setPassword($password);
        $user->setRememberToken(str_random(60));
        $user->setConfirmedAt(Carbon::now());
        
        $this->authenticationRepository->persistAuthenticatableUser($user);
    }
    
    /**
     * Login user.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    private function login(AuthenticatableInterface $user)
    {
        return $this->dispatchTask(new WebLoginTask($user));
    }
}
