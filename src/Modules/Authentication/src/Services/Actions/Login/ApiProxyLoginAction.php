<?php

namespace Modules\Authentication\Services\Actions\Login;

use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Services\Tasks\Login\CallOAuthServerTask;
use Modules\Authentication\Services\Tasks\Login\MakeRefreshCookieTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;

/**
 * Class ApiProxyLoginAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Login\CallOAuthServerTask
 * @uses    \Modules\Authentication\Services\Tasks\Login\MakeRefreshCookieTask
 *
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class ApiProxyLoginAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        LoginFailedException::class,
    ];
    
    /**
     * Request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * Client ID.
     *
     * @var string
     */
    private $clientId;
    
    /**
     * Client password.
     *
     * @var string
     */
    private $clientPassword;
    
    /**
     * ApiProxyLoginAction constructor.
     *
     * @param \Modules\Support\Collection $data
     * @param string                      $client_id
     * @param string                      $client_password
     */
    public function __construct(Collection $data, string $client_id, string $client_password)
    {
        $this->data           = $data;
        $this->clientId       = $client_id;
        $this->clientPassword = $client_password;
    }
    
    /**
     * Proxy login.
     *
     * @return \Modules\Support\Collection
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    public function handle()
    {
        $data = [
            'grant_type'    => 'password',
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientPassword,
            'username'      => $this->data->get('email'),
            'password'      => $this->data->get('password'),
            'scope'         => '',
        ];
        
        $responseContent = $this->callServer($data);
        $refreshCookie   = $this->dispatchTask(new MakeRefreshCookieTask($responseContent->get('refresh_token')));
        
        // make sure we only send the refresh_token in the cookie
        $responseContent->forget('refresh_token');
        
        return new Collection([
            'content' => to_array($responseContent),
            'cookie'  => $refreshCookie,
        ]);
    }
    
    /**
     * Call OAuth server.
     *
     * @param array $data
     *
     * @return \Modules\Support\Collection
     */
    private function callServer(array $data): Collection
    {
        return $this->dispatchTask(new CallOAuthServerTask($data));
    }
}
