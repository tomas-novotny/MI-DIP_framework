<?php

namespace Modules\Authentication\Services\Actions\Login;

use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Exceptions\TooManyLoginAttemptsException;
use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Tasks\Login\WebLoginAttemptTask;
use Modules\Authentication\Services\Tasks\Login\WebLogoutTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class WebLoginAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Login\WebLoginAttemptTask
 * @uses    \Modules\Authentication\Services\Tasks\Login\WebLogoutTask
 *
 * @throws  \Modules\Authentication\Exceptions\TooManyLoginAttemptsException
 * @throws  \Modules\Authentication\Exceptions\UserNotConfirmedException
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class WebLoginAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        TooManyLoginAttemptsException::class,
        UserNotConfirmedException::class,
        LoginFailedException::class,
    ];
    
    /**
     * Login request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * WebLoginAction constructor.
     *
     * @param \Modules\Support\Collection $data
     */
    public function __construct(Collection $data)
    {
        $this->data = $data;
    }
    
    /**
     * Handle action or task.
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\TooManyLoginAttemptsException
     * @throws \Modules\Authentication\Exceptions\UserNotConfirmedException
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    public function handle()
    {
        $user = $this->login($this->data);
        
        $this->validateUserRegistered($user);
        
        $this->validateUserConfirmed($user);
        
        return $user;
    }
    
    /**
     * Rollback action or task.
     *
     * @param \Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        $this->logout();
    }
    
    /**
     * Login user.
     *
     * @param \Modules\Support\Collection $data
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\TooManyLoginAttemptsException
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    private function login(Collection $data)
    {
        return $this->dispatchTask(
            new WebLoginAttemptTask(
                $data->get('email'),
                $data->get('password'),
                $data->get('remember_me', false),
                $data->get('ip', request()->ip())
            )
        );
    }
    
    /**
     * Validate if user is registered.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return void
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    private function validateUserRegistered(AuthenticatableInterface $user): void
    {
        if ($user->isRegistered() === false) {
            $this->logout();
            
            // TODO: custom authorization exception
            throw new LoginFailedException('The user has not been registered.', null, Response::HTTP_UNAUTHORIZED);
        }
    }
    
    /**
     * Validate if user is confirmed.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return void
     *
     * @throws \Modules\Authentication\Exceptions\UserNotConfirmedException
     */
    private function validateUserConfirmed(AuthenticatableInterface $user): void
    {
        if ($user->isConfirmed() === false) {
            $this->logout();
            
            throw new UserNotConfirmedException(null, null, $user);
        }
    }
    
    /**
     * Logout user.
     *
     * @return void
     */
    private function logout()
    {
        $this->dispatchTask(new WebLogoutTask());
    }
}
