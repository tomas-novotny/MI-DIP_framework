<?php

namespace Modules\Authentication\Services\Actions\Login;

use Illuminate\Support\Facades\DB;
use Lcobucci\JWT\Parser;
use Modules\Bus\Parents\AbstractAction;

/**
 * Class ApiLogoutAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class ApiLogoutAction extends AbstractAction
{
    /**
     * JWT Parser
     *
     * @var \Lcobucci\JWT\Parser
     */
    private $parser;
    
    /**
     * Refresh token.
     *
     * @var string
     */
    private $token;
    
    /**
     * ApiLogoutAction constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->parser = $this->resolve(Parser::class);
        
        $this->token = $token;
    }
    
    /**
     * Logout API user.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->parser->parse($this->token)->getHeader('jti');
        
        DB::table('oauth_access_tokens')->where('id', '=', $id)->update(['revoked' => true]);
    }
}
