<?php

namespace Modules\Authentication\Services\Actions\Login;

use Modules\Authentication\Services\Concerns\StatefulGuardTrait;
use Modules\Authentication\Services\Tasks\Login\WebLogoutTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;

/**
 * Class WebLogoutAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Login\WebLogoutTask
 */
class WebLogoutAction extends AbstractAction
{
    use StatefulGuardTrait;
    
    /**
     * Logout request data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * WebLogoutAction constructor.
     *
     * @param \Modules\Support\Collection $data
     */
    public function __construct(Collection $data)
    {
        $this->data = $data;
    }
    
    /**
     * Logout user.
     *
     * @return void
     */
    public function handle()
    {
        $this->dispatchTask(new WebLogoutTask());
    }
}
