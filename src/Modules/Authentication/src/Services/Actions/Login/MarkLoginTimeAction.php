<?php

namespace Modules\Authentication\Services\Actions\Login;

use Carbon\Carbon;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Bus\Parents\AbstractAction;

/**
 * Class MarkLoginTimeAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class MarkLoginTimeAction extends AbstractAction
{
    /**
     * User repository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface
     */
    private $userRepository;
    
    /**
     * Authenticatable user.
     *
     * @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private $user;
    
    /**
     * MarkLoginTimeAction constructor.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     */
    public function __construct(AuthenticatableInterface $user)
    {
        $this->userRepository = $this->resolve(AuthenticationRepositoryInterface::class);
        
        $this->user = $user;
    }
    
    /**
     * Handle action or task.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->setLastLogin(Carbon::now());
        
        $this->userRepository->persistAuthenticatableUser($this->user);
    }
}
