<?php

namespace Modules\Authentication\Services\Actions\Login;

use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Services\Tasks\Login\CallOAuthServerTask;
use Modules\Authentication\Services\Tasks\Login\MakeRefreshCookieTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;

/**
 * Class ApiProxyRefreshAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Login\CallOAuthServerTask
 * @uses    \Modules\Authentication\Services\Tasks\Login\MakeRefreshCookieTask
 *
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class ApiProxyRefreshAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        LoginFailedException::class,
    ];
    
    /**
     * Refresh token.
     *
     * @var string|null
     */
    private $token;
    
    /**
     * Client ID.
     *
     * @var string
     */
    private $clientId;
    
    /**
     * Client password.
     *
     * @var string
     */
    private $clientPassword;
    
    /**
     * ApiProxyRefreshAction constructor.
     *
     * @param string|null $token
     * @param string      $client_id
     * @param string      $client_password
     */
    public function __construct(string $token = null, string $client_id, string $client_password)
    {
        $this->token          = $token;
        $this->clientId       = $client_id;
        $this->clientPassword = $client_password;
    }
    
    /**
     * Proxy refresh token.
     *
     * @return \Modules\Support\Collection
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    public function handle()
    {
        if ($this->token === null) {
            throw new LoginFailedException('Missing or invalid Refresh token.');
        }
        
        $data = [
            'grant_type'    => 'refresh_token',
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientPassword,
            'refresh_token' => $this->token,
            'scope'         => '',
        ];
        
        $responseContent = $this->callServer($data);
        $refreshCookie   = $this->dispatchTask(new MakeRefreshCookieTask($responseContent->get('refresh_token')));
        
        // make sure we only send the refresh_token in the cookie
        $responseContent->forget('refresh_token');
        
        return new Collection([
            'content' => to_array($responseContent),
            'cookie'  => $refreshCookie,
        ]);
    }
    
    
    /**
     * Call OAuth server.
     *
     * @param array $data
     *
     * @return \Modules\Support\Collection
     */
    private function callServer(array $data): Collection
    {
        return $this->dispatchTask(new CallOAuthServerTask($data));
    }
}
