<?php

namespace Modules\Authentication\Services\Actions\Registration;

use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Modules\Authentication\Events\UserRegistration;
use Modules\Authentication\Exceptions\RegisterFailedException;
use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Factories\Contracts\AuthenticationFactoryInterface;
use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Authentication\Notifications\RegisteredNotification as RegisteredNotification;
use Modules\Authentication\Services\Tasks\Confirmation\CreateConfirmationTokenTask;
use Modules\Authentication\Services\Tasks\Login\WebLoginTask;
use Modules\Authentication\Services\Tasks\User\GetRegisterUserByEmailTask;
use Modules\Authentication\Services\Tasks\User\GetUserByEmailTask;
use Modules\Bus\Parents\AbstractAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\CreateResourceFailedException;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Ramsey\Uuid\Uuid;

/**
 * Class RegisterAction
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @uses    \Modules\Authentication\Services\Tasks\Confirmation\CreateConfirmationTokenTask
 * @uses    \Modules\Authentication\Services\Tasks\Login\WebLoginTask
 * @uses    \Modules\Authentication\Services\Tasks\User\GetRegisterUserByEmailTask
 * @uses    \Modules\Authentication\Services\Tasks\User\GetUserByEmailTask
 *
 * @throws  \Modules\Authentication\Exceptions\UserNotConfirmedException
 * @throws  \Modules\Authentication\Exceptions\RegisterFailedException
 */
class RegisterAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        UserNotConfirmedException::class,
        RegisterFailedException::class,
    ];
    
    /**
     * User factory.
     *
     * @var \Modules\Authentication\Model\Factories\Contracts\AuthenticationFactoryInterface
     */
    protected $userFactory;
    
    /**
     * User repository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface
     */
    protected $userRepository;
    
    /**
     * Register request data.
     *
     * @var \Modules\Support\Collection
     */
    protected $data;
    
    /**
     * Custom register options.
     *
     * @var array
     */
    protected $options;
    
    /**
     * Register user.
     *
     * @param \Modules\Support\Collection $data
     * @param array                       $options
     */
    public function __construct(Collection $data, array $options = [])
    {
        $this->userRepository = $this->resolve(AuthenticationRepositoryInterface::class);
        $this->userFactory    = $this->resolve(AuthenticationFactoryInterface::class);
        
        $this->data    = $data;
        $this->options = $options;
    }
    
    /**
     * Handle action or task.
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\UserNotConfirmedException
     * @throws \Modules\Authentication\Exceptions\RegisterFailedException
     */
    public function handle()
    {
        // email was already used
        $this->validateEmailNotAlreadyUsed($this->data->get('email'));
        
        // merge default options
        $options = $this->mergeDefaultOptions($this->options);
        
        // create (or update) user
        $user = $this->createOrUpdateUser($this->data, $options);
        
        // raise registered event
        event(new Registered($user));
        
        // send welcome email (with confirmation link if needed)
        if ($options->get('send_mail')) {
            // create new confirmation token
            $token = $user->isConfirmed() === false
                ? $this->dispatchTask(new CreateConfirmationTokenTask($user))
                : null;
            
            // sent email notification
            $user->notify(new RegisteredNotification($token));
        }
        
        // validate that user not need to be confirmed
        $this->validateUserConfirmed($user);
        
        // login user into application
        $user = $this->login($user);
        
        // return registered user
        return $user;
    }
    
    /**
     * Validate if given email not already used by registered user.
     *
     * @param string $email
     *
     * @return void
     *
     * @throws \Modules\Authentication\Exceptions\RegisterFailedException
     */
    private function validateEmailNotAlreadyUsed(string $email): void
    {
        try {
            // try to find already registered user with given email
            $this->dispatchTask(new GetRegisterUserByEmailTask($email));
            
            throw new RegisterFailedException('Email ['.$email.'] already used.');
        }
        catch (ResourceNotFoundException $exception) {
            // no such user exists
        }
    }
    
    /**
     * Add default options
     *
     * @param array $options
     *
     * @return \Modules\Support\Collection
     */
    private function mergeDefaultOptions(array $options): Collection
    {
        $default = [
            'send_mail'    => (bool) config('registration.mail'),
            'confirmation' => (bool) config('registration.confirmation'),
        ];
        
        return new Collection(array_merge($default, $options));
    }
    
    /**
     * Create or Update user.
     *
     * @param \Modules\Support\Collection $data
     * @param \Modules\Support\Collection $options
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\RegisterFailedException
     */
    private function createOrUpdateUser(Collection $data, Collection $options): AuthenticatableInterface
    {
        /* @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user */
        
        try {
            // get non-registered user with this email
            $user      = $this->dispatchTask(new GetUserByEmailTask($data->get('email')));
            $confirmed = false;
        }
        catch (ResourceNotFoundException $exception) {
            $user      = $this->userFactory->newAuthenticatableUser($data->get('email'));
            $confirmed = $options->get('confirmation') === false;
        }
        
        try {
            // raise event to apply custom registration proccess
            event(new UserRegistration($user, $data));
            
            // override custom registration process
            $user->setPassword($data->get('password'));
            $user->setRegisteredAt(Carbon::now());
            $user->setProvider('web', Uuid::uuid4()->toString());
            $user->setConfirmedAt($confirmed ? Carbon::now() : null);
            
            $this->userRepository->persistAuthenticatableUser($user);
        }
        catch (CreateResourceFailedException | UpdateResourceFailedException $exception) {
            throw new RegisterFailedException(null, $exception);
        }
        
        return $user;
    }
    
    /**
     * Validate if user is confirmed
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return void
     *
     * @throws \Modules\Authentication\Exceptions\UserNotConfirmedException
     */
    private function validateUserConfirmed(AuthenticatableInterface $user): void
    {
        // if account validation is needed
        if ($user->isConfirmed() === false) {
            throw new UserNotConfirmedException(null, null, $user);
        }
    }
    
    /**
     * Login user.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private function login(AuthenticatableInterface $user): AuthenticatableInterface
    {
        return $this->dispatchTask(new WebLoginTask($user));
    }
}
