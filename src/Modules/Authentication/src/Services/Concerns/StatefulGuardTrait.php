<?php

namespace Modules\Authentication\Services\Concerns;

trait StatefulGuardTrait
{
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return auth()->guard();
    }
}
