<?php

namespace Modules\Authentication\Services\Concerns;

use Illuminate\Support\Facades\Password;

trait PasswordBrokerTrait
{
    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Auth\Passwords\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }
}
