<?php

namespace Modules\Authentication\Services\Concerns;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait ThrottlesLoginsTrait
{
    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param string $throttleKey
     *
     * @return bool
     */
    protected function hasTooManyLoginAttempts(string $throttleKey)
    {
        return $this->limiter()->tooManyAttempts($throttleKey, 5);
    }
    
    /**
     * Get the rate limiter instance.
     *
     * @return \Illuminate\Cache\RateLimiter
     */
    protected function limiter()
    {
        return app(RateLimiter::class);
    }
    
    /**
     * Increment the login attempts for the user.
     *
     * @param string $throttleKey
     *
     * @return void
     */
    protected function incrementLoginAttempts(string $throttleKey)
    {
        $this->limiter()->hit($throttleKey);
    }
    
    /**
     * Clear the login locks for the given user credentials.
     *
     * @param string $throttleKey
     *
     * @return void
     */
    protected function clearLoginAttempts(string $throttleKey)
    {
        $this->limiter()->clear($throttleKey);
    }
    
    /**
     * Fire an event when a lockout occurs.
     *
     * @param array $data
     *
     * @return void
     */
    protected function fireLockoutEvent(array $data)
    {
        $request = new Request($data);
        
        event(new Lockout($request));
    }
    
    /**
     * Get the throttle key for the given request.
     *
     * @param string $email
     * @param string $ip
     *
     * @return string
     */
    protected function throttleKey(string $email, string $ip)
    {
        return Str::lower($email.'|'.$ip);
    }
}
