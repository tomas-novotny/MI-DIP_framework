<?php

namespace Modules\Authentication\Services\Tasks\User;

use Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class GetUserEmailByConfirmationTokenTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class GetUserEmailByConfirmationTokenTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * Confirmation repository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface
     */
    private $tokenRepository;
    
    /**
     * Token.
     *
     * @var string
     */
    private $token;
    
    /**
     * GetUserEmailByConfirmationTokenTask constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->tokenRepository = $this->resolve(ConfirmationTokenRepositoryInterface::class);
        
        $this->token = $token;
    }
    
    /**
     * Find user by confirmation token
     *
     * @return string
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        $email = $this->tokenRepository->getEmailByToken($this->token);
        
        return $email;
    }
}
