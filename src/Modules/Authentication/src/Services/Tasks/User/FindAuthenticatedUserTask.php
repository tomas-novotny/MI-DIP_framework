<?php

namespace Modules\Authentication\Services\Tasks\User;

use Modules\Authentication\Services\Concerns\StatefulGuardTrait;
use Modules\Bus\Parents\AbstractTask;

/**
 * Class FindAuthenticatedUserTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class FindAuthenticatedUserTask extends AbstractTask
{
    use StatefulGuardTrait;
    
    /**
     * Get authenticated user.
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface|null
     */
    public function handle()
    {
        $user = $this->guard()->user();
        
        return $user;
    }
}
