<?php

namespace Modules\Authentication\Services\Tasks\User;

use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\ResourceNotFoundException;

/**
 * Class GetRegisterUserByEmailTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ResourceNotFoundException
 */
class GetRegisterUserByEmailTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ResourceNotFoundException::class,
    ];
    
    /**
     * User repository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface
     */
    private $authenticationRepository;
    
    /**
     * User email.
     *
     * @var string
     */
    private $email;
    
    /**
     * Find registered user by given email
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->authenticationRepository = $this->resolve(AuthenticationRepositoryInterface::class);
        
        $this->email = $email;
    }
    
    /**
     * Find registered user by given email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function handle()
    {
        $user = $this->authenticationRepository->getRegisteredByEmail($this->email);
        
        return $user;
    }
}
