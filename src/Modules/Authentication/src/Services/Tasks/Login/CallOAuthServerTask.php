<?php

namespace Modules\Authentication\Services\Tasks\Login;

use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Collection;
use Modules\Support\Request;

/**
 * Class CallOAuthServerTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class CallOAuthServerTask extends AbstractTask
{
    /**
     * Auth api route
     *
     * @var string
     */
    const AUTH_ROUTE = '/oauth/token';
    
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        LoginFailedException::class,
    ];
    
    /**
     * Data.
     *
     * @var array
     */
    private $data;
    
    /**
     * CallOAuthServerTask constructor.
     *
     * @param $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
    
    /**
     * Proxy login
     *
     * @return \Modules\Support\Collection
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    public function handle()
    {
        // Full url to the oauth token endpoint
        $url = (string) url(self::AUTH_ROUTE);
        
        $headers = [
            'HTTP_ACCEPT' => 'application/json',
        ];
        
        // Create and handle the oauth request
        $request  = Request::create($url, Request::METHOD_POST, $this->data, [], [], $headers);
        $response = app()->handle($request);
        $content  = json_decode($response->getContent(), true);
        
        // if the internal request to the oauth token endpoint was not successful we throw an exception
        if ($response->isSuccessful() === false) {
            $title  = array_get($content, 'errors.0.title');
            $detail = array_get($content, 'errors.0.detail');
            $detail = empty($detail) ? '' : '('.$detail.')';
            
            throw new LoginFailedException($title.$detail, null, $response->getStatusCode());
        }
        
        return new Collection($content);
    }
}
