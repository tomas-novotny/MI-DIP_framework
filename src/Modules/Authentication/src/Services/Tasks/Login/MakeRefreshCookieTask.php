<?php

namespace Modules\Authentication\Services\Tasks\Login;

use Modules\Bus\Parents\AbstractTask;

/**
 * Class MakeRefreshCookieTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class MakeRefreshCookieTask extends AbstractTask
{
    /**
     * Refresh token cookie name.
     */
    public const REFRESH_TOKEN = 'refreshToken';
    
    /**
     * Refresh token.
     *
     * @var string
     */
    private $token;
    
    /**
     * MakeRefreshCookieTask constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }
    
    /**
     * Make refresh cookie.
     *
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    public function handle()
    {
        // save the refresh token in a HttpOnly cookie to minimize the risk of XSS attacks
        $refreshCookie = cookie()->make(self::REFRESH_TOKEN, $this->token, 120, null, null, false, true);
        
        return $refreshCookie;
    }
}
