<?php

namespace Modules\Authentication\Services\Tasks\Login;

use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Concerns\StatefulGuardTrait;
use Modules\Bus\Parents\AbstractTask;

/**
 * Class WebLoginTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class WebLoginTask extends AbstractTask
{
    use StatefulGuardTrait;
    
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        LoginFailedException::class,
    ];
    
    
    /**
     * User to auth.
     *
     * @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private $user;
    
    /**
     * Remember flag.
     *
     * @var bool
     */
    private $remember;
    
    /**
     * WebLoginTask constructor.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param bool                                                                      $remember
     */
    public function __construct(AuthenticatableInterface $user, bool $remember = false)
    {
        $this->user     = $user;
        $this->remember = $remember;
    }
    
    /**
     * Login user
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    public function handle()
    {
        $user = $this->login($this->user, $this->remember);
        
        if ($user === null) {
            throw new LoginFailedException();
        }
        
        return $user;
    }
    
    /**
     * Login user.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param bool                                                                      $remember
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private function login(AuthenticatableInterface $user, bool $remember)
    {
        $this->guard()->login($user, $remember);
        
        $user = $this->guard()->user();
        
        return $user;
    }
}
