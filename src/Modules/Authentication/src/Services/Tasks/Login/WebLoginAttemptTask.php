<?php

namespace Modules\Authentication\Services\Tasks\Login;

use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Exceptions\TooManyLoginAttemptsException;
use Modules\Authentication\Services\Concerns\StatefulGuardTrait;
use Modules\Authentication\Services\Concerns\ThrottlesLoginsTrait;
use Modules\Bus\Parents\AbstractTask;

/**
 * Class WebLoginAttemptTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Authentication\Exceptions\TooManyLoginAttemptsException
 * @throws  \Modules\Authentication\Exceptions\LoginFailedException
 */
class WebLoginAttemptTask extends AbstractTask
{
    use ThrottlesLoginsTrait;
    use StatefulGuardTrait;
    
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        TooManyLoginAttemptsException::class,
        LoginFailedException::class,
    ];
    
    /**
     * Auth email.
     *
     * @var string
     */
    private $email;
    
    /**
     * Auth password.
     *
     * @var string
     */
    private $password;
    
    /**
     * Remember flag.
     *
     * @var bool
     */
    private $remember;
    
    /**
     * Request IP address.
     *
     * @var string
     */
    private $ip;
    
    /**
     * WebLoginAttemptTask constructor.
     *
     * @param string      $email
     * @param string      $password
     * @param bool        $remember
     * @param string|null $ip
     */
    public function __construct(string $email, string $password, bool $remember = false, string $ip = null)
    {
        $this->email    = $email;
        $this->password = $password;
        $this->remember = $remember;
        $this->ip       = $ip ?: request()->ip();
    }
    
    /**
     * Login user
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\TooManyLoginAttemptsException
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    public function handle()
    {
        // validate login attemps
        $this->validateLoginAttempts($this->email, $this->ip);
        
        // login user
        $user = $this->login($this->email, $this->password, $this->remember, $this->ip);
        
        return $user;
    }
    
    /**
     * Validate login attempts
     *
     * @param string $email
     * @param string $ip
     *
     * @throws \Modules\Authentication\Exceptions\TooManyLoginAttemptsException
     */
    private function validateLoginAttempts(string $email, string $ip)
    {
        $throttleKey = $this->throttleKey($email, $ip);
        
        if ($this->hasTooManyLoginAttempts($throttleKey)) {
            $this->fireLockoutEvent(compact('email', 'ip'));
            
            throw new TooManyLoginAttemptsException();
        }
    }
    
    /**
     * Login user.
     *
     * @param string $email
     * @param string $password
     * @param bool   $remember
     * @param string $ip
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Authentication\Exceptions\LoginFailedException
     */
    private function login(string $email, string $password, bool $remember, string $ip)
    {
        $login       = $this->guard()->attempt(compact('email', 'password'), $remember);
        $throttleKey = $this->throttleKey($email, $ip);
        
        if ($login === false) {
            $this->incrementLoginAttempts($throttleKey);
            
            throw new LoginFailedException();
        }
        
        $this->clearLoginAttempts($throttleKey);
        
        $user = $this->guard()->user();
        
        return $user;
    }
}
