<?php

namespace Modules\Authentication\Services\Tasks\Login;

use Modules\Authentication\Services\Concerns\StatefulGuardTrait;
use Modules\Bus\Parents\AbstractTask;
use RuntimeException as BaseRuntimeException;

/**
 * Class WebLogoutTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class WebLogoutTask extends AbstractTask
{
    use StatefulGuardTrait;
    
    /**
     * Logout user
     *
     * @return void
     */
    public function handle()
    {
        $this->guard()->logout();
        
        // delete user session
        try {
            request()->session()->flush();
            forget_cookie(config('session.cookie'));
        }
        catch (BaseRuntimeException $exception) {
            // session store not set on request
        }
    }
}
