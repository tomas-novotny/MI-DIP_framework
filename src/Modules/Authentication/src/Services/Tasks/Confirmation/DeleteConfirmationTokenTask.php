<?php

namespace Modules\Authentication\Services\Tasks\Confirmation;

use Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;

/**
 * Class CreateConfirmationTokenTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class DeleteConfirmationTokenTask extends AbstractTask
{
    /**
     * Confirmation token repository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface
     */
    private $tokenRepository;
    
    /**
     * Confirmation token.
     *
     * @var string
     */
    private $token;
    
    /**
     * DeleteConfirmationTokenTask constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->tokenRepository = $this->resolve(ConfirmationTokenRepositoryInterface::class);
        
        $this->token = $token;
    }
    
    /**
     * Delete confirmation token
     *
     * @return void
     */
    public function handle()
    {
        $this->tokenRepository->deleteToken($this->token);
    }
}
