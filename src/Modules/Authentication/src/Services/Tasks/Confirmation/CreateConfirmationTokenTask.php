<?php

namespace Modules\Authentication\Services\Tasks\Confirmation;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface;
use Modules\Bus\Parents\AbstractTask;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\ValidationFailedException;

/**
 * Class CreateConfirmationTokenTask
 *
 * @package Modules\Authentication
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 *
 * @throws  \Modules\Support\Exceptions\ValidationFailedException
 */
class CreateConfirmationTokenTask extends AbstractTask
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        ValidationFailedException::class,
    ];
    
    /**
     * Confirmation token repository.
     *
     * @var \Modules\Authentication\Model\Repositories\Contracts\ConfirmationTokenRepositoryInterface
     */
    private $tokenRepository;
    
    /**
     * User.
     *
     * @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private $user;
    
    /**
     * After which delay another validation email should be send (in minutes)
     *
     * @var int
     */
    private $resendAfter = 20;
    
    /**
     * CreateConfirmationTokenTask constructor.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param int                                                                       $resendAfter
     */
    public function __construct(AuthenticatableInterface $user, int $resendAfter = 20)
    {
        $this->tokenRepository = $this->resolve(ConfirmationTokenRepositoryInterface::class);
        
        $this->user        = $user;
        $this->resendAfter = $resendAfter;
        
        $this->tokenRepository->setExpiresTime($resendAfter);
    }
    
    /**
     * Create confirmation token for given user.
     *
     * @return string
     *
     * @throws \Modules\Support\Exceptions\ValidationFailedException
     */
    public function handle()
    {
        $this->validateCreateNewToken($this->user);
        
        $token = $this->tokenRepository->createToken($this->user);
        
        return $token;
    }
    
    /**
     * Validate create confirmation token.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return void
     *
     * @throws \Modules\Support\Exceptions\ValidationFailedException
     */
    private function validateCreateNewToken(AuthenticatableInterface $user)
    {
        try {
            // get activation record by user
            $this->tokenRepository->getTokenByUser($user);
            
            throw new ValidationFailedException('Confirmation token already exists.');
        }
        catch (ResourceNotFoundException $exception) {
            // no token exists
        }
    }
}
