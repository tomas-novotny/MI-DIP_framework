<?php

namespace Modules\Authentication\UI\API\Requests;

use Modules\Support\Request;
use Modules\User\Model\Entities\Contracts\UserInterface;

class LoginRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => [
                'required',
                'email',
                'max:40',
            ],
            'password' => [
                'required',
                'min:8',
                'max:30',
            ],
        ];
    }
}
