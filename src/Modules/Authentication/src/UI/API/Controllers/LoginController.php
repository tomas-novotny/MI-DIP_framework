<?php

namespace Modules\Authentication\UI\API\Controllers;

use Modules\Authentication\Services\Actions\Login\ApiLogoutAction;
use Modules\Authentication\Services\Actions\Login\ApiProxyLoginAction;
use Modules\Authentication\Services\Actions\Login\ApiProxyRefreshAction;
use Modules\Authentication\Services\Tasks\Login\MakeRefreshCookieTask;
use Modules\Authentication\UI\API\Requests\LoginRequest;
use Modules\Authentication\UI\API\Requests\LogoutRequest;
use Modules\Authentication\UI\API\Requests\RefreshRequest;
use Modules\Support\Parents\Controllers\AbstractApiController;

class LoginController extends AbstractApiController
{
    /**
     * Proxy login.
     *
     * @param \Modules\Authentication\UI\API\Requests\LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function proxyLogin(LoginRequest $request)
    {
        // TODO: do not use env(), use config() instead
        /* @var \Modules\Support\Collection $result */
        $result = $this->dispatchService(
            new ApiProxyLoginAction(
                $request->data(),
                env('PASSWORD_CLIENT_ID'),
                env('PASSWORD_CLIENT_SECRET')
            ));
        
        return $this->json($result->get('content'))->withCookie($result->get('cookie'));
    }
    
    /**
     * Proxy refresh token.
     *
     * @param \Modules\Authentication\UI\API\Requests\RefreshRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function proxyRefresh(RefreshRequest $request)
    {
        // TODO: do not use env(), use config() instead
        // TODO: move cookie name cost
        /* @var \Modules\Support\Collection $result */
        $result = $this->dispatchService(
            new ApiProxyRefreshAction(
                (string) $request->cookie(MakeRefreshCookieTask::REFRESH_TOKEN),
                env('PASSWORD_CLIENT_ID'),
                env('PASSWORD_CLIENT_SECRET')
            ));
        
        return $this->json($result->get('content'))->withCookie($result->get('cookie'));
    }
    
    /**
     * Logout.
     *
     * @param \Modules\Authentication\UI\API\Requests\LogoutRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(LogoutRequest $request)
    {
        $this->dispatchService(new ApiLogoutAction($request->bearerToken()));
        
        return $this
            ->accepted(['message' => 'Token revoked successfully.'])
            ->withCookie(cookie()->forget(MakeRefreshCookieTask::REFRESH_TOKEN));
    }
}
