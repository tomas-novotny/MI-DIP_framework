<?php

use Illuminate\Routing\Router;
use Modules\Authentication\UI\API\Controllers\LoginController;

$router->group(
  ['middleware' => ['web', 'api', 'stateful'],
  ], function (Router $router) {
    
    $router->post('oauth/login', [
      'uses' => LoginController::class.'@proxyLogin',
      'middleware' => [
          'stateful',
      ],
    ]);
    
    $router->post('oauth/refresh', [
      'uses' => LoginController::class.'@proxyRefresh',
      'middleware' => [
          'auth:api',
      ],
    ]);
    
    $router->post('oauth/logout', [
      'uses'       => LoginController::class.'@logout',
      'middleware' => [
        'auth:api',
      ],
    ]);
});
