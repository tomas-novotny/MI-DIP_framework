<?php

use Illuminate\Routing\Router;
use Modules\Authentication\UI\Web\Controllers\LoginController;
use Modules\Authentication\UI\Web\Controllers\RegisterController;
use Modules\Authentication\UI\Web\Controllers\ResetPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

web_router(function (Router $router) {
    $router->group(['middleware' => ['stateful', 'guest']], function (Router $router) {
        
        // login form
        $router->get(trans('authentication::routes.login'), [
            'as'   => 'auth.login',
            'uses' => LoginController::class.'@showLoginForm',
        ]);
        
        // login
        $router->post(trans('authentication::routes.login'), [
            'uses' => LoginController::class.'@login',
        ]);
        
        // activation
        $router->get(trans('authentication::routes.confirm'), [
            'as'   => 'auth.confirm',
            'uses' => LoginController::class.'@confirm',
        ]);
        
        // register form
        $router->get(trans('authentication::routes.register'), [
            'as'   => 'auth.register',
            'uses' => RegisterController::class.'@showRegistrationForm',
        ]);
        
        // register
        $router->post(trans('authentication::routes.register'), [
            'uses' => RegisterController::class.'@register',
        ]);
        
        // forget password form
        $router->get(trans('authentication::routes.forget_password'), [
            'as'   => 'password.email',
            'uses' => ResetPasswordController::class.'@showResetPasswordLinkForm',
        ]);
        
        // send password reset link
        $router->post(trans('authentication::routes.forget_password'), [
            'uses' => ResetPasswordController::class.'@sendResetPasswordLink',
        ]);
        
        // forget reset form
        $router->get(trans('authentication::routes.reset_password_token'), [
            'as'   => 'password.reset.token',
            'uses' => ResetPasswordController::class.'@showResetPasswordForm',
        ]);
        
        // reset password
        $router->post(trans('authentication::routes.reset_password'), [
            'as'   => 'password.reset',
            'uses' => ResetPasswordController::class.'@reset',
        ]);
    });
    
    $router->group(['middleware' => ['auth', 'stateful']], function (Router $router) {
        // logout
        $router->get(trans('authentication::routes.logout'), [
            'as'   => 'auth.logout',
            'uses' => LoginController::class.'@logout',
        ]);
    });
});
