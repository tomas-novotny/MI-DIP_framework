@extends('app:web::_layouts.master')

@section('title'){{ trans('authentication::templates/auth.reset_password') }} | @parent @endsection

@section('content')

    <h1>{{ trans('authentication::templates/auth.reset_password') }}</h1>

    {!! $form->render() !!}

@endsection
