@extends('app:web::_layouts.master')

@section('title'){{ trans('authentication::templates/auth.login') }} | @parent @endsection

@section('content')

    <h1>{{ trans('authentication::templates/auth.login') }}</h1>

    {!! $form->render() !!}

    <div>
        <a href="{{ route('password.email') }}">{{ trans('authentication::templates/auth.forgot_password') }}</a>
    </div>
    <div>
        <a href="{{ route('auth.register') }}">{{ trans('authentication::templates/auth.register') }}</a>
    </div>

@endsection
