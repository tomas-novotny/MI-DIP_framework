@extends('app:web::_layouts.master')

@section('title'){{ trans('authentication::templates/auth.register') }} | @parent @endsection

@section('content')

    <h1>{{ trans('authentication::templates/auth.register') }}</h1>

    {!! $form->render() !!}

@endsection
