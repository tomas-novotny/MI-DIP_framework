<?php

namespace Modules\Authentication\UI\Web\Requests;

use Modules\Support\Request;

class ResetPasswordRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'token'    => 'required',
            'email'    => [
                'required',
                'email',
                'max:40',
            ],
            'password' => [
                'required',
                'confirmed',
                'min:8',
                'max:30',
            ],
        ];
    }
}
