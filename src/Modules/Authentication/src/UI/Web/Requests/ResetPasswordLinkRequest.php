<?php

namespace Modules\Authentication\UI\Web\Requests;

use Modules\Support\Request;

class ResetPasswordLinkRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                'max:40',
            ],
        ];
    }
}
