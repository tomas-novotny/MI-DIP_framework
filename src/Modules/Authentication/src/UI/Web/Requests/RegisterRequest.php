<?php

namespace Modules\Authentication\UI\Web\Requests;

use Modules\Support\Request;

class RegisterRequest extends Request
{
    /**
     * The input keys that should not be flashed on redirect.
     *
     * @var array
     */
    protected $dontFlash = [
        'password_confirmation',
        'terms_and_conditions',
        'password',
    ];
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //          'name'                 => 'required|min:2',
            'email'                => [
                'required',
                'email',
                'max:40',
            ],
            //           TODO: use trait to phone regex (or some package)
            //          'phone'                => ['regex:'.$this->regex('phone', $country)],
            'password'             => [
                'required',
                'confirmed',
                'min:8',
                'max:30',
            ],
            'terms_and_conditions' => [
                'required',
                'accepted',
            ],
            //          'address.street'       => 'required',
            //          'address.city'         => 'required',
            //          'address.country_code' => 'required|exists:countries,code',
            //          'address.postcode'     => ['required', 'regex:'.$this->regex('postcode', $country)],
        ];
    }
}
