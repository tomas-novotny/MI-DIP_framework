<?php

namespace Modules\Authentication\UI\Web\Requests;

use Modules\Support\Request;

class ConfirmationRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'token'    => 'required',
        ];
    }
}
