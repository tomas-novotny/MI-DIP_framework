<?php

namespace Modules\Authentication\UI\Web\Requests;

use Modules\Support\Collection;
use Modules\Support\Request;

class LoginRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => [
                'required',
                'email',
                'max:40',
            ],
            'password' => [
                'required',
                'min:8',
                'max:30',
                // TODO: add regex to: One capital, numeric, etc.
            ],
        ];
    }
    
    /**
     * Get all of the input and files for the request.
     *
     * @param array|null $keys
     *
     * @return \Modules\Support\Collection
     */
    public function data(array $keys = null): Collection
    {
        return parent::data($keys ? $keys : ['email', 'password', 'remember']);
    }
}
