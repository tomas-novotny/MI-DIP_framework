<?php

namespace Modules\Authentication\UI\Web\Requests;

use Modules\Support\Request;

class LogoutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }
}
