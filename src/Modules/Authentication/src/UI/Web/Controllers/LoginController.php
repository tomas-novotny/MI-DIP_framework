<?php

namespace Modules\Authentication\UI\Web\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Actions\Confirmation\ConfirmUserAction;
use Modules\Authentication\Services\Actions\Confirmation\SendConfirmationLinkAction;
use Modules\Authentication\Services\Actions\Login\WebLoginAction;
use Modules\Authentication\Services\Actions\Login\WebLogoutAction;
use Modules\Authentication\UI\Web\Forms\LoginForm;
use Modules\Authentication\UI\Web\Requests\ConfirmationRequest;
use Modules\Authentication\UI\Web\Requests\LoginRequest;
use Modules\Authentication\UI\Web\Requests\LogoutRequest;
use Modules\Support\Parents\Controllers\AbstractWebController;
use Modules\Support\Request;
use Throwable;

class LoginController extends AbstractWebController
{
    use AuthorizesRequests;
    
    /**
     * Show the application login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        $form = $this->createForm(LoginForm::class);
        
        return $this->view(config('module-authentication.views.login'), compact('form'));
    }
    
    /**
     * Handle a login request to the application.
     *
     * @param \Modules\Authentication\UI\Web\Requests\LoginRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LoginRequest $request)
    {
        try {
            // login user
            $user = $this->dispatchService(new WebLoginAction($request->data()));
            
            // return authenticated response on success
            return $this->authenticatedResponse($user, $request);
        }
        catch (UserNotConfirmedException $e) {
            // send email with confirmation link
            if ($user = $e->getUser()) {
                $this->sendConfirmationLink($user);
            }
            
            // redirect back with inputs
            return $this->redirect()->route('home');
        }
        catch (Throwable $e) {
            
            // redirect back with inputs and errors
            return $this->redirect()->back()
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors(['email' => $e->getMessage()]);
        }
    }
    
    /**
     * The user has been authenticated.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param \Modules\Support\Request                                                  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function authenticatedResponse(AuthenticatableInterface $user, Request $request)
    {
        // show flash success messages
        $this->addSuccessMessage(trans('authentication::messages.login_successful', [
            'email' => $user->getEmail(),
        ]));
        
        // redirect to homepage
        return $request->filled('redirect_uri')
            ? $this->redirect()->to($request->get('redirect_uri'))
            : $this->redirect()->intended(route('home'));
    }
    
    /**
     * Send email with confirmation link.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return void
     */
    protected function sendConfirmationLink(AuthenticatableInterface $user)
    {
        try {
            // send email with confirmation link
            $this->dispatchService(new SendConfirmationLinkAction($user));
            // show flash error message
            $this->addErrorMessage('authentication::messages.confirm_your_account');
        }
        catch (Throwable $e) {
            // show flash error message
            $this->addInfoMessage('authentication::messages.confirmation_token_already_exists');
        }
    }
    
    /**
     * Log the user out of the application.
     *
     * @param \Modules\Authentication\UI\Web\Requests\LogoutRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(LogoutRequest $request)
    {
        $this->dispatchService(new WebLogoutAction($request->data()));
        
        // redirect back to homepage
        return $this->redirect()->route('home', ['logout']);
    }
    
    /**
     * Confirm user by token.
     *
     * @param string                                                      $token
     * @param \Modules\Authentication\UI\Web\Requests\ConfirmationRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm(string $token, ConfirmationRequest $request)
    {
        try {
            // confirm & login user
            /* @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user */
            $user = $this->dispatchService(new ConfirmUserAction($token));
            
            // return authenticated response on success
            return $this->authenticatedResponse($user, $request);
        }
        catch (Throwable $e) {
            // show flash error message
            $this->addErrorMessage('authentication::messages.expired_activation_token');
            
            // redirect back to homepage
            return $this->redirect()->route('home');
        }
    }
}
