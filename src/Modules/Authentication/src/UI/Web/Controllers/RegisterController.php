<?php

namespace Modules\Authentication\UI\Web\Controllers;

use Modules\Authentication\Exceptions\RegisterFailedException;
use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Services\Actions\Registration\RegisterAction;
use Modules\Authentication\UI\Web\Forms\RegisterForm;
use Modules\Authentication\UI\Web\Requests\RegisterRequest;
use Modules\Support\Parents\Controllers\AbstractWebController;
use Modules\Support\Request;
use Modules\User\Model\Entities\Contracts\UserInterface;

class RegisterController extends AbstractWebController
{
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        $form = $this->createForm(RegisterForm::class);
        
        return $this->view(config('module-authentication.register'), compact('form'));
    }
    
    /**
     * Handle a registration request for the application.
     *
     * @param \Modules\Authentication\UI\Web\Requests\RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(RegisterRequest $request)
    {
        try {
            /* @var \Modules\User\Model\Entities\Contracts\UserInterface $user */
            $user = $this->dispatchService(new RegisterAction($request->data()));
            
            // return registered response on success
            return $this->registeredResponse($user, $request);
        }
        catch (UserNotConfirmedException $e) {
            // show flash info message
            $this->addInfoMessage('authentication::messages.confirm_your_account');
            
            // redirect to login form
            return $this->redirect()->route('auth.login');
        }
        catch (RegisterFailedException $e) {
            // show flash error message
            $this->addErrorMessage($e->getMessage());
            
            // redirect back with inputs and errors
            return $this->redirect()->back()
                        ->withInput($request->exceptDontFlash())
                        ->withErrors(['email' => $e->getMessage()]);
        }
    }
    
    /**
     * The user has been registered.
     *
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     * @param \Modules\Support\Request                             $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function registeredResponse(UserInterface $user, Request $request)
    {
        // show flash success message
        $this->addSuccessMessage(trans('authentication::messages.registration_successful', [
            'email' => $user->getEmail(),
        ]));
        
        // redirect to homepage
        return $request->filled('redirect_uri')
            ? $this->redirect()->to($request->get('redirect_uri'))
            : $this->redirect()->intended(route('home'));
    }
}
