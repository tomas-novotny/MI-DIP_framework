<?php

namespace Modules\Authentication\UI\Web\Controllers;

use Modules\Authentication\Services\Actions\Password\ResetPasswordAction;
use Modules\Authentication\Services\Actions\Password\SendResetPasswordLinkAction;
use Modules\Authentication\UI\Web\Forms\ResetPasswordForm;
use Modules\Authentication\UI\Web\Forms\ResetPasswordLinkForm;
use Modules\Authentication\UI\Web\Requests\ResetPasswordLinkRequest;
use Modules\Authentication\UI\Web\Requests\ResetPasswordRequest;
use Modules\Support\Parents\Controllers\AbstractWebController;
use Modules\Support\Request;
use Throwable;

class ResetPasswordController extends AbstractWebController
{
    /**
     * Show the application reset password link request form.
     *
     * @return \Illuminate\View\View
     */
    public function showResetPasswordLinkForm()
    {
        $form = $this->createForm(ResetPasswordLinkForm::class);
        
        return $this->view(config('module-authentication.forgot-password'), compact('form'));
    }
    
    /**
     * Send a reset password link to the given user.
     *
     * @param \Modules\Authentication\UI\Web\Requests\ResetPasswordLinkRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetPasswordLink(ResetPasswordLinkRequest $request)
    {
        try {
            // send email with confirmation link
            $this->dispatchService(new SendResetPasswordLinkAction($request->get('email')));
            
            // show flash error message
            $this->addSuccessMessage('authentication::messages.password_reset_link_send');
            
            // redirect back to homepage
            return $this->redirect()->route('home');
        }
        catch (Throwable $e) {
            
            return $this->redirect()->back()->withErrors(['email' => $e->getMessage()]);
        }
    }
    
    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param string                   $token
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function showResetPasswordForm(string $token, Request $request)
    {
        $form = $this->createForm(ResetPasswordForm::class, [
            'email' => $request->get('email'),
            'token' => $token,
        ]);
        
        return $this->view(config('module-authentication.reset-password'), compact('form'));
    }
    
    /**
     * Reset the given user's password.
     *
     * @param \Modules\Authentication\UI\Web\Requests\ResetPasswordRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(ResetPasswordRequest $request)
    {
        try {
            $this->dispatchService(new ResetPasswordAction($request->data()));
            
            $this->addSuccessMessage('authentication::messages.password_reset_successful');
            
            return $this->redirect()->route('home');
        }
        catch (Throwable $e) {
            return $this
                ->redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => $e->getMessage()]);
        }
    }
}
