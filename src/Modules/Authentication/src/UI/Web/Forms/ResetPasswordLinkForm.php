<?php

namespace Modules\Authentication\UI\Web\Forms;

use Modules\Support\Parents\AbstractForm;

class ResetPasswordLinkForm extends AbstractForm
{
    /**
     * Build form fields.
     *
     * @return void
     */
    public function build(): void
    {
        $this->setMethod('POST');
        $this->setUrl(route('password.email'));
        
        $this->add('email', 'email', [
            'label' => trans('authentication::templates/forms.email').':',
        ]);
        
        $this->add('before_submit', 'hidden', ['attr' => ['disabled' => 'disabled']]);
        $this->add('submit', 'submit', [
            'label' => trans('authentication::templates/forms.request_new_password'),
            'attr'  => ['class' => 'btn btn-lg btn-block btn-primary'],
        ]);
    }
}
