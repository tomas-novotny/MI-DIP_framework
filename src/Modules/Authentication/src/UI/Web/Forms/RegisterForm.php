<?php

namespace Modules\Authentication\UI\Web\Forms;

use Modules\Support\Parents\AbstractForm;

class RegisterForm extends AbstractForm
{
    /**
     * Build form fields.
     *
     * @return void
     */
    public function build(): void
    {
        $this->setMethod('post');
        $this->setUrl(route('auth.register'));
        
        if ($url = request()->get('redirect_uri', session('url.intended'))) {
            $this->add('redirect_uri', 'hidden', [
                'value' => $url,
            ]);
        }
        
        $this->add('name', 'text', [
            'label'    => trans('authentication::templates/forms.name').':',
            'required' => true,
        ]);
        
        $this->add('email', 'email', [
            'label'    => trans('authentication::templates/forms.email').':',
            'required' => true,
        ]);
        
        //        $this->add('phone', 'text', [
        //          'label' => trans('authentication::templates/forms.phone').':',
        //        ]);
        
        $this->add('password', 'password', [
            'label'    => trans('authentication::templates/forms.password').':',
            'required' => true,
        ]);
        
        $this->add('password_confirmation', 'password', [
            'label'    => trans('authentication::templates/forms.password_confirmation').':',
            'required' => true,
        ]);
        
        $this->add('terms_and_conditions', 'checkbox', [
            'label'    => trans('authentication::templates/forms.terms_agree'),
            'required' => true,
        ]);
        
        $this->add('before_submit', 'hidden', ['attr' => ['disabled' => 'disabled']]);
        $this->add('submit', 'submit', [
            'label' => trans('authentication::templates/forms.register'),
            'attr'  => [
                'id'    => 'registration-submit',
                'class' => 'btn btn-lg btn-block btn-primary',
            ],
        ]);
    }
}
