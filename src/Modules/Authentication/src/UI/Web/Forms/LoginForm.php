<?php

namespace Modules\Authentication\UI\Web\Forms;

use Modules\Support\Parents\AbstractForm;

class LoginForm extends AbstractForm
{
    /**
     * Build form fields.
     *
     * @return void
     */
    public function build(): void
    {
        $this->setMethod('post');
        $this->setUrl(route('auth.login'));
        
        if ($url = request()->get('redirect_uri', session('url.intended'))) {
            $this->add('redirect_uri', 'hidden', [
                'value' => $url,
            ]);
        }
        
        $this->add('email', 'email', [
            'label' => trans('authentication::templates/forms.email').':',
        ]);
        
        $this->add('password', 'password', [
            'label' => trans('authentication::templates/forms.password').':',
        ]);
        
        $this->add('remember', 'checkbox', [
            'label' => trans('authentication::templates/forms.remember_login'),
        ]);
        
        $this->add('before_submit', 'hidden', ['attr' => ['disabled' => 'disabled']]);
        $this->add('submit', 'submit', [
            'label' => trans('authentication::templates/forms.login'),
            'attr'  => ['class' => 'btn btn-lg btn-block btn-primary'],
        ]);
    }
}
