<?php

namespace Modules\Authentication\UI\Web\Forms;

use Modules\Support\Parents\AbstractForm;

class ResetPasswordForm extends AbstractForm
{
    /**
     * Build form fields.
     *
     * @return void
     */
    public function build(): void
    {
        $this->setMethod('POST');
        $this->setUrl(route('password.reset'));
        
        $this->add('token', 'hidden', [
            'value' => $this->form->getData('token'),
        ]);
        
        $this->add('email', 'email', [
            'label' => trans('authentication::templates/forms.email').':',
            'value' => $this->form->getData('email'),
        ]);
        
        $this->add('password', 'password', [
            'label' => trans('authentication::templates/forms.password').':',
        ]);
        
        $this->add('password_confirmation', 'password', [
            'label' => trans('authentication::templates/forms.password_confirmation').':',
        ]);
        
        $this->add('before_submit', 'hidden', ['attr' => ['disabled' => 'disabled']]);
        $this->add('submit', 'submit', [
            'label' => trans('authentication::templates/forms.reset_password'),
            'attr'  => [
                'class' => 'btn btn-lg btn-block btn-primary',
            ],
        ]);
    }
}
