<?php

namespace Modules\Authentication\Exceptions;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class UserNotConfirmedException extends AbstractException
{
    /**
     * Default status code.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_UNAUTHORIZED;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'The user has not been confirmed.';
    
    /**
     * Authenticatable user
     *
     * @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface|null
     */
    protected $user;
    
    /**
     * UserNotConfirmedException constructor.
     *
     * @param string|null                                                                    $message
     * @param \Throwable|null                                                                $previous
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface|null $user
     */
    public function __construct(
        string $message = null,
        Throwable $previous = null,
        AuthenticatableInterface $user = null
    ) {
        $this->user = $user;
        
        parent::__construct($message, $previous);
    }
    
    /**
     * Get authenticatable user
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface|null
     */
    public function getUser(): ?AuthenticatableInterface
    {
        return $this->user;
    }
}
