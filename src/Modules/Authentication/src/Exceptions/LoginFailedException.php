<?php

namespace Modules\Authentication\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response;

class LoginFailedException extends AbstractException
{
    /**
     * Default status code.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Login failed.';
}
