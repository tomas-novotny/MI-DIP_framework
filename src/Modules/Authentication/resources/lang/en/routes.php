<?php

return [
    'confirm'              => 'confirm/{token}',
    'login'                => 'login',
    'forget_password'      => 'forget-password',
    'reset_password'       => 'reset-password',
    'reset_password_token' => 'reset-password-token/{token}',
    'logout'               => 'logout',
    'register'             => 'register',
];
