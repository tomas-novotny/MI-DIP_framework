<?php

return [
    'login'           => 'Login',
    'logout'          => 'Logout',
    'forgot_password' => 'Forget password',
    'reset_password'  => 'Reset password',
    'authorization'   => 'Authorization',
    'register'        => 'Register',
];
