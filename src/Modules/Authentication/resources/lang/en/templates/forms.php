<?php

return [
    'login'                 => 'Login',
    'email'                 => 'Email',
    'remember_login'        => 'Remember',
    'password'              => 'Password',
    'password_confirmation' => 'Password confirmation',
    'request_new_password'  => 'Request new password',
    'reset_password'        => 'Reset password',
];
