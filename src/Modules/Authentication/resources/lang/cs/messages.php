<?php

return [
    'login_successful'                  => 'Login was successful',
    'expired_activation_token'          => 'The activation link is no longer valid',
    'confirm_your_account'              => 'You need to confirm your account. We have sent you an activation code, please check your email.',
    'confirmation_token_already_exists' => 'Confirmation token already exists.',
    'password_reset_link_send'          => 'Password reset link was send.',
    'password_reset_successful'         => 'Password reset was successful.',
];
