<?php

return [
    'login'                 => 'Přihlásit',
    'register'              => 'Registrovat',
    'name'                  => 'Jméno',
    'email'                 => 'Email',
    'remember_login'        => 'Pamatovat si přihlášení',
    'password'              => 'Heslo',
    'password_confirmation' => 'Potvrzení hesla',
    'request_new_password'  => 'Požádat o nové heslo',
    'reset_password'        => 'Obnovit heslo',
    'phone'                 => 'Telefon',
    'terms_agree'           => 'Souhlasím s obchodními podmínkami',
];
