<?php

return [
    'login'           => 'Přihlášení',
    'logout'          => 'Odhlásit',
    'forgot_password' => 'Zapomenuté heslo',
    'reset_password'  => 'Obnovení heslo',
    'authorization'   => 'Autorizace',
    'register'        => 'Registrace',
];
