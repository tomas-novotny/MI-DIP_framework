<?php

return [
    'confirm'              => 'aktivovat/{token}',
    'login'                => 'prihlasit',
    'forget_password'      => 'zapomenute-heslo',
    'reset_password'       => 'resetovani-hesla',
    'reset_password_token' => 'resetovani-hesla/{token}',
    'logout'               => 'odhlasit',
    'register'             => 'registrovat',
];
