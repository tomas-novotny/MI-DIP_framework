# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-07
### Added
- Add [`RegisterAction`](src/Services/Actions/Registration/RegisterAction.php)
- Add [`SendConfirmationLink`](src/Services/Actions/Confirmation/SendConfirmationLinkAction.php)
- Add [`ConfirmUser`](src/Services/Actions/Confirmation/ConfirmUserAction.php)
- Add [`WebLoginAction`](src/Services/Actions/Login/WebLoginAction.php)
- Add [`WebLogoutAction`](src/Services/Actions/Login/WebLogoutAction.php)
- Add [`ApiProxyLoginAction`](src/Services/Actions/Login/ApiProxyLoginAction.php)
- Add [`ApiProxyRefreshAction`](src/Services/Actions/Login/ApiProxyRefreshAction.php)
- Add [`ApiLogoutAction`](src/Services/Actions/Login/ApiLogoutAction.php)
- Add [`MarkLoginTimeAction`](src/Services/Actions/Login/MarkLoginTimeAction.php)
- Add [`ResetPasswordAction`](src/Services/Actions/Password/ResetPasswordAction.php)
- Add [`SendResetPasswordLinkAction`](src/Services/Actions/Password/SendResetPasswordLinkAction.php)

