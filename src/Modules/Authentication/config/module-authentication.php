<?php

return [
    'bindings' => [
        'eloquent' => [
            //
        ],
        'doctrine' => [
            //
        ],
    ],
    
    'views' => [
        'login'           => 'authentication:web::login',
        'register'        => 'authentication:web::register',
        'forgot-password' => 'authentication:web::forgot-password',
        'reset-password'  => 'authentication:web::reset-password',
    
    ],
];