<?php

return [
    
    // confirm user email after registration
    'confirmation' => env('REGISTER_CONFIRMATION', false),
    'mail'         => env('REGISTER_MAIL', true),

];
