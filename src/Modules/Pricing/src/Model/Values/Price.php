<?php

namespace Modules\Pricing\Model\Values;

class Price
{
    /**
     * Price with VAT.
     *
     * @var float
     */
    private $priceWithVat;
    
    /**
     * Price without VAT.
     *
     * @var float
     */
    private $priceWithoutVat;
    
    /**
     * Vat amount.
     *
     * @var float
     */
    private $vatAmount;
    
    /**
     * Price constructor.
     *
     * @param float $priceWithVat
     * @param float $priceWithoutVat
     */
    public function __construct(float $priceWithVat, float $priceWithoutVat)
    {
        $this->priceWithVat    = $priceWithVat;
        $this->priceWithoutVat = $priceWithoutVat;
        $this->vatAmount       = $priceWithVat - $priceWithoutVat;
    }
    
    /**
     * @return float
     */
    public function getPriceWithoutVat(): float
    {
        return $this->priceWithoutVat;
    }
    
    /**
     * @return float
     */
    public function getPriceWithVat(): float
    {
        return $this->priceWithVat;
    }
    
    /**
     * @return float
     */
    public function getVatAmount(): float
    {
        return $this->vatAmount;
    }
    
    /**
     * @param \Modules\Pricing\Model\Values\Price $priceToAdd
     *
     * @return self
     */
    public function add(Price $priceToAdd): self
    {
        return new self(
            $this->priceWithoutVat + $priceToAdd->getPriceWithoutVat(),
            $this->priceWithVat + $priceToAdd->getPriceWithVat()
        );
    }
    
    /**
     * @param \Modules\Pricing\Model\Values\Price $priceToSubtract
     *
     * @return self
     */
    public function subtract(Price $priceToSubtract): self
    {
        return new self(
            $this->priceWithoutVat - $priceToSubtract->getPriceWithoutVat(),
            $this->priceWithVat - $priceToSubtract->getPriceWithVat()
        );
    }
}