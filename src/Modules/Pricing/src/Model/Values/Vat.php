<?php

namespace Modules\Pricing\Model\Values;

class Vat
{
    /**
     * Name.
     *
     * @var string
     */
    private $name;
    
    /**
     * Value.
     *
     * @var float
     */
    private $value;
    
    /**
     * Vat constructor.
     *
     * @param float       $value
     * @param string|null $name
     */
    public function __construct(float $value, string $name = null)
    {
        $this->name  = $name ?? 'Basic';
        $this->value = $this->normalize($value);
    }
    
    /**
     * Get value name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * Get value (0–1).
     *
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }
    
    /**
     * Get value in percent (0–100).
     *
     * @return float
     */
    public function getPercent(): float
    {
        return round($this->value * 100);
    }
    
    /**
     * Normalize value to percent.
     *
     * @param float $value
     *
     * @return float
     */
    private function normalize(float $value): float
    {
        $value = min($value, 1);
        $value = max($value, 0);
        
        return round($value, 2);
    }
}