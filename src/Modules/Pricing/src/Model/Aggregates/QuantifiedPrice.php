<?php


namespace Modules\Pricing\Model\Aggregates;

use Modules\Pricing\Model\Values\Price;
use Modules\Pricing\Model\Values\Vat;

class QuantifiedPrice
{
    /**
     * @var \Modules\Pricing\Model\Values\Price
     */
    private $unitPrice;
    
    /**
     * @var \Modules\Pricing\Model\Values\Price
     */
    private $totalPrice;
    
    /**
     * @var \Modules\Pricing\Model\Values\Vat
     */
    private $vat;
    
    /**
     * QuantifiedPrice constructor.
     *
     * @param \Modules\Pricing\Model\Values\Price $unitPrice
     * @param \Modules\Pricing\Model\Values\Price $totalPrice
     * @param \Modules\Pricing\Model\Values\Vat   $vat
     */
    public function __construct(Price $unitPrice, Price $totalPrice, Vat $vat)
    {
        $this->unitPrice  = $unitPrice;
        $this->totalPrice = $totalPrice;
        $this->vat        = $vat;
    }
    
    /**
     * @return \Modules\Pricing\Model\Values\Price
     */
    public function getUnitPrice(): Price
    {
        return $this->unitPrice;
    }
    
    /**
     * @return \Modules\Pricing\Model\Values\Price
     */
    public function getTotalPrice(): Price
    {
        return $this->totalPrice;
    }
    
    /**
     * @return \Modules\Pricing\Model\Values\Vat
     */
    public function getVat(): Vat
    {
        return $this->vat;
    }
}
