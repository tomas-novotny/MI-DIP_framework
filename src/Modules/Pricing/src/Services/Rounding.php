<?php

namespace Modules\Pricing\Services;

class Rounding
{
    /**
     * @param float $priceWithVat
     *
     * @return float
     */
    public function roundPriceWithVat(float $priceWithVat): float
    {
        return round($priceWithVat, 2);
    }
    
    /**
     * @param float $priceWithoutVat
     *
     * @return float
     */
    public function roundPriceWithoutVat(float $priceWithoutVat): float
    {
        return round($priceWithoutVat, 2);
    }
    
    /**
     * @param float $vatAmount
     *
     * @return float
     */
    public function roundVatAmount(float $vatAmount): float
    {
        return round($vatAmount, 2);
    }
}
