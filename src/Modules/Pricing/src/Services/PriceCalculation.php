<?php

namespace Modules\Pricing\Services;

use Modules\Pricing\Model\Values\Price;
use Modules\Pricing\Model\Values\Vat;

class PriceCalculation
{
    /**
     * @var \Modules\Pricing\Services\Rounding
     */
    private $rounding;
    
    /**
     * PriceCalculation constructor.
     *
     * @param \Modules\Pricing\Services\Rounding $rounding
     */
    public function __construct(Rounding $rounding)
    {
        $this->rounding = $rounding;
    }
    
    /**
     * @param float                             $inputPrice
     * @param bool                              $withVat
     * @param \Modules\Pricing\Model\Values\Vat $vat
     *
     * @return \Modules\Pricing\Model\Values\Price
     */
    public function calculatePrice(float $inputPrice, bool $withVat, Vat $vat): Price
    {
        $priceWithVat    = $this->getPriceWithVat($inputPrice, $withVat, $vat);
        $priceWithoutVat = $this->getPriceWithoutVat($priceWithVat, $vat);
        
        return new Price($priceWithVat, $priceWithoutVat);
    }
    
    /**
     * @param float                             $price
     * @param bool                              $withVat
     * @param \Modules\Pricing\Model\Values\Vat $vat
     *
     * @return float
     */
    private function getPriceWithVat(float $price, bool $withVat, Vat $vat): float
    {
        if ($withVat === false) {
            $price = $this->applyVatPercent($price, $vat);
        }
        
        $roundedPrice = $this->rounding->roundPriceWithoutVat($price);
        
        return $roundedPrice;
    }
    
    /**
     * @param float                             $priceWithVat
     * @param \Modules\Pricing\Model\Values\Vat $vat
     *
     * @return float
     */
    private function getPriceWithoutVat(float $priceWithVat, Vat $vat): float
    {
        $vatAmount             = $this->getVatAmountByPriceWithVat($priceWithVat, $vat);
        $priceWithoutVat       = $priceWithVat - $vatAmount;
        $roudedPriceWithoutVat = $this->rounding->roundPriceWithoutVat($priceWithoutVat);
        
        return $roudedPriceWithoutVat;
    }
    
    /**
     * @param float                             $priceWithVat
     * @param \Modules\Pricing\Model\Values\Vat $vat
     *
     * @return float
     */
    public function getVatAmountByPriceWithVat(float $priceWithVat, Vat $vat): float
    {
        $vatAmount        = $priceWithVat * $this->getVatCoefficientByPercent($vat->getPercent());
        $roundedVatAmount = $this->rounding->roundVatAmount($vatAmount);
        
        return $roundedVatAmount;
    }
    
    /**
     * @param float $percent
     *
     * @return float
     */
    public function getVatCoefficientByPercent(float $percent): float
    {
        $ratio = $percent / (100 + $percent);
        
        return round($ratio, 4);
    }
    
    /**
     * @param float                             $priceWithoutVat
     * @param \Modules\Pricing\Model\Values\Vat $vat
     *
     * @return float
     */
    public function applyVatPercent(float $priceWithoutVat, Vat $vat): float
    {
        $coefficient = (100 + $vat->getPercent()) / 100;
        
        return $priceWithoutVat * $coefficient;
    }
}
