<?php

use Modules\Localization\Model\Entities\Concretes\Doctrine\Language as DoctrineLanguage;
use Modules\Localization\Model\Entities\Concretes\Eloquent\Language as EloquentLanguage;
use Modules\Localization\Model\Entities\Contracts\LanguageInterface;
use Modules\Localization\Model\Factories\Concretes\DoctrineLanguageFactory;
use Modules\Localization\Model\Factories\Contracts\LanguageFactoryInterface;
use Modules\Localization\Model\Repositories\Concretes\DoctrineLanguageRepository;
use Modules\Localization\Model\Repositories\Concretes\EloquentLanguageFactory;
use Modules\Localization\Model\Repositories\Concretes\EloquentLanguageRepository;
use Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface;

return [
    'bindings' => [
        'eloquent' => [
            LanguageInterface::class           => EloquentLanguage::class,
            LanguageFactoryInterface::class    => EloquentLanguageFactory::class,
            LanguageRepositoryInterface::class => EloquentLanguageRepository::class,
        ],
        'doctrine' => [
            LanguageInterface::class           => DoctrineLanguage::class,
            LanguageFactoryInterface::class    => DoctrineLanguageFactory::class,
            LanguageRepositoryInterface::class => DoctrineLanguageRepository::class,
        ],
    ],
    
    'views' => [
        //
    ],
];