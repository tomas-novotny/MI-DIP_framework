<?php

namespace Modules\Localization\Model\Repositories\Contracts;

use Modules\Localization\Model\Entities\Contracts\LanguageInterface;
use Modules\Support\Contracts\CollectionInterface;

interface LanguageRepositoryInterface
{
    /**
     * Persist model to database.
     *
     * @param \Modules\Localization\Model\Entities\Contracts\LanguageInterface $language
     *
     * @return void
     */
    public function persist(LanguageInterface $language): void;
    
    /**
     * Get all languages
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface;
}
