<?php

namespace Modules\Localization\Model\Repositories\Concretes;

use Modules\Eloquent\EntityRepository;
use Modules\Localization\Model\Entities\Concretes\Eloquent\Language;
use Modules\Localization\Model\Entities\Contracts\LanguageInterface;
use Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;

class EloquentLanguageRepository implements LanguageRepositoryInterface
{
    /**
     * Entity repository.
     *
     * @var \Modules\Eloquent\EntityRepository
     */
    private $entityRepository;
    
    /**
     * EloquentProductRepository constructor.
     */
    public function __construct()
    {
        $this->entityRepository = new EntityRepository(Language::class);
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Localization\Model\Entities\Contracts\LanguageInterface $language
     *
     * @return void
     */
    public function persist(LanguageInterface $language): void
    {
        $this->entityRepository->persistAndFlush($language);
    }
    
    /**
     * Get all languages
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface
    {
        return new Collection($this->entityRepository->all());
    }
}
