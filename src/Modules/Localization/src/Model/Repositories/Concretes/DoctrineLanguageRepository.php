<?php

namespace Modules\Localization\Model\Repositories\Concretes;

use Doctrine\ORM\EntityManagerInterface;
use Modules\Doctrine\EntityRepository;
use Modules\Localization\Model\Entities\Concretes\Doctrine\Language;
use Modules\Localization\Model\Entities\Contracts\LanguageInterface;
use Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface;
use Modules\Support\Collection;
use Modules\Support\Contracts\CollectionInterface;

class DoctrineLanguageRepository implements LanguageRepositoryInterface
{
    /**
     * Entity repository.
     *
     * @var \Modules\Doctrine\EntityRepository
     */
    private $entityRepository;
    
    /**
     * DoctrineProductRepository constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityRepository = new EntityRepository($entityManager, Language::class);
    }
    
    /**
     * Persist model to database.
     *
     * @param \Modules\Localization\Model\Entities\Contracts\LanguageInterface $language
     *
     * @return void
     */
    public function persist(LanguageInterface $language): void
    {
        $this->entityRepository->persistAndFlush($language);
    }
    
    /**
     * Get all languages
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    public function all(): CollectionInterface
    {
        return new Collection($this->entityRepository->all());
    }
}
