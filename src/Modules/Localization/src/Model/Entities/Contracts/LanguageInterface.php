<?php

namespace Modules\Localization\Model\Entities\Contracts;

interface LanguageInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes constants
    |--------------------------------------------------------------------------
    */
    
    public const    TABLE                 = 'languages';
    
    public const    PRIMARY_KEY           = 'code';
    
    public const    FOREIGN_KEY           = 'language_code';
    
    public const    PRIMARY_KEY_SIZE      = 3;
    
    public const    ATTR_CODE             = self::PRIMARY_KEY;
    
    public const    ATTR_DISPLAY_NAME     = 'name';
    
    /**
     * Get language two-character code.
     *
     * @return string
     */
    public function getCode(): string;
    
    /**
     * Get language name.
     *
     * @return string
     */
    public function getName(): string;
    
    /**
     * Set language code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void;
    
    /**
     * Set language name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void;
}
