<?php

namespace Modules\Localization\Model\Entities\Concretes\Eloquent;

use Modules\Eloquent\Parents\AbstractModel;
use Modules\Localization\Model\Entities\Contracts\LanguageInterface;

class Language extends AbstractModel implements LanguageInterface
{
    /*
    |--------------------------------------------------------------------------
    | Basic connection setup
    |--------------------------------------------------------------------------
    */
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = LanguageInterface::TABLE;
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = LanguageInterface::PRIMARY_KEY;
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    
    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get language two-character code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->getAttribute(LanguageInterface::ATTR_CODE);
    }
    
    /**
     * Get language name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(LanguageInterface::ATTR_DISPLAY_NAME);
    }
    
    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */
    
    /**
     * Set language code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void
    {
        $this->setAttribute(LanguageInterface::ATTR_CODE, $code);
    }
    
    /**
     * Set language name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->setAttribute(LanguageInterface::ATTR_DISPLAY_NAME, $name);
    }
}
