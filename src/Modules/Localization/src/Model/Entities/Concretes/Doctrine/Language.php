<?php

namespace Modules\Localization\Model\Entities\Concretes\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Modules\Localization\Model\Entities\Contracts\LanguageInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="languages")
 */
class Language implements LanguageInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    
    /**
     * Language two-character code (ISO 639-1)
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $code;
    
    /**
     * Language name.
     *
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $name;
    
    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get language two-character code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
    
    /**
     * Set language code.
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */
    
    /**
     * Get language name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * Set language name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
