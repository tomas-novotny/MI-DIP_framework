<?php

namespace Modules\Localization\Model\Repositories\Concretes;

use Modules\Localization\Model\Entities\Concretes\Eloquent\Language;
use Modules\Localization\Model\Entities\Contracts\LanguageInterface;
use Modules\Localization\Model\Factories\Contracts\LanguageFactoryInterface;

class EloquentLanguageFactory implements LanguageFactoryInterface
{
    /**
     * Create new language instance.
     *
     * @param string $code
     * @param string $name
     *
     * @return \Modules\Localization\Model\Entities\Contracts\LanguageInterface
     */
    public function newLanguage(string $code, string $name): LanguageInterface
    {
        $language = new Language();
        
        $language->setCode($code);
        $language->setName($name);
        
        return $language;
    }
}
