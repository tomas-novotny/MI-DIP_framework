<?php

namespace Modules\Localization\Model\Factories\Contracts;

use Modules\Localization\Model\Entities\Contracts\LanguageInterface;

interface LanguageFactoryInterface
{
    /**
     * Create new language instance.
     *
     * @param string $code
     * @param string $name
     *
     * @return \Modules\Localization\Model\Entities\Contracts\LanguageInterface
     */
    public function newLanguage(string $code, string $name): LanguageInterface;
}
