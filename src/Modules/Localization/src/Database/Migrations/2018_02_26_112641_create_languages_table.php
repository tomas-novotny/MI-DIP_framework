<?php

use Illuminate\Database\Schema\Blueprint;
use Modules\Support\Parents\AbstractMigration;

class CreateLanguagesTable extends AbstractMigration
{
    /**
     * Run the migration up.
     *
     * @return void
     */
    public function up(): void
    {
        $this->builder()->create('languages', function (Blueprint $table) {
            $table->string('code', 3);
            $table->string('name');
            $table->boolean('active')->default(1);
            $table->boolean('fallback')->default(0);
            $table->timestamps();
            
            $table->primary('code');
        });
    }
    
    /**
     * Run the migration down.
     *
     * @return void
     */
    public function down(): void
    {
        //
    }
}
