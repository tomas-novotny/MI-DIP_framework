<?php

namespace Modules\Localization\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Localization\Model\Factories\Contracts\LanguageFactoryInterface;
use Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface;
use Modules\Support\Parents\AbstractSeeder;

class LanguagesTableSeeder extends AbstractSeeder
{
    /**
     * Language repository.
     *
     * @var \Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface
     */
    protected $repository;
    
    /**
     * Language factory.
     *
     * @var \Modules\Localization\Model\Factories\Contracts\LanguageFactoryInterface
     */
    protected $factory;
    
    /**
     * LanguagesTableSeeder constructor.
     *
     * @param \Modules\Localization\Model\Factories\Contracts\LanguageFactoryInterface       $factory
     * @param \Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface $repository
     */
    public function __construct(LanguageFactoryInterface $factory, LanguageRepositoryInterface $repository)
    {
        $this->factory    = $factory;
        $this->repository = $repository;
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();
        
        $language = $this->factory->newLanguage('Czech', 'cs');
        $language->setCode('cs');
        $language->setName('Czech');
        
        $this->repository->persist($language);
        
        $language = $this->factory->newLanguage('Czech', 'cs');
        $language->setCode('en');
        $language->setName('English');
        
        $this->repository->persist($language);
    }
}
