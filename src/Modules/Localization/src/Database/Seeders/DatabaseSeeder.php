<?php

namespace Modules\Localization\Database\Seeders;

use Modules\Support\Parents\AbstractSeeder;

class DatabaseSeeder extends AbstractSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguagesTableSeeder::class);
    }
}
