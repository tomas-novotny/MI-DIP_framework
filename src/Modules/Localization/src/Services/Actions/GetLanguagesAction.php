<?php

namespace Modules\Localization\Services\Actions;

use Modules\Bus\Parents\AbstractAction;
use Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface;

/**
 * Class GetLanguagesAction
 *
 * @package Modules\Order
 *
 * @author  Tomas Novotny <novott20@fit.cvut.cz>
 */
class GetLanguagesAction extends AbstractAction
{
    /**
     * Language repository.
     *
     * @var \Modules\Localization\Model\Repositories\Contracts\LanguageRepositoryInterface
     */
    private $languageRepository;
    
    /**
     * GetLanguagesAction constructor.
     */
    public function __construct()
    {
        $this->languageRepository = $this->resolve(LanguageRepositoryInterface::class);
    }
    
    /**
     * Get all languages.
     *
     * @return \Modules\Support\Collection
     */
    public function handle()
    {
        $languages = $this->languageRepository->all();
        
        return $languages;
    }
}
