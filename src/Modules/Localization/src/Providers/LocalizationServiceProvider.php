<?php

namespace Modules\Localization\Providers;

use Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider;
use Modules\Bus\Concerns\DispatchesServicesTrait;
use Modules\Bus\States\Contracts\UIInterface;
use Modules\Bus\States\UI\None;
use Modules\Localization\Services\Actions\GetLanguagesAction;
use RuntimeException;

class LocalizationServiceProvider extends LaravelLocalizationServiceProvider
{
    use DispatchesServicesTrait;
    
    /**
     * Get current UI.
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    public function getUI(): UIInterface
    {
        return new None();
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->setConfig();
        
        parent::register();
    }
    
    /**
     * Register localization.
     *
     * @return void
     */
    private function setConfig()
    {
        // get locales
        $locales = $this->getLocales();
        
        // always get some locales (from app config)
        $locales or $locales = $this->getDefaultLocales();
        
        // register config
        config(['laravellocalization' => [
            'supportedLocales'        => $locales,
            'useAcceptLanguageHeader' => true,
            'hideDefaultLocaleInURL'  => true,
        ]]);
    }
    
    /**
     * Get locales
     *
     * @return array
     */
    private function getLocales()
    {
        try {
            $locales   = [];
            $languages = $this->getLanguages();
            
            // transform to array
            foreach ($languages as $language) {
                $locales[$language->getCode()] = [
                    'name' => $language->getName(),
                ];
            }
            
            return $locales;
        }
        catch (RuntimeException $e) {
            return [];
        }
    }
    
    /**
     * Get languages.
     *
     * @return \Modules\Localization\Model\Entities\Contracts\LanguageInterface[]
     */
    private function getLanguages()
    {
        $languages = $this->dispatchService(new GetLanguagesAction());
        
        return $languages;
    }
    
    /**
     * Get default locale(s)
     *
     * @return array
     */
    private function getDefaultLocales()
    {
        return [
            config('app.locale') => ['name' => ''],
        ];
    }
}
