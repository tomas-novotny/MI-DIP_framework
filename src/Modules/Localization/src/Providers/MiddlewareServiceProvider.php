<?php

namespace Modules\Localization\Providers;

use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRoutes;
use Modules\Support\Parents\Providers\AbstractMiddlewareServiceProvider;

class MiddlewareServiceProvider extends AbstractMiddlewareServiceProvider
{
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'localize' => [
            LaravelLocalizationRoutes::class,
            LaravelLocalizationRedirectFilter::class,
        ],
    ];
}
