<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Http cache (varnish)
    |--------------------------------------------------------------------------
    |
    | "enabled"     use http-cache headers
    | "default"     if pages without http-cache specification shoul be cached
    | "lifetime"    http-cache lifetime in minutes
    |
    */
    
    'http' => [
        'enabled'  => env('CACHE_HTTP_ENABLED', false),
        'default'  => env('CACHE_HTTP_DEFAULT', true),
        'lifetime' => env('CACHE_HTTP_LIFETIME', 120),
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Eloquent cache
    |--------------------------------------------------------------------------
    |
    | "enabled"     use memcache for eloquent models
    | "queries"     use memcache for SQL queries
    | "lifetime"    eloquent-cache lifetime in minutes
    |
    */
    
    'eloquent' => [
        'enabled'  => env('CACHE_ELOQUENT_ENABLED', false),
        'queries'  => env('CACHE_ELOQUENT_QUERIES', false),
        'lifetime' => env('CACHE_ELOQUENT_LIFETIME', 0),
    ],
    
    /*
    |--------------------------------------------------------------------------
    | View cache
    |--------------------------------------------------------------------------
    |
    | "enabled"     use memcache for rendered html files
    | "lifetime"    view-cache lifetime in minutes
    |
    */
    
    'views' => [
        'enabled'  => env('CACHE_VIEWS_ENABLED', false),
        'lifetime' => env('CACHE_VIEWS_LIFETIME', 0),
    ],
];
