<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Eloquent cache
    |--------------------------------------------------------------------------
    |
    | "enabled"     use cdn for assets files
    | "domain"      full cdn domain
    | "subdomain"   cdn is in local subdomain
    | "secure"      use https scheme in cdn url
    |
    */
    
    'enabled'   => env('CDN_ENABLED', false),
    'domain'    => env('CDN_DOMAIN', null),
    'subdomain' => env('CDN_SUBDOMAIN', 'cdn'),
    'secure'    => env('CDN_SECURE', env('APP_SECURE', false)),
];
