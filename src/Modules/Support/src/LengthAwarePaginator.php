<?php

namespace Modules\Support;

use Illuminate\Pagination\LengthAwarePaginator as LaravelLengthAwarePaginator;
use Modules\Support\Contracts\CollectionInterface;

class LengthAwarePaginator extends LaravelLengthAwarePaginator implements CollectionInterface
{
    /**
     * Create a new paginator instance.
     *
     * @param  mixed    $items
     * @param  int      $total
     * @param  int      $perPage
     * @param  int|null $currentPage
     */
    public function __construct(array $items, int $total, int $perPage, int $currentPage = null)
    {
        parent::__construct($items, $total, $perPage, $currentPage);
    }
}
