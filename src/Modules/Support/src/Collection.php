<?php

namespace Modules\Support;

use Illuminate\Support\Collection as BaseCollection;
use Modules\Support\Contracts\CollectionInterface;

class Collection extends BaseCollection implements CollectionInterface
{
    /**
     * Create a new collection.
     * TODO: remove $depth from constructor -> move logic to private method
     *
     * @param mixed $items
     * @param int   $limit
     * @param int   $depth
     */
    public function __construct($items = [], int $limit = PHP_INT_MAX, int $depth = 1)
    {
        // should be collection in this depth
        if ($depth <= $limit) {
            // to array (only one depth)
            $items = to_array($items, $depth, $depth);
            
            // recursive call if array (to test another depth level)
            if (is_array($items)) {
                foreach ($items as $k => $v) {
                    if (is_array($v)) {
                        $items[$k] = new self($v, $limit, $depth + 1);
                    }
                }
            }
        }
        
        parent::__construct($items);
    }
}
