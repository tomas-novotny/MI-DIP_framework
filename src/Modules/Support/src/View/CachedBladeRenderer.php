<?php

namespace Modules\Support\View;

use BadMethodCallException;
use Illuminate\Contracts\Cache\Factory as CacheFactory;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Contracts\Config\Repository as Config;
use Modules\Support\Collection;
use Modules\Support\Contracts\ViewRendererInterface;

class CachedBladeRenderer implements ViewRendererInterface
{
    /**
     * Blade Renderer instance.
     *
     * @var \Modules\Support\View\BladeRenderer
     */
    private $bladeRenderer;
    
    /**
     * Cache store.
     *
     * @var \Illuminate\Contracts\Cache\Repository
     */
    private $cache;
    
    /**
     * View cache config.
     *
     * @var array
     */
    private $config;
    
    /**
     * CachedBladeRenderer constructor.
     *
     * @param \Modules\Support\View\BladeRenderer     $bladeRenderer
     * @param \Illuminate\Contracts\Cache\Factory     $cache
     * @param \Illuminate\Contracts\Config\Repository $config
     */
    public function __construct(BladeRenderer $bladeRenderer, CacheFactory $cache, Config $config)
    {
        $this->bladeRenderer = $bladeRenderer;
        $this->cache         = $cache->store();
        $this->config        = (array) $config->get('cache.views');
    }
    
    /**
     * Render view.
     *
     * @param string|null $view
     * @param array       $data
     * @param bool        $cached
     * @param array       $additionalKeyData
     *
     * @return string
     */
    public function render(string $view, array $data = [], bool $cached = true, array $additionalKeyData = []): string
    {
        // render view
        $renderedView = ($cached and $this->config['enabled'])
            ? $this->getCachedView($view, $data, $additionalKeyData)
            : $this->renderView($view, $data);
        
        // apply post-render logic (remove testing attributes, etc.)
        $renderedView = $this->updateRenderedView($renderedView);
        
        // return rendered view
        return $renderedView;
    }
    
    /**
     * Render view.
     *
     * @param string $view
     * @param array  $data
     *
     * @return string
     */
    private function renderView(string $view, array $data): string
    {
        return $this->bladeRenderer->render($view, $data, false);
    }
    
    /**
     * Get rendered view from cache.
     *
     * @param string $view
     * @param array  $data
     * @param array  $additionalKeyData
     *
     * @return string
     */
    private function getCachedView(string $view, array $data, array $additionalKeyData): string
    {
        $key  = $this->getCacheKey($view, $data, $additionalKeyData);
        $tags = $this->getCacheTags($view);
        
        // get cached view as rendered html code
        $renderedView = $this->getRenderedViewFromCache($key, $tags);
        
        // if view is not stored in cache, render it
        if ($renderedView === null) {
            $renderedView = $this->cacheView($key, $tags, $view, $data);
        }
        
        // replace token and exluded parts (auth user, shopping cart, CSRF token, etc.)
        $renderedView = $this->updateCachedView($renderedView, $data);
        
        // return rendered view
        return $renderedView;
    }
    
    /**
     * Get rendered view from cache.
     *
     * @param string   $key
     * @param string[] $tags
     *
     * @return string|null
     */
    private function getRenderedViewFromCache(string $key, array $tags): ?string
    {
        return $this->getTaggedCache($tags)->get($key);
    }
    
    /**
     * Cache view
     *
     * @param string   $key
     * @param string[] $tags
     * @param string   $view
     * @param array    $data
     *
     * @return string
     */
    private function cacheView(string $key, array $tags, string $view, array $data): string
    {
        // render html file from template as callback
        $renderedViewCallback = function () use ($view, $data) {
            return $this->renderView($view, $data);
        };
        
        // store value in tagged cache repository
        $minutes = (int) $this->config['lifetime'];
        
        // use different method for caching without timeout
        if ($minutes === 0) {
            return $this->getTaggedCache($tags)->rememberForever($key, $renderedViewCallback);
        }
        
        return $this->getTaggedCache($tags)->remember($key, $minutes, $renderedViewCallback);
    }
    
    /**
     * Get tagged cache if possible.
     *
     * @param string[] $tags
     *
     * @return \Illuminate\Contracts\Cache\Repository
     */
    private function getTaggedCache(array $tags): Cache
    {
        try {
            return $this->cache->tags($tags);
        }
        catch (BadMethodCallException $exception) {
            return $this->cache;
        }
    }
    
    /**
     * Apply post-render logic
     *
     * @param string $renderedView
     *
     * @return string
     */
    private function updateRenderedView(string $renderedView)
    {
        // TODO: implement post-render logic
        
        return $renderedView;
    }
    
    /**
     *  Update excluded html parts
     *
     * @param string $renderedView
     * @param array  $data
     *
     * @return string
     */
    private function updateCachedView(string $renderedView, array $data): string
    {
        // TODO: replace <exclude> parts
        
        // TODO: replace CSRF token
        
        return $renderedView;
    }
    
    /**
     * Get cache key for given view and data
     *
     * @param string $view
     * @param array  $data
     * @param array  $additionalKeyData
     *
     * @return string
     */
    private function getCacheKey(string $view, array $data, array $additionalKeyData): string
    {
        // get shared variables
        $shared = new Collection($this->bladeRenderer->getFactory()->getShared(), 1);
        
        // get key data
        $keyData = array_merge($additionalKeyData, [
            'ajax'           => request()->ajax(),
            'currency'       => null,
            'flash_messages' => session()->get('flash_notification', []),
            'host'           => request()->getHttpHost(),
            'locale'         => $shared->get('__locale', config('app.locale')),
            'view'           => $view,
        ]);
        
        // sort by keys
        ksort($keyData);
        
        // forget common variables
        $shared->forget($this->getCommonSharedDataKeys());
        
        // get view shared data as array
        $shared = $shared->toArray();
        ksort($shared);
        
        // get GET request data
        $getData = request()->except(['_']);
        ksort($getData);
        
        // generate view cache key with data
        $key = $this->generateHashKey($keyData + $data + $shared + $getData);
        
        // return hash key
        return $key;
    }
    
    /**
     * Get cache tags for given view
     *
     * @param string $view
     *
     * @return string[]
     */
    private function getCacheTags(string $view): array
    {
        // get cache tags
        $tags = [
            'views',
            $view,
        ];
        
        return $tags;
    }
    
    /**
     * Generate view hash key from given data.
     *
     * @param array $data
     *
     * @return string
     */
    private function generateHashKey(array $data): string
    {
        // TODO: use custom serialization
        return hash('md4', serialize($data));
    }
    
    /**
     * Get variables that are shared to all view but are unimportant for caching.
     *
     * @return string[]
     */
    private function getCommonSharedDataKeys(): array
    {
        // TODO: add more.
        return [
            '__env',
            '__stateless',
            '__locale',
            'app',
        ];
    }
}
