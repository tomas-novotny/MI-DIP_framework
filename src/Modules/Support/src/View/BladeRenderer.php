<?php

namespace Modules\Support\View;

use Illuminate\View\Factory as ViewFactory;
use Modules\Support\Contracts\ViewRendererInterface;

class BladeRenderer implements ViewRendererInterface
{
    /**
     * View factory instance.
     *
     * @var \Illuminate\View\Factory
     */
    private $viewFactory;
    
    /**
     * BladeRenderer constructor.
     *
     * @param \Illuminate\View\Factory $viewFactory
     */
    public function __construct(ViewFactory $viewFactory)
    {
        $this->viewFactory = $viewFactory;
    }
    
    /**
     * Render view.
     *
     * @param string|null $view
     * @param array       $data
     * @param bool        $cached
     * @param array       $additionalKeyData
     *
     * @return string
     */
    public function render(string $view, array $data = [], bool $cached = true, array $additionalKeyData = []): string
    {
        return $this->viewFactory->make($view, $data)->render();
    }
    
    /**
     * Get view factory.
     *
     * @return \Illuminate\View\Factory
     */
    public function getFactory(): ViewFactory
    {
        return $this->viewFactory;
    }
}
