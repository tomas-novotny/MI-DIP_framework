<?php

namespace Modules\Support;

use Kris\LaravelFormBuilder\FormBuilder as LaravelFormBuilder;
use Modules\Support\Contracts\FormBuilderInterface;
use Modules\Support\Contracts\FormInterface;
use RuntimeException as BaseRuntimeException;

class FormBuilder implements FormBuilderInterface
{
    /**
     * Kris Form builder
     *
     * @var \Kris\LaravelFormBuilder\FormBuilder
     */
    protected $builder;
    
    /**
     * FormBuilder constructor.
     *
     * @param \Kris\LaravelFormBuilder\FormBuilder $builder
     */
    public function __construct(LaravelFormBuilder $builder)
    {
        $this->builder = $builder;
    }
    
    /**
     * Create form instance.
     *
     * @param string $class
     * @param array  $data
     * @param array  $options
     *
     * @return \Modules\Support\Contracts\FormInterface
     */
    public function create(string $class, array $data = [], array $options = []): FormInterface
    {
        $form = $this->createFormInstance($class, $data, $options);
        
        if (!$form instanceof FormInterface) {
            throw new BaseRuntimeException('Form class does not implement ['.FormInterface::class.'] interface');
        }
        
        $form->build();
        
        return $form;
    }
    
    /**
     * Create form instance.
     *
     * @param string $class
     * @param array  $data
     * @param array  $options
     *
     * @return mixed
     */
    protected function createFormInstance(string $class, array $data = [], array $options = [])
    {
        $plainForm = $this->builder->plain($options, $data);
        
        return new $class($plainForm);
    }
}
