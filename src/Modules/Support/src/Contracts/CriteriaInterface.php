<?php

namespace Modules\Support\Contracts;

interface CriteriaInterface
{
    /**
     * Gets the expression attached to this Criteria.
     *
     * @return \Doctrine\Common\Collections\Expr\Expression|null
     */
    public function getWhereExpression();
    
    /**
     * Gets the current orderings of this Criteria.
     *
     * @return string[]
     */
    public function getOrderings();
    
    /**
     * Gets the current first result option of this Criteria.
     *
     * @return int|null
     */
    public function getOffset(): ?int;
    
    /**
     * Gets limit.
     *
     * @return int|null
     */
    public function getLimit(): ?int;
}
