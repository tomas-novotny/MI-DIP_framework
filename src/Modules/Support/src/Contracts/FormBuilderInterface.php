<?php

namespace Modules\Support\Contracts;

interface FormBuilderInterface
{
    /**
     * Create form instance.
     *
     * @param string $class
     * @param array  $data
     * @param array  $options
     *
     * @return \Modules\Support\Contracts\FormInterface
     */
    public function create(string $class, array $data = [], array $options = []): FormInterface;
}
