<?php

namespace Modules\Support\Contracts;

use Countable;
use ArrayAccess;
use JsonSerializable;
use IteratorAggregate;

interface CollectionInterface extends ArrayAccess, Countable, IteratorAggregate, JsonSerializable
{
    //
}
