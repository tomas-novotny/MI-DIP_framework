<?php

namespace Modules\Support\Contracts;

interface ViewRendererInterface
{
    /**
     * Render view.
     *
     * @param string|null $view
     * @param array       $data
     * @param bool        $cached
     * @param array       $additionalKeyData
     *
     * @return string
     */
    public function render(string $view, array $data = [], bool $cached = true, array $additionalKeyData = []): string;
}
