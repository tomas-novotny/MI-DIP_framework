<?php

namespace Modules\Support\Contracts;

use Illuminate\Contracts\Validation\ValidatesWhenResolved;

interface RequestInterface extends ValidatesWhenResolved
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool;
}
