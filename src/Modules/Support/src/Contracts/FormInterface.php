<?php

namespace Modules\Support\Contracts;

interface FormInterface
{
    /**
     * Build form fields.
     *
     * @return void
     */
    public function build(): void;
    
    /**
     * Render the whole form.
     *
     * @return string
     */
    public function render(): string;
}
