<?php

namespace Modules\Support\Contracts;

interface CanBeNotifyInterface
{
    /**
     * Send the given notification.
     *
     * @param \Modules\Support\Contracts\NotificationInterface $notification
     *
     * @return void
     */
    public function notify($notification);
}
