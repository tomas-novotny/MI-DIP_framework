<?php

namespace Modules\Support\Contracts;

interface NotificationInterface
{
    /**
     * Get the notification's delivery channels.
     *
     * @param \Modules\Support\Contracts\CanBeNotifyInterface $notifiable
     *
     * @return array
     */
    public function via(CanBeNotifyInterface $notifiable): array;
}
