<?php

namespace Modules\Support\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class UndefinedMethodException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = SymfonyResponse::HTTP_FORBIDDEN;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Undefined HTTP Verb.';
}
