<?php

namespace Modules\Support\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response;

class ValidationFailedException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Invalid Input.';
}
