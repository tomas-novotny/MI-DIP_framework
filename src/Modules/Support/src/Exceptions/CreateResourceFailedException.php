<?php

namespace Modules\Support\Exceptions;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class CreateResourceFailedException extends ResourceException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_EXPECTATION_FAILED;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Failed to create Resource.';
    
    /**
     * CreateResourceFailedException constructor.
     *
     * @param string                                    $model
     * @param \Throwable|null                           $previous
     * @param string|null                               $message
     * @param int|null                                  $statusCode
     * @param \Illuminate\Support\MessageBag|array|null $errors
     * @param int                                       $code
     * @param array                                     $headers
     */
    public function __construct(
        string $model,
        Throwable $previous = null,
        string $message = null,
        int $statusCode = null,
        $errors = null,
        int $code = 0,
        array $headers = []
    ) {
        parent::__construct($model, [], $previous, $message, $statusCode, $errors, $code, $headers);
    }
}
