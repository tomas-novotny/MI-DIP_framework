<?php

namespace Modules\Support\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response;

class NotImplementedException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_NOT_IMPLEMENTED;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'This method is not yet implemented.';
}
