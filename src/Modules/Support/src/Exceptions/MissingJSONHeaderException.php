<?php

namespace Modules\Support\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response;

class MissingJSONHeaderException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Your request must contain [Accept = application/json].';
}
