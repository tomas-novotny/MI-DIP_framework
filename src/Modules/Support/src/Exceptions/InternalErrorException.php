<?php

namespace Modules\Support\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response;

class InternalErrorException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Internal server error.';
}
