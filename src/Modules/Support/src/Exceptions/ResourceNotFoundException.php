<?php

namespace Modules\Support\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class ResourceNotFoundException extends ResourceException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_NOT_FOUND;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Resource not found.';
}
