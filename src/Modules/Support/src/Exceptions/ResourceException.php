<?php

namespace Modules\Support\Exceptions;

use Modules\Support\Parents\AbstractException;
use ReflectionClass;
use ReflectionException as BaseReflectionException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ResourceException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_NOT_FOUND;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Resource error.';
    
    /**
     * Name of the affected Eloquent model.
     *
     * @var string
     */
    protected $model;
    
    /**
     * The affected model IDs.
     *
     * @var array
     */
    protected $ids;
    
    /**
     * CreateResourceFailedException constructor.
     *
     * @param string                                    $model
     * @param int|array                                 $ids
     * @param \Throwable|null                           $previous
     * @param string|null                               $message
     * @param int|null                                  $statusCode
     * @param \Illuminate\Support\MessageBag|array|null $errors
     * @param int                                       $code
     * @param array                                     $headers
     */
    public function __construct(
        string $model,
        $ids = [],
        Throwable $previous = null,
        string $message = null,
        int $statusCode = null,
        $errors = null,
        int $code = 0,
        array $headers = []
    ) {
        $this->setModel($model, $ids);
        
        parent::__construct($message, $previous, $statusCode, $errors, $code, $headers);
    }
    
    /**
     * Get the affected Eloquent model.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }
    
    /**
     * Set the affected Eloquent model and instance ids.
     *
     * @param string    $model
     * @param int|array $ids
     *
     * @return $this
     */
    public function setModel(string $model, $ids = [])
    {
        try {
            $class = new ReflectionClass($model);
        }
        catch (BaseReflectionException $e) {
            return $this;
        }
        
        $this->model = config('app.debug') ? $class->name : str_replace('Interface', '', $class->getShortName());
        
        $this->ids = (array) ($ids);
        
        $message = rtrim($this->message, '.');
        
        $message .= ' for model ['.$this->model.']';
        
        if (count($this->ids) > 0) {
            $message .= ' with primary key(s) ['.implode(', ', $this->ids).'].';
        }
        else {
            $message .= '.';
        }
        
        $this->message = $message;
        
        return $this;
    }
}
