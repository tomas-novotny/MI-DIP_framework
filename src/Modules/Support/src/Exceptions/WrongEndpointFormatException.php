<?php

namespace Modules\Support\Exceptions;

use Modules\Support\Parents\AbstractException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class WrongEndpointFormatException extends AbstractException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Tests [$this->endpoint] property must be formatted as "verb@url".';
}
