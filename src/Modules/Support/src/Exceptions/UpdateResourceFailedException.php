<?php

namespace Modules\Support\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class UpdateResourceFailedException extends ResourceException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_EXPECTATION_FAILED;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Failed to update Resource.';
}
