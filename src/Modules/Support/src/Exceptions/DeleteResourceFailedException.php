<?php

namespace Modules\Support\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class DeleteResourceFailedException extends ResourceException
{
    /**
     * HTTP status error.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_EXPECTATION_FAILED;
    
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Failed to delete Resource.';
}
