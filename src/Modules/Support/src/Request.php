<?php

namespace Modules\Support;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Support\Contracts\RequestInterface;

class Request extends FormRequest implements RequestInterface
{
    /**
     * The input keys that should not be flashed on redirect.
     *
     * @var array
     */
    protected $dontFlash = [
        'password_confirmation',
        'password',
    ];
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get all of the input and files for the request.
     *
     * @param array|null $keys
     *
     * @return \Modules\Support\Collection
     */
    public function data(array $keys = null): Collection
    {
        $keys = is_array($keys) ? $keys : func_get_args();
        
        return new Collection($keys ? $this->only($keys) : $this->except(['_token']));
    }
    
    /**
     * The input without keys that should not be flashed on redirect.
     *
     * @var array
     */
    public function exceptDontFlash(): array
    {
        return $this->except($this->dontFlash);
    }
}
