<?php

namespace Modules\Support\Events;

use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Parents\AbstractEvent;

class ViewCreated extends AbstractEvent
{
    /**
     * View name.
     *
     * @var string
     */
    private $view;
    
    /**
     * View data.
     *
     * @var \Modules\Support\Collection
     */
    private $data;
    
    /**
     * ViewBuilding constructor.
     *
     * @param string                      $view
     * @param \Modules\Support\Collection $data
     */
    public function __construct(string $view, Collection $data)
    {
        $this->view = $view;
        $this->data = $data;
    }
    
    /**
     * Get view name.
     *
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }
    
    /**
     * Compare view by name.
     *
     * @param string $viewName
     * @param bool   $strict
     *
     * @return bool
     */
    public function isView(string $viewName, bool $strict = false): bool
    {
        if ($strict) {
            return $viewName === $this->view;
        }
        
        return str_contains($this->view, $viewName);
    }
    
    /**
     * Get view data
     *
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function getData(string $key, $default = null)
    {
        return $this->data->get($key, $default);
    }
    
    /**
     * Add new data to view.
     *
     * @param string $key
     * @param mixed  $data
     */
    public function addData(string $key, $data): void
    {
        if ($this->data->has($key)) {
            throw new InvalidArgumentException('Data with given key ['.$key.'] already exists');
        }
        
        $this->data->put($key, $data);
    }
    
    /**
     * Edit data.
     *
     * @param string $key
     * @param mixed  $data
     */
    public function editData(string $key, $data): void
    {
        $this->data->put($key, $data);
    }
    
    /**
     * Remove data from view.
     *
     * @param string $key
     */
    public function removeData(string $key): void
    {
        $this->data->offsetUnset($key);
    }
}
