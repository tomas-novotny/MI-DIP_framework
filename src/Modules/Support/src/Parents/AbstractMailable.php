<?php

namespace Modules\Support\Parents;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable as LaravelMailable;
use Illuminate\Queue\SerializesModels;

abstract class AbstractMailable extends LaravelMailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    abstract public function build();
}
