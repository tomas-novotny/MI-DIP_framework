<?php

namespace Modules\Support\Parents;

use Kris\LaravelFormBuilder\Form as LaravelForm;
use Modules\Support\Contracts\FormInterface;

abstract class AbstractForm implements FormInterface
{
    /**
     * Kris form instance.
     *
     * @var \Kris\LaravelFormBuilder\Form
     */
    protected $form;
    
    /**
     * Form constructor.
     *
     * @param \Kris\LaravelFormBuilder\Form $form
     */
    public function __construct(LaravelForm $form)
    {
        $this->form = $form;
    }
    
    /**
     * Build form fields.
     *
     * @return void
     */
    abstract public function build(): void;
    
    /**
     * Render the whole form.
     *
     * @return string
     */
    public function render(): string
    {
        return $this->form->renderForm();
    }
    
    /**
     * Create a new field and add it to the form.
     *
     * @param string $name
     * @param string $type
     * @param array  $options
     *
     * @return $this
     */
    protected function add(string $name, string $type = 'text', array $options = []): self
    {
        $this->form->add($name, $type, $options);
        
        return $this;
    }
    
    /**
     * Set form action url.
     *
     * @param string $url
     *
     * @return $this
     */
    protected function setUrl(string $url): self
    {
        $this->form->setUrl($url);
        
        return $this;
    }
    
    /**
     * Set form http method.
     *
     * @param string $method
     *
     * @return $this
     */
    protected function setMethod(string $method): self
    {
        $this->form->setMethod($method);
        
        return $this;
    }
}
