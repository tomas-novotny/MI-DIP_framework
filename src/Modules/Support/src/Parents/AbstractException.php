<?php

namespace Modules\Support\Parents;

use Error;
use Exception;
use Illuminate\Contracts\Support\MessageBag as MessageBagInterface;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException as SymfonyHttpException;
use Throwable;

abstract class AbstractException extends SymfonyHttpException
{
    /**
     * Default status code.
     *
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
    
    /**
     * Application enviroment
     *
     * @var string
     */
    protected $environment;
    
    /**
     * Errors
     *
     * @var \Illuminate\Contracts\Support\MessageBag
     */
    protected $errors;
    
    /**
     * Exception constructor.
     *
     * @param string|null                                         $message
     * @param \Throwable|null                                     $previous
     * @param int|null                                            $statusCode
     * @param \Illuminate\Contracts\Support\MessageBag|array|null $errors
     * @param int                                                 $code
     * @param array                                               $headers
     */
    public function __construct(
        string $message = null,
        Throwable $previous = null,
        int $statusCode = null,
        $errors = null,
        int $code = 0,
        array $headers = []
    ) {
        // detect and set the running environment
        $this->environment = env('app.env');
        
        $message      = $this->prepareMessage($message);
        $statusCode   = $this->prepareStatusCode($statusCode);
        $this->errors = $this->prepareErrors($errors);
    
        if ($previous instanceof Error) {
            $previous = new Exception($previous->getMessage(), $previous->getCode(), $previous);
        }
        
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }
    
    /**
     * Prepare exception error message.
     *
     * @param string|null $message
     *
     * @return string
     */
    private function prepareMessage(string $message = null): string
    {
        return $message !== null ? $message : $this->message;
    }
    
    /**
     * Prepare HTTP status code.
     *
     * @param int|null $statusCode
     *
     * @return int
     */
    private function prepareStatusCode(int $statusCode = null): int
    {
        return $statusCode !== null ? $statusCode : (int) $this->httpStatusCode;
    }
    
    /**
     * Prepare message error bag.
     *
     * @param \Illuminate\Contracts\Support\MessageBag|array|null $errors
     *
     * @return \Illuminate\Contracts\Support\MessageBag|null
     */
    private function prepareErrors($errors = null): ?MessageBagInterface
    {
        if ($errors instanceof MessageBag) {
            return $errors;
        }
        
        $messages = $errors !== null ? $errors : [];
        
        return new MessageBag((array) ($messages));
    }
}
