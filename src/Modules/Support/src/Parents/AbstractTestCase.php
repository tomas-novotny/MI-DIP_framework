<?php

namespace Modules\Support\Parents;

use Illuminate\Foundation\Testing\TestCase as LaravelTestCase;

abstract class AbstractTestCase extends LaravelTestCase
{
    //
}
