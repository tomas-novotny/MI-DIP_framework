<?php

namespace Modules\Support\Parents;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification as LaravelNotification;
use Modules\Support\Contracts\CanBeNotifyInterface;
use Modules\Support\Contracts\NotificationInterface;

abstract class AbstractNotification extends LaravelNotification implements ShouldQueue, NotificationInterface
{
    use Queueable;
    
    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param \Modules\Support\Contracts\CanBeNotifyInterface $notifiable
     *
     * @return array
     */
    abstract public function via(CanBeNotifyInterface $notifiable): array;
}
