<?php

namespace Modules\Support\Parents\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as LaravelEventServiceProvider;

abstract class AbstractEventServiceProvider extends LaravelEventServiceProvider
{
    //
}
