<?php

namespace Modules\Support\Parents\Providers;

use Illuminate\Contracts\Http\Kernel as KernelContract;
use Illuminate\Routing\Router;

abstract class AbstractMiddlewareServiceProvider extends AbstractServiceProvider
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middlewares = [
        //
    ];
    
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        //
    ];
    
    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        //
    ];
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMiddlewares();
    }
    
    /**
     * Register auth middlewares and middleware groups
     *
     * @return void
     */
    public function loadMiddlewares(): void
    {
        $this->registerMiddleware($this->middlewares);
        $this->registerMiddlewareGroups($this->middlewareGroups);
        $this->registerRouteMiddleware($this->routeMiddleware);
    }
    
    /**
     * Register middlewares.
     *
     * @param array $middlewares
     *
     * @return void
     */
    private function registerMiddleware(array $middlewares): void
    {
        $httpKernel = $this->app->make(KernelContract::class);
        
        foreach ($middlewares as $middleware) {
            $httpKernel->prependMiddleware($middleware);
        }
    }
    
    /**
     * Registering route middleware groups.
     *
     * @param array $middlewareGroups
     *
     * @return void
     */
    private function registerMiddlewareGroups(array $middlewareGroups): void
    {
        $router = $this->getRouter();
        
        foreach ($middlewareGroups as $key => $middlewares) {
            foreach ($middlewares as $middleware) {
                $router->pushMiddlewareToGroup($key, $middleware);
            }
        }
    }
    
    /**
     * Get router instance.
     *
     * @return \Illuminate\Routing\Router
     */
    private function getRouter(): Router
    {
        return $this->app->make(Router::class);
    }
    
    /**
     *  Registering route middlewares.
     *
     * @param array $routeMiddleware
     *
     * @return void
     */
    private function registerRouteMiddleware(array $routeMiddleware): void
    {
        $router = $this->getRouter();
        
        foreach ($routeMiddleware as $key => $middleware) {
            $router->aliasMiddleware($key, $middleware);
        }
    }
}
