<?php

namespace Modules\Support\Parents\Providers;

use Illuminate\Support\AggregateServiceProvider;
use Modules\Loader\Contracts\ModuleInterface;
use Modules\Loader\Loaders\AutoLoaderTrait;
use Modules\Loader\Module;
use ReflectionClass;

abstract class AbstractAutoloadServiceProvider extends AggregateServiceProvider
{
    use AutoLoaderTrait;
    
    /**
     * Modules.
     *
     * @var \Modules\Loader\Contracts\ModuleInterface instance
     */
    private $module;
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigsFromModule($this->getLocalModule());
        
        parent::register();
        
        $this->autoloadModule($this->getLocalModule());
    }
    
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    
    /**
     * Get local module instance
     *
     * @return \Modules\Loader\Contracts\ModuleInterface
     */
    protected function getLocalModule(): ModuleInterface
    {
        if ($this->module === null) {
            $class       = new ReflectionClass($this);
            $module_path = dirname($class->getFileName()).'/../../'.ModuleInterface::CONFIG_FILE;
            
            $this->module = Module::createFromConfig($module_path);
        }
        
        return $this->module;
    }
}
