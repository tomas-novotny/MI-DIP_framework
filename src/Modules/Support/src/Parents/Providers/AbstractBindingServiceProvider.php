<?php

namespace Modules\Support\Parents\Providers;

abstract class AbstractBindingServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    protected $contracts = [
        //
    ];
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->contracts as $abstract => $concrete) {
            // $this->app->bindIf($abstract, $concrete);
            $this->app->alias($concrete, $abstract);
        }
    }
    
    /**
     * Load bindings from config.
     *
     * @param string $config
     *
     * @return void
     */
    public function loadBindingsFromConfig(string $config): void
    {
        $driver = $this->config()->get('bindings.default');
        
        $bindings = $this->config()->get($config.'.'.$driver);
        
        if (is_array($bindings)) {
            $this->contracts = array_merge($this->contracts, $bindings);
        }
    }
}
