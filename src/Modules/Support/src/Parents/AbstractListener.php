<?php

namespace Modules\Support\Parents;

use Modules\Bus\Concerns\DispatchesServicesTrait;
use Modules\Bus\Contracts\CanDispatchServiceInterface;
use Modules\Bus\States\Contracts\UIInterface;
use Modules\Bus\States\UI\None;

abstract class AbstractListener implements CanDispatchServiceInterface
{
    use DispatchesServicesTrait;
    
    /**
     * Get current UI.
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    public function getUI(): UIInterface
    {
        return new None();
    }
}
