<?php

namespace Modules\Support\Parents\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as LaravelController;
use Modules\Bus\Concerns\DispatchesServicesTrait;
use Modules\Bus\Contracts\CanDispatchServiceInterface;
use Modules\Bus\Contracts\KnowsUIInterface;
use Modules\Bus\States\Contracts\UIInterface;

abstract class AbstractController extends LaravelController implements KnowsUIInterface, CanDispatchServiceInterface
{
    use ValidatesRequests;
    use DispatchesServicesTrait;
    
    /**
     * User interface state.
     *
     * @var \Modules\Bus\States\Contracts\UIInterface
     */
    protected $interface;
    
    /**
     * AbstractController constructor.
     *
     * @param \Modules\Bus\States\Contracts\UIInterface $interface
     */
    public function __construct(UIInterface $interface)
    {
        $this->interface = $interface;
    }
    
    /**
     * Set current UI.
     *
     * @param \Modules\Bus\States\Contracts\UIInterface $interface
     *
     * @return void
     */
    public function setUI(UIInterface $interface): void
    {
        $this->interface = $interface;
    }
    
    /**
     * Get current UI.
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    /**
     * Get current UI
     *
     * @return \Modules\Bus\States\Contracts\UIInterface
     */
    public function getUI(): UIInterface
    {
        return $this->interface;
    }
}
