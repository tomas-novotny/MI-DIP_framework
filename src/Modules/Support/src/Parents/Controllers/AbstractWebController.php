<?php

namespace Modules\Support\Parents\Controllers;

use Illuminate\Routing\Redirector;
use Modules\Bus\States\UI\Web;
use Modules\Support\Collection;
use Modules\Support\Contracts\FormBuilderInterface;
use Modules\Support\Contracts\FormInterface;
use Modules\Support\Contracts\ViewRendererInterface;
use Modules\Support\Events\ViewCreated;

abstract class AbstractWebController extends AbstractController
{
    /**
     * Additional data shared to view.
     *
     * @var array
     */
    private $additionalData = [];
    
    /**
     * Flash notifier.
     *
     * @var \Laracasts\Flash\FlashNotifier
     */
    private $flashNotifier;
    
    /**
     * Form builder.
     *
     * @var \Modules\Support\Contracts\FormBuilderInterface
     */
    private $formBuilder;
    
    /**
     * View renderer.
     *
     * @var \Modules\Support\Contracts\ViewRendererInterface
     */
    private $viewRenderer;
    
    /**
     * AbstractWebController constructor.
     *
     * @param \Modules\Support\Contracts\ViewRendererInterface $viewRenderer
     * @param \Modules\Support\Contracts\FormBuilderInterface  $formBuilder
     */
    public function __construct(ViewRendererInterface $viewRenderer, FormBuilderInterface $formBuilder)
    {
        // TODO: use interface
        $this->flashNotifier = app()->make('flash');
        $this->formBuilder   = $formBuilder;
        $this->viewRenderer  = $viewRenderer;
        
        parent::__construct(new Web());
    }
    
    /**
     * Share data to view.
     *
     * @param string $key
     * @param mixed  $data
     *
     * @return void
     */
    protected function shareData(string $key, $data): void
    {
        $this->additionalData[$key] = $data;
    }
    
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param string $view
     * @param array  $data
     * @param bool   $data
     *
     * @return string
     *
     * @event \Modules\Support\Events\ViewCreated
     */
    protected function view(string $view, array $data = [], bool $cached = false): string
    {
        $data = array_merge($this->additionalData, $data);
        $data = new Collection($data, 1);
        
        event(new ViewCreated($view, $data));
        
        return $this->viewRenderer->render($view, $data->all(), $cached);
    }
    
    /**
     * Create form via Form builder
     *
     * @param string $form
     * @param array  $data
     * @param array  $options
     *
     * @return \Modules\Support\Contracts\FormInterface
     */
    protected function createForm(string $form, array $data = [], array $options = []): FormInterface
    {
        return $this->formBuilder->create($form, $data, $options);
    }
    
    /**
     * Get an instance of the redirector.
     *
     * @return \Illuminate\Routing\Redirector
     */
    protected function redirect(): Redirector
    {
        return app()->make('redirect');
    }
    
    /**
     * Create generic flash message.
     *
     * @param string $type
     * @param string $translation
     * @param int    $count
     *
     * @return void
     */
    protected function addFlashMessage(string $type, string $translation, int $count = 1): void
    {
        $translation = trans_choice($translation, $count);
        
        $this->flashNotifier->message($translation, $type);
    }
    
    /**
     * Create info flash message.
     *
     * @param string $translation
     * @param int    $count
     *
     * @return void
     */
    protected function addInfoMessage(string $translation, int $count = 1): void
    {
        $this->addFlashMessage('info', $translation, $count);
    }
    
    /**
     * Create error flash message.
     *
     * @param string $translation
     * @param int    $count
     *
     * @return void
     */
    protected function addErrorMessage(string $translation, int $count = 1): void
    {
        $this->addFlashMessage('danger', $translation, $count);
    }
    
    /**
     * Create warning flash message.
     *
     * @param string $translation
     * @param int    $count
     *
     * @return void
     */
    protected function addWarningMessage(string $translation, int $count = 1): void
    {
        $this->addFlashMessage('warning', $translation, $count);
    }
    
    /**
     * Create success flash message.
     *
     * @param string $translation
     * @param int    $count
     *
     * @return void
     */
    protected function addSuccessMessage(string $translation, int $count = 1): void
    {
        $this->addFlashMessage('success', $translation, $count);
    }
    
    /**
     * Revitalize all flash messages before redirect
     *
     * @return void
     */
    protected function revitalizeMessages(): void
    {
        $messages = session()->get('flash_notification', []);
        
        foreach ($messages as $message) {
            $this->flashNotifier->message($message);
        }
    }
}
