<?php

namespace Modules\Support\Parents\Controllers;

use Illuminate\Http\JsonResponse;
use Modules\Bus\States\UI\API;
use ReflectionClass;
use Spatie\Fractal\Fractal;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController extends AbstractController
{
    /**
     * Meta data.
     *
     * @var array
     */
    protected $metaData = [];
    
    /**
     * AbstractApiController constructor.
     */
    public function __construct()
    {
        parent::__construct(new API());
    }
    
    /**
     * Transform data.
     *
     * @param mixed       $data
     * @param string $transformerClassName
     * @param array|null  $includes
     * @param array       $meta
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function transform($data, string $transformerClassName, array $includes = null, array $meta = [])
    {
        /* @var \Modules\Support\Parents\AbstractTransformer $transformer */
        $transformer = app()->make($transformerClassName, [$data]);
        
        // override default includes by the request includes
        if ($request_includes = request('include', '')) {
            $includes = explode(',', $request_includes);
        }
        
        if ($includes) {
            $includes = array_unique(array_merge($transformer->getDefaultIncludes(), $includes));
            $transformer->setDefaultIncludes($includes);
        }
        
        // add specific meta information to the response message
        $this->withMeta([
            'include' => $transformer->getAvailableIncludes(),
            'custom'  => $meta,
        ]);
        
        $fractal = Fractal::create($data, $transformer)->addMeta($this->metaData);
        
        // apply request filters if available in the request
        if ($requestFilters = request('filter')) {
            $result = $this->filterResponse($fractal->toArray(), explode(';', $requestFilters));
        }
        else {
            $result = $fractal->toJson();
        }
        
        return $result;
    }
    
    /**
     * Create json response.
     *
     * @param string|array $data
     * @param int          $status
     * @param array        $headers
     * @param int          $options
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function json($data, int $status = Response::HTTP_OK, array $headers = [], int $options = 0): JsonResponse
    {
        return new JsonResponse($data, $status, $headers, $options);
    }
    
    /**
     * Create accepted response
     *
     * @param string|array|null $message
     * @param int               $status
     * @param array             $headers
     * @param int               $options
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function accepted(
        $message = null,
        int $status = Response::HTTP_ACCEPTED,
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return $this->json($message !== null ? $message : '', $status, $headers, $options);
    }
    
    /**
     * Create deleted reponse
     * TODO: interface
     *
     * @param object|null $object
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleted($object = null): JsonResponse
    {
        if (is_null($object)) {
            return $this->accepted();
        }
        
        $type = (new ReflectionClass($object))->getShortName();
        
        return $this->accepted(['message' => "$type deleted Successfully."], Response::HTTP_NO_CONTENT);
    }
    
    /**
     * No content response.
     *
     * @param int $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function noContent(int $status = Response::HTTP_NO_CONTENT): JsonResponse
    {
        return $this->json(null, $status);
    }
    
    /**
     * Filter response
     *
     * @param array $data
     * @param array $filters
     *
     * @return array
     */
    private function filterResponse(array $data, array $filters): array
    {
        foreach ($data as $k => $v) {
            if (in_array($k, $filters, true)) {
                // we have found our element - so continue with the next one
                continue;
            }
            
            if (is_array($v)) {
                // it is an array - so go one step deeper
                $v = $this->filterResponse($v, $filters);
                if (empty($v)) {
                    // it is an empty array - delete the key as well
                    unset($data[$k]);
                }
                else {
                    $data[$k] = $v;
                }
                continue;
            }
            else {
                // check if the array is not in our filter-list
                if (in_array($k, $filters) === false) {
                    unset($data[$k]);
                    continue;
                }
            }
        }
        
        return $data;
    }
    
    /**
     * Add meta-data
     *
     * @param array $data
     *
     * @return $this
     */
    public function withMeta(array $data)
    {
        $this->metaData = $data;
        
        return $this;
    }
    
}