<?php

namespace Modules\Support\Parents;

use League\Fractal\TransformerAbstract as FractalTransformer;

abstract class AbstractTransformer extends FractalTransformer
{
    /**
     * Turn model object into a generic array.
     *
     * @param mixed $model
     *
     * @return array
     */
    abstract public function transform($model);
}
