<?php

namespace Modules\Support\Parents;

use Faker\Factory;
use Illuminate\Database\Seeder as LaravelSeeder;
use Illuminate\Support\Facades\DB;

abstract class AbstractSeeder extends LaravelSeeder
{
    /**
     * Flush table.
     *
     * @param string $table
     *
     * @return void
     */
    protected function flushTable(string $table)
    {
        DB::table($table)->delete();
    }
    
    /**
     * Create a new generator.
     *
     * @param string|null $locale
     *
     * @return \Faker\Generator
     */
    protected function faker(string $locale = null)
    {
        $locale = $locale ?: 'cs_CZ';
        
        return Factory::create($locale);
    }
}
