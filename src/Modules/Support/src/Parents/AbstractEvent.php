<?php

namespace Modules\Support\Parents;

use Illuminate\Queue\SerializesModels;

abstract class AbstractEvent
{
    use SerializesModels;
}
