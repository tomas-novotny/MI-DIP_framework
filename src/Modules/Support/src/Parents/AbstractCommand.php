<?php

namespace Modules\Support\Parents;

use Illuminate\Console\Command as LaravelCommand;

abstract class AbstractCommand extends LaravelCommand
{
    /**
     * Execute the console command.
     *
     * @return void
     */
    abstract public function handle(): void;
}
