<?php

namespace Modules\Support\Parents;

use Doctrine\Common\Collections\Criteria as DoctrineCriteria;
use Modules\Support\Contracts\CriteriaInterface;

// TODO: add interface
abstract class AbstractCriteria extends DoctrineCriteria implements CriteriaInterface
{
    public function getOffset(): ?int
    {
        return $this->getFirstResult();
    }
    
    public function getLimit(): ?int
    {
        return $this->getMaxResults();
    }
}
