<?php

namespace Modules\Support\Parents;

use App;
use Closure;
use Config;
use Illuminate\Http\Request;
use Modules\Support\Contracts\MiddlewareInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractMiddleware implements MiddlewareInterface
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function handle(Request $request, Closure $next): Response;
    
    /**
     * Get response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function response(Request $request, Closure $next): Response
    {
        return $next($request);
    }
}
