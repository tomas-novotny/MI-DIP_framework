<?php

namespace Modules\Support\Parents;

use Illuminate\Database\Migrations\Migration as LaravelMigration;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Facades\Schema;

//use Doctrine\DBAL\Migrations\AbstractMigration as DoctrineMigration;

abstract class AbstractMigration extends LaravelMigration
{
    const CASCADE = 'cascade';
    const SET_NULL = 'set null';
    
    /**
     * Schema builder.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function builder(): Builder
    {
        return Schema::getFacadeRoot();
    }
    
    /**
     * Run the migration up.
     *
     * @return void
     */
    abstract public function up(): void;
    
    /**
     * Run the migration down.
     *
     * @return void
     */
    abstract public function down(): void;
}
