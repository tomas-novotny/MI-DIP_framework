<?php

namespace Modules\Support\Providers;

use Illuminate\Support\Facades\Schema;
use Kris\LaravelFormBuilder\FormBuilderServiceProvider;
use Laracasts\Flash\FlashServiceProvider;
use Modules\Support\Parents\Providers\AbstractAutoloadServiceProvider;
use Vinkla\Hashids\HashidsServiceProvider;

class ServiceProvider extends AbstractAutoloadServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        FormBuilderServiceProvider::class,
        FlashServiceProvider::class,
        HashidsServiceProvider::class,
        BindingServiceProvider::class,
    ];
    
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        
        Schema::defaultStringLength(191);
    }
}
