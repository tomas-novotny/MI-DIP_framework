<?php

namespace Modules\Support\Providers;

use Modules\Support\Contracts\FormBuilderInterface;
use Modules\Support\Contracts\ViewRendererInterface;
use Modules\Support\FormBuilder;
use Modules\Support\Parents\Providers\AbstractBindingServiceProvider;
use Modules\Support\View\CachedBladeRenderer;

class BindingServiceProvider extends AbstractBindingServiceProvider
{
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $contracts = [
        ViewRendererInterface::class => CachedBladeRenderer::class,
        FormBuilderInterface::class  => FormBuilder::class,
    ];
}
