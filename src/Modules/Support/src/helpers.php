<?php

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection as LaravelCollection;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

if (function_exists('to_array') === false) {
    
    /**
     * Create array
     *
     * @param mixed $data
     * @param int   $limit
     * @param int   $depth
     *
     * @return array|mixed
     */
    function to_array($data, $limit = INF, int $depth = 1)
    {
        // should be array in this depth
        if ($depth <= $limit) {
            // convert arrayable objects to array
            if ($data instanceof LaravelCollection) {
                $data = $data->all();
            }
            elseif ($data instanceof Arrayable) {
                $data = $data->toArray();
            }
            
            // recursive call if array (to test another depth level)
            if (is_array($data)) {
                foreach ($data as $k => $v) {
                    $data[$k] = to_array($v, $limit, $depth + 1);
                }
            } // make array from anything if in depth level 1
            elseif ($depth == 1) {
                $data = [$data];
            }
        }
        
        return $data;
    }
}

if (function_exists('locale_prefix') === false) {
    
    /**
     * Get current url (with proper http(s) scheme)
     *
     * @param string|null $locale
     *
     * @return string|null
     */
    function locale_prefix(string $locale = null)
    {
        try {
            /* @var \Mcamara\LaravelLocalization\LaravelLocalization $localization */
            $localization = app()->make('laravellocalization');
            $prefix       = $localization->setLocale($locale);
        }
        catch (ReflectionException $e) {
            $prefix = $locale;
        }
        
        return $prefix;
    }
}

if (function_exists('web_router') === false) {
    
    /**
     * Register web routes with proper names, prefix and middlewares
     *
     * @param \Closure $callback
     *
     * @return void
     */
    function web_router(Closure $callback): void
    {
        $config = [
            'middleware' => ['web'],
        ];
        
        if (modules('localization')) {
            $config['middleware'][] = 'localize';
            $config['prefix']       = locale_prefix();
        }
        
        Route::group($config, $callback);
    }
}

if (function_exists('api_router') === false) {
    
    /**
     * Register api routes with proper names, prefix and middlewares
     *
     * @param string   $version
     * @param \Closure $callback
     *
     * @return void
     */
    function api_router(string $version, Closure $callback): void
    {
        $config = [
            'middleware' => ['web', 'api'],
            'prefix'     => 'api/'.$version,
            'as'         => 'api.',
        ];
        
        Route::group($config, $callback);
    }
}

if (function_exists('get_cookie') === false) {
    
    /**
     * Get cookie
     *
     * @param string     $name
     * @param mixed|null $default
     *
     * @return string|null
     */
    function get_cookie(string $name, $default = null): ?string
    {
        return Cookie::get($name, $default);
    }
}

if (function_exists('has_cookie') === false) {
    
    /**
     * Determinate if cookie exists (and has given value)
     *
     * @param string      $name
     * @param string|null $value
     *
     * @return bool
     */
    function has_cookie(string $name, string $value = null): bool
    {
        $exists = Cookie::has($name);
        
        if ($value !== null) {
            $exists = $exists and cookie($name)->getValue() === $value;
        }
        
        return $exists;
    }
}

if (function_exists('set_cookie') === false) {
    
    /**
     * Set cookie to next response
     *
     * @param string      $name
     * @param string|null $value
     * @param int|null    $minutes
     * @param bool        $httpOnly
     * @param bool        $forced
     * @param string|null $path
     * @param string|null $domain
     * @param bool        $secure
     *
     * @return void
     */
    function set_cookie(
        string $name,
        string $value = null,
        int $minutes = null,
        bool $httpOnly = true,
        bool $forced = false,
        string $path = null,
        string $domain = null,
        bool $secure = false
    ): void {
        if (has_cookie($name) === false or $forced) {
            $lifetime = $minutes ?: config('session.cookie_lifetime');
            
            cookie()->queue($name, $value, $lifetime, $path, $domain, $secure, $httpOnly);
        }
    }
}

if (function_exists('forget_cookie') === false) {
    
    /**
     * Forget cookie
     *
     * @param string $name
     *
     * @return void
     */
    function forget_cookie(string $name): void
    {
        if (has_cookie($name)) {
            cookie()->queue($name, 0, -2628000);
        };
    }
}

if (function_exists('currency_format') === false) {
    
    /**
     * Format number and add currency
     *
     * @param float    $value
     * @param int|null $precision
     *
     * @return string
     */
    function currency_format(float $value, int $precision = null): string
    {
        // TODO: set dynamically from currency instance
        $precision          = $precision ?: 2;
        $decimalPoint       = ',';
        $thousandsSeparator = ' ';
        
        $currencyPlacementAfter = true;
        $currencySymbol         = 'Kč';
        
        $price = number_format($value, $precision, $decimalPoint, $thousandsSeparator);
        
        $price = $currencyPlacementAfter ? $price.' '.$currencySymbol : $currencySymbol.' '.$price;
        
        return $price;
    }
}