#!/usr/bin/env bash

set -e
set -x

CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`

function split()
{
#    split_new_repo $1 $2

    SHA1=`./bin/splitsh-lite --prefix=$1`
    git push --force  $2 "$SHA1:$CURRENT_BRANCH"
}

function split_new_repo()
{
    TMP_DIR="/tmp/enqueue-repo"
    REMOTE_URL=`git remote get-url $2`

    rm -rf $TMP_DIR;
    mkdir $TMP_DIR;

    (
        cd $TMP_DIR;
        git clone $REMOTE_URL .;
        git checkout -b master;
        touch foo;
        git add foo;
        git commit -m "foo";
        git push origin master;
    );

    SHA1=`./bin/splitsh-lite --prefix=$1`
    git fetch $2
    git push $2 "$SHA1:$CURRENT_BRANCH" -f
}


function remote()
{
    git remote add $1 $2 || true
}

# TODO: use some config for used repositories
remote app https://github.com/novott20/modules-app.git
remote authentication https://github.com/novott20/modules-authentication.git
remote authorization https://github.com/novott20/modules-authorization.git
remote bus https://github.com/novott20/modules-bus.git
remote catalog https://github.com/novott20/modules-catalog.git
remote doctrine https://github.com/novott20/modules-doctrine.git
remote eloquent https://github.com/novott20/modules-eloquent.git
remote loader https://github.com/novott20/modules-loader.git
remote order https://github.com/novott20/modules-order.git
remote pricing https://github.com/novott20/modules-pricing.git
remote support https://github.com/novott20/modules-support.git
remote user https://github.com/novott20/modules-user.git

split 'src/Modules/App/' app
split 'src/Modules/Authentication/' authentication
split 'src/Modules/Authorization/' authorization
split 'src/Modules/Bus/' bus
split 'src/Modules/Catalog/' catalog
split 'src/Modules/Doctrine/' doctrine
split 'src/Modules/Eloquent/' eloquent
split 'src/Modules/Loader/' loader
split 'src/Modules/Order/' order
split 'src/Modules/Pricing/' pricing
split 'src/Modules/Support/' support
split 'src/Modules/User/' user