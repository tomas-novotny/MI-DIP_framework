# Modular ecommerce framework

[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]

> !!! WARNING, THIS MODULE IS UNDER DEVELOPEMENT. DO NOT INSTALL.

Ecommerce framework which use edited software architectural pattern [Porto SAP][link-porto]. 
Implements several modules to support quick and easy developement of web applications.

### Main Components Interaction Diagram
![](docs/components.png)

## Install

Via Composer

``` bash
$ composer require novott20/MI-DIP_framework
```

### Register Service Provider
Register Service Provider in `providers` array in `config/app.php` before your `AppServiceProvider::class`:
```php
'providers' => [
    // ...
    Modules\Loader\Providers\ServiceProvider::class,
    // ...
],
```

### Register bootstrap classes

Register main bootstrap classes into application. Extends existing classes with theirs equivalent in `app` module. 

Extends HTTP Kernel (`app\Http\Kernel.php`):
```php
namespace App\Http;

use Modules\App\Engine\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    //
}
```

Extends Console Kernel (`app\Console\Kernel.php`):
```php
namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Modules\App\Engine\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
  protected $commands = [
    // commands provided by your application
  ];

  protected function schedule(Schedule $schedule)
  {
      parent::schedule($schedule);
      // $schedule->command('inspire')->hourly();
  }
}
```

Extends Exception Handler (`app\Exceptions\Handler.php`):
```php
namespace App\Exceptions;

use Modules\App\Engine\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
  //
}
```

### Configuration files
Remove `config/auth.php` config file to use config from `user` module. You can always run 
```bash
$ php artisan vendor:publish --tag=config
```
to publish modules configuration files into project config folder.

## Usage

You can read how to use specific module in its own readme file.

### Modular architecture

This is monolithic repository, which consits of several modules. Module splitting its done with [Splitsh-lite](https://github.com/splitsh/lite) tool.

Splith modules into separate modules.
```bash
./bin/split.sh
```

Make new release (`v1.0.1`):
```bash
./bin/release.sh v1.0.1
```


#### Support modules

Support modules which can be used standalone.

+ [Loader](src/Modules/Loader/README.md) – Helps with modules autoloading. Automaticly register all modules.
+ [Support](src/Modules/Support/README.md) – Contains abstract classes and interfaces of common classes.
+ [Bus](src/Modules/Bus/README.md) – Implements service bus logic. System which hanlde bussiness login in specilized classes.
+ [App](src/Modules/App/README.md) – Set common app setting, exception handler and kernel middlewares.

#### Authentication modules

+ [Authentication](src/Modules/Authentication/README.md) – Extends Laravel authenticaton system with use of service bus.
+ [Authorization](src/Modules/Authorization/README.md) – Extends Laravel authorization system with use of service bus. Add roles and permissions to Laravel policies.
+ [User](src/Modules/User/README.md) – Implements both Authentication and Authorization module with User entity.

#### Ecommerce modules

+ [Catalog](src/Modules/Catalog/README.md) – Contains support classes to create custome catalog products.
+ [Order](src/Modules/Order/README.md) – Add shopping carts and orders with customizable shoppable products.

### Bussiness logic

There are service classes `Action` and `Tasks` which handle bussiness logic. For examples see individual module readme. 

Create Action class via extending class to `Modules\Bus\Parents\AbstractAction`

```php
use Modules\Bus\Parents\AbstractAction;

class AddPositiveNumbersAction extends AbstractAction
{
    protected $expectedExceptions = [
        \InvalidArgumentException::class,
    ];
    
    private $a;
    
    private $b;

    public function __construct(int $a, int $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
    
    public function handle()
    {
        if($this->a < 0 or $this->b < 0) {
            throw new \InvalidArgumentException('Can add only positive numbers');
        }
        
        return $this->a + $this->b; 
    }
}
```

Action can dispatch Tasks with accesible `dispatchTask()` method.

```php
use Modules\Bus\Parents\AbstractAction;
use Modules\Bus\Parents\AbstractTask;

class DivideNumbersTask extends AbstractTask
{
    protected $expectedExceptions = [
        \InvalidArgumentException::class,
    ];
    
    private $a;
    
    private $b;

    public function __construct(float $a, float $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
    
    public function handle()
    {
        if($this->b == 0) {
            throw new \InvalidArgumentException('Can add only positive numbers');
        }
        
        return $this->a / $this->b; 
    }
}

class MakeAdvanceCalculationAction extends AbstractAction
{   
    private $a;
    
    private $b;

    public function __construct(float $a, float $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
    
    public function handle()
    {
        try {
            $result = $this->dispatchTask(new DivideNumbersTask(3.14, $this->b));
        } catch(\InvalidArgumentException $exception) {
            $result = 2;
        }
        
        return $this->a * $result;
    }
}
```

You can dispatch services from Controllers or other classes with adding `Modules\Bus\Concerns\DispatchesServicesTrait`. 
It will add `dispatchService()` and also require `getUI()` method which should return current UI.
```php
use Modules\Bus\Concerns\DispatchesServicesTrait;
use Modules\Bus\States\Contracts\UIInterface;
use Modules\Bus\States\UI\Web;

class Controller
{
    use DispatchesServicesTrait;
    
    public function getUI(): UIInterface
    {
        return new Web();
    }
    
    public function getAction()
    {
        $data = $this->dispatchService(new MakeAdvanceCalculationAction(3.8, 10));
        
        return response()->json($data);
    }
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

To show coverage, run:

``` bash
$ composer test-coverage
```


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email novott20@fit.cvut.cz instead of using the issue tracker.

## Credits

- [Tomáš Novotný][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/novott20/MI-DIP_framework.svg
[ico-license]: https://img.shields.io/badge/license-MIT-blue.svg
[ico-travis]: https://travis-ci.org/novott20/MI-DIP_framework.svg?branch=master
[ico-scrutinizer]: https://scrutinizer-ci.com/g/novott20/MI-DIP_framework/badges/coverage.png?b=master
[ico-code-quality]: https://scrutinizer-ci.com/g/novott20/MI-DIP_framework/badges/quality-score.png?b=master
[ico-downloads]: https://img.shields.io/packagist/dt/novott20/MI-DIP_framework.svg

[link-packagist]: https://packagist.org/packages/novott20/MI-DIP_framework
[link-travis]: https://travis-ci.org/novott20/MI-DIP_framework
[link-scrutinizer]: https://scrutinizer-ci.com/g/novott20/MI-DIP_framework/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/novott20/MI-DIP_framework
[link-downloads]: https://packagist.org/packages/novott20/MI-DIP_framework
[link-author]: https://github.com/novott20
[link-contributors]: ../../contributors
[link-porto]: https://github.com/Mahmoudz/Porto
