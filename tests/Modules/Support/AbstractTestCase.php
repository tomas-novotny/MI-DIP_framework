<?php

namespace Modules\Tests\Support;

use Modules\Tests\AbstractTestCase as FrameworkAbstractTestCase;

abstract class AbstractTestCase extends FrameworkAbstractTestCase
{
    /**
     * Setup the test environment, before each test.
     */
    protected function setUp()
    {
         parent::setUp();
    }
}
