<?php

namespace Modules\Tests\Support\Unit;

use Modules\Eloquent\Collection as EloquentCollection;
use Modules\Support\Collection;
use Modules\Tests\Support\AbstractTestCase;

class CollectionTest extends AbstractTestCase
{
    public function testEmpty()
    {
        $this->assertEquals(new Collection([]), new Collection());
    }
    
    public function testNonArrayArgument()
    {
        $this->assertEquals(new Collection(['a']), new Collection('a'));
        $this->assertEquals(new Collection([2]), new Collection(2));
        $this->assertEquals(new Collection([0.99]), new Collection(0.99));
    }
    
    public function testNested()
    {
        $collection = new Collection(['a' => ['b', 'c']]);
        
        $this->assertInstanceOf(Collection::class, $collection->get('a'));
        $this->assertEquals(new Collection(['b', 'c']), $collection->get('a'));
    }
    
    public function testNestedWithLimitToOne()
    {
        $collection = new Collection(['a' => ['b', 'c']], 0);
        $this->assertEquals(['b', 'c'], $collection->get('a'));
        
        $collection = new Collection(['a' => ['b', 'c']], 1);
        $this->assertEquals(new Collection(['b', 'c']), $collection->get('a'));
    }
    
    public function testNestedWithLimit()
    {
        $nested = ['a' => ['b', 'c' => ['d']]];
        
        $collection = new Collection($nested, 0);
        $this->assertEquals(['b', 'c' => ['d']], $collection->get('a'));
        
        $collection = new Collection($nested, 1);
        $this->assertInstanceOf(Collection::class, $collection->get('a'));
        $this->assertEquals(['d'], $collection->get('a')->get('c'));
        
        $collection = new Collection($nested, 2);
        
        $this->assertInstanceOf(Collection::class, $collection->get('a'));
        $this->assertEquals(new Collection(['d']), $collection->get('a')->get('c'));
        
        $collection = new Collection($nested);
        
        $this->assertInstanceOf(Collection::class, $collection->get('a'));
        $this->assertEquals(new Collection(['d']), $collection->get('a')->get('c'));
    }
    
    public function testArrayableCollection()
    {
        $this->assertEquals(new Collection(['a']), new Collection(new EloquentCollection(['a'])));
        
        $collection = new Collection([new EloquentCollection(['a'])]);
        
        $this->assertInstanceOf(EloquentCollection::class, $collection->get(0));
        $this->assertEquals(new EloquentCollection(['a']), $collection->get(0));
    }
}
