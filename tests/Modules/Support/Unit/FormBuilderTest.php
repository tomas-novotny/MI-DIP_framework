<?php

namespace Modules\Tests\Support\Unit;

use Kris\LaravelFormBuilder\FormBuilder as LaravelFormBuilder;
use Modules\Support\Contracts\FormBuilderInterface;
use Modules\Support\Contracts\FormInterface;
use Modules\Support\FormBuilder;
use Modules\Tests\Support\AbstractTestCase;
use RuntimeException;

class FormBuilderTest extends AbstractTestCase
{
    public function testUseInterface()
    {
        $builder = $this->getBuilder();
        
        $this->assertInstanceOf(FormBuilderInterface::class, $builder);
    }
    
    public function testFormInterfaceValidation()
    {
        $builder = $this->getBuilder();
        
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Form class does not implement ['.FormInterface::class.'] interface');
        
        $form = new class
        {
            //
        };
        
        $builder->create(get_class($form));
    }
    
    public function testFormIsBuild()
    {
        $builder = $this->getBuilder();
        $form    = new class implements FormInterface
        {
            public $build = false;
            
            public function build(): void
            {
                $this->build = true;
            }
            
            public function render(): string
            {
                return $this->build ? 'data' : '';
            }
        };
        
        $this->assertEmpty($form->render());
        
        $output = $builder->create(get_class($form));
    
        $this->assertEquals('data', $output->render());
    }
    
    /**
     * @return \Modules\Support\FormBuilder
     */
    private function getBuilder()
    {
        return new FormBuilder($this->app->make(LaravelFormBuilder::class));
    }
}
