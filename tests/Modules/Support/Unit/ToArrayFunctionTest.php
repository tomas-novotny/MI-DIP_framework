<?php

namespace Modules\Tests\Support\Unit;

use Illuminate\Support\Collection;
use Modules\Tests\Support\AbstractTestCase;

class ToArrayFunctionTest extends AbstractTestCase
{
    /**
     * Test "to_array" function
     *
     * @return void
     */
    public function testStringInput()
    {
        $this->assertEquals([''], to_array(''));
        $this->assertEquals(['a'], to_array('a'));
        
        $this->assertEquals([2], to_array(2));
        $this->assertEquals(2, to_array(2, 0));
        
        $this->assertEquals('a', to_array('a', -1));
        $this->assertEquals('a', to_array('a', 0));
        $this->assertEquals(['a'], to_array('a', 1));
        $this->assertEquals(['a'], to_array('a', 2));
    }
    
    /**
     * Test "to_array" function
     *
     * @return void
     */
    public function testArrayInput()
    {
        $this->assertEquals([], to_array([]));
        $this->assertEquals([1, 2, 3], to_array([1, 2, 3]));
        
        $this->assertEquals(['a', 'b', 'c'], to_array(['a', 'b', 'c']));
        $this->assertEquals(['a' => 1, 'b' => 2, 'c'], to_array(['a' => 1, 'b' => 2, 'c']));
        $this->assertEquals(['a' => 1, 'b' => [2 => 'd'], 'c'], to_array(['a' => 1, 'b' => [2 => 'd'], 'c']));
    }
    
    /**
     * Test "to_array" function
     *
     * @return void
     */
    public function testCollectionInput()
    {
        $array      = ['a' => 1, 'b' => 2];
        $collection = new Collection($array);
        
        $this->assertEquals($collection, to_array($collection, 0));
        $this->assertEquals($array, to_array($collection, 1));
        $this->assertEquals($array, to_array($collection, 2));
        
        $collection_2 = new Collection(['a' => $collection, 'b' => 2]);
        
        $this->assertEquals(['a' => $array, 'b' => 2], to_array($collection_2));
        $this->assertEquals($collection_2, to_array($collection_2, 0));
        $this->assertEquals(['a' => $collection, 'b' => 2], to_array($collection_2, 1));
        $this->assertEquals(['a' => $array, 'b' => 2], to_array($collection_2, 2));
    }
}
