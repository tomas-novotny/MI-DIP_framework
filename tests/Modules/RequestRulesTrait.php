<?php

namespace Modules\Tests;

trait RequestRulesTrait
{
    /**
     * @param array $rules
     *
     * @return array
     */
    protected function getRulesNames(array $rules): array
    {
        $keys = array_keys($rules);
        
        sort($keys);
        
        return $keys;
    }
    
    /**
     * @param array  $rules
     * @param string $key
     *
     * @return array|mixed
     */
    protected function prepareRule(array $rules, string $key): array
    {
        $data = $rules[$key] ?? [];
        
        if (is_string($data)) {
            $data = explode('|', $data);
        }
        sort($data);
        
        return $data;
    }
}
