<?php

namespace Modules\Tests\Bus;

use Modules\Bus\ServiceDispatcher;
use Modules\Tests\AbstractTestCase as FrameworkAbstractTestCase;

abstract class AbstractTestCase extends FrameworkAbstractTestCase
{
    /**
     * @var \Modules\Bus\ServiceDispatcher
     */
    protected $dispatcher;
    
    /**
     * Setup the test environment, before each test.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        
        $this->dispatcher = $this->app->make(ServiceDispatcher::class);
    }
}
