<?php

namespace Modules\Tests\Bus;

use Modules\Bus\Parents\AbstractAction;
use Throwable;

class TestAction extends AbstractAction
{
    /**
     * Exceptions which do not trigger rollback() method.
     *
     * @var array
     */
    protected $expectedExceptions = [
        TestExpectedException::class,
    ];
    
    /**
     * Flag, if action should fail.
     *
     * @var bool
     */
    private $fails;
    
    /**
     * Flag, if action should fail.
     *
     * @var bool
     */
    private $expected;
    
    /**
     * Flag, if action "failed" method was called.
     *
     * @var bool
     */
    public $rollback;
    
    /**
     * Flag, if action was succesfully handled.
     *
     * @var bool
     */
    public $handled;
    
    /**
     * Actions tasks list
     *
     * @var array
     */
    public $tasks;
    
    /**
     * TestAction constructor.
     *
     * @param bool $fails
     * @param bool $expected
     */
    public function __construct(bool $fails, bool $expected = false, array $tasks = [])
    {
        $this->fails    = $fails;
        $this->expected = $expected;
        $this->tasks    = $tasks;
        $this->handled  = false;
        $this->rollback = false;
    }
    
    /**
     * Handle test action.
     *
     * @return bool
     *
     * @throws \Modules\Tests\Bus\TestExpectedException
     * @throws \Modules\Tests\Bus\TestUnexpectedException
     */
    public function handle()
    {
        if ($this->fails) {
            if ($this->expected) {
                throw new TestExpectedException('This should be expected.');
            }
            
            throw new TestUnexpectedException('This is unexpected.');
        }
        
        foreach ($this->tasks as $task) {
            $this->dispatchTask($task);
        }
        
        $this->handled = true;
        
        return true;
    }
    
    /**
     * Rollback action or task.
     *
     * @param \Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        $this->handled  = false;
        $this->rollback = true;
    }
}
