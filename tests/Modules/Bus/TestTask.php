<?php

namespace Modules\Tests\Bus;

use Modules\Bus\Parents\AbstractTask;
use Throwable;

class TestTask extends AbstractTask
{
    /**
     * Flag, if action "failed" method was called.
     *
     * @var bool
     */
    public $rollback;
    
    /**
     * Flag, if action was succesfully handled.
     *
     * @var bool
     */
    public $handled;
    
    /**
     * TestTask constructor.
     */
    public function __construct()
    {
        $this->handled  = false;
        $this->rollback = false;
    }
    
    /**
     * Handle test task.
     *
     * @return bool
     */
    public function handle()
    {
        $this->handled = true;
        
        return true;
    }
    
    /**
     * Rollback action or task.
     *
     * @param \Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        $this->handled  = false;
        $this->rollback = true;
    }
}
