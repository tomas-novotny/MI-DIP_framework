<?php

namespace Modules\Tests\Bus\Unit;

use Modules\Bus\Contracts\ServiceInterface;
use Modules\Bus\Exceptions\ServiceFailedException;
use Modules\Tests\Bus\AbstractTestCase;
use Modules\Tests\Bus\TestAction;
use Modules\Tests\Bus\TestExpectedException;

class ServiceDispatcherTest extends AbstractTestCase
{
    public function testActionIsHandled()
    {
        $action = new TestAction(false, false);
        
        $result = $this->dispatchService($action);
        
        $this->assertTrue($result);
        $this->assertTrue($action->handled, 'Service should be handled');
    }
    
    public function testExpectedExceptionDoNotTriggerFailedMethod()
    {
        $action = new TestAction(true, true);
        
        try {
            $this->dispatchService($action);
        }
        catch (TestExpectedException $exception) {
            $this->assertFalse($action->handled, 'Service should not be handled');
            $this->assertFalse($action->rollback, 'Failed method should not be called');
            
            return;
        }
        
        $this->assertTrue(false, 'Action did not thrown expected exception');
    }
    
    public function testFailedMethodIsCalledOnUnexpectedException()
    {
        $action = new TestAction(true, false);
        
        try {
            $this->dispatchService($action);
        }
        catch (ServiceFailedException $exception) {
            $this->assertFalse($action->handled, 'Service should not be handled');
            $this->assertTrue($action->rollback, 'Failed method should be called');
            
            return;
        }
        
        $this->assertTrue(false, 'Action did not thrown ServiceFailedException exception');
    }
    
    /**
     * @param \Modules\Bus\Contracts\ServiceInterface $service
     *
     * @return mixed
     *
     * @throws \Modules\Tests\Bus\TestExpectedException
     * @throws \Modules\Tests\Bus\TestUnexpectedException
     */
    private function dispatchService(ServiceInterface $service)
    {
        return $this->dispatcher->dispatchService($service);
    }
}
