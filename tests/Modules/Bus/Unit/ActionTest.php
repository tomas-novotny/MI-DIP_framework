<?php

namespace Modules\Tests\Bus\Unit;

use Modules\Bus\States\UI\API;
use Modules\Bus\States\UI\None;
use Modules\Tests\Bus\AbstractTestCase;
use Modules\Tests\Bus\TestAction;
use Modules\Tests\Bus\TestTask;

class ActionTest extends AbstractTestCase
{
    public function testActionHandleTask()
    {
        $task = new TestTask();
        
        $action = new TestAction(false, false, [$task]);
        
        $action->handle();
        
        $this->assertTrue($task->handled);
    }
    
    public function testActionHasNoneUIByDefault()
    {
        $task   = new TestTask();
        $action = new TestAction(false, false, [$task]);
        
        $action->handle();
        
        $this->assertEquals(new None(), $task->getUI());
    }
    
    public function testActionSetUIToTask()
    {
        $task = new TestTask();
        
        $action = new TestAction(false, false, [$task]);
        $action->setUI(new API());
        
        $action->handle();
    
        $this->assertEquals(new API(), $task->getUI());
    }
}
