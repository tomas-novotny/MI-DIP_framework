<?php

namespace Modules\Tests\Authorization;

use Faker\Generator;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface;
use Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface;

trait RoleFactoryTrait
{
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private $roleRepo;
    
    /**
     * Role repository.
     *
     * @var \Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface
     */
    private $roleFactory;
    
    /**
     * Create a new generator.
     *
     * @param string|null $locale
     *
     * @return \Faker\Generator
     */
    abstract protected function faker(string $locale = null): Generator;
    
    /**
     * @param string|null $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private function newRole(string $code = null): RoleInterface
    {
        $code = $code ?: $this->faker()->unique()->word;
        
        $role = $this->getRoleFactory()->newRole($code);
        
        $role->setDisplayName($this->faker()->name);
        
        return $role;
    }
    
    /**
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    protected function createRole(): RoleInterface
    {
        $role = $this->newRole();
        
        $this->getRoleRepo()->persist($role);
        
        return $role;
    }
    
    /**
     * @param int $count
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface[]
     */
    protected function createRoles(int $count = 1): array
    {
        $roles = [];
        
        $codes = $this->uniqueWords($count);
        
        for ($i = 0; $i < $count; $i++) {
            $role = $this->newRole($codes[$i]);
            
            $this->getRoleRepo()->persist($role);
            
            $roles[] = $role;
        }
        
        return $roles;
    }
    
    /**
     * @return \Modules\Authorization\Model\Repositories\Contracts\RoleRepositoryInterface
     */
    private function getRoleRepo()
    {
        if ($this->roleRepo === null) {
            $this->roleRepo = $this->app->make(RoleRepositoryInterface::class);
        }
        
        return $this->roleRepo;
    }
    
    /**
     * @return \Modules\Authorization\Model\Factories\Contracts\RoleFactoryInterface
     */
    private function getRoleFactory()
    {
        if ($this->roleFactory === null) {
            $this->roleFactory = $this->app->make(RoleFactoryInterface::class);
        }
        
        return $this->roleFactory;
    }
}
