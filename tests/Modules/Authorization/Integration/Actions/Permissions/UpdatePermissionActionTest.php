<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Events\PermissionDataEditing;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Services\Actions\Permission\UpdatePermissionAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;

class UpdatePermissionActionTest extends AbstractTestCase
{
    use PermissionFactoryTrait;
    
    public function testMissingCode()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test');
    }
    
    public function testRaiseEvent()
    {
        $permission = $this->createPermission();
        
        $this->expectsEvents(PermissionDataEditing::class);
        
        $this->handleAction($permission->getCode(), ['displayName' => 'Test user']);
    }
    
    public function testAttributes()
    {
        $permission = $this->createPermission();
        
        $code = $permission->getCode();
        $name = $permission->getDisplayName();
        
        $permission = $this->handleAction($code, ['displayName' => 'Test user']);
        
        $this->assertInstanceOf(PermissionInterface::class, $permission);
        $this->assertNotEquals($name, $permission->getDisplayName());
        $this->assertEquals('Test user', $permission->getDisplayName());
    }
    
    /**
     * Update permission action
     *
     * @param string $code
     * @param array  $data
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    private function handleAction(string $code, array $data = []): PermissionInterface
    {
        $action = new UpdatePermissionAction($code, new Collection($data));
        
        return $action->handle();
    }
}
