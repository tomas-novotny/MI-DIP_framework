<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Services\Actions\Permission\DeletePermissionAction;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;

class DeletePermissionActionTest extends AbstractTestCase
{
    use PermissionFactoryTrait;
    
    public function testPermissionNotFound()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test');
    }
    
    public function testDeleteNotPersisted()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $permission = $this->newPermission();
        
        $this->handleAction($permission->getCode());
    }
    
    public function testDeleted()
    {
        $permission = $this->createPermission();
        
        $this->assertCount(1, $this->getPermissionRepo()->all());
        
        $this->handleAction($permission->getCode());
        
        $this->assertCount(0, $this->getPermissionRepo()->all());
    }
    
    /**
     * Create permission action
     *
     * @param string $code
     *
     * @return void
     */
    private function handleAction(string $code): void
    {
        $action = new DeletePermissionAction($code);
        
        $action->handle();
    }
}
