<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Services\Actions\Permission\GetPermissionsAction;
use Modules\Support\Contracts\CollectionInterface;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;

class GetPermissionActionTest extends AbstractTestCase
{
    use PermissionFactoryTrait;
    
    public function testEmpty()
    {
        $permissions = $this->handleAction();
        
        $this->assertEmpty($permissions);
    }
    
    public function testCount()
    {
        $this->createPermissions(3);
        
        $permissions = $this->handleAction();
        
        $this->assertCount(3, $permissions);
    }
    
    /**
     * Get permissions.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    private function handleAction(): CollectionInterface
    {
        $action = new GetPermissionsAction();
        
        return $action->handle();
    }
}
