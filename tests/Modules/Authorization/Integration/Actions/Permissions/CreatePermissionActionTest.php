<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Events\PermissionDataEditing;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Services\Actions\Permission\CreatePermissionAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Tests\Authorization\AbstractTestCase;

class CreatePermissionActionTest extends AbstractTestCase
{
    public function testMissingCode()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('code');
        
        $this->handleAction([]);
    }
    
    public function testRaiseEvent()
    {
        $this->expectsEvents(PermissionDataEditing::class);
        
        $this->handleAction(['code' => 'test', 'displayName' => 'Test user']);
    }
    
    public function testAttributes()
    {
        $permission = $this->handleAction(['code' => 'test', 'displayName' => 'Test user']);
        
        $this->assertInstanceOf(PermissionInterface::class, $permission);
        $this->assertEquals('test', $permission->getCode());
        $this->assertEquals('Test user', $permission->getDisplayName());
    }
    
    /**
     * Create permission action
     *
     * @param array $data
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    private function handleAction(array $data): PermissionInterface
    {
        $action = new CreatePermissionAction(new Collection($data));
        
        return $action->handle();
    }
}
