<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Services\Actions\Role\RemovePermissionsFromRoleAction;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;
use Modules\Tests\Authorization\RoleFactoryTrait;

class RemovePermissionsFromRoleActionTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    use PermissionFactoryTrait;
    
    public function testRoleNotExists()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test', []);
    }
    
    public function testNoCodesDoNothing()
    {
        $role = $this->newRole();
        $role->addPermission($this->createPermission());
        $this->getRoleRepo()->persist($role);
        
        $this->handleAction($role->getCode(), []);
        
        $this->assertCount(1, $role->getPermissions());
    }
    
    public function testPermissionNotExists()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $role = $this->createRole();
        
        $this->handleAction($role->getCode(), ['test']);
    }
    
    public function testRemovePermissions()
    {
        $role = $this->newRole();
        
        $permissions = $this->createPermissions(3);
        
        foreach ($permissions as $permission) {
            $role->addPermission($permission);
        }
        
        $this->getRoleRepo()->persist($role);
        
        $this->handleAction($role->getCode(), [$permissions[1]->getCode()]);
        
        $this->assertCount(2, $role->getPermissions());
    }
    
    public function testIfAnyPermissionDontExistsDoNotAddAny()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $role = $this->newRole();
        
        $permissions = $this->createPermissions(3);
        
        foreach ($permissions as $permission) {
            $role->addPermission($permission);
        }
        
        $this->getRoleRepo()->persist($role);
        
        $this->handleAction($role->getCode(), [$permissions[1]->getCode(), '_non_existing']);
    }
    
    /**
     * Remove permissions from role.
     *
     * @param string $roleCode
     * @param array  $permissionCodes
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private function handleAction(string $roleCode, array $permissionCodes): RoleInterface
    {
        $action = new RemovePermissionsFromRoleAction($roleCode, $permissionCodes);
        
        return $action->handle();
    }
}
