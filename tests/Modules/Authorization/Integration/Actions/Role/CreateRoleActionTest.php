<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Events\RoleDataEditing;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Services\Actions\Role\CreateRoleAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Tests\Authorization\AbstractTestCase;

class CreateRoleActionTest extends AbstractTestCase
{
    public function testMissingCode()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('code');
        
        $this->handleAction([]);
    }
    
    public function testRaiseEvent()
    {
        $this->expectsEvents(RoleDataEditing::class);
        
        $this->handleAction(['code' => 'test', 'displayName' => 'Test user']);
    }
    
    public function testAttributes()
    {
        $role = $this->handleAction(['code' => 'test', 'displayName' => 'Test user']);
        
        $this->assertInstanceOf(RoleInterface::class, $role);
        $this->assertEquals('test', $role->getCode());
        $this->assertEquals('Test user', $role->getDisplayName());
    }
    
    /**
     * Create role action
     *
     * @param array $data
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private function handleAction(array $data): RoleInterface
    {
        $action = new CreateRoleAction(new Collection($data));
        
        return $action->handle();
    }
}
