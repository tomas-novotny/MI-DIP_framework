<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Events\RoleDataEditing;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Services\Actions\Role\UpdateRoleAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\RoleFactoryTrait;

class UpdateRoleActionTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    
    public function testMissingCode()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test');
    }
    
    public function testRaiseEvent()
    {
        $role = $this->createRole();
        
        $this->expectsEvents(RoleDataEditing::class);
        
        $this->handleAction($role->getCode(), ['displayName' => 'Test user']);
    }
    
    public function testAttributes()
    {
        $role = $this->createRole();
        
        $code = $role->getCode();
        $name = $role->getDisplayName();
        
        $role = $this->handleAction($code, ['displayName' => 'Test user']);
        
        $this->assertInstanceOf(RoleInterface::class, $role);
        $this->assertNotEquals($name, $role->getDisplayName());
        $this->assertEquals('Test user', $role->getDisplayName());
    }
    
    /**
     * Update role action
     *
     * @param string $code
     * @param array  $data
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private function handleAction(string $code, array $data = []): RoleInterface
    {
        $action = new UpdateRoleAction($code, new Collection($data));
        
        return $action->handle();
    }
}
