<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Services\Actions\Role\AddPermissionsToRoleAction;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;
use Modules\Tests\Authorization\RoleFactoryTrait;

class AddPermissionsToRoleActionTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    use PermissionFactoryTrait;
    
    public function testRoleNotExists()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test', []);
    }
    
    public function testNoCodesDoNothing()
    {
        $role = $this->createRole();
        
        $this->handleAction($role->getCode(), []);
        
        $this->assertEmpty($role->getPermissions());
    }
    
    public function testPermissionNotExists()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $role = $this->createRole();
        
        $this->handleAction($role->getCode(), ['test']);
    }
    
    public function testAddPermissions()
    {
        $role = $this->createRole();
        
        $permissions = $this->createPermissions(3);
        $codes       = [];
        
        foreach ($permissions as $permission) {
            $codes[] = $permission->getCode();
        }
        
        $this->handleAction($role->getCode(), $codes);
        
        $this->assertCount(3, $role->getPermissions());
    }
    
    public function testIfAnyPermissionDontExistsDoNotAddAny()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $role = $this->createRole();
        
        $permissions = $this->createPermissions(3);
        $codes       = [];
        
        foreach ($permissions as $permission) {
            $codes[] = $permission->getCode();
        }
        
        $codes[] = '_non_existing';
        
        $this->handleAction($role->getCode(), $codes);
    }
    
    /**
     * Add permissions to role.
     *
     * @param string $roleCode
     * @param array  $permissionCodes
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private function handleAction(string $roleCode, array $permissionCodes): RoleInterface
    {
        $action = new AddPermissionsToRoleAction($roleCode, $permissionCodes);
        
        return $action->handle();
    }
}
