<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Services\Actions\Role\GetRolesAction;
use Modules\Support\Contracts\CollectionInterface;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\RoleFactoryTrait;

class GetRoleActionTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    
    public function testEmpty()
    {
        $roles = $this->handleAction();
        
        $this->assertEmpty($roles);
    }
    
    public function testCount()
    {
        $this->createRoles(2);
        
        $roles = $this->handleAction();
        
        $this->assertCount(2, $roles);
    }
    
    /**
     * Get roles.
     *
     * @return \Modules\Support\Contracts\CollectionInterface
     */
    private function handleAction(): CollectionInterface
    {
        $action = new GetRolesAction();
        
        return $action->handle();
    }
}
