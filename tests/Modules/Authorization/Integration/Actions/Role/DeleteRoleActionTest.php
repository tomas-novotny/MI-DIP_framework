<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Authorization\Services\Actions\Role\DeleteRoleAction;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\RoleFactoryTrait;

class DeleteRoleActionTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    
    public function testRoleNotFound()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test');
    }
    
    public function testDeleteNotPersisted()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $role = $this->newRole();
        
        $this->handleAction($role->getCode());
    }
    
    public function testDeleted()
    {
        $role = $this->createRole();
        
        $this->assertCount(1, $this->getRoleRepo()->all());
        
        $this->handleAction($role->getCode());
        
        $this->assertCount(0, $this->getRoleRepo()->all());
    }
    
    /**
     * Create role action
     *
     * @param string $code
     *
     * @return void
     */
    private function handleAction(string $code): void
    {
        $action = new DeleteRoleAction($code);
        
        $action->handle();
    }
}
