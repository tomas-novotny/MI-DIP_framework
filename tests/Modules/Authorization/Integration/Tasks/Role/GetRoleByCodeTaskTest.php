<?php

namespace Modules\Tests\Authorization\Integration\Test\Role;

use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Services\Tasks\Role\GetRoleByCodeTask;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\RoleFactoryTrait;

class GetRoleByCodeTaskTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    
    public function testEmpty()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleTask('test');
    }
    
    public function testFound()
    {
        $roles = $this->createRoles(5);
        
        $role = $roles[3];
        
        $foundRole = $this->handleTask($role->getCode());
        
        $this->assertEquals($role->getCode(), $foundRole->getCode());
        $this->assertEquals($role->getDisplayName(), $foundRole->getDisplayName());
    }
    
    /**
     * Get roles.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    private function handleTask(string $code): RoleInterface
    {
        $task = new GetRoleByCodeTask($code);
        
        return $task->handle();
    }
}
