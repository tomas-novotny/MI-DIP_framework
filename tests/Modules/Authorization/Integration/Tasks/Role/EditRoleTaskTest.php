<?php

namespace Modules\Tests\Authorization\Integration\Tasks\Role;

use Modules\Authorization\Events\RoleDataEditing;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Authorization\Services\Tasks\Role\EditRoleTask;
use Modules\Support\Collection;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\RoleFactoryTrait;

class EditRoleTaskTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    
    public function testRaiseEvent()
    {
        $this->expectsEvents(RoleDataEditing::class);
        
        $role = $this->newRole();
        
        $this->handleTask($role, 'test');
    }
    
    public function testEditDisplayName()
    {
        $role = $this->newRole();
        
        $edited = $this->handleTask($role, 'test');
        
        $this->assertEquals('test', $edited->getDisplayName());
    }
    
    private function handleTask(RoleInterface $role, string $displayName, array $data = []): RoleInterface
    {
        $task = new EditRoleTask($role, $displayName, new Collection($data));
        
        return $task->handle();
    }
}
