<?php

namespace Modules\Tests\Authorization\Integration\Test\Permission;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Services\Tasks\Permission\GetPermissionByCodeTask;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;

class GetPermissionByCodeTaskTest extends AbstractTestCase
{
    use PermissionFactoryTrait;
    
    public function testEmpty()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleTask('test');
    }
    
    public function testFound()
    {
        $permissions = $this->createPermissions(5);
        
        $permission = $permissions[3];
        
        $foundPermission = $this->handleTask($permission->getCode());
        
        $this->assertEquals($permission->getCode(), $foundPermission->getCode());
        $this->assertEquals($permission->getDisplayName(), $foundPermission->getDisplayName());
    }
    
    /**
     * Get permissions.
     *
     * @param string $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    private function handleTask(string $code): PermissionInterface
    {
        $task = new GetPermissionByCodeTask($code);
        
        return $task->handle();
    }
}
