<?php

namespace Modules\Tests\Authorization\Integration\Tasks\Permission;

use Modules\Authorization\Events\PermissionDataEditing;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Services\Tasks\Permission\EditPermissionTask;
use Modules\Support\Collection;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;

class EditPermissionTaskTest extends AbstractTestCase
{
    use PermissionFactoryTrait;
    
    public function testRaiseEvent()
    {
        $this->expectsEvents(PermissionDataEditing::class);
        
        $permission = $this->newPermission();
        
        $this->handleTask($permission, 'test');
    }
    
    public function testEditDisplayName()
    {
        $permission = $this->newPermission();
        
        $edited = $this->handleTask($permission, 'test');
        
        $this->assertEquals('test', $edited->getDisplayName());
    }
    
    private function handleTask(PermissionInterface $permission, string $displayName, array $data = []): PermissionInterface
    {
        $task = new EditPermissionTask($permission, $displayName, new Collection($data));
        
        return $task->handle();
    }
}
