<?php

namespace Modules\Tests\Authorization;

use Modules\Tests\AbstractTestCase as FrameworkAbstractTestCase;

abstract class AbstractTestCase extends FrameworkAbstractTestCase
{
    /**
     * Setup the test environment, before each test.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        
        $this->runDatabaseMigrations();
        
        $this->withoutNotifications();
    }
}
