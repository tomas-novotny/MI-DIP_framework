<?php

namespace Modules\Tests\Authorization;

use Faker\Generator;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Factories\Contracts\PermissionFactoryInterface;
use Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface;

trait PermissionFactoryTrait
{
    /**
     * Permission repository.
     *
     * @var \Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface
     */
    private $permissionRepo;
    
    /**
     * Permission factory.
     *
     * @var \Modules\Authorization\Model\Factories\Contracts\PermissionFactoryInterface
     */
    private $permissionFactory;
    
    /**
     * Create a new generator.
     *
     * @param string|null $locale
     *
     * @return \Faker\Generator
     */
    abstract protected function faker(string $locale = null): Generator;
    
    /**
     * @param string|null $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    private function newPermission(string $code = null): PermissionInterface
    {
        $code = $code ?: $this->faker()->unique()->word;
        
        $permission = $this->getPermissionFactory()->newPermission($code);
        
        $permission->setDisplayName($this->faker()->name);
        
        return $permission;
    }
    
    /**
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    protected function createPermission(): PermissionInterface
    {
        $permission = $this->newPermission();
        
        $this->getPermissionRepo()->persist($permission);
        
        return $permission;
    }
    
    /**
     * @param int $count
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface[]
     */
    protected function createPermissions(int $count = 1): array
    {
        $permissions = [];
        
        $codes = $this->uniqueWords($count);
        
        for ($i = 0; $i < $count; $i++) {
            $permission = $this->newPermission($codes[$i]);
            
            $this->getPermissionRepo()->persist($permission);
            
            $permissions[] = $permission;
        }
        
        return $permissions;
    }
    
    /**
     * @return \Modules\Authorization\Model\Repositories\Contracts\PermissionRepositoryInterface
     */
    protected function getPermissionRepo()
    {
        if ($this->permissionRepo === null) {
            $this->permissionRepo = $this->app->make(PermissionRepositoryInterface::class);
        }
        
        return $this->permissionRepo;
    }
    
    /**
     * @return \Modules\Authorization\Model\Factories\Contracts\PermissionFactoryInterface
     */
    protected function getPermissionFactory()
    {
        if ($this->permissionFactory === null) {
            $this->permissionFactory = $this->app->make(PermissionFactoryInterface::class);
        }
        
        return $this->permissionFactory;
    }
}
