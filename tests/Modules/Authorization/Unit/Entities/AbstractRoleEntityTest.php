<?php

namespace Modules\Tests\Authorization\Unit\Entities;

use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Tests\Authorization\AbstractTestCase;

abstract class AbstractRoleEntityTest extends AbstractTestCase
{
    abstract public function newRole(string $code = null): RoleInterface;
    
    abstract public function newPermission(string $code = null): PermissionInterface;
    
    public function testAddPermission()
    {
        $role = $this->newRole();
        
        $this->assertEmpty($role->getPermissions());
        
        $role->addPermission($this->newPermission('test1'));
        
        $this->assertCount(1, $role->getPermissions());
        $role->hasPermission('test1');
        
        $this->assertEquals('test1', $role->getPermissions()->offsetGet(0)->getCode());
    }
    
    public function testDoNotAddPermissionIfExists()
    {
        $role = $this->newRole();
        
        $permission = $this->newPermission('test1');
        
        $role->addPermission($permission);
        $role->addPermission($permission);
        $role->addPermission($permission);
        
        $this->assertCount(1, $role->getPermissions());
    }
    
    public function testHasPermission()
    {
        $role = $this->newRole();
        
        $permissions = [
            $this->newPermission('test1'),
            $this->newPermission('test2'),
            $this->newPermission('test3'),
        ];
        
        foreach ($permissions as $permission) {
            $role->addPermission($permission);
        }
    
        $this->assertTrue($role->hasPermission('test1'));
        $this->assertFalse($role->hasPermission('test4'));
        $this->assertTrue($role->hasPermission('test2'));
        $this->assertFalse($role->hasPermission(''));
        $this->assertTrue($role->hasPermission('test3'));
    }
    
    public function testRemovePermission()
    {
        $role = $this->newRole();
        
        $this->assertEmpty($role->getPermissions());
        
        $permissions = [
            $this->newPermission('test1'),
            $this->newPermission('test2'),
            $this->newPermission('test3'),
        ];
        
        foreach ($permissions as $permission) {
            $role->addPermission($permission);
        }
        
        $this->assertCount(3, $role->getPermissions());
        
        $role->removePermission($permissions[1]);
        
        $this->assertCount(2, $role->getPermissions());
        
        $this->assertFalse($role->hasPermission('test2'));
    }
}
