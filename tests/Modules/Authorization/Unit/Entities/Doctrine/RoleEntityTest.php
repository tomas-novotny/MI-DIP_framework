<?php

namespace Modules\Tests\Authorization\Unit\Entities\Doctrine;

use Modules\Authorization\Model\Entities\Concretes\Doctrine\Permission;
use Modules\Authorization\Model\Entities\Concretes\Doctrine\Role;
use Modules\Authorization\Model\Entities\Contracts\PermissionInterface;
use Modules\Authorization\Model\Entities\Contracts\RoleInterface;
use Modules\Tests\Authorization\Unit\Entities\AbstractRoleEntityTest;

class RoleEntityTest extends AbstractRoleEntityTest
{
    public function setUp()
    {
        //
    }
    
    /**
     * @param string|null $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\RoleInterface
     */
    public function newRole(string $code = null): RoleInterface
    {
        $role = new Role();
        $role->setCode($code ?: $this->faker()->word);
        
        return $role;
    }
    
    /**
     * @param string|null $code
     *
     * @return \Modules\Authorization\Model\Entities\Contracts\PermissionInterface
     */
    public function newPermission(string $code = null): PermissionInterface
    {
        $permission = new Permission();
        $permission->setCode($code ?: $this->faker()->word);
        
        return $permission;
    }
}
