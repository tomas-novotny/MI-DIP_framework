<?php

namespace Modules\Tests\Authorization\Unit\Transformers;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\PermissionTransformer;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\PermissionFactoryTrait;

class PermissionTransformerTest extends AbstractTestCase
{
    use PermissionFactoryTrait;
    
    public function testAvailableIncudes()
    {
        $includes = $this->getTransformer()->getAvailableIncludes();
        $expected = [];
        
        $this->assertEquals($expected, $includes);
    }
    
    public function testDefaultIncudes()
    {
        $includes = $this->getTransformer()->getDefaultIncludes();
        $expected = [];
        
        $this->assertEquals($expected, $includes);
    }
    
    public function testTransform()
    {
        $permission = $this->newPermission();
        
        $transformed = $this->getTransformer()->transform($permission);
        ksort($transformed);
        
        $expected = [
            'code'        => $permission->getCode(),
            'displayName' => $permission->getDisplayName(),
        ];
        
        $this->assertEquals($expected, $transformed);
    }
    
    public function getTransformer()
    {
        return new PermissionTransformer();
    }
}
