<?php

namespace Modules\Tests\Authorization\Unit\Transformers;

use Modules\Authorization\Http\Controllers\API\Transformers\Concretes\RoleTransformer;
use Modules\Tests\Authorization\AbstractTestCase;
use Modules\Tests\Authorization\RoleFactoryTrait;

class RoleTransformerTest extends AbstractTestCase
{
    use RoleFactoryTrait;
    
    public function testAvailableIncudes()
    {
        $includes = $this->getTransformer()->getAvailableIncludes();
        $expected = [
            'permissions',
        ];
        
        $this->assertEquals($expected, $includes);
    }
    
    public function testDefaultIncudes()
    {
        $includes = $this->getTransformer()->getDefaultIncludes();
        $expected = [];
        
        $this->assertEquals($expected, $includes);
    }
    
    public function testTransform()
    {
        $role = $this->newRole();
        
        $transformed = $this->getTransformer()->transform($role);
        ksort($transformed);
        
        $expected = [
            'code'        => $role->getCode(),
            'displayName' => $role->getDisplayName(),
        ];
        
        $this->assertEquals($expected, $transformed);
    }
    
    public function getTransformer()
    {
        return new RoleTransformer();
    }
}
