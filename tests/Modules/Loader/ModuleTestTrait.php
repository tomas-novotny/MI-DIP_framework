<?php

namespace Modules\Tests\Loader;

use Modules\Loader\Contracts\ModuleRepositoryInterface as ModuleRepositoryInterface;
use Modules\Loader\Repositories\ModuleRepository;
use Spatie\TemporaryDirectory\TemporaryDirectory;

trait ModuleTestTrait
{
    /**
     * Create module with manifest module.json file.
     *
     * @param string $path
     * @param string $directory
     * @param string $name
     * @param int    $priority
     * @param bool   $active
     *
     * @return string
     */
    protected function updateModule(
        string $path,
        string $directory,
        string $name,
        int $priority = 0,
        bool $active = true
    ): string
    {
        return $this->updateManifest($path, $directory, [
            'name'      => $name,
            'priority'  => $priority,
            'autoload'  => $active,
            'namespace' => '//',
        ]);
    }
    
    /**
     * Update module manifest module.json file.
     *
     * @param string $path
     * @param string $name
     * @param array  $config
     *
     * @return string
     */
    protected function updateManifest(string $path, string $name, array $config): string
    {
        file_put_contents($path.$name.'/module.json', json_encode($config));
        
        return $name;
    }
    
    /**
     * Get concrete module repository.
     *
     * @param string $directory
     *
     * @return \Modules\Loader\Contracts\ModuleRepositoryInterface
     */
    protected function getRepository($directory): ModuleRepositoryInterface
    {
        return $this->getRepositoryWithMultiplePaths([$directory]);
    }
    
    /**
     * Get concrete module repository.
     *
     * @param array $directories
     *
     * @return \Modules\Loader\Contracts\ModuleRepositoryInterface
     */
    protected function getRepositoryWithMultiplePaths(array $directories): ModuleRepositoryInterface
    {
        $repository = new ModuleRepository($directories);
    
        $this->app->instance(ModuleRepositoryInterface::class, $repository);
        
        return $repository;
    }
    
    /**
     * Create temp directory.
     *
     * @return string
     */
    protected function getDirectory(): string
    {
        return (new TemporaryDirectory())->create()->path().'/';
    }
    
    /**
     * Create module with empty manifest module.json file.
     *
     * @param string $path
     *
     * @return string
     */
    protected function createEmptyModule(string $path): string
    {
        return $this->writeManifest($path, []);
    }
    
    /**
     * Create module manifest module.json file.
     *
     * @param string $path
     * @param array  $config
     *
     * @return string
     */
    protected function writeManifest(string $path, array $config): string
    {
        $name = bin2hex(random_bytes(5));
        mkdir($path.$name);
        file_put_contents($path.$name.'/module.json', json_encode($config));
        
        return $name;
    }
    
    /**
     * Create module with manifest module.json file.
     *
     * @param string $path
     * @param array  $config
     *
     * @return string
     */
    protected function createModuleFromConfig(string $path, array $config): string
    {
        return $this->writeManifest($path, $config);
    }
    
    /**
     * Create module with manifest module.json file.
     *
     * @param string $path
     * @param string $name
     * @param int    $priority
     * @param bool   $autoload
     *
     * @return string
     */
    protected function createModule(string $path, string $name, int $priority = 0, bool $autoload = true): string
    {
        return $this->writeManifest($path, [
            'name'      => $name,
            'priority'  => $priority,
            'autoload'  => $autoload,
            'namespace' => '//',
        ]);
    }
}
