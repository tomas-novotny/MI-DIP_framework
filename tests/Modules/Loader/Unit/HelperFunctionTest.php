<?php

namespace Modules\Tests\Loader\Unit;

use Modules\Loader\Contracts\ModuleRepositoryInterface;
use Modules\Loader\Repositories\ModuleRepository;
use Modules\Tests\Loader\AbstractTestCase;
use Modules\Tests\Loader\ModuleTestTrait;

class HelperFunctionTest extends AbstractTestCase
{
    use ModuleTestTrait;
    
    public function testGlobalFunction()
    {
        app()->bindIf(ModuleRepositoryInterface::class, ModuleRepository::class);
        
        $this->assertInstanceOf(ModuleRepositoryInterface::class, modules());
    }
    
    public function testFlobalFunctionWithKey()
    {
        $directory = $this->getDirectory();
        
        app()->bind(ModuleRepositoryInterface::class, function () use ($directory) {
            return new ModuleRepository([$directory]);
        });
        
        $this->createModule($directory, 'a', 5, true);
        $this->createModule($directory, 'b', 6, false);
        
        $this->assertTrue(modules('a'));
        $this->assertFalse(modules('b'));
        $this->assertFalse(modules('c'));
    }
}
