<?php

namespace Modules\Tests\Loader\Unit;

use InvalidArgumentException;
use Modules\Loader\Contracts\ModuleInterface;
use Modules\Loader\Events\ModuleLoaded;
use Modules\Tests\Loader\AbstractTestCase;
use Modules\Tests\Loader\ModuleTestTrait;

class RepositoryTest extends AbstractTestCase
{
    use ModuleTestTrait;
    
    public function testEmptyDirectory()
    {
        $repository = $this->getRepository($this->getDirectory());
        $modules    = $repository->all();
        
        $this->assertCount(0, $modules);
    }
    
    public function testNoNameInConfig()
    {
        $this->expectException(InvalidArgumentException::class);
        
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createEmptyModule($directory);
        
        $repository->all();
    }
    
    public function testDefaultValues()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModuleFromConfig($directory, [
            'name' => 'app',
        ]);
        
        $modules = $repository->all();
        
        $this->assertCount(1, $modules);
        
        $module = $modules[0];
        
        $this->assertEquals(0, $module->getPriority());
        $this->assertFalse($module->isAutoload());
    }
    
    public function testModulesCount()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModule($directory, 'app');
        $this->createModule($directory, 'support');
        $this->createModule($directory, 'auth');
        $this->createModule($directory, 'mailer');
        
        $modules = $repository->all();
        
        $this->assertCount(4, $modules);
    }
    
    public function testModulesWithSameName()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModule($directory, 'app');
        $this->createModule($directory, 'app');
        $this->createModule($directory, 'support');
        $this->createModule($directory, 'support');
        $this->createModule($directory, 'support');
        
        $modules = $repository->all();
        
        $this->assertCount(2, $modules);
    }
    
    public function testModuleValues()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModule($directory, 'app', 5, true);
        
        $modules = $repository->all();
        
        $this->assertCount(1, $modules);
        
        $module = $modules[0];
        
        $this->assertInstanceOf(ModuleInterface::class, $module);
        $this->assertEquals(5, $module->getPriority());
        $this->assertTrue($module->isAutoload());
    }
    
    public function testOrdering()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModule($directory, 'a', 5);
        $this->createModule($directory, 'b', 6);
        $this->createModule($directory, 'c', -1);
        $this->createModule($directory, 'd', 3);
        
        $modules = $repository->ordered();
        
        $this->assertCount(4, $modules);
        
        $this->assertEquals('c', $modules[0]->getName());
        $this->assertEquals('d', $modules[1]->getName());
        $this->assertEquals('a', $modules[2]->getName());
        $this->assertEquals('b', $modules[3]->getName());
    }
    
    public function testActiveIsOrdered()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModule($directory, 'a', 5);
        $this->createModule($directory, 'b', 6);
        $this->createModule($directory, 'c', -1);
        $this->createModule($directory, 'd', 3);
        
        $modules = $repository->ordered();
        
        $this->assertCount(4, $modules);
        
        $this->assertEquals('c', $modules[0]->getName());
        $this->assertEquals('d', $modules[1]->getName());
        $this->assertEquals('a', $modules[2]->getName());
        $this->assertEquals('b', $modules[3]->getName());
    }
    
    public function testActive()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModule($directory, 'a', 5, true);
        $this->createModule($directory, 'b', 6, false);
        $this->createModule($directory, 'c', -1, false);
        $this->createModule($directory, 'd', 3, true);
        
        $modules = $repository->active();
        
        $this->assertCount(2, $modules);
        
        $this->assertEquals('d', $modules[0]->getName());
        $this->assertEquals('a', $modules[1]->getName());
    }
    
    public function testMultipleSourcePaths()
    {
        $first_directory  = $this->getDirectory();
        $second_directory = $this->getDirectory();
        
        $repository = $this->getRepositoryWithMultiplePaths([$first_directory, $second_directory]);
        
        $this->createModule($first_directory, 'a', 5);
        $this->createModule($first_directory, 'b', 6);
        $this->createModule($second_directory, 'c', -1);
        $this->createModule($second_directory, 'd', 3);
        
        $modules = $repository->ordered();
        
        $this->assertCount(4, $modules);
        
        $this->assertEquals('c', $modules[0]->getName());
        $this->assertEquals('d', $modules[1]->getName());
        $this->assertEquals('a', $modules[2]->getName());
        $this->assertEquals('b', $modules[3]->getName());
    }
    
    public function testMultipleSourcePathsWithSameModule()
    {
        $first_directory  = $this->getDirectory();
        $second_directory = $this->getDirectory();
        
        $repository = $this->getRepositoryWithMultiplePaths([$first_directory, $second_directory]);
        
        $this->createModule($first_directory, 'app');
        $this->createModule($second_directory, 'app');
        
        $modules = $repository->all();
        
        $this->assertCount(1, $modules);
    }
    
    public function testMultipleSourcePathsWithDisabledModules()
    {
        $first_directory  = $this->getDirectory();
        $second_directory = $this->getDirectory();
        
        $repository = $this->getRepositoryWithMultiplePaths([$first_directory, $second_directory]);
        
        $this->createModule($first_directory, 'a', 5, true);
        $this->createModule($first_directory, 'b', 6, false);
        $this->createModule($second_directory, 'c', -1, false);
        $this->createModule($second_directory, 'd', 3, true);
        
        $modules = $repository->active();
        
        $this->assertCount(2, $modules);
        
        $this->assertEquals('d', $modules[0]->getName());
        $this->assertEquals('a', $modules[1]->getName());
    }
    
    public function testEventOnLoaded()
    {
        $this->expectsEvents(ModuleLoaded::class);
        
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        $this->createModule($directory, 'a', 5);
        $this->createModule($directory, 'b', 6);
        $this->createModule($directory, 'c', -1);
        $this->createModule($directory, 'd', 3);
        
        $repository->all();
        
        $this->assertCount(4, $this->firedEvents);
    }
    
    public function testCache()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        
        $this->createModule($directory, 'a', 5, true);
        $moduleB = $this->createModule($directory, 'b', 6, false);
        
        $modules = $repository->active();
        
        $this->assertCount(1, $modules);
        
        $this->updateModule($directory, $moduleB, 'b', 5, true);
        $moduleC = $this->createModule($directory, 'c');
        $this->createModule($directory, 'd');
        
        $modules = $repository->active();
        
        $this->assertCount(4, $modules);
        
        $this->updateModule($directory, $moduleC, 'c', 0, false);
        
        $modules = $repository->active();
        
        $this->assertCount(3, $modules);
    }
    
    public function testHasMethod()
    {
        $directory  = $this->getDirectory();
        $repository = $this->getRepository($directory);
        
        $this->createModule($directory, 'a', 5, true);
        $this->createModule($directory, 'b', 6, false);
        
        $this->assertTrue($repository->has('a'));
        $this->assertFalse($repository->has('b'));
        $this->assertFalse($repository->has('c'));
    }
}
