<?php

namespace Modules\Tests\Loader\Unit;

use Modules\Loader\Contracts\ModuleInterface;
use Modules\Loader\Module;
use Modules\Tests\Loader\AbstractTestCase;

class ModuleTest extends AbstractTestCase
{
    public function testValues()
    {
        $module = new Module('app', '/abc/edf/', 'Module\\App\\', 1, true);
        
        $this->assertInstanceOf(ModuleInterface::class, $module);
        $this->assertEquals('app', $module->getName());
        $this->assertEquals('/abc/edf/', $module->getPath());
        $this->assertEquals('Module\\App\\', $module->getNamespace());
        $this->assertEquals(1, $module->getPriority());
        $this->assertEquals(true, $module->isAutoload());
    }
    
    public function testDifferentValues()
    {
        $module = new Module('support', '/', '\\', '3', false);
        
        $this->assertInstanceOf(ModuleInterface::class, $module);
        $this->assertEquals('support', $module->getName());
        $this->assertEquals('/', $module->getPath());
        $this->assertEquals('\\', $module->getNamespace());
        $this->assertEquals(3, $module->getPriority());
        $this->assertEquals(false, $module->isAutoload());
    }
    
    public function testAvailability()
    {
        $module = new Module('support', '/', '\\', '1', false);
        
        $this->assertFalse($module->isAutoload());
        
        $module->activate();
        $this->assertTrue($module->isAutoload());
        
        $module->disable();
        $this->assertFalse($module->isAutoload());
    }
    
    public function testModuleNameisLowerCase()
    {
        $module = new Module('TestModuleA', '/', '\\', '1', false);
        
        $this->assertNotEquals('TestModuleA', $module->getName());
        $this->assertEquals('testmodulea', $module->getName());
    }
}
