<?php

namespace Modules\Tests\Loader\Unit;

use Illuminate\Contracts\Console\Kernel;
use Modules\Loader\Contracts\ModuleRepositoryInterface;
use Modules\Loader\Repositories\ModuleRepository;
use Modules\Loader\UI\CLI\Commands\ListCommand;
use Modules\Loader\UI\CLI\Commands\SeedCommand;
use Modules\Tests\Loader\AbstractTestCase;
use Modules\Tests\Loader\ModuleTestTrait;

class SeedCommandTest extends AbstractTestCase
{
    use ModuleTestTrait;
    
    public function testCommandCredentials()
    {
        $command = $this->getCommand();

        $this->assertEquals('module:seed', $command->getName());
    }

    public function testArguments()
    {
        $output = $this->runCommand('module:seed', ['-h']);

        $this->assertContains('module:seed [options] [--] [<module>]', $output);
        $this->assertContains('--class[=CLASS]', $output);
        $this->assertContains('--database[=DATABASE]', $output);
    }

    public function testNoModules()
    {
        $directory = $this->getDirectory();
        $this->getRepository($directory);

        $output = $this->runCommand('module:seed');

        $this->assertContains('No modules found.', $output);
    }
    
    public function testModuleNotFound()
    {
        $directory = $this->getDirectory();
        $this->getRepository($directory);
        
        $output = $this->runCommand('module:seed', ['module' => 'test']);
        
        $this->assertContains('Module [Test] does not exists.', $output);
    }
    
    public function testModuleNotActive()
    {
        $directory = $this->getDirectory();
        $this->getRepository($directory);
    
        $this->createModule($directory, 'test', 5, false);
        
        $output = $this->runCommand('module:seed', ['module' => 'test']);
        
        $this->assertContains('Module [Test] is not active.', $output);
    }
    
    public function testModuleSeed()
    {
        $directory = $this->getDirectory();
        $this->getRepository($directory);
        
        $this->createModule($directory, 'test');
        
        $output = $this->runCommand('module:seed', ['module' => 'test']);
        
        $this->assertContains('All modules seeded.', $output);
    }
    
    public function testModulesSeed()
    {
        $directory = $this->getDirectory();
        $this->getRepository($directory);
        
        $this->createModule($directory, 'test', 5, true);
        $this->createModule($directory, 'test2', 2, true);
        
        $output = $this->runCommand('module:seed');
        
        $this->assertContains('All modules seeded.', $output);
    }
    
    /**
     * Call artisan command and return code.
     *
     * @param  string  $command
     * @param  array  $parameters
     * @return int
     */
    public function runCommand($command, $parameters = [])
    {
        $this->artisan($command, $parameters);
        
        return $this->app[Kernel::class]->output();
    }
    
    /**
     * @return \Modules\Loader\UI\CLI\Commands\SeedCommand
     */
    private function getCommand()
    {
        return $this->app->make(SeedCommand::class);;
    }
}
