<?php

namespace Modules\Tests\Loader\Unit;

use Illuminate\Contracts\Console\Kernel;
use Modules\Loader\UI\CLI\Commands\ListCommand;
use Modules\Tests\Loader\AbstractTestCase;
use Modules\Tests\Loader\ModuleTestTrait;

class ListCommandTest extends AbstractTestCase
{
    use ModuleTestTrait;
    
    public function testCommandCredentials()
    {
        $command = $this->getCommand();
        
        $this->assertEquals('module:list', $command->getName());
    }
    
    public function testOutput()
    {
        $directory = $this->getDirectory();
        $this->getRepository($directory);
        
        $this->createModule($directory, 'testModuleA', 5);
        $this->createModule($directory, 'testModuleB', 6);
        
        $output = $this->runCommand('module:list');
        
        $this->assertContains('testmodulea', $output);
        $this->assertContains('testmoduleb', $output);
    }
    
    /**
     * Call artisan command and return code.
     *
     * @param  string $command
     * @param  array  $parameters
     *
     * @return int
     */
    public function runCommand($command, $parameters = [])
    {
        $this->artisan($command, $parameters);
        
        return $this->app[Kernel::class]->output();
    }
    
    /**
     * @return \Modules\Loader\UI\CLI\Commands\ListCommand
     */
    private function getCommand()
    {
        return $this->app->make(ListCommand::class);;
    }
}
