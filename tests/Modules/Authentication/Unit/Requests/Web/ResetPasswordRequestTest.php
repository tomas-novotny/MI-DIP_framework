<?php

namespace Modules\Tests\Authentication\Unit\Requests\Web;

use Modules\Authentication\UI\Web\Requests\ResetPasswordRequest;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\RequestRulesTrait;

class ResetPasswordRequestTest extends AbstractTestCase
{
    use RequestRulesTrait;
    
    public function setUp()
    {
        //
    }
    
    public function testRulesNames()
    {
        $request = $this->getRequest();
        
        $rules    = $this->getRulesNames($request->rules());
        $expected = [
            'email',
            'password',
            'token',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testEmailRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'email');
        $expected = [
            'email',
            'max:40',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testPasswordRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'password');
        $expected = [
            'confirmed',
            'max:30',
            'min:8',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testTokenRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'token');
        $expected = [
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    private function getRequest(array $data = [])
    {
        return new ResetPasswordRequest($data);
    }
}
