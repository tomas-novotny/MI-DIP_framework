<?php

namespace Modules\Tests\Authentication\Unit\Requests\Web;

use Modules\Authentication\UI\Web\Requests\RegisterRequest;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\RequestRulesTrait;

class RegisterRequestTest extends AbstractTestCase
{
    use RequestRulesTrait;
    
    public function setUp()
    {
        //
    }
    
    public function testRulesNames()
    {
        $request = $this->getRequest();
        
        $rules    = $this->getRulesNames($request->rules());
        $expected = [
            'email',
            'password',
            'terms_and_conditions',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testEmailRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'email');
        $expected = [
            'email',
            'max:40',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testPasswordRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'password');
        $expected = [
            'confirmed',
            'max:30',
            'min:8',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testTermsRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'terms_and_conditions');
        $expected = [
            'accepted',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    private function getRequest(array $data = [])
    {
        return new RegisterRequest($data);
    }
}
