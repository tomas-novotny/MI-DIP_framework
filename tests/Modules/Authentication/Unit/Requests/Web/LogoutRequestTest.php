<?php

namespace Modules\Tests\Authentication\Unit\Requests\Web;

use Modules\Authentication\UI\Web\Requests\LogoutRequest;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\RequestRulesTrait;
use Modules\User\Model\Entities\Contracts\UserInterface;

class LogoutRequestTest extends AbstractTestCase
{
    use RequestRulesTrait;
    
    public function testRulesNames()
    {
        $request = $this->getRequest();
        
        $rules    = $this->getRulesNames($request->rules());
        $expected = [
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testAuthorizeIfLogged()
    {
        $this->be(\Mockery::mock(UserInterface::class));
        
        $this->assertTrue($this->getRequest()->authorize());
    }
    
    public function testNotAuthorizeIfNotLogged()
    {
        $this->assertFalse($this->getRequest()->authorize());
    }
    
    private function getRequest(array $data = [])
    {
        return new LogoutRequest($data);
    }
}
