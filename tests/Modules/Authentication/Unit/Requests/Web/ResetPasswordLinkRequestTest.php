<?php

namespace Modules\Tests\Authentication\Unit\Requests\Web;

use Modules\Authentication\UI\Web\Requests\ResetPasswordLinkRequest;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\RequestRulesTrait;

class ResetPasswordLinkRequestTest extends AbstractTestCase
{
    use RequestRulesTrait;
    
    public function setUp()
    {
        //
    }
    
    public function testRulesNames()
    {
        $request = $this->getRequest();
        
        $rules    = $this->getRulesNames($request->rules());
        $expected = [
            'email',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testEmailRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'email');
        $expected = [
            'email',
            'max:40',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    private function getRequest(array $data = [])
    {
        return new ResetPasswordLinkRequest($data);
    }
}
