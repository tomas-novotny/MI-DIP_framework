<?php

namespace Modules\Tests\Authentication\Unit\Requests\API;

use Modules\Authentication\UI\API\Requests\LogoutRequest;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\RequestRulesTrait;
use Modules\User\Model\Entities\Contracts\UserInterface;

class LogoutRequestTest extends AbstractTestCase
{
    use RequestRulesTrait;
    
    public function setUp()
    {
        //
    }
    
    public function testRulesNames()
    {
        $request = $this->getRequest();
        
        $rules    = $this->getRulesNames($request->rules());
        $expected = [
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    private function getRequest(array $data = [])
    {
        return new LogoutRequest($data);
    }
}
