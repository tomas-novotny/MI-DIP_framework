<?php

namespace Modules\Tests\Authentication\Unit\Requests\API;

use Modules\Authentication\UI\API\Requests\LoginRequest;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\RequestRulesTrait;

class LoginRequestTest extends AbstractTestCase
{
    use RequestRulesTrait;
    
    public function setUp()
    {
        //
    }
    
    public function testRulesNames()
    {
        $request = $this->getRequest();
        
        $rules    = $this->getRulesNames($request->rules());
        $expected = [
            'email',
            'password',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testEmailRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'email');
        $expected = [
            'email',
            'max:40',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    public function testPasswordRules()
    {
        $request = $this->getRequest();
        
        $rules    = $this->prepareRule($request->rules(), 'password');
        $expected = [
            'max:30',
            'min:8',
            'required',
        ];
        
        $this->assertEquals($expected, $rules);
    }
    
    private function getRequest(array $data = [])
    {
        return new LoginRequest($data);
    }
}
