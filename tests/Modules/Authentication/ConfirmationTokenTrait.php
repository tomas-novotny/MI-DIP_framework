<?php

namespace Modules\Tests\Authentication;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;

trait ConfirmationTokenTrait
{
    /**
     * Create a new token record.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return string
     */
    public function createConfirmationToken(AuthenticatableInterface $user): string
    {
        return $this->createConfirmationTokenFromData($user->getEmailForPasswordReset());
    }
    
    /**
     * Create a new token record.
     *
     * @param string              $email
     * @param \Carbon\Carbon|null $createdAt
     * @param string|null         $token
     *
     * @return string
     */
    public function createConfirmationTokenFromData(string $email, Carbon $createdAt = null, string $token = null)
    {
        $token     = $token ?: str_random(60);
        $createdAt = $createdAt ?: Carbon::now();
        
        $data = [
            'created_at' => $createdAt,
            'token'      => $token,
            'email'      => $email,
        ];
        
        $this->confirmationTokenTable()->insert($data);
        
        return $token;
    }
    
    /**
     * @param string $token
     *
     * @return bool
     */
    public function confirmationTokenExists(string $token): bool
    {
        foreach ($this->getConfirmationTokens() as $tokenData) {
            if ($tokenData->token == $token) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Get token storage.
     *
     * @return array
     */
    public function getConfirmationTokens(): array
    {
        return $this->confirmationTokenTable()->get()->toArray();
    }
    
    /**
     * Get confirmation token table builder.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    private function confirmationTokenTable(): Builder
    {
        // TODO: use constant
        return DB::table('user_confirmations');
    }
}
