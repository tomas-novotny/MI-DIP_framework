<?php

namespace Modules\Tests\Authentication;

use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Auth\UserProvider;

class TestMemoryUserProvider implements UserProvider
{
    /**
     * @var \Modules\Tests\Authentication\TestAuthenticationRepository
     */
    private $repository;
    
    /**
     * TestMemoryUserProvider constructor.
     *
     * @param \Modules\Tests\Authentication\TestAuthenticationRepository $repository
     */
    public function __construct(TestAuthenticationRepository $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        foreach ($this->repository->storage as $user) {
            if ($user->getAuthIdentifier() === $identifier) {
                return $user;
            }
        }
        
        return null;
    }
    
    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string $token
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return $this->retrieveById($identifier);
    }
    
    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string                                     $token
     *
     * @return void
     */
    public function updateRememberToken(UserContract $user, $token)
    {
        return;
    }
    
    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $email = $credentials['email'];
        foreach ($this->repository->storage as $user) {
            if ($user->getEmailForPasswordReset() === $email) {
                return $user;
            }
        }
        
        return null;
    }
    
    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array                                      $credentials
     *
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        return $credentials['password'] === $user->getAuthPassword();
    }
}
