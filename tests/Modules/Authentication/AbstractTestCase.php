<?php

namespace Modules\Tests\Authentication;

use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Tests\AbstractTestCase as FrameworkAbstractTestCase;

abstract class AbstractTestCase extends FrameworkAbstractTestCase
{
    /**
     * Test authentication repository.
     *
     * @var \Modules\Tests\Authentication\TestAuthenticationRepository
     */
    protected $authRepo;
    
    /**
     * Setup the test environment, before each test.
     *
     * @return void
     */
    protected function setUp()
    {
        // TODO: use Eloquent repositories to run tests in SQLITE (problem with different connection sockets)
        
        parent::setUp();
        
        $this->runDatabaseMigrations();
        
        $this->withoutNotifications();
        
        $this->setAuthProviders();
    }
    
    /**
     * Set test auth providers a repositories.
     *
     * @return void
     */
    private function setAuthProviders()
    {
        $this->authRepo = new TestAuthenticationRepository();
        $this->app->instance(AuthenticationRepositoryInterface::class, $this->authRepo);
        
        $this->app['auth']->provider('memory', function () {
            return new TestMemoryUserProvider($this->authRepo);
        });
        
        $this->setAuthConfig();
    }
    
    /**
     * Set test auth config.
     *
     * @return void
     */
    private function setAuthConfig()
    {
        $authConfig = [
            'auth' => [
                'defaults'  => [
                    'guard'     => 'test',
                    'passwords' => 'memory',
                ],
                'guards'    => [
                    'test' => [
                        'driver'   => 'session',
                        'provider' => 'memory',
                    ],
                ],
                'providers' => [
                    'memory' => [
                        'driver' => 'memory',
                    ],
                ],
                'passwords' => [
                    'memory' => [
                        'provider' => 'memory',
                        'table'    => 'user_password_resets',
                        'expire'   => 60,
                    ],
                ],
            ],
        ];
        
        $this->app['config']->set($authConfig);
    }
}
