<?php

namespace Modules\Tests\Authentication;

use Carbon\Carbon;
use Illuminate\Notifications\RoutesNotifications;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Ramsey\Uuid\Uuid;

class TestAuthenticatableUser implements AuthenticatableInterface
{
    use RoutesNotifications;
    
    private $id;
    
    private $email;
    
    private $confirmedAt;
    
    private $registeredAt;
    
    private $lastLogin;
    
    private $password;
    
    private $rememberToken;
    
    private $provider;
    
    private $providerId;
    
    /**
     * TestAuthenticatableUser constructor.
     */
    public function __construct(string $email)
    {
        $this->id    = Uuid::uuid4()->toString();
        $this->email = $email;
    }
    
    public function getAuthIdentifierName()
    {
        return 'id';
    }
    
    public function getAuthIdentifier()
    {
        return $this->id;
    }
    
    public function getAuthPassword()
    {
        return $this->password;
    }
    
    public function getRememberToken()
    {
        return $this->rememberToken;
    }
    
    public function setRememberToken($value)
    {
        $this->rememberToken = $value;
    }
    
    public function getRememberTokenName()
    {
        return 'rememberToken';
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }
    
    public function setLastLogin(Carbon $date): void
    {
        $this->lastLogin = $date;
    }
    
    public function getLastLogin(): ?Carbon
    {
        return $this->lastLogin;
    }
    
    public function setRegisteredAt(Carbon $date): void
    {
        $this->registeredAt = $date;
    }
    
    public function getRegisteredAt(): Carbon
    {
        return $this->registeredAt;
    }
    
    public function isRegistered(): bool
    {
        return $this->registeredAt !== null;
    }
    
    public function setProvider(string $type, ?string $id): void
    {
        $this->provider   = $type;
        $this->providerId = $id;
    }
    
    public function setConfirmedAt(?Carbon $date): void
    {
        $this->confirmedAt = $date;
    }
    
    public function getConfirmedAt(): ?Carbon
    {
        return $this->confirmedAt;
    }
    
    public function isConfirmed(): bool
    {
        return $this->confirmedAt !== null;
    }
    
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }
    
    public function sendPasswordResetNotification($token)
    {
        return null;
    }
}