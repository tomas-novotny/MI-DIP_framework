<?php

namespace Modules\Tests\Authentication;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Modules\Support\Collection;

trait PasswordTokenTrait
{
    /**
     * @param string         $email
     * @param \Carbon\Carbon $createdAt
     * @param string|null    $token
     *
     * @return string
     */
    protected function createPasswordResetToken(string $email, Carbon $createdAt = null, string $token = null): string
    {
        $createdAt = $createdAt ?: Carbon::now();
        $token     = $token ?: str_random(60);
        
        // insert record to table
        $this->passwordTokenTable()->insert([
            'email'      => $email,
            'token'      => $token,
            'created_at' => $createdAt->toDateTimeString(),
        ]);
        
        return $token;
    }
    
    /**
     * Get all tokens.
     *
     * @return array
     */
    protected function getPasswordTokens(): array
    {
        return $this->passwordTokenTable()->get()->toArray();
    }
    
    /**
     * @param string $token
     *
     * @return bool
     */
    protected function passwordTokenExists(string $token): bool
    {
        foreach ($this->getPasswordTokens() as $tokenData) {
            if ($tokenData->token == $token) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * @return \Illuminate\Database\Query\Builder
     */
    private function passwordTokenTable(): Builder
    {
        return DB::table('user_password_resets');
    }
}
