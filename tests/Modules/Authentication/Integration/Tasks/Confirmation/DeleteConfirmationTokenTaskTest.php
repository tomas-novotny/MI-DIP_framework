<?php

namespace Modules\Tests\Authentication\Integration\Tasks\Confirmation;

use Modules\Authentication\Services\Tasks\Confirmation\DeleteConfirmationTokenTask;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\Authentication\ConfirmationTokenTrait;

class DeleteConfirmationTokenTaskTest extends AbstractTestCase
{
    use ConfirmationTokenTrait;
    
    public function testDeletion()
    {
        $token = $this->createConfirmationTokenFromData('test@email.cz');
        
        $this->assertCount(1, $this->getConfirmationTokens());
        
        $this->handleTask($token);
        
        $this->assertCount(0, $this->getConfirmationTokens());
    }
    
    public function testMultipleTokens()
    {
        $this->createConfirmationTokenFromData('test@email.cz');
        $token = $this->createConfirmationTokenFromData('test@email.cz');
        $this->createConfirmationTokenFromData('another@email.cz');
        
        $this->assertCount(3, $this->getConfirmationTokens());
        
        $this->handleTask($token);
        
        $this->assertCount(2, $this->getConfirmationTokens());
    }
    
    public function testNonExistingToken()
    {
        $this->assertCount(0, $this->getConfirmationTokens());
        
        $this->handleTask(str_random(40));
        
        $this->assertCount(0, $this->getConfirmationTokens());
    }
    
    /**
     * Create confirmation token for given user.
     *
     * @param string $token
     *
     * @return void
     */
    private function handleTask(string $token): void
    {
        $task = new DeleteConfirmationTokenTask($token);
        
        $task->handle();
    }
}
