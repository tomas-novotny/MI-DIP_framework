<?php

namespace Modules\Tests\Authentication\Integration\Tasks\Confirmation;

use Carbon\Carbon;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Tasks\Confirmation\CreateConfirmationTokenTask;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Support\Exceptions\ValidationFailedException;
use Modules\Tests\Authentication\ConfirmationTokenTrait;

class CreateConfirmationTokenTaskTest extends AbstractTestCase
{
    use ConfirmationTokenTrait;
    
    public function testBasic()
    {
        $user = $this->authRepo->createConfirmedUser();
        
        $token = $this->handleTask($user);
        
        $this->assertNotEmpty($token);
        $this->assertTrue($this->confirmationTokenExists($token));
    }
    
    public function testExists()
    {
        $this->expectException(ValidationFailedException::class);
        $this->expectExceptionMessage('Confirmation token already exists');
    
        $user = $this->authRepo->createConfirmedUser();
        
        $this->handleTask($user);
        $this->handleTask($user);
    }
    
    public function testUnique()
    {
        $user_1 = $this->authRepo->createConfirmedUser('test1@email.com', false);
        $user_2 = $this->authRepo->createConfirmedUser('test2@email.com', false);
        
        $token_1 = $this->handleTask($user_1);
        $token_2 = $this->handleTask($user_2);
        
        $this->assertNotEquals($token_1, $token_2);
        $this->assertTrue($this->confirmationTokenExists($token_1));
        $this->assertTrue($this->confirmationTokenExists($token_2));
    }
    
    public function testDeleteOldToken()
    {
        $email = 'test@email.com';
        
        $this->createConfirmationTokenFromData($email, Carbon::now()->subMinutes(100));
        $this->createConfirmationTokenFromData($email, Carbon::now()->subMinutes(120));
        
        $this->assertCount(2, $this->getConfirmationTokens());
        
        $user = $this->authRepo->createConfirmedUser($email, false);
        
        $token = $this->handleTask($user);
        
        $this->assertCount(1, $this->getConfirmationTokens());
        $this->assertTrue($this->confirmationTokenExists($token));
    }
    
    public function testDeleteOnlyUsersOldToken()
    {
        $email = 'test@email.com';
        
        $this->createConfirmationTokenFromData($email, Carbon::now()->subMinutes(100));
        $this->createConfirmationTokenFromData('diff@email.com', Carbon::now()->subMinutes(120));
        
        $this->assertCount(2, $this->getConfirmationTokens());
        
        $user = $this->authRepo->createConfirmedUser($email, false);
        
        $token = $this->handleTask($user);
        
        $this->assertCount(2, $this->getConfirmationTokens());
        $this->assertTrue($this->confirmationTokenExists($token));
    }
    
    public function testResentTimer()
    {
        $email = 'test@email.com';
        
        $old_token = $this->createConfirmationTokenFromData($email, Carbon::now()->subMinutes(20));
        
        $user = $this->authRepo->createConfirmedUser($email);
        
        try {
            $this->handleTask($user, 30);
        }
        catch (ValidationFailedException $exception) {
            //
        }
        
        $this->assertCount(1, $this->getConfirmationTokens());
        
        $token = $this->handleTask($user, 10);
        
        $this->assertCount(1, $this->getConfirmationTokens());
        $this->assertTrue($this->confirmationTokenExists($token));
        $this->assertFalse($this->confirmationTokenExists($old_token));
    }
    
    /**
     * Create confirmation token for given user.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     * @param int|null                                                                  $resendAfter
     *
     * @return string
     */
    private function handleTask(AuthenticatableInterface $user, int $resendAfter = null): string
    {
        if ($resendAfter !== null) {
            $task = new CreateConfirmationTokenTask($user, $resendAfter);
        }
        else {
            $task = new CreateConfirmationTokenTask($user);
        }
        
        return $task->handle();
    }
}
