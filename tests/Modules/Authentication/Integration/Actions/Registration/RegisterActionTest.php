<?php

namespace Modules\Tests\Authentication\Integration\Actions\Registration;

use Illuminate\Auth\Events\Registered as RegisteredEvent;
use Modules\Authentication\Events\UserRegistration as UserRegistrationEvent;
use Modules\Authentication\Exceptions\RegisterFailedException;
use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Notifications\RegisteredNotification as RegisteredNotification;
use Modules\Authentication\Services\Actions\Registration\RegisterAction;
use Modules\Support\Collection;
use Modules\Tests\Authentication\AbstractTestCase;

class RegisterActionTest extends AbstractTestCase
{
    /**
     * @var array
     */
    private $registrationData = [
        'email'    => 'test@email.com',
        'password' => 'password',
    ];
    
    /**
     * @expectedException UserNotConfirmedException
     */
    public function testConfirmUser()
    {
        $this->expectException(UserNotConfirmedException::class);
        
        $this->emailConfirmationEnabled();
        
        $this->handleAction($this->registrationData);
    }
    
    /**
     * Test user registration.
     */
    public function testCreateUser()
    {
        $this->emailConfirmationDisabled();
        $this->expectsEvents([UserRegistrationEvent::class, RegisteredEvent::class]);
        
        $user = $this->handleAction($this->registrationData);
        
        $this->assertInstanceOf(AuthenticatableInterface::class, $user);
        
        $this->assertEquals($this->registrationData['email'], $user->getEmail());
        $this->assertTrue($user->isRegistered());
        $this->assertTrue($user->isConfirmed());
    }
    
    /**
     * Test user registration with used email.
     *
     * @expectedException RegisterFailedException
     */
    public function testRegisteredUserAlreadyExists()
    {
        $this->expectException(RegisterFailedException::class);
        $this->expectExceptionMessage('already used');
        
        $this->authRepo->createUser($this->registrationData['email'], null, true);
        
        $this->handleAction($this->registrationData);
    }
    
    /**
     * Test user registration with used email.
     */
    public function testNonRegisteredUserAlreadyExists()
    {
        $this->authRepo->createUser($this->registrationData['email'], null, false);
        
        try {
            $this->handleAction($this->registrationData);
        }
        catch (UserNotConfirmedException $exception) {
            $this->assertInstanceOf(AuthenticatableInterface::class, $exception->getUser());
            // TODO: uncomment
            // $this->assertEquals($user->getName(), $exception->getUser()->getName());
        }
    }
    
    /**
     * Test user registration with used email.
     */
    public function testNonRegisteredUserNotConfirmed()
    {
        $this->expectException(UserNotConfirmedException::class);
        
        $this->emailConfirmationEnabled();
        
        $this->authRepo->createUser($this->registrationData['email'], null, false);
        
        $this->handleAction($this->registrationData);
    }
    
    /**
     * Test user registration with used email.
     */
    public function testNonRegisteredUserAlwaysNotConfirmed()
    {
        $this->expectException(UserNotConfirmedException::class);
        
        $this->emailConfirmationDisabled();
        
        $this->authRepo->createUser($this->registrationData['email'], null, false);
        
        $this->handleAction($this->registrationData);
    }
    
    /**
     * Test user registration with used email.
     *
     * @expectedException RegisterFailedException
     */
    public function testRegistrationTwiceExists()
    {
        $this->expectException(RegisterFailedException::class);
        $this->expectExceptionMessage('already used');
        
        $this->emailConfirmationDisabled();
        
        $this->handleAction($this->registrationData);
        $this->handleAction($this->registrationData);
    }
    
    /**
     * Test user registration fire notification.
     */
    public function testNotification()
    {
        $user = $this->handleAction($this->registrationData);
        
        $this->assertInstanceOf(AuthenticatableInterface::class, $user);
        $this->expectsNotification($user, RegisteredNotification::class);
    }
    
    /**
     * @param array $data
     * @param array $option
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private function handleAction(array $data, array $option = [])
    {
        $action = new RegisterAction(new Collection($data), $option);
        
        return $action->handle();
    }
    
    /**
     * Enable email confirmation requirement.
     *
     * @return void
     */
    private function emailConfirmationEnabled()
    {
        $this->app['config']->set(['registration.confirmation' => true]);
    }
    
    /**
     * Disable email confirmation requirement.
     *
     * @return void
     */
    private function emailConfirmationDisabled()
    {
        $this->app['config']->set(['registration.confirmation' => false]);
    }
}
