<?php

namespace Modules\Tests\Authentication\Integration\Actions\Confirmation;

use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Notifications\ConfirmationNotification;
use Modules\Authentication\Services\Actions\Confirmation\SendConfirmationLinkAction;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\Authentication\ConfirmationTokenTrait;
use Modules\Support\Exceptions\InternalErrorException;
use Modules\Support\Exceptions\InvalidArgumentException;

class SendConfirmationLinkActionTest extends AbstractTestCase
{
    use ConfirmationTokenTrait;
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testAlreadyConfirmed()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('User already confirmed');

        $user = $this->authRepo->createConfirmedUser('test@email.com', true);

        $this->handleAction($user);
    }

    public function testNotification()
    {
        $user = $this->authRepo->createConfirmedUser('test@email.com', false);

        $this->expectsNotification($user, ConfirmationNotification::class);

        $this->assertCount(0, $this->getConfirmationTokens());
    
        // sent email notification
        $this->handleAction($user);

        $this->assertCount(1, $this->getConfirmationTokens());
    }

    /**
     * @expectedException InternalErrorException
     */
    public function testMultiple()
    {
        $this->expectException(InternalErrorException::class);
        $this->expectExceptionMessage('Confirmation token already exists');

        $this->withoutNotifications();
    
        $user = $this->authRepo->createConfirmedUser('test@email.com', false);

        $this->handleAction($user);
        $this->handleAction($user);
    }
    
    /**
     * Create confirmation token for given user.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return void
     */
    private function handleAction(AuthenticatableInterface $user): void
    {
        $action = new SendConfirmationLinkAction($user);
        
        $action->handle();
    }
}
