<?php

namespace Modules\Tests\Authentication\Integration\Actions\Confirmation;

use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Actions\Confirmation\ConfirmUserAction;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\Authentication\ConfirmationTokenTrait;

class ConfirmUserActionTest extends AbstractTestCase
{
    use ConfirmationTokenTrait;
    
    /**
     * @expectedException UserNotConfirmedException
     */
    public function testBadToken()
    {
        $this->expectException(UserNotConfirmedException::class);
        $this->expectExceptionMessage('User with given confirmation token does not exist');

        $token = str_random(60);

        $this->handleAction($token);
    }

    /**
     * @expectedException UserNotConfirmedException
     */
    public function testUserNotPersisted()
    {
        $this->expectException(UserNotConfirmedException::class);

        $token = str_random(60);

        $this->handleAction($token);
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testAlreadyConfirmed()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('User already confirmed');
        
        $user  = $this->authRepo->createConfirmedUser('test@email.com', true);
        $token = $this->createConfirmationTokenFromData($user->getEmail());
        
        $this->handleAction($token);
    }
    
    public function testConfirmed()
    {
        $user  = $this->authRepo->createConfirmedUser('test@email.com', false);
        $token = $this->createConfirmationTokenFromData($user->getEmail());
        
        $this->assertCount(1, $this->getConfirmationTokens());
        $this->assertFalse($user->isConfirmed());
        
        $this->handleAction($token);
        
        $this->assertTrue($user->isConfirmed());
        $this->assertCount(0, $this->getConfirmationTokens());
    }
    
    /**
     * Create confirmation token for given user.
     *
     * @param string $token
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private function handleAction(string $token): AuthenticatableInterface
    {
        $action = new ConfirmUserAction($token);
        
        return $action->handle();
    }
}
