<?php

namespace Modules\Tests\Authentication\Integration\Actions\Login;

use Carbon\Carbon;
use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Services\Actions\Login\MarkLoginTimeAction;
use Modules\Tests\Authentication\AbstractTestCase;

class MarkLoginTimeActionTest extends AbstractTestCase
{
    public function testMarkLoginTime()
    {
        $user = $this->authRepo->createUser();
        
        $this->assertNull($user->getLastLogin());
        
        $now = Carbon::now();
        
        $this->handleAction($user);
        
        $this->assertNotNull($user->getLastLogin());
        $this->assertTrue($user->getLastLogin()->gte($now));
    }
    
    public function testMarkLoginTimeOverride()
    {
        $user = $this->authRepo->createUser();
    
        $lastLogin = Carbon::now()->subMinutes(40);
        $user->setLastLogin($lastLogin);
        
        $this->handleAction($user);
        
        $this->assertNotNull($user->getLastLogin());
        $this->assertTrue($user->getLastLogin()->gt($lastLogin));
    }
    
    /**
     * Mark login time.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private function handleAction(AuthenticatableInterface $user)
    {
        $action = new MarkLoginTimeAction($user);
        
        return $action->handle();
    }
}
