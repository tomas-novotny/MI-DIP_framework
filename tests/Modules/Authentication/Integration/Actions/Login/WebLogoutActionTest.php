<?php

namespace Modules\Tests\Authentication\Integration\Actions\Login;

use Modules\Authentication\Services\Actions\Login\WebLogoutAction;
use Modules\Support\Collection;
use Modules\Tests\Authentication\AbstractTestCase;

class WebLogoutActionTest extends AbstractTestCase
{
    public function testLogout()
    {
        $user = $this->authRepo->createUser('test@email.com');
        
        auth()->setUser($user);
        $authUser = auth()->user();
        
        $this->assertEquals($authUser->getEmail(), $user->getEmail());
        
        $this->handleAction();
        
        $this->assertGuest();
    }
    
    /**
     * Logout.
     *
     * @return void
     */
    private function handleAction()
    {
        $action = new WebLogoutAction(new Collection());
        
        $action->handle();
    }
}
