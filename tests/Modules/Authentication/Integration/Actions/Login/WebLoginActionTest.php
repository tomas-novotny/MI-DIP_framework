<?php

namespace Modules\Tests\Authentication\Integration\Actions\Login;

use Modules\Authentication\Exceptions\LoginFailedException;
use Modules\Authentication\Exceptions\TooManyLoginAttemptsException;
use Modules\Authentication\Exceptions\UserNotConfirmedException;
use Modules\Authentication\Services\Actions\Login\WebLoginAction;
use Modules\Support\Collection;
use Modules\Tests\Authentication\AbstractTestCase;

class WebLoginActionTest extends AbstractTestCase
{
    /**
     * @expectedException LoginFailedException
     */
    public function testNoUser()
    {
        $this->expectException(LoginFailedException::class);
        
        $this->handleAction('test@email.com', 'abcdef');
    }
    
    /**
     * @expectedException LoginFailedException
     */
    public function testWrongPassword()
    {
        $this->expectException(LoginFailedException::class);
        
        $user = $this->authRepo->createUser('test@email.com', 'test', true, true);
        
        $this->handleAction($user->getEmail(), 'abcdef');
    }
    
    /**
     * @expectedException LoginFailedException
     */
    public function testNotRegistered()
    {
        $this->expectException(LoginFailedException::class);
        $this->expectExceptionMessage('The user has not been registered');
        
        $user = $this->authRepo->createUser('test@email.com', 'test');
        
        $this->handleAction($user->getEmail(), 'test');
    }
  
    /**
     * @expectedException UserNotConfirmedException
     */
    public function testNotConfirmed()
    {
        $this->expectException(UserNotConfirmedException::class);

        $user = $this->authRepo->createUser('test@email.com', 'test', true);

        $this->handleAction($user->getEmail(), 'test');
    }

    public function testLoginLimit()
    {
        $user = $this->authRepo->createUser('test@email.com', 'test', true, true);

        for ($i = 0; $i < 4; $i++) {
            try {
                $this->handleAction($user->getEmail(), 'wrong');
            }
            catch (LoginFailedException $exception) {
                // login failed with wrong password
            }
        }

        $authUser = $this->handleAction($user->getEmail(), 'test');

        $this->assertEquals($user->getEmail(), $authUser->getEmail());
        $this->assertEquals($user->getAuthPassword(), $authUser->getAuthPassword());
    }

    /**
     * @expectedException TooManyLoginAttemptsException
     */
    public function testTooManyLogin()
    {
        $this->expectException(TooManyLoginAttemptsException::class);

        $user = $this->authRepo->createUser('test@email.com', 'test', true, true);

        for ($i = 0; $i < 10; $i++) {
            try {
                $this->handleAction($user->getEmail(), 'wrong');
            }
            catch (LoginFailedException $exception) {
                //
            }
        }
    }

    public function testSuccess()
    {
        $user = $this->authRepo->createUser('test@email.com', 'test', true, true);

        $authUser = $this->handleAction($user->getEmail(), 'test');

        $this->assertEquals($user->getEmail(), $authUser->getEmail());
        $this->assertEquals($user->getAuthPassword(), $authUser->getAuthPassword());
    }
    
    /**
     * Login.
     *
     * @param string $email
     * @param string $password
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    private function handleAction(string $email, string $password, bool $remember = false)
    {
        $action = new WebLoginAction(new Collection(compact('email', 'password', 'remember')));
        
        return $action->handle();
    }
}
