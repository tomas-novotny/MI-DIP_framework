<?php

namespace Modules\Tests\Authentication\Integration\Actions\Password;

use Modules\Authentication\Notifications\ResetPasswordNotification;
use Modules\Authentication\Services\Actions\Password\SendResetPasswordLinkAction;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Support\Exceptions\ResourceNotFoundException;

class SendResetPasswordLinkActionTest extends AbstractTestCase
{
    /**
     * @expectedException ResourceNotFoundException
     */
    public function testNoUser()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        // send link
        $this->handleAction('test@email.com');
    }
    
    public function testNotification()
    {
        $this->withoutNotifications();
        
        // create user
        $user = $this->authRepo->createUser('test@email.com');
        
        $this->expectsNotification($user, ResetPasswordNotification::class);
        
        // send link
        $this->handleAction($user->getEmailForPasswordReset());
    }
    
    /**
     * Send reset password link
     *
     * @param string $email
     *
     * @return void
     */
    private function handleAction(string $email)
    {
        $action = new SendResetPasswordLinkAction($email);
        
        $action->handle();
    }
}
