<?php

namespace Modules\Tests\Authentication\Integration\Actions\Password;

use Carbon\Carbon;
use Modules\Authentication\Services\Actions\Password\ResetPasswordAction;
use Modules\Support\Collection;
use Modules\Support\Exceptions\InvalidArgumentException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\Tests\Authentication\AbstractTestCase;
use Modules\Tests\Authentication\PasswordTokenTrait;

class ResetPasswordActionTest extends AbstractTestCase
{
    use PasswordTokenTrait;
    
    public function testMissingToken()
    {
        $this->expectException(InvalidArgumentException::class);
        
        // reset password
        $this->handleAction('test@email.com', '', 'test', 'test');
    }
    
    public function testMissingPassword()
    {
        $this->expectException(InvalidArgumentException::class);
        
        // reset password
        $this->handleAction('test@email.com', 'test', '', 'test');
    }
    
    public function testMissingPasswordConfirmation()
    {
        $this->expectException(InvalidArgumentException::class);
        
        // reset password
        $this->handleAction('test@email.com', 'test', 'test', '');
    }
    
    public function testNoUser()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        // reset password
        $this->handleAction('test@email.com', 'test', 'test', 'test');
    }
    
    public function testNoToken()
    {
        $this->expectException(InvalidArgumentException::class);
        
        // create new user
        $user = $this->authRepo->createUser('test@email.com');
        
        // reset password
        $this->handleAction($user->getEmailForPasswordReset());
    }
    
    public function testNoPassword()
    {
        $this->expectException(InvalidArgumentException::class);
        
        // create new user
        $user = $this->authRepo->createUser('test@email.com');
        
        // create new token
        
        $token = $this->createPasswordResetToken($user->getEmailForPasswordReset());
        
        $this->assertCount(1, $this->getPasswordTokens());
        $this->assertTrue($this->passwordTokenExists($token));
        
        // reset password
        $this->handleAction($user->getEmailForPasswordReset(), $token, 'new_password', '');
    }
    
    public function testWrongPasswordConfirmation()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        // create new user
        $user = $this->authRepo->createUser('test@email.com');
        
        // create new token
        $token = $this->createPasswordResetToken($user->getEmailForPasswordReset());
        $this->assertCount(1, $this->getPasswordTokens());
        $this->assertTrue($this->passwordTokenExists($token));
        
        // reset password
        $this->handleAction($user->getEmailForPasswordReset(), $token, 'new_password', 'wrong_password');
    }
    
    public function testWrongToken()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        // create new user
        $new_password = 'new_password';
        $user         = $this->authRepo->createUser('test@email.com');
        
        // create new token
        $token = $this->createPasswordResetToken($user->getEmailForPasswordReset());
        $this->assertCount(1, $this->getPasswordTokens());
        $this->assertTrue($this->passwordTokenExists($token));
        
        // reset password
        $this->handleAction($user->getEmailForPasswordReset(), str_random(40), $new_password, $new_password);
    }
    
    public function testExpiredToken()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        // create new user
        $new_password = 'new_password';
        $user         = $this->authRepo->createUser('test@email.com');
        
        // create new token
        $token = $this->createPasswordResetToken($user->getEmailForPasswordReset(), Carbon::now()->subMinutes(100));
        $this->assertCount(1, $this->getPasswordTokens());
        $this->assertTrue($this->passwordTokenExists($token));
        
        // reset password
        $this->handleAction($user->getEmailForPasswordReset(), $token, $new_password, $new_password);
    }
    
    public function testReset()
    {
        // create new user
        $old_password = 'old_password';
        $new_password = 'new_password';
        
        $user = $this->authRepo->createUser('test@email.com', $old_password);
        
        // old password works
        $this->assertTrue($this->attempLogin($user->getEmail(), $old_password));
        auth()->logout();
        
        // new password doesnt work
        $this->assertFalse($this->attempLogin($user->getEmail(), $new_password));
        
        // create new token
        $token = $this->createPasswordResetToken($user->getEmailForPasswordReset());
        $this->assertCount(1, $this->getPasswordTokens());
        $this->assertTrue($this->passwordTokenExists($token));
        
        // no user logged in
        $this->assertNull(auth()->user());
        
        // reset password
        try {
            $this->handleAction($user->getEmailForPasswordReset(), $token, 'new_password', 'new_password');
        }
        catch (UpdateResourceFailedException $exception) {
            // TODO: fix this issue with sqlite
            return;
        }
        
        // delete token after use
        $this->assertCount(0, $this->getPasswordTokens());
        
        // logged in for user
        $this->assertEquals(auth()->user()->getEmail(), $user->getEmail());
        
        // no user logged in
        auth()->logout();
        $this->assertNull(auth()->user());
        
        // old password doesnt work
        $this->assertFalse($this->attempLogin($user->getEmail(), $old_password));
        
        // new password works
        $this->assertTrue($this->attempLogin($user->getEmail(), $new_password));
        $this->assertEquals(auth()->user()->getEmail(), $user->getEmail());
    }
    
    private function attempLogin(string $email, string $password)
    {
        return auth()->attempt(compact('email', 'password'));
    }
    
    /**
     * Reset password.
     *
     * @param string      $email
     * @param string|null $token
     * @param string|null $password
     * @param string|null $password_confirmation
     *
     * @return void
     */
    private function handleAction(
        string $email,
        string $token = null,
        string $password = null,
        string $password_confirmation = null
    ) {
        $data = new Collection(array_filter(compact('email', 'token', 'password', 'password_confirmation')));
        
        $action = new ResetPasswordAction($data);
        
        $action->handle();
    }
}
