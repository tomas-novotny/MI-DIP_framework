<?php

namespace Modules\Tests\Authentication;

use Carbon\Carbon;
use Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface;
use Modules\Authentication\Model\Repositories\Contracts\AuthenticationRepositoryInterface;
use Modules\Support\Exceptions\ResourceNotFoundException;

class TestAuthenticationRepository implements AuthenticationRepositoryInterface
{
    /**
     * @var \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface[]
     */
    public $storage = [];
    
    /**
     * New user.
     *
     * @param string|null $email
     * @param bool        $confirmed
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    public function createConfirmedUser(string $email = null, $confirmed = false): AuthenticatableInterface
    {
        return $this->createUser($email, null, true, $confirmed);
    }
    
    /**
     * Create new user instance.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    public function newAuthenticatableUser(string $email): AuthenticatableInterface
    {
        $user = new TestAuthenticatableUser($email);
        
        return $user;
    }
    
    /**
     * New user.
     *
     * @param string|null $email
     * @param bool        $confirmed
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     */
    public function createUser(
        string $email = null,
        $password = null,
        $registered = false,
        $confirmed = false
    ): AuthenticatableInterface
    {
        $email = $email ?: 'test@email.com';
        
        $user = $this->newAuthenticatableUser($email);
        
        $user->setPassword($password ?: str_random(8));
        
        if ($confirmed) {
            $user->setConfirmedAt(Carbon::now());
        }
        
        if ($registered) {
            $user->setRegisteredAt(Carbon::now());
        }
        
        $this->persistAuthenticatableUser($user);
        
        return $user;
    }
    
    /**
     * Persist authenticatable user to database.
     *
     * @param \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface $user
     */
    public function persistAuthenticatableUser(AuthenticatableInterface $user): void
    {
        $this->storage[$user->getEmailForPasswordReset()] = $user;
    }
    
    /**
     * Get registered user by email.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getRegisteredByEmail(string $email): AuthenticatableInterface
    {
        foreach ($this->storage as $user) {
            if ($user->getEmailForPasswordReset() === $email and $user->isRegistered()) {
                return $user;
            }
        }
        
        throw new ResourceNotFoundException(TestAuthenticatableUser::class);
    }
    
    /**
     * Get user by email.
     *
     * @param string $email
     *
     * @return \Modules\Authentication\Model\Entities\Contracts\AuthenticatableInterface
     *
     * @throws \Modules\Support\Exceptions\ResourceNotFoundException
     */
    public function getByEmail(string $email): AuthenticatableInterface
    {
        foreach ($this->storage as $user) {
            if ($user->getEmailForPasswordReset() === $email) {
                return $user;
            }
        }
        
        throw new ResourceNotFoundException(TestAuthenticatableUser::class);
    }
}
