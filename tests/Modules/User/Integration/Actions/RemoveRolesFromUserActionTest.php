<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\Tests\Authorization\RoleFactoryTrait;
use Modules\Tests\User\AbstractTestCase;
use Modules\Tests\User\UserFactoryTrait;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Services\Actions\RemoveRolesFromUserAction;

class RemoveRolesFromUserActionTest extends AbstractTestCase
{
    use UserFactoryTrait;
    use RoleFactoryTrait;
    
    public function testUserNotExists()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test', []);
    }
    
    public function testNoCodesDoNothing()
    {
        $user = $this->newUser();
        $user->addRole($this->createRole());
        $this->getUserRepo()->persist($user);
        
        $this->handleAction($user->getKey(), []);
        
        $this->assertCount(1, $user->getRoles());
    }
    
    public function testRoleNotExists()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $user = $this->createUser();
        
        $this->handleAction($user->getKey(), ['test']);
    }
    
    public function testRemoveRoles()
    {
        $user = $this->newUser();
        
        $roles = $this->createRoles(3);
        
        foreach ($roles as $role) {
            $user->addRole($role);
        }
        
        $this->getUserRepo()->persist($user);
        
        $this->handleAction($user->getKey(), [$roles[1]->getCode()]);
        
        $this->assertCount(2, $user->getRoles());
    }
    
    public function testIfAnyRoleDontExistsDoNotAddAny()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $user = $this->newUser();
        
        $roles = $this->createRoles(3);
        
        foreach ($roles as $role) {
            $user->addRole($role);
        }
        
        $this->getUserRepo()->persist($user);
        
        $this->handleAction($user->getKey(), [$roles[1]->getCode(), '_non_existing']);
    }
    
    /**
     * Remove roles from user.
     *
     * @param string $userCode
     * @param array  $roleCodes
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private function handleAction(string $userCode, array $roleCodes): UserInterface
    {
        $action = new RemoveRolesFromUserAction($userCode, $roleCodes);
        
        return $action->handle();
    }
}
