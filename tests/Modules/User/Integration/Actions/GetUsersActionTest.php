<?php

namespace Modules\Tests\User\Integration\Actions;

use Modules\Support\Collection;
use Modules\Tests\User\AbstractTestCase;
use Modules\Tests\User\UserFactoryTrait;
use Modules\User\Services\Actions\GetUsersAction;

class GetUsersActionTest extends AbstractTestCase
{
    use UserFactoryTrait;
    
    public function testCount()
    {
        $users = $this->handleAction();
        
        $this->assertCount(0, $users);
        
        $this->createUsers(5);
        
        $users = $this->handleAction();
        
        $this->assertCount(5, $users);
    }
    
    /**
     * Hanlde action.
     *
     * @return \Modules\Support\Collection
     */
    private function handleAction(): Collection
    {
        $action = new GetUsersAction();
        
        return $action->handle();
    }
}
