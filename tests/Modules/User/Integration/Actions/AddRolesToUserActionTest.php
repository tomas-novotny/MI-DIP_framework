<?php

namespace Modules\Tests\Authorization\Integration\Actions\Confirmation;

use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Support\Exceptions\UpdateResourceFailedException;
use Modules\Tests\Authorization\RoleFactoryTrait;
use Modules\Tests\User\AbstractTestCase;
use Modules\Tests\User\UserFactoryTrait;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Services\Actions\AddRolesToUserAction;

class AddRolesToUserActionTest extends AbstractTestCase
{
    use UserFactoryTrait;
    use RoleFactoryTrait;
    
    public function testUserNotExists()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleAction('test', []);
    }
    
    public function testNoCodesDoNothing()
    {
        $user = $this->createUser();
        
        $this->handleAction($user->getKey(), []);
        
        $this->assertEmpty($user->getRoles());
    }
    
    public function testRoleNotExists()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $user = $this->createUser();
        
        $this->handleAction($user->getKey(), ['test']);
    }
    
    public function testAddRoles()
    {
        $user = $this->createUser();
        
        $roles = $this->createRoles(3);
        $codes = [];
        
        foreach ($roles as $role) {
            $codes[] = $role->getCode();
        }
        
        $this->handleAction($user->getKey(), $codes);
        
        $this->assertCount(3, $user->getRoles());
    }
    
    public function testIfAnyRoleDontExistsDoNotAddAny()
    {
        $this->expectException(UpdateResourceFailedException::class);
        
        $user = $this->createUser();
        
        $roles = $this->createRoles(3);
        $codes = [];
        
        foreach ($roles as $role) {
            $codes[] = $role->getCode();
        }
        
        $codes[] = '_non_existing';
        
        $this->handleAction($user->getKey(), $codes);
    }
    
    /**
     * Add roles to user.
     *
     * @param string $userCode
     * @param array  $roleCodes
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private function handleAction(string $userCode, array $roleCodes): UserInterface
    {
        $action = new AddRolesToUserAction($userCode, $roleCodes);
        
        return $action->handle();
    }
}
