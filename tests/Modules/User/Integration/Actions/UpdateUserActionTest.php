<?php

namespace Modules\Tests\User\Integration\Actions\Confirmation;

use Carbon\Carbon;
use Modules\Support\Collection;
use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\User\AbstractTestCase;
use Modules\Tests\User\UserFactoryTrait;
use Modules\Tests\User\UserFactoryTraitTrait;
use Modules\User\Events\UserDataEditing;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Services\Actions\UpdateUserAction;

class UpdateUserActionTest extends AbstractTestCase
{
    use UserFactoryTrait;
    
    public function testMissingCode()
    {
        $this->expectException(ResourceNotFoundException::class);

        $this->handleAction('2');
    }

    public function testRaiseEvent()
    {
        $user = $this->createUser();

        $this->expectsEvents(UserDataEditing::class);

        $this->handleAction($user->getKey());
    }
    
    public function testAttributes()
    {
        $user = $this->createUser();
        
        $date = Carbon::now()->subMinutes(15);
        $edited = $this->handleAction($user->getKey(), [
            'confirmedAt' => $date->toAtomString(),
        ]);
        
        $this->assertInstanceOf(UserInterface::class, $user);
        $this->assertEquals($date->toDateString(), $edited->getConfirmedAt()->toDateString());
    }
    
    /**
     * Update user action
     *
     * @param string $id
     * @param array  $data
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private function handleAction(string $id, array $data = []): UserInterface
    {
        $action = new UpdateUserAction($id, new Collection($data));
        
        return $action->handle();
    }
}
