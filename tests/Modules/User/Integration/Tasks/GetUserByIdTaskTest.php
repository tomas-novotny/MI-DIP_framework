<?php

namespace Modules\Tests\User\Integration\Tasks;

use Modules\Support\Exceptions\ResourceNotFoundException;
use Modules\Tests\User\AbstractTestCase;
use Modules\Tests\User\UserFactoryTrait;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Services\Tasks\GetUserByIdTask;

class GetUserByIdTaskTest extends AbstractTestCase
{
    use UserFactoryTrait;
    
    public function testNotFound()
    {
        $this->expectException(ResourceNotFoundException::class);
        
        $this->handleTask('asd');
    }
    
    public function testFound()
    {
        $user = $this->createUser();
        
        $foundUser = $this->handleTask($user->getKey());
        
        $this->assertEquals($user->getEmail(), $foundUser->getEmail());
        $this->assertEquals($user->getKey(), $foundUser->getKey());
    }
    
    /**
     * @param string $key
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private function handleTask(string $key): UserInterface
    {
        $action = new GetUserByIdTask($key);
        
        return $action->handle();
    }
}
