<?php

namespace Modules\Tests\User\Integration\Tasks\User;

use Carbon\Carbon;
use Modules\Support\Collection;
use Modules\Tests\User\AbstractTestCase;
use Modules\Tests\User\UserFactoryTrait;
use Modules\User\Events\UserDataEditing;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Services\Tasks\EditUserTask;

class EditUserTaskTest extends AbstractTestCase
{
    use UserFactoryTrait;
    
    public function testRaiseEvent()
    {
        $this->expectsEvents(UserDataEditing::class);
        
        $user = $this->newUser();
        
        $this->handleTask($user, []);
    }
    
    public function testEditDisplayName()
    {
        $user = $this->newUser();
    
        $date = Carbon::now()->subMinutes(15);
        $edited = $this->handleTask($user, [
            'confirmedAt' => $date->toAtomString(),
        ]);
        
        $this->assertEquals($date->toDateString(), $edited->getConfirmedAt()->toDateString());
    }
    
    /**
     * @param \Modules\User\Model\Entities\Contracts\UserInterface $user
     * @param array                                                $data
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private function handleTask(UserInterface $user, array $data = []): UserInterface
    {
        $task = new EditUserTask($user, new Collection($data));
        
        return $task->handle();
    }
}
