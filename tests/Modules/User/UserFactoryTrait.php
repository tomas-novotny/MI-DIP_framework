<?php

namespace Modules\Tests\User;

use Faker\Generator;
use Modules\User\Model\Entities\Contracts\UserInterface;
use Modules\User\Model\Factories\Contracts\UserFactoryInterface;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;
use Ramsey\Uuid\Uuid;

trait UserFactoryTrait
{
    /**
     * User repository.
     *
     * @var \Modules\User\Model\Repositories\Contracts\UserRepositoryInterface
     */
    private $userRepo;
    
    /**
     * User factory.
     *
     * @var \Modules\User\Model\Factories\Contracts\UserFactoryInterface
     */
    private $userFactory;
    
    /**
     * Create a new generator.
     *
     * @param string|null $locale
     *
     * @return \Faker\Generator
     */
    abstract protected function faker(string $locale = null): Generator;
    
    /**
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    private function newUser(): UserInterface
    {
        $email = $this->faker()->unique()->safeEmail;
        
        $user = $this->getUserFactory()->newUser($email);
        
        $user->setProvider('web', Uuid::uuid4()->toString());
        
        return $user;
    }
    
    /**
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    protected function createUser(): UserInterface
    {
        $user = $this->newUser();
        
        $this->getUserRepo()->persist($user);
        
        return $user;
    }
    
    /**
     *
     * @param int $count
     *
     * @return array
     */
    protected function createUsers(int $count = 1): array
    {
        $users = [];
        
        for ($i = 0; $i < $count; $i++) {
            $user = $this->newUser();
            
            $this->getUserRepo()->persist($user);
            
            $users[] = $user;
        }
        
        return $users;
    }
    
    /**
     * @return \Modules\User\Model\Repositories\Contracts\UserRepositoryInterface
     */
    protected function getUserRepo()
    {
        if ($this->userRepo === null) {
            $this->userRepo = $this->app->make(UserRepositoryInterface::class);
        }
        
        return $this->userRepo;
    }
    
    /**
     * @return \Modules\User\Model\Factories\Contracts\UserFactoryInterface
     */
    protected function getUserFactory()
    {
        if ($this->userFactory === null) {
            $this->userFactory = $this->app->make(UserFactoryInterface::class);
        }
        
        return $this->userFactory;
    }
}
