<?php

namespace Modules\Tests\User;

use Modules\Tests\AbstractTestCase as FrameworkAbstractTestCase;
use Modules\User\Model\Repositories\Contracts\UserRepositoryInterface;

abstract class AbstractTestCase extends FrameworkAbstractTestCase
{
    /**
     * Setup the test environment, before each test.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        
        $this->runDatabaseMigrations();
    }
}
