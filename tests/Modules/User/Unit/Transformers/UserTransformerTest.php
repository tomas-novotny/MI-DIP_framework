<?php

namespace Modules\Tests\Authorization\Unit\Transformers;

use Modules\Tests\User\AbstractTestCase;
use Modules\Tests\User\UserFactoryTrait;
use Modules\User\Http\Controllers\API\Transformers\Concretes\UserTransformer;

class UserTransformerTest extends AbstractTestCase
{
    use UserFactoryTrait;
    
    public function testAvailableIncudes()
    {
        $includes = $this->getTransformer()->getAvailableIncludes();
        $expected = [
            'roles',
        ];
        
        $this->assertEquals($expected, $includes);
    }
    
    public function testDefaultIncudes()
    {
        $includes = $this->getTransformer()->getDefaultIncludes();
        $expected = [];
        
        $this->assertEquals($expected, $includes);
    }
    
    public function testTransform()
    {
        $user = $this->createUser();
        
        $transformed = $this->getTransformer()->transform($user);
        ksort($transformed);
        
        $expected = [
            'id'           => $user->getKey(),
            'email'        => $user->getEmail(),
            'registeredAt' => null,
            'confirmedAt'  => null,
        ];
        
        $this->assertEquals($expected, $transformed);
    }
    
    public function getTransformer()
    {
        return new UserTransformer();
    }
}
