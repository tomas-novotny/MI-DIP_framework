<?php

namespace Modules\Tests\User\Unit\Entities\Doctrine;

use Modules\Tests\User\Unit\Entities\AbstractUserEntityTest;
use Modules\User\Model\Entities\Concretes\Doctrine\User;
use Modules\User\Model\Entities\Contracts\UserInterface;

class UserEntityTest extends AbstractUserEntityTest
{
    public function setUp()
    {
        //
    }
    
    /**
     * @param string|null $email
     *
     * @return \Modules\User\Model\Entities\Contracts\UserInterface
     */
    public function newUser(string $email = null): UserInterface
    {
        $user = new User();
        $user->setEmail($email ?: $this->faker()->email);
        
        return $user;
    }
}
