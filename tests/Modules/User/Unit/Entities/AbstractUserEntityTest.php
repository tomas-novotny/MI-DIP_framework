<?php

namespace Modules\Tests\User\Unit\Entities;

use Modules\Tests\User\AbstractTestCase;
use Modules\User\Model\Entities\Contracts\UserInterface;

abstract class AbstractUserEntityTest extends AbstractTestCase
{
    abstract public function newUser(string $email = null): UserInterface;
    
    public function testUserHasEmail()
    {
        $user = $this->newUser();
    
        $this->assertNotEmpty($user->getEmail());
    }
}
