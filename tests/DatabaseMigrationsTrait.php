<?php

namespace Modules\Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use RuntimeException;

trait DatabaseMigrationsTrait
{
    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations(): void
    {
        $this->artisan('migrate:fresh');
        
        $this->app[Kernel::class]->setArtisan(null);
        
        $this->beforeApplicationDestroyed(function () {
            try {
                $this->artisan('migrate:rollback');
            }
            catch (RuntimeException $exception) {
                //
            }
            
            RefreshDatabaseState::$migrated = false;
        });
    }
    
    /**
     * Seed a given database connection.
     *
     * @param string $path
     *
     * @return void
     */
    public function migrate(string $path): void
    {
        $this->artisan('migrate', ['--path' => $path.'/']);
        
        $this->app[Kernel::class]->setArtisan(null);
    }
    
    /**
     * Get path from app base
     *
     * @param string $path
     *
     * @return string
     */
    protected function getPath(string $path): string
    {
        $path = realpath($path);
        
        return str_replace($this->app->basePath(), '', $path);
    }
}
