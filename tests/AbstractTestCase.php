<?php

namespace Modules\Tests;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Support\Parents\AbstractTestCase as SupportAbstractTestCase;

abstract class AbstractTestCase extends SupportAbstractTestCase
{
    use CreatesApplicationTrait;
    use DatabaseMigrationsTrait;
    use DatabaseTransactions;
    
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl;
    
    /**
     * Setup the test environment, before each test.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();
        // add stuff
    }
    
    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    protected function tearDown()
    {
        parent::tearDown();
        // add stuff
    }
    
    /**
     * Create a new generator.
     *
     * @param string|null $locale
     *
     * @return \Faker\Generator
     */
    protected function faker(string $locale = null): Generator
    {
        $locale = $locale ?: 'cs_CZ';
        
        return Factory::create($locale);
    }
    
    /**
     * @param int $count
     *
     * @return array
     */
    protected function uniqueWords(int $count = 1): array
    {
        $words = [];
        while (count($words) < $count) {
            $words = array_unique(array_merge($words, $this->addWords($count - count($words))));
        }
        
        return $words;
    }
    
    /**
     * @param int $count
     *
     * @return array
     */
    private function addWords(int $count): array
    {
        return $this->faker()->unique()->words($count);
    }
}
