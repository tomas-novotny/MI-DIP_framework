<?php

namespace Modules\Tests;

use Illuminate\Contracts\Console\Kernel as ConsoleKernelContract;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerContract;
use Illuminate\Contracts\Http\Kernel as HttpKernelContract;
use Illuminate\Foundation\Application;
use Modules\App\Engine\Console\Kernel as AppConsoleKernel;
use Modules\App\Engine\Exceptions\Handler as AppHandler;
use Modules\App\Engine\Http\Kernel as AppHttpKernel;

trait CreatesApplicationTrait
{
    /**
     * Creates the application.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        // $tempApp = (new TemporaryDirectory())->create()->path();
        $tempApp = __DIR__.'/app';
        
        $app = new Application($tempApp);
        
        $app->singleton(HttpKernelContract::class, AppHttpKernel::class);
        $app->singleton(ConsoleKernelContract::class, AppConsoleKernel::class);
        $app->singleton(ExceptionHandlerContract::class, AppHandler::class);
        
        $app->make(ConsoleKernelContract::class)->bootstrap();
        
        return $app;
    }
}
